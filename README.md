# Master_Thesis-BridgeSLAM
This is the code of my master's thesis. This project aimed to create a SLAM pipeline that can reconstruct a digital twin from 3D from recorded 3D data of real-world bridge constructions.


## Getting started

This is a python project. The packages within the requirements.txt should be installed before launching the pipeline. I used python 3.10.11 throughout the development. Some of the packages might not be accessible for other versions. Thats why I recommend to set up a environment for this purpose using the requirements.txt file or the environment.yml file.

As a first step, the acquired sequence needs to be preprocessed by the file BagExtractor.py which is located in the SLAM folder. It takes a .bag file as input. I used a Intel Realsense d435i to acquire sequences. If other cameras or formats are used, the file most likely need to be adapted to it.

The python script SLAM_Pipeline.py can be used to start the pipeline using the following arguments:
1. The configuration file for the specific run. A few examples can be found in the Configuration_Files folder.
2. The folder of the preprocessed dataset
3. The folder for the output files

Note, that I had a second version of the GCNet used for training which is located in the GCNet folder (I had to rewrite some of the code to make it run parallel and able to perform a hyperparameter search).
The GCNet version that is used for the Pipeline is located in SLAM/External.

