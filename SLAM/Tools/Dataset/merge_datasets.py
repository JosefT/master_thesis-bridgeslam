import pickle

import numpy as np
import open3d as o3d
import cv2


def main():
    with open("input/01/train_info.pkl", "rb") as f:
        train_info_0 = pickle.load(f)
    with open("input/02/train_info.pkl", "rb") as f:
        train_info_1 = pickle.load(f)
    with open("input/01/val_info.pkl", "rb") as f:
        val_info_0 = pickle.load(f)
    with open("input/02/val_info.pkl", "rb") as f:
        val_info_1 = pickle.load(f)
    train_info = {}
    val_info = {}
    for key in train_info_0.keys():
        if isinstance(train_info_1[key], np.ndarray):
            train_info_1[key] = list(train_info_1[key])
        train_info[key] = train_info_0[key] + train_info_1[key]
    for key in val_info_0.keys():
        if isinstance(val_info_1[key], np.ndarray):
            val_info_1[key] = list(val_info_1[key])
        val_info[key] = val_info_0[key] + val_info_1[key]

    with open("output/train_info.pkl", "wb") as f:
        pickle.dump(train_info, f)
    with open("output/val_info.pkl", "wb") as f:
        pickle.dump(val_info, f)




if __name__ == "__main__":
    main()
