import os

import numpy as np
import open3d as o3d
import cv2

from PIL import Image

def remove_white_padding(path, file_name):
    image_path = os.path.join(path, file_name)
    image = Image.open(image_path)
    width, height = image.size
    left, top, right, bottom = 0, 0, width, height

    # Define the threshold for considering a pixel as white (adjust as needed)
    white_threshold = 255

    # Find the left boundary
    for x in range(width):
        vals = []
        for y in range(height):
            pixel = image.getpixel((x, y))
            for val in pixel:
                vals.append(val)
        if all(val >= white_threshold for val in vals):
            left = x
        else:
            break

    # Find the right boundary
    for x in range(width - 1, -1, -1):
        vals = []
        for y in range(height):
            pixel = image.getpixel((x, y))
            for val in pixel:
                vals.append(val)
        if all(val >= white_threshold for val in vals):
            right = x
        else:
            break

    # Find the top boundary
    for y in range(height):
        vals = []
        for x in range(width):
            pixel = image.getpixel((x, y))
            for val in pixel:
                vals.append(val)
        if all(val >= white_threshold for val in vals):
            top = y
        else:
            break

    # Find the bottom boundary
    for y in range(height - 1, -1, -1):
        vals = []
        for x in range(width):
            pixel = image.getpixel((x, y))
            for val in pixel:
                vals.append(val)
        if all(val >= white_threshold for val in vals):
            bottom = y
        else:
            break

    # Crop the image to the determined boundaries
    image = image.crop((left, top, right, bottom-1))
    image.save(image_path)


if __name__ == "__main__":
    # read folder content
    img_links = []
    path = "/home/josef/Masterarbeit/Masterarbeit/LateX/images/GCNet_False_Alignments"
    for file in os.listdir(path):
        if file.endswith(".png"):
            img_links.append(file)
    for img_link in img_links:
        remove_white_padding(path, img_link)
