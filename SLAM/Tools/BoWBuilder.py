import collections

import cv2
import numpy as np
import os
import matplotlib.pyplot as plt
import random
import pylab as pl
from joblib.numpy_pickle_utils import xrange

from scipy.spatial import distance
def get_descriptors(img, detector):
    # returns descriptors of an image
    return detector.detectAndCompute(img, None)[1]



if __name__ == '__main__':
    dictionary_size = 512

    #Load rgb images from freyburg dataset
    images = []
    for idx in range(500):
        images.append(cv2.imread("../datasets/my_room/04/rgb/{}.png".format(idx)))

    images_histograms = []
    for i in range(len(images)):
        # create a numpy to hold the histogram for each image
        images_histograms.insert(i, np.zeros((dictionary_size, 1)))

    detector = cv2.SIFT_create()
    desc_all = np.array([])
    for i, img in enumerate(images):
        img_des = get_descriptors(img, detector)
        if img_des is not None:
            desc_all = np.vstack((desc_all, img_des)) if desc_all.size else img_des
    desc_all = np.float32(desc_all)

    # Cluster all descriptors using k-means
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 0.01)
    flags = cv2.KMEANS_PP_CENTERS
    compactness, labels, dictionary = cv2.kmeans(desc_all, dictionary_size, None, criteria, 1, flags)

    # Create a histogram for each image
    for i, img in enumerate(images):
        img_des = get_descriptors(img, detector)
        if img_des is not None:
            for j, desc in enumerate(img_des):
                images_histograms[i][labels[j]] += 1

    # Normalize histograms
    for i, img in enumerate(images):
        norm = np.linalg.norm(images_histograms[i])
        if norm != 0:
            images_histograms[i] /= np.linalg.norm(images_histograms[i])
        else:
            images_histograms[i] = np.ones((dictionary_size, 1))
            images_histograms[i] /= np.linalg.norm(images_histograms[i])
    # Save dictionary

    np.save("../Vocabulary/my_room/04/dictionary.npy", dictionary)


    # Plot first histogram using matplotlib
    fig, axs = plt.subplots(3)
    fig.suptitle('Vertically stacked subplots')
    axs[0].plot(images_histograms[0])
    axs[1].plot(images_histograms[2])
    axs[2].plot(images_histograms[50])
    plt.show()


    for i in range(450):
        cos_dist = distance.cosine(np.squeeze(images_histograms[0]), np.squeeze(images_histograms[i]))
        if cos_dist < 0.01:
            print("{}: {}".format(i, cos_dist))