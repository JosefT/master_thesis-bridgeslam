import math
import random

import numpy as np
import pyrealsense2 as rs
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from pyquaternion import Quaternion

import matplotlib.pyplot as plt
from ahrs.filters import Madgwick, Mahony

#
# class PoseEstimator:
#     def __init__(self):
#
#         self.velocity = np.zeros(3, float)
#         self.position = np.zeros(3, float)
#         self.origin = np.zeros(3, float)
#         self.initial_gravity = np.zeros(3, float)
#         self.lastAccelTime = 0
#         self.first_run = True
#
#
#         self.fig = plt.figure()
#         self.ax = self.fig.add_subplot(111, projection='3d')
#
#     def process_acceleration(self, rotation_q, accel, el_time):
#         # Subtract acceleration due to gravity
#         accel = np.array([accel.x, accel.y, accel.z])
#         if self.first_run and np.linalg.norm(accel) > 5:
#             self.lastAccelTime = el_time
#             self.initial_gravity = accel
#             self.first_run = False
#             return
#         elif self.first_run:
#             return
#
#         dt_accel = (el_time - self.lastAccelTime) / 1000
#         self.lastAccelTime = el_time
#
#
#
#
#         # Compute rotation matrix from Euler angles
#
#         # Rotate gravity vector to device coordinate system
#         gravity_device = rotation_q.inverse.rotate(self.initial_gravity)
#         # print(R)
#         # Subtract gravity from acceleration
#         accel = accel - gravity_device
#         # Compute velocity
#         v1 = accel
#         v2 = gravity_device
#
#
#         self.ax.clear()
#         # Plot the first vector
#         self.ax.quiver(0, 0, 0, v1[0], v1[1], v1[2], color='r', arrow_length_ratio=0.1)
#
#         # Plot the second vector
#         self.ax.quiver(0, 0, 0, v2[0], v2[1], v2[2], color='b', arrow_length_ratio=0.1)
#
#         # Set the limits of the plot
#         max_val = np.max(np.abs(np.concatenate([v1, v2])))
#         self.ax.set_xlim([-10, 10])
#         self.ax.set_ylim([-10, 10])
#         self.ax.set_zlim([-10, 10])
#
#         # Set the labels for the axes
#         self.ax.set_xlabel('X')
#         self.ax.set_ylabel('Y')
#         self.ax.set_zlabel('Z')
#         plt.draw()
#         plt.pause(0.1)
#
#
#         # Compute velocity
#         self.velocity += accel * dt_accel
#         # Compute position
#         self.position += self.velocity * dt_accel
#         print(self.position)
#
#     def __set_self_origin(self, origin):
#         self.origin = origin
#         self.origin_set = True
#
#     def __calculate_rotation_matrix(self, euler_angles):
#         np.set_printoptions(precision=5, suppress=True)
#         # Compute rotation matrix from Euler angles
#         rot_x = -euler_angles[2]
#         rot_y = 0  # euler_angles[1]
#         rot_z = 0  # euler_angles[0]
#         # Print euler angles
#         # gyro_str = str(euler_angles)
#         # sys.stdout.write('\r' + gyro_str)
#
#         R_x = [[1, 0, 0],
#                [0, np.cos(rot_x), -np.sin(rot_x)],
#                [0, np.sin(rot_x), np.cos(rot_x)]]
#
#         R_y = [[np.cos(rot_y), 0, np.sin(rot_y)],
#                [0, 1, 0],
#                [-np.sin(rot_y), 0, np.cos(rot_y)]]
#
#         R_z = [[np.cos(rot_z), -np.sin(rot_z), 0],
#                [np.sin(rot_z), np.cos(rot_z), 0],
#                [0, 0, 1]]
#
#         R = np.dot(R_z, np.dot(R_y, R_x))
#
#         return R





class RotationEstimator:
    def __init__(self):
        self.alpha = 0
        self.firstGyro = True
        self.firstMeasurement = True
        self.lastMeasurementTime = -1
        self.rotation_pre = Quaternion(axis=[1, 0, 0], angle=np.pi/2)
        self.quaternion = None
        self.accel_rotation_q = None
        self.mahony = None
        self.lastValidAccelVector = np.ones(3, float)



    def get_rotation_matrix(self):
        # rotate by 90 degrees around x axis
        return self.quaternion.rotation_matrix @ self.rotation_pre.rotation_matrix


    def register_new_IMU_Frame(self, gyro_data, accel_data, timestamp):

        if self.firstMeasurement:
            self.lastMeasurementTime = timestamp
            self.firstMeasurement = False
            self.mahony = Mahony(learning_rate=0.4, )
            self.quaternion = self.get_first_orientation(accel_data)
            return
        else:
            dt_gyro = (timestamp - self.lastMeasurementTime) / 1000
            self.lastMeasurementTime = timestamp
            gyro = np.array([gyro_data.x, gyro_data.y, gyro_data.z])
            accel = np.array([accel_data.x, accel_data.y, accel_data.z])
            q = self.quaternion
            self.mahony.Dt = dt_gyro

            # We need to check if the accel vector is valid. It is not if we are moving
            if abs(np.linalg.norm(accel)-9.81) > 0.4:
                accel = self.lastValidAccelVector # We just continue using the last valid accel vector until we stop moving
            else:
                self.lastValidAccelVector = accel
            quad_np = self.mahony.updateIMU(gyr=gyro, acc=accel, q=np.array([q.w, q.x, q.y, q.z]))
            self.quaternion = Quaternion(quad_np)


    # SRC-Ref: Code borrowed from ahrs library (acc2q function)
    def get_first_orientation(self, accel):
        a = np.array([accel.x, accel.y, accel.z])
        q = np.array([1.0, 0.0, 0.0, 0.0])
        ex, ey, ez = 0.0, 0.0, 0.0
        if np.linalg.norm(a) > 0 and len(a) == 3:
            ax, ay, az = a
            # Normalize accelerometer measurements
            a_norm = np.linalg.norm(a)
            ax /= a_norm
            ay /= a_norm
            az /= a_norm
            # Euler Angles from Gravity vector
            ex = np.arctan2(ay, az)
            ey = np.arctan2(-ax, np.sqrt(ay ** 2 + az ** 2))
            ez = 0.0
            # Euler to Quaternion
            cx2 = np.cos(ex / 2.0)
            sx2 = np.sin(ex / 2.0)
            cy2 = np.cos(ey / 2.0)
            sy2 = np.sin(ey / 2.0)
            q = np.array([cx2 * cy2, sx2 * cy2, cx2 * sy2, -sx2 * sy2])
            q /= np.linalg.norm(q)
            q = Quaternion(q)
        return q

    def calculate_linear_acceleration(self, accel):
        accel = repr([accel.x, accel.y, accel.z])
        print(accel,",")




def __to_euler_angles(self, quat):
    # Convert quaternion to rotation matrix
    rot_matrix = quat.rotation_matrix

    # Extract Euler angles from rotation matrix
    sy = math.sqrt(rot_matrix[0, 0] * rot_matrix[0, 0] + rot_matrix[1, 0] * rot_matrix[1, 0])
    singular = sy < 1e-6
    if not singular:
        x = math.atan2(rot_matrix[2, 1], rot_matrix[2, 2])
        y = math.atan2(-rot_matrix[2, 0], sy)
        z = math.atan2(rot_matrix[1, 0], rot_matrix[0, 0])
    else:
        x = math.atan2(-rot_matrix[1, 2], rot_matrix[1, 1])
        y = math.atan2(-rot_matrix[2, 0], sy)
        z = 0

    # Return Euler angles in radians
    return [x, y, z]


def euler_to_matrix(euler):
    # normalize the Euler vector
    rot_x = -euler[2]
    rot_y = euler[1]
    rot_z = euler[0]

    R_x = [[1, 0, 0],
           [0, np.cos(rot_x), -np.sin(rot_x)],
           [0, np.sin(rot_x), np.cos(rot_x)]]
    R_y = [[np.cos(rot_y), 0, np.sin(rot_y)],
           [0, 1, 0],
           [-np.sin(rot_y), 0, np.cos(rot_y)]]
    R_z = [[np.cos(rot_z), -np.sin(rot_z), 0],
           [np.sin(rot_z), np.cos(rot_z), 0],
           [0, 0, 1]]

    R = np.dot(R_z, np.dot(R_y, R_x))
    return R


if __name__ == '__main__':
    pipeline = rs.pipeline()

    config = rs.config()
    config.enable_stream(rs.stream.gyro, rs.format.motion_xyz32f, 200)
    config.enable_stream(rs.stream.accel, rs.format.motion_xyz32f, 200)

    # Declare object that handles camera pose calculations
    rotation_estimator = RotationEstimator()
    #pose_estimator = PoseEstimator()
    # Start streaming with the given configuration
    pipeline.start(config)

    # Create vertices representing the corners of a rectangular box

    vertices = np.array([[-1, -0.25, -0.25],
                         [1, -0.25, -0.25],
                         [1, 0.25, -0.25],
                         [-1, 0.25, -0.25],
                         [-1, -0.25, 0.25],
                         [1, -0.25, 0.25],
                         [1, 0.25, 0.25],
                         [-1, 0.25, 0.25],

                         [-0.25, -0.25, 0.25],
                         [0.25, -0.25, 0.25],
                         [0.25, -0.25, 0.375],
                         [-0.25, -0.25, 0.375],
                         [-0.25, 0.25, 0.25],
                         [0.25, 0.25, 0.25],
                         [0.25, 0.25, 0.375],
                         [-0.25, 0.25, 0.375]])
    # Translate to origin
    # define the initial edges
    edges = [(0, 1), (1, 2), (2, 3), (3, 0),
             (4, 5), (5, 6), (6, 7), (7, 4),
             (0, 4), (1, 5), (2, 6), (3, 7),
             (8, 9), (9, 10), (10, 11), (11, 8),
             (12, 13), (13, 14), (14, 15), (15, 12),
             (8, 12), (9, 13), (10, 14), (11, 15)]

    faces = [[0, 1, 2, 3], [4, 5, 6, 7], [0, 4, 5, 1], [6,7,3,2], [0, 3, 7, 4], [1, 2, 6, 5],
             [8, 9, 10, 11],  [12, 13, 14, 15], [10, 11, 15, 14], [8, 11, 15, 12], [9, 10, 14, 13]]
    # create the figure and axes
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(elev=115, azim=90)
    # generate random shades of blue for each face
    colors0 = [(random.uniform(0.2, 0.8), random.uniform(0.2, 0.8), random.uniform(0.8, 1.0)) for _ in range(6)]
    colors1 = [(1, random.uniform(0.8, 1.), random.uniform(0.0, 0.6)) for _ in range(5)]
    colors = colors0 + colors1
    while True:
        frame = pipeline.wait_for_frames()
        gyro_data = frame[1].as_motion_frame().get_motion_data()
        accel_data = frame[0].as_motion_frame().get_motion_data()

        ts = frame[1].get_timestamp()
        rotation_estimator.register_new_IMU_Frame(gyro_data, accel_data, ts)
        rotation_estimator.calculate_linear_acceleration(accel_data)
        #pose_estimator.process_acceleration(rotation_estimator.quaternion, accel_data, ts)
        vertices_rotated = (rotation_estimator.get_rotation_matrix() @ vertices.T).T
        #vertices_rotated = vertices



        ax.clear()
        for edge in edges:
            ax.plot(vertices_rotated[edge, 0], vertices_rotated[edge, 1], vertices_rotated[edge, 2], 'b')

        # Plot the faces of the 3D object with their specified colors
        ax.add_collection3d(Poly3DCollection(vertices_rotated[faces], facecolors=colors, edgecolors='black'))
        # update the rotation matrix
        # set the plot limits and labels
        ax.set_xlim(-1, 1)
        ax.set_ylim(-1, 1)
        ax.set_zlim(-1, 1)
        ax.view_init(elev=115, azim=90)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

        # show the plot
        plt.pause(0.01)
