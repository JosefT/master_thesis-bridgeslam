import os

import numpy as np
import open3d as o3d
import cv2
from tqdm import tqdm


def main():
    pass


if __name__ == "__main__":
    path = "../datasets/my_room/04/"
    pcd_path = "../datasets/my_room/04/"
    color_file_names = []
    depth_file_names = []
    n_frames = len(os.listdir(path + "rgb"))
    for i in range(0, n_frames):
        color_file_names.append(path + "rgb/{}.png".format(i))
        depth_file_names.append(path + "depth/{}.png".format(i))

    #intrinsic = o3d.camera.PinholeCameraIntrinsic(width=640, height=480, fx=525.0, fy=525.0, cx=319.5, cy=239.5)
    intrinsic = o3d.camera.PinholeCameraIntrinsic(width=1080, height=720, fx=911.7221069335938, fy=911.2312622070312, cx=651.0254516601562, cy=361.3611145019531)
    #intrinsic = o3d.camera.PinholeCameraIntrinsic(width=1080, height=720, fx=906.2353515625, fy=904.79638671875, cx=644.62548828125, cy=371.9884948730469)
    depth_scale = 1000.0
    extrinsic = np.identity(4)
    for i in tqdm(range(len(color_file_names))):
        rgbd = o3d.geometry.RGBDImage().create_from_color_and_depth(o3d.io.read_image(color_file_names[i]), o3d.io.read_image(depth_file_names[i]), depth_scale, 15.0,
                                                                   False)
        pcd = o3d.geometry.PointCloud().create_from_rgbd_image(rgbd, intrinsic, extrinsic)
        #num_pts = np.asarray(pcd.points).shape[0]
        # random sample 1000 points
        #o3d.visualization.draw_geometries([pcd])
        #n_sample_pts = 15000
        #if num_pts < n_sample_pts:
        #    n_sample_pts = num_pts / 1.5
        #idx = np.random.randint(0, num_pts, n_sample_pts)
        #pcd = pcd.select_by_index(idx)

        pcd = pcd.voxel_down_sample(voxel_size=0.01)
        #o3d.visualization.draw_geometries([pcd])
        pcd = pcd.remove_statistical_outlier(nb_neighbors=30, std_ratio=2)[0]
        #pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.25, max_nn=200))
        #pcd.orient_normals_towards_camera_location(camera_location=np.array([0., 0., 0.]))
        #o3d.visualization.draw_geometries([pcd])
        #n_sample_pts = 5000
        #idx = np.random.randint(0, np.asarray(pcd.points).shape[0], n_sample_pts)
        #pcd = pcd.select_by_index(idx)

        #o3d.visualization.draw_geometries([pcd])
        o3d.io.write_point_cloud(pcd_path + "{}.pcd".format(i), pcd)