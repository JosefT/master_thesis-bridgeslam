import copy

import numpy as np
import os
import pickle
import torch
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset
CUR = os.path.dirname(os.path.abspath(__file__))
from utils import npy2pcd, pcd2npy, vis_plys, get_correspondences, format_lines, normal, voxel_ds
import open3d as o3d


def normalize_colors(colors):
    colors_t = torch.tensor(colors)
    norms = torch.norm(colors_t, dim=1)
    norms[norms == 0] = 1e-6
    normalized_colors = colors_t / norms.view(-1, 1)
    return normalized_colors.numpy()

class Bridge_Dataset(Dataset):
    def __init__(self, root,  overlap_radius, include_noisy=False, scene_order=None):
        super().__init__()
        self.root = root
        self.include_noisy = include_noisy
        self.max_points = 30000
        self.aug = True
        self.overlap_radius = overlap_radius
        self.noise_scale = 0.
        self.voxel_size_3dmatch = 0.025
        self.voxel_size = 0.05
        self.scale = self.voxel_size / self.voxel_size_3dmatch

        pkl_path = os.path.join(CUR, 'Bridge_Dataset/', 'bridge_dataset.pkl')
        with open(pkl_path, 'rb') as f:
            raw_infos = pickle.load(f)
        # Filter all infos with class not being 0 or 1
        # create empty dict to store filtered infos
        self.infos = {}
        self.infos["src"] = list()
        self.infos["tgt"] = list()
        self.infos["rot"] = list()
        self.infos["trans"] = list()
        self.infos["class"] = list()
        if scene_order is None:
            print("No scene order provided, using default order")
            exit()
        for scene in scene_order:
            for i in range(len(raw_infos["src"])):
                if scene + "/" in raw_infos["src"][i] and( raw_infos['class'][i] ==0 or (self.include_noisy and raw_infos['class'][i] == 1)):
                    self.infos["src"].append(raw_infos["src"][i])
                    self.infos["tgt"].append(raw_infos["tgt"][i])
                    self.infos["rot"].append(raw_infos["rot"][i])
                    self.infos["trans"].append(raw_infos["trans"][i])
                    self.infos["class"].append(raw_infos["class"][i])

    def __len__(self):
        return len(self.infos['rot'])

    def __getitem__(self, item):
        src_path, tgt_path = self.infos['src'][item], self.infos['tgt'][item] # str, str
        rot, trans = self.infos['rot'][item], self.infos['trans'][item] # (3, 3), (3, 1)


        #check if path contains "bridge_013
        if "bridge_013" in src_path:
            self.voxel_size = 0.05
        else:
            self.voxel_size = 0.04
        self.scale = self.voxel_size / self.voxel_size_3dmatch
        T = np.eye(4).astype(np.float32)
        T[:3, :3] = rot
        T[:3, 3] = trans
        rot = np.eye(3).astype(np.float32)
        trans = np.zeros(3).astype(np.float32)
        #classification = self.infos['class'][item] # float
        src_pcd = o3d.io.read_point_cloud(self.root+src_path).transform(T)
        tgt_pcd = o3d.io.read_point_cloud(self.root+tgt_path)
        # calcualte avg distance between points
        src_points = np.asarray(src_pcd.points)

        src_ds = voxel_ds(copy.deepcopy(src_pcd), self.voxel_size)
        tgt_ds = voxel_ds(copy.deepcopy(tgt_pcd), self.voxel_size)

        src_colors = np.asarray(src_ds.colors)
        tgt_colors = np.asarray(tgt_ds.colors)

        # scale pcd
        src_ds.scale(1/self.scale, center=(0,0,0))
        tgt_ds.scale(1/self.scale, center=(0,0,0))


        src_points = pcd2npy(src_ds)
        tgt_points = pcd2npy(tgt_ds)

        # for gpu memory
        # add scaling to rot using matrix multiplication

        if (src_points.shape[0] > self.max_points):
            idx = np.random.permutation(src_points.shape[0])[:self.max_points]
            src_points = src_points[idx]
            src_colors = src_colors[idx]
        if (tgt_points.shape[0] > self.max_points):
            idx = np.random.permutation(tgt_points.shape[0])[:self.max_points]
            tgt_points = tgt_points[idx]
            tgt_colors = tgt_colors[idx]

        if self.aug:
            euler_ab = np.random.rand(3) * 2 * np.pi
            rot_ab = Rotation.from_euler('zyx', euler_ab).as_matrix()
            if np.random.rand() > 0.5:
                src_points = src_points @ rot_ab.T
                rot = rot @ rot_ab.T
            else:
                tgt_points = tgt_points @ rot_ab.T
                rot = rot_ab @ rot
                trans = rot_ab @ trans

            src_points += (np.random.rand(src_points.shape[0], 3) - 0.5) * self.noise_scale
            tgt_points += (np.random.rand(tgt_points.shape[0], 3) - 0.5) * self.noise_scale

        T = np.eye(4).astype(np.float32)
        T[:3, :3] = rot

        T[:3, 3] = trans

        coors = get_correspondences(npy2pcd(src_points),
                                    npy2pcd(tgt_points),
                                    T,
                                    self.overlap_radius)

        src_feats = np.ones_like(src_points[:, :1], dtype=np.float32)
        tgt_feats = np.ones_like(tgt_points[:, :1], dtype=np.float32)

        src_pcd, tgt_pcd = normal(npy2pcd(src_points)), normal(npy2pcd(tgt_points))
        src_pcd.colors = o3d.utility.Vector3dVector(src_colors)
        tgt_pcd.colors = o3d.utility.Vector3dVector(tgt_colors)
        src_normals = np.array(src_pcd.normals).astype(np.float32)
        tgt_normals = np.array(tgt_pcd.normals).astype(np.float32)
        # print min and max point of tgt
        # Normalize Colors
        #src_colors = normalize_colors(src_colors)
        #tgt_colors = normalize_colors(tgt_colors)

        pair = dict(
            src_points=src_points,
            tgt_points=tgt_points,
            src_feats=src_feats,
            tgt_feats=tgt_feats,
            src_normals=src_normals,
            tgt_normals=tgt_normals,
            src_colors=src_colors,
            tgt_colors=tgt_colors,
            transf=T,
            coors=coors,
            src_points_raw=src_points,
            tgt_points_raw=tgt_points,
            )
        return pair
