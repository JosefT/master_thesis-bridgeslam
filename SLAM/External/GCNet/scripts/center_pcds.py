import os

import numpy as np
import open3d as o3d


def main():
    path = "../../Datasets/GCNet/Bridge_Dataset/pcd"

    for folder_name in os.listdir(path):
        for pcd in os.listdir(os.path.join(path, folder_name)):
            if not pcd.endswith(".pcd"):
                continue
            pcd_path = os.path.join(path, folder_name, pcd)
            pcd = o3d.io.read_point_cloud(pcd_path)
            # center pcd
            pcd.translate(-pcd.get_center())
            o3d.io.write_point_cloud(pcd_path, pcd)



if __name__ == "__main__":
    main()
