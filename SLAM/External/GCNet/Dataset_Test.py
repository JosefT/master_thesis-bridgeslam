import copy

import numpy as np
import open3d as o3d
from data.Custom_Dataset import Custom_Dataset
def draw_registration_result(source, target, transformation=np.eye(4), uniform_color=True, correspondences=None, rotate=True, draw=True):
    source_cp = copy.deepcopy(source)
    target_cp = copy.deepcopy(target)
    if uniform_color:
        source_cp.paint_uniform_color([1, 0.706, 0])
        target_cp.paint_uniform_color([0, 0.651, 0.929])
    source_cp = source_cp.transform(transformation)
    if rotate:
        deg_180_rot = np.eye(4)
        #deg_180_rot[0, 0] = -1
        deg_180_rot[1, 1] = -1
        deg_180_rot[2, 2] = -1
        source_cp = source_cp.transform(deg_180_rot)
        target_cp = target_cp.transform(deg_180_rot)


    if correspondences is not None:
        line_set = o3d.geometry.LineSet().create_from_point_cloud_correspondences(source_cp, target_cp, correspondences)
        if draw:
            o3d.visualization.draw_geometries([source_cp, target_cp, line_set])
        return [source_cp, target_cp, line_set]
    else:
        if draw:
            o3d.visualization.draw_geometries([source_cp, target_cp])
        return [source_cp, target_cp]

def main():
    root = "Datasets/Custom_Dataset/"
    dataset = Custom_Dataset(root=root,
                               split="train",
                               aug=False,
                               overlap_radius=0.03)
    for i in range(dataset.__len__()):
        data = dataset.__getitem__(i)
        print(np.shape(data["src_points_raw"]))
        print(np.shape(data["src_normals"]))
        print(np.shape(data["src_colors"]))
        print(np.shape(data["src_feats"]))
        pcd0, pcd1 = o3d.geometry.PointCloud(), o3d.geometry.PointCloud()

        pcd0.points = o3d.utility.Vector3dVector(np.asarray(data["src_points_raw"]))
        pcd0.normals = o3d.utility.Vector3dVector(np.asarray(data["src_normals"]))
        pcd0.colors = o3d.utility.Vector3dVector(np.asarray(data["src_colors"]))

        pcd1.points = o3d.utility.Vector3dVector(np.asarray(data["tgt_points_raw"]))
        pcd1.normals = o3d.utility.Vector3dVector(np.asarray(data["tgt_normals"]))
        pcd1.colors = o3d.utility.Vector3dVector(np.asarray(data["src_colors"]))
        T = data["transf"]

        draw_registration_result(pcd0, pcd1, T)

if __name__ == "__main__":
    main()
