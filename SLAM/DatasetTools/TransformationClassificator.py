import copy
import os
import pickle

import numpy as np
import open3d as o3d

from General import get_yellow, get_blue
from Serialization import load_fragments


def button_click(choice, root):
    print(choice)
    root.destroy()

def main():
    folder = "../Outputs"
    fragment_file = "fragment_collector.pkl"
    transformation_file = "transformations.npy"
    # loop through all foloders in Outputs
    # check if folder exists. If not, create it


    output_path = "../../Datasets/GCNet/Bridge_Dataset"
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    dict_file = "bridge_dataset.pkl"
    info_dict = {}
    info_dict["src"] = list()
    info_dict["tgt"] = list()
    info_dict["rot"] = list()
    info_dict["trans"] = list()
    info_dict["class"] = list()

    for folder_name in os.listdir(folder):
        path = os.path.join(folder, folder_name)
        # check if file exists
        if not os.path.exists(os.path.join(path, fragment_file)) or not os.path.exists(os.path.join(path, transformation_file)):
            continue
        # load fragment collector
        fragment_collector = load_fragments(os.path.join(path, fragment_file))
        # load transformations
        transformations = np.load(os.path.join(path, transformation_file), allow_pickle=True)
        # loop through all fragments
        counter = 0
        for i in range(len(fragment_collector)-1):
            output_pcd_path = os.path.join(output_path, "pcd", folder_name)
            if not os.path.exists(output_pcd_path):
                os.makedirs(output_pcd_path)
            # check if transformation is not None
            if transformations[i] is not None:
                pcd0 = copy.deepcopy(fragment_collector[i].map)
                pcd1 = copy.deepcopy(fragment_collector[i+1].map)
                T = transformations[i]
                pcd0_center = pcd0.get_center()
                pcd1_center= pcd1.get_center()

                pcd0 = pcd0.translate(-pcd0_center)
                pcd1 = pcd1.translate(-pcd1_center)
                # update transformation
                center_transformation0 = np.eye(4)
                center_transformation0[:3, 3] = pcd0_center
                center_transformation1 = np.eye(4)
                center_transformation1[:3, 3] = pcd1_center
                T = np.matmul(T, center_transformation0)
                T = np.matmul(np.linalg.inv(center_transformation1), T)
                #transformations[i] = np.matmul(transformations[i], center_transformation)
                # translate both point clouds to the origin
                # scale pcds by 0.5
                pcd0.scale(0.5, center=pcd0.get_center())
                pcd1.scale(0.5, center=pcd1.get_center())

                pcd0_cpy = copy.deepcopy(pcd0).transform(T)
                pcd1_cpy = copy.deepcopy(pcd1)
                pcd0_cpy.paint_uniform_color(get_yellow())
                pcd1_cpy.paint_uniform_color(get_blue())

                # rotate by 180 degrees
                pcd = pcd0_cpy + pcd1_cpy


                pcd.rotate(np.asarray([[1, 0, 0], [0, -1, 0], [0, 0, -1]]), center=pcd.get_center())
                o3d.visualization.draw_geometries([pcd])

                result = -1
                okay = input("Is ok?")
                if okay == "y":
                    noNoise = input("Low Noise?")
                    if noNoise == "y":
                        result = 0
                    else:
                        result = 1
                else:
                    discard = input("Discard?")
                    if discard == "y":
                        continue
                    else:
                        noNoise = input("Low Noise?")
                        if noNoise == "y":
                            result = 2
                        else:
                            result = 3
                if result != -1:
                    R = T[:3, :3]
                    t = T[:3, 3]

                    o3d.io.write_point_cloud(os.path.join(output_pcd_path, str(counter) + ".pcd"), pcd0)
                    o3d.io.write_point_cloud(os.path.join(output_pcd_path, str(counter+1) + ".pcd"), pcd1)

                    info_dict["src"].append(os.path.join(folder_name, str(counter) + ".pcd"))
                    info_dict["tgt"].append(os.path.join(folder_name, str(counter+1) + ".pcd"))
                    info_dict["rot"].append(R)
                    info_dict["trans"].append(t)
                    info_dict["class"].append(result)
                    counter += 2

    with open(os.path.join(output_path, dict_file), "wb") as file:
        pickle.dump(info_dict, file)
if __name__ == "__main__":
    main()
