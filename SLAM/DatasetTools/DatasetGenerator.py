import os
import pickle

import numpy as np
import open3d as o3d
import yaml
from tqdm import tqdm

from SLAM5 import Frame
from SLAM_Model import SLAM
from Serialization import save_fragments, save_array, load_fragments, load_array
from src.DataLoadingMethods import get_imu_data
from src.IMU import RotationEstimator
from src.ModelAdapter import GCNModelAdapter


# def main():
#     # load file as np array from .pth
#     path = "../data/data/indoor/train/7-scenes-fire/cloud_bin_12.pth"
#     src_pcd = torch.load(path).astype(np.float32)
#     print(src_pcd)
#     pcd = o3d.geometry.PointCloud()
#     pcd.points = o3d.utility.Vector3dVector(src_pcd)
#     o3d.visualization.draw_geometries([pcd])

def main():
    perform_patch_creation = False
    perform_alignment = True

    #### get frames
    with open("../Configuration_Files/bridge_01.yaml", 'r') as file:
        config = yaml.safe_load(file)

        # load imu data
    IMU_Data_Sequence = get_imu_data("datasets/bridge/01/")
    rotation_estimator = RotationEstimator()
    rotation_estimator.add_data(t=IMU_Data_Sequence[:, 0], gyro=IMU_Data_Sequence[:, 4:7],
                                accel=IMU_Data_Sequence[:, 1:4])
    Model = GCNModelAdapter(True, config["Mapping"]["model_voxel_size"], True)
    slam = SLAM(config, Model, None, False)
    slam.rotation_estimator = rotation_estimator
    path = config['Path']
    frames = []
    # find out how many frames there are
    n_frames = len(os.listdir(path + "rgb"))
    for i in range(0, n_frames):
        frame = Frame(path + "rgb/{}.png".format(i), path + "depth/{}.png".format(i), path + "pcd/{}.pcd".format(i))
        frames.append(frame)

    ### make fragments
    output_folder = "Outputs/dataset_creation/"
    fragment_file = "fragment_collector.pkl"
    if perform_patch_creation:
        fragment_collector = slam.create_local_maps(frames, False, optimize_maps=True)
        # save fragments
        save_fragments(fragment_collector, output_folder+fragment_file)
        for idx, fragment in enumerate(fragment_collector):
            o3d.io.write_point_cloud(output_folder + "train/{}.pcd".format(idx), fragment.map)
    else:
        fragment_collector = load_fragments(output_folder+fragment_file)



    #for idx, fragment in enumerate(fragment_collector):
    #    print(fragment.map)
    #    o3d.visualization.draw_geometries([fragment.map])

    ### align consecuitive fragments
    consecutive_tans_file = "transformations.npy"
    loss = o3d.pipelines.registration.TukeyLoss(k=0.1)
    p2l = o3d.pipelines.registration.TransformationEstimationPointToPlane(loss)
    T_est = np.eye(4)
    transformations = []
    if perform_alignment:
        for idx, tgt_fragment in tqdm(enumerate(fragment_collector)):
            target = tgt_fragment.map
            T_est = np.linalg.inv(tgt_fragment.extrinsics[-1])
            for idy, src_fragment in enumerate(fragment_collector[idx+1:]):
                idy = idy + idx + 1
                source = src_fragment.map
                source_down = source.voxel_down_sample(voxel_size=0.01)

                icp_result = o3d.pipelines.registration.registration_icp(source_down, target, 0.03,T_est,p2l)


                # get proportion of correspondences to total points
                # if proportion is too low, break
                overlap = len(icp_result.correspondence_set)/len(source_down.points)
                print(overlap)
                if overlap > 0.3:
                    transformations.append((idx, idy, icp_result.transformation, overlap, icp_result.information))
                else:
                    break

                T_est = icp_result.transformation @ np.linalg.inv(src_fragment.extrinsics[-1])
        save_array(transformations, output_folder+consecutive_tans_file)
    else:
        transformations = load_array(output_folder+consecutive_tans_file)
    #### Create info dict
    dict_file = "train_info.pkl"
    pcd_path = "train/bridge_01/"
    info_dict = {}
    info_dict["src"] =  list()
    info_dict["tgt"] =  list()
    info_dict["rot"] = list()
    info_dict["trans"] =  list()
    info_dict["overlap"] = list()
    for tranf_pair in transformations:
        tgt_idx = tranf_pair[0]
        src_idx = tranf_pair[1]
        transformation = tranf_pair[2]
        overlap = tranf_pair[3]

        rot = transformation[:3, :3]
        trans = transformation[:3, 3]
        info_dict["src"].append(pcd_path+"{}.pcd".format(src_idx))
        info_dict["tgt"].append(pcd_path+"{}.pcd".format(tgt_idx))
        info_dict["rot"].append(rot)
        info_dict["trans"].append(trans)
        info_dict["overlap"].append(overlap)
    # save info_dict as .pkl
    print(info_dict)
    with open(output_folder+dict_file, 'wb') as handle:
        pickle.dump(info_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

    # save down info, transformation matrices and overlap
    tranf, info, overlap = [], [], []
    for tranf_pair in transformations:
        tranf += [tranf_pair[2]]
        info += [tranf_pair[4]]
        overlap += [tranf_pair[3]]
    np.save(output_folder+pcd_path+"transformations.npy", tranf)
    np.save(output_folder+pcd_path+"information.npy", info)
    np.save(output_folder+pcd_path+"overlap.npy", overlap)


if __name__ == '__main__':
    main()