import os
import pickle

import numpy as np
import open3d as o3d
import cv2


def main():
    path = "../../Datasets/GCNet/Bridge_Dataset"
    with open(os.path.join(path, "bridge_dataset.pkl"), "rb") as f:
        dataset0 = pickle.load(f)
    with open(os.path.join(path, "bridge_dataset_new.pkl"), "rb") as f:
        dataset1 = pickle.load(f)

    combined_dict = {}
    for key in dataset0.keys():
        if isinstance(dataset1[key], np.ndarray):
            dataset1[key] = list(dataset1[key])
        combined_dict[key] = dataset0[key] + dataset1[key]

    print(combined_dict)
    with open(os.path.join(path, "bridge_dataset_combined.pkl"), "wb") as f:
        pickle.dump(combined_dict, f)
    #with open("output/train_info.pkl", "wb") as f:
    #    pickle.dump(train_info, f)
    #with open("output/val_info.pkl", "wb") as f:
    #    pickle.dump(val_info, f)




if __name__ == "__main__":
    main()
