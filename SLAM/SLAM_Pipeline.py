import argparse
import datetime
import os

import open3d as o3d

from DataLoadingMethods import get_imu_data, get_config_file, get_frames
from IMU import RotationEstimator
from Loop_Closure_Pipeline import get_visual_lc_candidates_using_binary_BoWs
from ModelAdapter import GCNModelAdapter
from SLAM_Model import SLAM
from Serialization import save_fragments, save_array, load_fragments, load_array
from VisualizationUtils import draw_func, draw_registration_result
from transformation_utils import poses_from_transformations


def main(config_file, sequence_path, output_path):
    # Following variables are used to control the execution of the pipeline. Individual parts can be turned off if they
    # have already been executed successfully before on the respective sequence.

    # Execution
    perform_map_creation = True
    perform_consecutive_alignment = True
    visual_candidate_extraction = True
    perform_loop_detection = True
    extract_trajectory = True
    final_tsdf_creation = True
    color_optimization = True

    # Visualization
    draw_errors = False  # General
    visualize_icp_alignment = False  # Fragment Creation
    visualize_fragments_during_registration = False  # Fragment Creation
    visualize_fragments = False  # Fragment Creation
    visualize_consecutive_alignments = False  # Consecutive Alignment
    visualize_global_map_before_LC = False  # LC
    visualize_global_map_after_LC = False  # LC
    visualize_candidates = False  # LC
    visualize_LC_fragment_alignments = False  # LC
    visualize_tsdf = False  # Map Integration
    visualize_final_map = True  # Color Optimization

    config = get_config_file(config_file)
    output_path = output_path + config["Name"] + "/"
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    # find out how many frames exist in the sequence folder
    n_frames = len(os.listdir(sequence_path + "rgb"))
    # load frames
    frames = get_frames(sequence_path, n_frames)

    # load IMU data
    rotation_estimator = RotationEstimator()
    if config["Mapping"]["use_imu_support"]:
        IMU_Data_Sequence = get_imu_data(sequence_path)
        rotation_estimator.add_data(t=IMU_Data_Sequence[:, 0], gyro=IMU_Data_Sequence[:, 4:7],
                                    accel=IMU_Data_Sequence[:, 1:4])

    # Create Adapter for the GCNet
    Model = GCNModelAdapter(True, config["Mapping"]["model_voxel_size"], True)

    # Create SLAM Object (includes the main methods of the pipeline and necessary variables)
    slam = SLAM(config, Model, draw_errors, visualize_LC_fragment_alignments)
    slam.rotation_estimator = rotation_estimator

    # ===============================================FRAGMENT CREATION==================================================

    fragment_file = output_path + "fragment_collector.pkl"
    active_frame_file = output_path + "active_frames.npy"

    if perform_map_creation:
        fragment_collector = slam.create_local_maps(frames, visualize_fragments_during_registration,
                                                    visualize_icp_alignment)
        save_fragments(fragment_collector, fragment_file)
        active_frames = [frame.active for frame in frames]
        save_array(active_frames, active_frame_file)
    else:
        fragment_collector = load_fragments(fragment_file)
        active_frames = load_array(active_frame_file)
        for idx, frame in enumerate(frames):
            frame.active = active_frames[idx]
        del active_frames

    # ==================================================================================================================

    if visualize_fragments:
        for idx, fragment in enumerate(fragment_collector):
            name = f"Map {idx}"
            draw_func("Local Map", name, fragment.map)
    # ========================================== CONSECUTIVE FRAGMENT ALIGNMENT ========================================
    consecutive_tans_file = output_path + "transformations.npy"

    if perform_consecutive_alignment:
        # Consecutive map registration:
        transformations = slam.estimate_consecutive_transformations_between_local_maps(fragment_collector)
        save_array(transformations, consecutive_tans_file)
    else:
        transformations = load_array(consecutive_tans_file)

    # ==================================================================================================================

    if visualize_consecutive_alignments:
        for i in range(len(transformations)):
            if transformations[i] is not None:
                geometrics = draw_registration_result(fragment_collector[i].map, fragment_collector[i + 1].map,
                                                      transformations[i],
                                                      uniform_color=True, draw=False)
                name = f"Map Combined {i}"
                geometric = o3d.geometry.PointCloud()
                geometric = geometric + geometrics[0]
                geometric += geometrics[1]
                draw_func("Consecutive Maps", name, geometric)

    if visualize_global_map_before_LC:
        temp_poses = poses_from_transformations(transformations)
        slam.visualize_aligned_fragments(fragment_collector, temp_poses)

    # ===============================================LOOP CLOSURE=======================================================
    loop_closure_file = output_path + "optimized_transformations.npy"
    if perform_loop_detection:
        if visual_candidate_extraction:
            visual_candidates = get_visual_lc_candidates_using_binary_BoWs(fragment_collector,
                                                                           config['Loop_Closure']['alpha'],
                                                                           config['Loop_Closure']['beta'],
                                                                           visualize_candidates)
            save_array(visual_candidates, output_path + "visual_candidates.npy")
        else:
            visual_candidates = load_array(output_path + "visual_candidates.npy")
        pose_graph = slam.build_consecutive_pose_graph(transformations, fragment_collector)
        optimized_transformations = slam.loop_closure_pipeline(visual_candidates, fragment_collector, pose_graph,
                                                               visualize_LC_fragment_alignments)
        save_array(optimized_transformations, loop_closure_file)
    elif os.path.isfile(loop_closure_file):
        print("Loop Closure: Loading optimized transformations")
        optimized_transformations = load_array(loop_closure_file)
    else:
        print("Loop Closure: No optimized transformations found")
        optimized_transformations = slam.build_consecuitive_poses(transformations)

    trajectory_file = output_path + "trajectory.npy"
    if extract_trajectory:
        trajectory = slam.extract_trajectory(fragment_collector, optimized_transformations)
        save_array(trajectory, trajectory_file)
    else:
        trajectory = load_array(trajectory_file)

    # ==================================================================================================================

    if visualize_global_map_after_LC:
        slam.visualize_aligned_fragments(fragment_collector, optimized_transformations)

    # ======================================= FINAL TSDF CREATION ======================================================

    if final_tsdf_creation:
        final_map_pcd, mesh = slam.RecreateTSDFFromTransformations(fragment_collector, trajectory)
        # write mesh to file
        o3d.io.write_triangle_mesh(output_path + "last_unoptimized_mesh.ply", mesh)
    else:
        final_map_pcd = None
        mesh = o3d.io.read_triangle_mesh(output_path + "last_unoptimized_mesh.ply")

        # ==================================================================================================================
        if visualize_tsdf:
            if final_map_pcd is not None:
                draw_func("TSDF Map", "PCD", final_map_pcd)
            # self.draw_func("TSDF Map", "Mesh", final_map_mesh)
            o3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)
    # ================================== COLOR OPTIMIZATION ============================================================
    if color_optimization:
        mesh = slam.optimize_color(fragment_collector, trajectory, mesh)

    time = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    o3d.io.write_triangle_mesh(output_path + f"map_{str(time)}.ply", mesh)
    # ==================================================================================================================

    if visualize_final_map:
        o3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Main SLAM Pipeline for the reconstruction of a 3D bridge model. The first argument should be the '
                    'path to the config file. The second argument should be the path to the folder with the '
                    'preprocessed sequences. The third argument should be the path to the output folder.')
    parser.add_argument('config_file', type=str, help='Path to the config file')
    parser.add_argument('sequence_path', type=str, help='Path to the prerpocessed sequence')
    parser.add_argument('output_path', type=str,
                        help='Path to the output folder. A name for the specific scene will be added automatically.')
    main(parser.parse_args().config_file, parser.parse_args().sequence_path, parser.parse_args().output_path)
