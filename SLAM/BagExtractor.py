import argparse
import csv
import os
from datetime import datetime

import open3d as o3d
import pyrealsense2 as rs
import numpy as np

from open3d.cuda.pybind.visualization import Visualizer
from open3d.visualization import draw
from tqdm import tqdm


def capture_stream(dataset_folder, bag_file_path, has_imu_data=True):
    # Configure depth and color streams
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_device_from_file(bag_file_path, False)

    config.enable_stream(rs.stream.color, rs.format.rgb8, 30)
    config.enable_stream(rs.stream.depth, rs.format.z16, 30)
    if has_imu_data:
        config.enable_stream(rs.stream.gyro, rs.format.motion_xyz32f, 200)
        config.enable_stream(rs.stream.accel, rs.format.motion_xyz32f, 100)

    # Start streaming
    pipeline.start(config)

    align = rs.align(rs.stream.color)

    gyro_collector = []
    accel_collector = []
    timestamp_collector = []

    rgb_collector, depth_collector = [], []
    try:
        for i in tqdm(range(10_000)):

            #    rgb_sensor = rgb_sensor.set_option(rs.option.enable_auto_white_balance, 0)
            dt0 = datetime.now()
            frames = pipeline.wait_for_frames()
            frames = align.process(frames)
            frames.keep()
            if i == 0:
                frame_profile = frames.get_profile()
                # Write intrinsics to file
                intrinsics = frame_profile.as_video_stream_profile().get_intrinsics()
                intrinsic_row = [intrinsics.width, intrinsics.height, intrinsics.fx, intrinsics.fy, intrinsics.ppx,
                                 intrinsics.ppy]
                write_to_file(f"{dataset_folder}/intrinsics.json", intrinsic_row)

            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()

            # Use first 10 frames to calibrate white balance and 10 more for exposure

            if has_imu_data:
                gyro_frame = frames[2].as_motion_frame().get_motion_data()
                accel_frame = frames[3].as_motion_frame().get_motion_data()
                timestamp_collector.append(frames.get_timestamp())
                gyro_collector.append(gyro_frame)
                accel_collector.append(accel_frame)
            rgb_collector.append(color_frame)
            depth_collector.append(depth_frame)

            process_time = datetime.now() - dt0
            print("FPS: " + str(1 / process_time.total_seconds()) + " at img " + str(i))
    finally:
        pipeline.stop()

        # Safe color and pcd files
        # write acceleration and gyro data to file
        if has_imu_data:
            with open(f"{dataset_folder}/imu_data.csv", mode='w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(['timestamp', 'ax', 'ay', 'az', 'gx', 'gy', 'gz'])

                # Read sensor data and write to file
                for motion_idx in range(gyro_collector.__len__()):
                    gyro_frame = gyro_collector[motion_idx]
                    gyro_data = [gyro_frame.x, gyro_frame.y, gyro_frame.z]
                    accel_frame = accel_collector[motion_idx]
                    accel_data = accel_frame.x, accel_frame.y, accel_frame.z

                    # Extract timestamp from frame metadata
                    timestamp = timestamp_collector[motion_idx]

                    # Write sensor data to file
                    writer.writerow(
                        [timestamp, accel_data[0], accel_data[1], accel_data[2], gyro_data[0], gyro_data[1],
                         gyro_data[2]])
        return rgb_collector, depth_collector


def write_to_file(file, row):
    with open(file, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(row)


def read_from_file(file):
    rows = []
    with open(file, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            rows.append(row)
    return rows


def filter_sky_points(pcd, white_threshold=230, blue_threshold=150):
    def is_light_blue(colorRGB):
        return abs(colorRGB[0] - colorRGB[1]) < 40 and colorRGB[2] > blue_threshold and colorRGB[0] < colorRGB[2] and \
            colorRGB[1] < colorRGB[
                2]

    def is_white(colorRGB):
        return colorRGB[0] > white_threshold and colorRGB[1] > white_threshold and colorRGB[
            2] > white_threshold and abs(
            colorRGB[0] - colorRGB[1]) <= 15 and abs(colorRGB[0] - colorRGB[2]) <= 15 and abs(
            colorRGB[1] - colorRGB[2]) <= 15

    colors = np.asarray(pcd.colors) * 255

    mask = []
    for c_idx, color in enumerate(colors):
        if is_light_blue(color) or is_white(color):
            mask.append(c_idx)

    pcd_remainder = pcd.select_by_index(mask, invert=True)
    return pcd_remainder


def construct_pcds(dataset_folder, num_imgs, visualize=True):
    if visualize:
        vis = Visualizer()
        vis.create_window('PCD', width=848, height=480)
    # read in intrinsics
    intrinsics = read_from_file(dataset_folder + "/intrinsics.json")[0]
    intrinsics_width, intrinsics_height, intrinsics_fx, intrinsics_fy, intrinsics_ppx, intrinsics_ppy = \
        int(intrinsics[0]), int(intrinsics[1]), float(intrinsics[2]), float(intrinsics[3]), \
            float(intrinsics[4]), float(intrinsics[5])
    pinhole_camera_intrinsic = o3d.camera.PinholeCameraIntrinsic(intrinsics_width, intrinsics_height, intrinsics_fx,
                                                                 intrinsics_fy, intrinsics_ppx, intrinsics_ppy)
    for i in tqdm(range(0, num_imgs)):
        # read in rgb and depth file
        img_color = o3d.io.read_image(dataset_folder + f"/rgb/{i}.png")
        img_depth = o3d.io.read_image(dataset_folder + f"/depth/{i}.png")
        rgbd = o3d.geometry.RGBDImage()
        rgbd = rgbd.create_from_color_and_depth(img_color, img_depth, convert_rgb_to_intensity=False, depth_trunc=6)

        pcd = o3d.geometry.PointCloud()
        pcd = pcd.create_from_rgbd_image(rgbd, pinhole_camera_intrinsic, np.eye(4), project_valid_depth_only=True)

        pcd = pcd.voxel_down_sample(voxel_size=0.01)
        pcd, _ = pcd.remove_statistical_outlier(nb_neighbors=50, std_ratio=2)
        pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.2, max_nn=50))
        # pcd = filter_sky_points(pcd)
        # pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.2, max_nn=50))
        o3d.io.write_point_cloud(dataset_folder + f"/pcd/{i}.pcd", pcd)
        if visualize:
            vis.add_geometry(pcd)
            vis.update_geometry(pcd)
            vis.poll_events()
            vis.update_renderer()
            vis.remove_geometry(pcd)
    if visualize:
        vis.destroy_window()


def show_examples(num_imgs=10, n_examples=10):
    # load pcd
    step = num_imgs // n_examples
    for i in range(n_examples):
        pcd = o3d.io.read_point_cloud(dataset_folder + f"/pcd/{i * step}.pcd")
        pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
        o3d.visualization.draw_geometries([pcd], window_name=f"{i}")


def postprocess(dataset_folder, rgbs, depths):
    for i, (rgb, depth) in tqdm(enumerate(zip(rgbs, depths))):
        #depth = rs.decimation_filter(2).process(depth)

        depth = rs.disparity_transform(True).process(depth)
        depth = rs.spatial_filter(smooth_alpha=1, smooth_delta=20, magnitude=5., hole_fill=0).process(depth)
        # depth = rs.temporal_filter(smooth_alpha=0.8, smooth_delta=20, persistence_control=3).process(depth)
        depth = rs.disparity_transform(False).process(depth)

        rgb_data = np.asanyarray(rgb.get_data())
        depth_data = np.asanyarray(depth.get_data())
        # rescale depth to original resolution
        # rgb_data = cv2.resize(rgb_data, (848, 480))

        img_color = o3d.geometry.Image(rgb_data)
        img_depth = o3d.geometry.Image(depth_data)
        o3d.io.write_image(dataset_folder + f"/rgb/{i}.png", img_color)
        o3d.io.write_image(dataset_folder + f"/depth/{i}.png", img_depth)

        del rgb, depth


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Preprocessing script for bag files recorded by a Realsense D435i. '
                    'The first argument should be the path to the bag file, '
                    'the second argument should be the path to the folder where the preprocessed files should be '
                    'saved.')
    parser.add_argument('bag_path', type=str, help='Path to the bag file.')
    parser.add_argument('dataset_path', type=str, help='Path to the folder where the preprocessed files should be '
                                                       'saved.')
    dataset_folder = parser.parse_args().dataset_path
    bag_file = parser.parse_args().bag_path

    # check if directory exists
    if not os.path.exists(dataset_folder):
        os.makedirs(dataset_folder)
        os.makedirs(dataset_folder + "/rgb")
        os.makedirs(dataset_folder + "/depth")
        os.makedirs(dataset_folder + "/pcd")
    rgbs, depths = capture_stream(dataset_folder, bag_file, has_imu_data=True)
    postprocess(dataset_folder, rgbs, depths)
    num_imgs = len(os.listdir(dataset_folder + "/rgb"))
    construct_pcds(dataset_folder, num_imgs, visualize=False)
    #show_examples(num_imgs, n_examples=10)
