from logging import info

import faiss
import numpy as np
from matplotlib import pyplot as plt
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.metrics.pairwise import cosine_similarity


class LoopDetectorBoW:
    def __init__(self, num_words=512):
        # Test Descriptor dimensions:
        self.descriptors = None
        self.rgb_desc_ptr = [0]
        # Dimensional values
        self.n_frames = 0
        self.desc_dim = -1
        self.n_words = num_words
        super().__init__()

    def add(self, descriptors):
        if self.descriptors is None:
            self.descriptors = descriptors
            self.desc_dim = self.descriptors.shape[-1]
        else:
            self.descriptors = np.concatenate((self.descriptors, descriptors), axis=0)
        self.n_frames += 1
        self.rgb_desc_ptr.append(self.descriptors.shape[0])

    def desc_for_rgb(self, rgb_idx):
        return self.descriptors[self.rgb_desc_ptr[rgb_idx]:self.rgb_desc_ptr[rgb_idx + 1]]

    def gen_vlad_representation(self):

        kmeans = faiss.Kmeans(d=self.desc_dim, k=self.n_words, niter=20, verbose=True, gpu=False)
        kmeans.train(self.descriptors)
        centroids = kmeans.centroids

        residuals = np.zeros((self.n_frames, self.n_words, self.desc_dim))
        for i in range(self.n_frames):
            local_descriptors = self.desc_for_rgb(i)
            D, V = kmeans.index.search(local_descriptors, 1)
            V = V.squeeze(1)
            for desc_idx in range(len(local_descriptors)):
                vote = V[desc_idx]
                residuals[i][vote] += local_descriptors[desc_idx] - centroids[vote]
        return residuals

    def similarity_search(self, img_vectors):
        pairwise_distances = cosine_similarity(img_vectors)
        # Compute pairs based on similarity and distance of the indices
        pairs = []
        num_images = len(pairwise_distances)
        for i in range(len(pairwise_distances)):
            for j in range(i + 1, len(pairwise_distances)):
                pairs.append((i, j, pairwise_distances[i][j]))
        return pairs

    def null_safe_cosine_similarity(self, a, b):
        a_norm = np.linalg.norm(a, axis=1)
        b_norm = np.linalg.norm(b, axis=1)
        norms = np.multiply(a_norm, b_norm)

        nonzero_norms = norms != 0
        cosine_sim = np.zeros((a.shape[0]))
        cosine_sim[nonzero_norms] = np.einsum('ij,ij->i', a[nonzero_norms], b[nonzero_norms]) / norms[nonzero_norms]
        return cosine_sim

    def filter_too_narrow_lc(self, pairs, threshold):
        # pairs.sort(key=lambda x: x[2] * 0.5 + 0.5 * (np.abs(x[0]-x[1]) / num_images), reverse=True)
        pairs.sort(key=lambda x: x[2], reverse=True)
        # filter all pairs where x[0] and x[1] are too close
        return [pair for pair in pairs if np.abs(pair[0] - pair[1]) > threshold]

    def sort_pairs(self, pairs):
        pairs.sort(key=lambda x: x[2], reverse=True, )
        return pairs

    def pipeline(self, threshold=0.015, pair_filter=None):
        info("Generating BoW representation for each Image")
        bows = self.gen_bows()
        info("Compute cosine similarities")

        print("TFIDF transform")
        word_count_sum = bows.sum(axis=0)
        tfidfTransf = TfidfTransformer(use_idf=True, norm='l2', smooth_idf=True)
        img_tfidf_vectors = tfidfTransf.fit_transform(X=bows)
        pairs = self.similarity_search(img_tfidf_vectors)

        pairs = self.sort_pairs(pairs)

        # cut off all pairs that have a similarity below the threshold
        for i in range(len(pairs)):
            if pairs[i][2] < threshold:
                pairs = pairs[:i]
                break
        info(f"Pairs after thresholding: {len(pairs)}")
        # pairs = self.filter_too_narrow_lc(pairs, 3)
        # pairs = self.filter_by_proximity(pairs, 20)
        return pairs

    def gen_bows(self, size=512):
        print(f"Kmeans using {len(self.descriptors)} descriptors for clustering.")
        # descriptor__arr = np.concatenate([np.asarray(desc) for desc in self.descriptors], axis=0, dtype=np.float32)

        ncentroids = size
        niter = 20
        gpu = False
        # d = descriptor__arr.shape[-1]
        d = self.descriptors.shape[-1]
        kmeans = faiss.Kmeans(d, ncentroids, niter=niter, verbose=True, gpu=gpu, spherical=True)
        kmeans.train(self.descriptors)

        # generate bows from labels
        n_frames = len(self.rgb_desc_ptr) - 1
        bows = np.zeros((n_frames, size))
        for i in range(n_frames):
            D, votings = kmeans.index.search(self.desc_for_rgb(i), 10)
            for vote in votings:
                for l_idx, label in enumerate(vote):
                    bows[i][label] += 1. / (l_idx + 1)
            # round down values

            bows[i] = np.floor(bows[i]).astype(int)  # idf needs ints
        return bows

    def filter_by_proximity(self, pairs, interval):
        new_pairs = []
        new_pairs.append(pairs[0])
        for pair in pairs[1:]:
            pairs_with_similar_src = list(filter(lambda x: np.abs(x[0] - pair[0]) < interval, new_pairs))
            pairs_with_similar_tgt = list(filter(lambda x: np.abs(x[1] - pair[1]) < interval, pairs_with_similar_src))
            if len(pairs_with_similar_tgt) == 0:
                new_pairs.append(pair)
        return new_pairs


def filter_by_frame_distance(pairs):
    new_pairs = []
    for pair in pairs:
        if np.abs(pair[0] - pair[1]) >= 20:
            new_pairs.append(pair)
    return new_pairs


def viz_bow(bow0, bow1):
    fig, axs = plt.subplots(2)
    fig.suptitle('Vertically stacked subplots')
    axs[0].plot(bow0)
    axs[1].plot(bow1)
    plt.show()
