import open3d as o3d
import numpy as np

from src.Color_Optimization import get_rgbs_indices_for_color_optimization
from src.General import measure_time
from src.Open3d_Tools import build_rgbd_from_image_paths, delete_non_connected_components_from_mesh


class MapFragment:
    def __init__(self, intrinsic, start_idx,
                 map_voxel_size, depth_scale, max_depth, sdf_trunc, keyframe=None, init_transformation=None):
        self.frames = []
        self.depth_scale = depth_scale
        self.max_depth = max_depth
        self.num_frames = 0
        self.start_idx = start_idx
        self.end_idx = start_idx
        self.intrinsic = intrinsic
        self.extrinsics = []
        self.map_voxel_size = map_voxel_size

        self.num_frames = 0
        self.map = None
        self.sdf_trunc = sdf_trunc
        self.tsdf_volume = self.create_new_volume(self.sdf_trunc)
        # Perform integration of keyframe
        if keyframe is not None:
            self.add_and_integrate_frame(keyframe, init_transformation, start_idx)

    def create_new_volume(self, sdf_trunc):
        return o3d.pipelines.integration.ScalableTSDFVolume(voxel_length=self.map_voxel_size,
                                                            sdf_trunc=sdf_trunc,
                                                            color_type=o3d.pipelines.integration.TSDFVolumeColorType.
                                                            RGB8)

    def add_and_integrate_frame(self, frame, extrinsic, cur_idx):
        self.frames.append(frame)
        self.extrinsics.append(extrinsic)

        rgbd = build_rgbd_from_image_paths(frame.color_path, frame.depth_path, self.depth_scale, self.max_depth)

        self.tsdf_volume.integrate(rgbd, self.intrinsic, extrinsic)
        self.end_idx = cur_idx
        self.num_frames += 1
        del rgbd

    def get_trajectory(self):
        return self.extrinsics

    def local_map_dense(self):
        return self.map.pcd

    def local_map_sparse(self):
        return self.map.pcd_down

    def __load_rgbd(self, idx):
        color = o3d.io.read_image(self.frames[idx].color_path)
        depth = o3d.io.read_image(self.frames[idx].depth_path)
        rgbd = o3d.geometry.RGBDImage().create_from_color_and_depth(color, depth, convert_rgb_to_intensity=False,
                                                                    depth_scale=self.depth_scale)
        return rgbd

    def head(self):
        return self.get_keyframe()

    # Equivalent to head()
    def get_keyframe(self):
        return self.__load_rgbd(0)

    def tail(self):
        return self.frames[-1]

    def update_map(self, voxel_size, filter_sky_points=False, optimize=False):
        if optimize:
            self.map = self.optimize()
        else:
            self.map = self.tsdf_volume.extract_point_cloud()
            del self.tsdf_volume
        # self.map = self.map.voxel_down_sample(voxel_size)
        if filter_sky_points:
            self.map = self.filter_sky_points(self.map)
        self.map, _ = self.map.remove_statistical_outlier(nb_neighbors=30, std_ratio=1.0)
        self.map.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=self.map_voxel_size*4, max_nn=30))
        self.map = self.map.voxel_down_sample(voxel_size)

    def filter_sky_points(self, pcd, white_threshold=230, blue_threshold=150):
        def is_light_blue(color):
            return abs(color[0] - color[1]) < 40 and color[2] > blue_threshold and color[0] < color[2] and color[1] < \
                color[2]

        def is_white(color):
            return color[0] > white_threshold and color[1] > white_threshold and color[2] > white_threshold and abs(
                color[0] - color[1]) <= 15 and abs(color[0] - color[2]) <= 15 and abs(color[1] - color[2]) <= 15

        colors = np.asarray(pcd.colors) * 255
        # Filter out sky points
        # o3d.visualization.draw_geometries([pcd])

        mask = []
        for c_idx, color in enumerate(colors):
            if is_light_blue(color) or is_white(color):
                mask.append(c_idx)
        pcd_remainder = pcd.select_by_index(mask, invert=True)
        return pcd_remainder

    @measure_time
    def optimize(self):
        print("Optimizing map...")
        mesh = self.tsdf_volume.extract_triangle_mesh()
        # remove outliers
        mesh = delete_non_connected_components_from_mesh(mesh, 10_000)

        cam_params = []
        for pose in self.extrinsics:
            cam_param = o3d.camera.PinholeCameraParameters()
            cam_param.extrinsic = pose
            cam_param.intrinsic = self.intrinsic
            cam_params.append(cam_param)
        o3d_trajectory = o3d.camera.PinholeCameraTrajectory()

        f_indices_to_use = get_rgbs_indices_for_color_optimization(self.frames, self.extrinsics)
        f_indices_to_use = np.unique(f_indices_to_use)

        o3d_trajectory.parameters = [cam_params[i] for i in f_indices_to_use]
        frames_thinned_out = [self.frames[i] for i in f_indices_to_use]
        # o3d_trajectory.parameters = cam_params
        # frames_thinned_out = self.frames
        # get rgbd images:

        rgbd_images = []
        for frame in frames_thinned_out:
            if frame.active:
                rgbd_images.append(
                    build_rgbd_from_image_paths(frame.color_path, frame.depth_path, self.depth_scale, self.max_depth))
        # Start optimization
        option = o3d.pipelines.color_map.RigidOptimizerOption(maximum_iteration=200,
                                                              maximum_allowable_depth=self.max_depth,
                                                              depth_threshold_for_visibility_check=0.03,
                                                              depth_threshold_for_discontinuity_check=2,
                                                              )

        mesh, _ = o3d.pipelines.color_map.run_rigid_optimizer(mesh, rgbd_images, o3d_trajectory, option)

        del self.tsdf_volume
        new_pcd = mesh.sample_points_uniformly(len(mesh.vertices))
        new_pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
        print("Done optimizing map.")
        return new_pcd

    def serialize(self):
        return {
            "frames": self.frames,
            "depth_scale": self.depth_scale,
            "num_frames": self.num_frames,
            "extrinsics": self.extrinsics,
            "intrinsic": self.intrinsic.intrinsic_matrix,
            "intrinsic_width": self.intrinsic.width,
            "intrinsic_height": self.intrinsic.height,
            "map_pts": np.asarray(self.map.points),
            "map_colors": np.asarray(self.map.colors),
            "map_normals": np.asarray(self.map.normals),
            "map_voxel_size": self.map_voxel_size,
            "start_idx": self.start_idx,
            "end_idx": self.end_idx,
            "max_depth": self.max_depth,
            "sdf_trunc": self.sdf_trunc
        }

    def deserialize(self, data):
        self.frames = data["frames"]
        self.extrinsics = data["extrinsics"]
        self.intrinsic = o3d.camera.PinholeCameraIntrinsic(width=data["intrinsic_width"],
                                                           height=data["intrinsic_height"],
                                                           intrinsic_matrix=data["intrinsic"])
        self.map = o3d.geometry.PointCloud()
        self.map.points = o3d.utility.Vector3dVector(data["map_pts"])
        self.map.colors = o3d.utility.Vector3dVector(data["map_colors"])
        self.map.normals = o3d.utility.Vector3dVector(data["map_normals"])
        self.map_voxel_size = data["map_voxel_size"]
        self.start_idx = data["start_idx"]
        self.end_idx = data["end_idx"]
        self.num_frames = data["num_frames"]
        self.depth_scale = data["depth_scale"]
        self.max_depth = data["max_depth"]
        self.sdf_trunc = data["sdf_trunc"]
        return self
