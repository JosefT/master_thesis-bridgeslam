import numpy as np
import open3d as o3d
import cv2
def manhattanForBinary(x,y):
    return np.sum(x ^ y)

class TreeNode:
    def __init__(self, descriptor, depth, count=0):
        self.descriptor = descriptor
        self.children = []
        self.count = count
        self.depth = depth

    def add_child(self, child_node):
        self.children.append(child_node)

    def is_leaf(self):
        return False

    def traverse_tree(self, query):
        optimal_hamming_distance = 256
        optimal_child = None
        for child in self.children:
            hamming_distance = manhattanForBinary(child.descriptor, query)
            if hamming_distance < optimal_hamming_distance:
                optimal_hamming_distance = hamming_distance
                optimal_child = child
        return optimal_child.traverse_tree(query)

class TreeLeaf(TreeNode):
    def __init__(self, descriptor, depth, count=0, word_id=0):
        super().__init__(descriptor, depth, count)
        self.word_id = word_id
    def traverse_tree(self, query):
        return self.word_id

    def is_leaf(self):
        return True