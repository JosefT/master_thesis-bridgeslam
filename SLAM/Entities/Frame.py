
class Frame:
    def __init__(self, color_path, depth_path, pcd_path, active=True):
        self.color_path = color_path
        self.depth_path = depth_path
        self.pcd_path = pcd_path
        self.active = active
