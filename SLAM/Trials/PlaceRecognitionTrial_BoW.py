

import faiss
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics.pairwise import cosine_similarity
from tqdm import tqdm

from src.DataLoadingMethods import load_rgbs_generic
import cv2

from sklearn.feature_extraction.text import TfidfTransformer
import scipy.cluster.hierarchy as sch
import scipy.spatial.distance as ssd


def get_img_descriptor(rgb):
    orb = cv2.ORB_create()
    kp = orb.detect(rgb, None)
    kp, des = orb.compute(rgb, kp)
    des_binary = np.zeros((len(des), 256))
    for i, desc in enumerate(des):
        des_binary[i] = np.unpackbits(desc)

    des = np.asarray([desc / np.linalg.norm(desc) for desc in des])
    return des


def generate_bows(rgbs, size=512):
    descriptors = []
    print("Get image descriptors")
    for rgb in tqdm(rgbs):
        descriptors.append(get_img_descriptor(rgb))

    # print("Processing {} descriptors".format(len(all_descriptors)))

    # Use 20 evenly distributed descriptors for clustering
    # descriptor_sample = descriptors[::len(descriptors) //] # sample descriptors

    print(f"Kmeans using the descriptors of {len(descriptors)} images for clustering.")
    descriptor__arr = np.concatenate([np.asarray(desc) for desc in descriptors], axis=0, dtype=np.float32)

    ncentroids = size
    niter = 20
    gpu = False
    d = descriptor__arr.shape[-1]

    # Calculate the pairwise Hamming distances

    kmeans = faiss.Kmeans(d, ncentroids, niter=niter, verbose=True, gpu=gpu, spherical=True)

    kmeans.train(descriptor__arr)


    # generate bows from labels
    print("Generate bows")
    bows = np.zeros((len(rgbs), size))
    for i in range(len(rgbs)):
        D, votings = kmeans.index.search(descriptors[i], 3)
        bows[i][votings] += 1
        for vote in votings:
            for l_idx, label in enumerate(vote):
                bows[i][label] += 1. / (l_idx + 1) ** 2
        # round down values

        bows[i] = np.floor(bows[i]).astype(int)  # idf needs ints
    return bows


def similarity_search_tfidf(bows):
    pass


def generate_image_representation():
    pass


def similarity_search(img_vectors):
    pairwise_distances = cosine_similarity(img_vectors)
    # Compute pairs based on similarity and distance of the indices
    pairs = []
    num_images = len(pairwise_distances)
    for i in range(len(pairwise_distances)):
        for j in range(i + 1, len(pairwise_distances)):
            pairs.append((i, j, pairwise_distances[i][j]))
    return pairs


def viz_bow(bow0, bow1):
    fig, axs = plt.subplots(2)
    fig.suptitle('Vertically stacked subplots')
    axs[0].plot(bow0)
    axs[1].plot(bow1)
    plt.show()


def sort_pairs(pairs):
    # pairs.sort(key=lambda x: x[2] * 0.5 + 0.5 * (np.abs(x[0]-x[1]) / num_images), reverse=True)
    pairs.sort(key=lambda x: x[2], reverse=True)
    return pairs


def visualize_pairs(pairs, rgbs):
    # visualize pairs
    already_shown = set()
    for i, j, sim in pairs:
        # check whether pair was already shown in an interval of 10
        #if any([abs(i - x) < 20 and abs(j - y) < 20 for x, y in already_shown]) or abs(i - j) < 50:
        #    continue
        already_shown.add((i, j))
        rgb0 = rgbs[i]
        rgb1 = rgbs[j]

        # concatenate rgbs to visualize them next to each other
        rgb = np.concatenate((rgb0, rgb1), axis=1)
        cv2.imshow(f"Frame {i} and {j} with similarity: {sim}", rgb)

        cv2.waitKey(0)
        cv2.destroyAllWindows()

def hamming_distance(x, y):
    return np.sum(x != y)

def main():
    # load rgbs
    path = "../../Datasets/SLAM/bridge/02/"
    amount = 499
    rgbs = load_rgbs_generic(path, amount)[::50]

    bows = generate_bows(rgbs)

    print("TFIDF transform")
    word_count_sum = bows.sum(axis=0)
    print("Word count sum")
    print(np.shape(word_count_sum))
    # bows : (num_images, num_words)
    tfidfTransf = TfidfTransformer(use_idf=True, norm='l2', smooth_idf=False, sublinear_tf=True)
    img_tfidf_vectors = tfidfTransf.fit_transform(X=bows)
    print(tfidfTransf.idf_)

    #viz_bow(bows[51], bows[395])
    #viz_bow(img_tfidf_vectors[51].toarray().flatten(), img_tfidf_vectors[395].toarray().flatten())
    pairs = similarity_search(img_tfidf_vectors)

    pairs = sort_pairs(pairs)
    # filter pairs that are to close to each other
    pairs = [pair for pair in pairs if abs(pair[0] - pair[1]) > 10]
    for pair in pairs:
        visualize_pairs([pair], rgbs)
        #viz_bow(bows[pair[0]], bows[pair[1]])
        #viz_bow(img_tfidf_vectors[pair[0]].toarray().flatten(), img_tfidf_vectors[pair[1]].toarray().flatten())

    """
   
    pair = [280, 838, cosine_similarity(img_tfidf_vectors[280], img_tfidf_vectors[838])[0][0]]
    print(pair)
    visualize_pairs([pair], rgbs)
    viz_bow(bows[pair[0]], bows[pair[1]])
    viz_bow(img_tfidf_vectors[pair[0]].toarray().flatten(), img_tfidf_vectors[pair[1]].toarray().flatten())

    pair = [0, 974, cosine_similarity(img_tfidf_vectors[0], img_tfidf_vectors[974])[0][0]]
    print(pair)
    visualize_pairs([pair], rgbs)
    viz_bow(bows[pair[0]], bows[pair[1]])
    viz_bow(img_tfidf_vectors[pair[0]].toarray().flatten(), img_tfidf_vectors[pair[1]].toarray().flatten())


    pair = [280, 460, cosine_similarity(img_tfidf_vectors[280], img_tfidf_vectors[460])[0][0]]
    print(pair)
    visualize_pairs([pair], rgbs)
    viz_bow(bows[pair[0]], bows[pair[1]])
    viz_bow(img_tfidf_vectors[pair[0]].toarray().flatten(), img_tfidf_vectors[pair[1]].toarray().flatten())
    """
if __name__ == '__main__':
    main()
