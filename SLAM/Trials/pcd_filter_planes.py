import numpy as np
import open3d as o3d
from polylidar import (Polylidar3D, MatrixDouble, MatrixFloat, extract_tri_mesh_from_float_depth,
                       extract_point_cloud_from_float_depth)
from polylidar.polylidarutil.open3d_util import construct_grid, create_lines, flatten, create_open_3d_mesh_from_tri_mesh
from polylidar.polylidarutil.plane_filtering import filter_planes_and_holes
def filter_and_create_open3d_polygons(points, polygons, rm=None):
    " Apply polygon filtering algorithm, return Open3D Mesh Lines "
    config_pp = dict(filter=dict(hole_area=dict(min=0.025, max=100.0), hole_vertices=dict(min=6), plane_area=dict(min=0.1)),
                     positive_buffer=0.00, negative_buffer=0.00, simplify=0.02)
    planes, obstacles = filter_planes_and_holes(polygons, points, config_pp, rm)
    all_poly_lines = create_lines(planes, obstacles, line_radius=0.01)
    return all_poly_lines

def main():
    pcd = o3d.io.read_point_cloud("../temp/noisy.ply")
    pcd = pcd.voxel_down_sample(voxel_size=0.03)
    print(pcd)
    o3d.visualization.draw_geometries([pcd])
    use_cuda = True
    if use_cuda:
        mesh, timings = create_meshes_cuda(opc, **meta['mesh']['filter'])
    else:
        mesh, timings = create_meshes(opc, **meta['mesh']['filter'])

if __name__ == "__main__":
    main()


