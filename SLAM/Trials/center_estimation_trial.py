import numpy as np
import open3d as o3d

from Serialization import load_fragments


class node:
    def __init__(self, box_points):
        self.points = box_points



def main():
    fragment_collector = load_fragments("Outputs/roof_structure_01/fragment_collector.pkl")
    pcd = fragment_collector[3].map
    #pcd.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])

    vol_limit = 27
    obb = pcd.get_axis_aligned_bounding_box()
    if obb.volume() > vol_limit:
        pcds = subdivide(pcd, vol_limit)
    else:
        pcds = [pcd]
    print(len(pcds))
    # paint pcds in different colors
    for i in range(len(pcds)):
        pcds[i].paint_uniform_color(np.random.rand(3))
    o3d.visualization.draw_geometries(pcds)


def subdivide(pcd, vol_limit):
    pcds = []
    points = np.asarray(pcd.points)
    # get min and max in each dimension
    min_bound = np.min(points, axis=0)
    max_bound = np.max(points, axis=0)
    diff = max_bound - min_bound

    max_dim = np.argmax(diff)
    threshold = min_bound[max_dim] + diff[max_dim] / 2

    # get points in each half
    points_left = points[points[:, max_dim] < threshold]
    points_right = points[points[:, max_dim] >= threshold]

    pcd0 = o3d.geometry.PointCloud()
    pcd0.points = o3d.utility.Vector3dVector(points_left)
    pcd1 = o3d.geometry.PointCloud()
    pcd1.points = o3d.utility.Vector3dVector(points_right)

    obb0 = pcd0.get_axis_aligned_bounding_box()
    obb1 = pcd1.get_axis_aligned_bounding_box()
    if obb0.volume() > vol_limit:
        pcds = pcds + subdivide(pcd0, vol_limit)
    else:
        if len(points_left) > 1000:
            pcds.append(pcd0)
    if obb1.volume() > vol_limit:
        pcds = pcds + subdivide(pcd1, vol_limit)
    else:
        if len(points_right) > 1000:
            pcds.append(pcd1)
    return pcds

if __name__ == "__main__":
    main()
