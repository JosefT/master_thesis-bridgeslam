import os

import numpy as np
import open3d as o3d
import cv2

from SLAM5 import Frame

def increase_brightness(image, value=60):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)
    v = cv2.add(v, value)
    v = np.clip(v, 0, 255)
    final_hsv = cv2.merge((h, s, v))
    brightened_image = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)

    return brightened_image

def increase_contrast(image, alpha=3, beta=0):
    # Apply contrast adjustment using the formula new_pixel = alpha * old_pixel + beta
    adjusted_image = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)
    return adjusted_image
def main():
    path = "../datasets/bridge/03/"
    n_frames = len(os.listdir(path + "rgb"))
    frames = []
    for i in range(0, n_frames):
        frame = Frame(path + "rgb/{}.png".format(i), path + "depth/{}.png".format(i), path + "pcd/{}.pcd".format(i))
        frames.append(frame)
    for frame in frames[::20]:
        image = cv2.imread(frame.color_path)
        brightened_image = increase_contrast(image)
        cv2.imshow('brightened_image', brightened_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()



if __name__ == "__main__":
    main()
