import faiss
import numpy
import numpy as np
import open3d as o3d
import cv2

from DataLoadingMethods import get_example_frames
from sklearn.cluster import KMeans

def kmeansFunc(descriptors, size=15):
    flattened_descriptors = np.concatenate(descriptors, axis=0)
    print(f"Kmeans using {len(flattened_descriptors)} descriptors for clustering.")
    # descriptor__arr = np.concatenate([np.asarray(desc) for desc in self.descriptors], axis=0, dtype=np.float32)

    ncentroids = size
    niter = 20
    gpu = False
    # d = descriptors
    d = flattened_descriptors.shape[-1]
    kmeans = KMeans(n_clusters=ncentroids, random_state=0, init='k-means++').fit(flattened_descriptors)

    return kmeans

def main():
    pass


if __name__ == "__main__":
    n_clusters = 10
    frames = get_example_frames()[::50]
    n_frames = len(frames)
    orb = cv2.SIFT_create()
    #load img
    kpts = []
    descriptors = []
    for i in range(n_frames):
        img = cv2.imread(frames[i].color_path)
        kp1 = orb.detect(img, None)
        kp1, des = orb.compute(img, kp1)
        #convert des1 to a binary array
        #des_binary = np.zeros((len(des), 256))
        #for i, desc in enumerate(des):
        #    des_binary[i] = np.unpackbits(desc)
        # visualize keypoints
        if len(kp1) != 500:
            print(len(kp1))
        descriptors.append(np.asarray(des, dtype=np.int32))
        kpts.append(kp1)
    kmeans = kmeansFunc(descriptors, n_clusters)
    colors = np.random.randint(0, 255, (n_clusters, 3))
    for i in range(n_frames):
        kp1 = kpts[i]
        des1 = descriptors[i]
        img = cv2.imread(frames[i].color_path)
        labels = kmeans.predict(des1)


        # color clusters
        print(labels)
        color_labels = [colors[labels[i]] for i in range(len(labels))]

        # visualize keypoints
        for i in range(len(kp1)):
            img = cv2.circle(img, (int(kp1[i].pt[0]), int(kp1[i].pt[1])), 3, color_labels[i].tolist(), 3)
        cv2.imshow("img", img)
        cv2.waitKey(0)
