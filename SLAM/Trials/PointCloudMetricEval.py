import os
import pickle

import numpy as np
import open3d as o3d
import cv2
from easydict import EasyDict

from src.metrics import NDT_score, get_normal_similarity, corAL


def main():
    pass


def edict(results):
    pass


if __name__ == "__main__":
    # load data
    CUR = os.path.dirname(os.path.abspath(__file__))
    dataset_path = os.path.join(CUR, '../../Datasets/GCNet/Bridge_Dataset/')
    pkl_path = os.path.join(dataset_path, 'bridge_dataset.pkl')
    with open(pkl_path, 'rb') as f:
        infos = pickle.load(f)
    voxel_size = 0.04
    ndt_box_size = 0.5
    min_pt_thres = 0.25
    # Iterate through all point clouds

    results = dict()
    results["NDT"] = dict()
    results["fitness"] = dict()
    results["normal_difference"] = dict()
    results["corAL"] = dict()
    for key in results.keys():
        results[key]["counts"] = dict()
        results[key]["counts"]["tp"] = 0
        results[key]["counts"]["fp"] = 0
        results[key]["counts"]["fn"] = 0
        results[key]["counts"]["tn"] = 0
        results[key]["indices"] = dict()
        results[key]["indices"]["tp"] = list()
        results[key]["indices"]["fp"] = list()
        results[key]["indices"]["fn"] = list()
        results[key]["indices"]["tn"] = list()

    results = EasyDict(results)
    for idx in range(len(infos["src"])):
        rot = infos["rot"][idx]
        trans = infos["trans"][idx]
        classification = infos["class"][idx]
        T = np.eye(4)
        T[:3, :3] = rot
        T[:3, 3] = trans

        source = o3d.io.read_point_cloud(os.path.join(dataset_path, "pcd", infos["src"][idx])).transform(T)
        target = o3d.io.read_point_cloud(os.path.join(dataset_path, "pcd", infos["tgt"][idx]))

        regResult = o3d.pipelines.registration.evaluate_registration(source, target, 0.12, np.eye(4))
        ndt_score = NDT_score(source, target, regResult, voxel_size, box_size=ndt_box_size, min_pt_thres=min_pt_thres,
                              visualize=False, visualize_threshold=50)
        fitness = regResult.fitness
        normal_difference = get_normal_similarity(source, target, regResult)

        corAlscore = corAL(source, target, regResult)
        print("Class: ", classification)
        print("ndt_score: ", ndt_score)
        print("fitness: ", fitness)
        print("normal_difference: ", normal_difference)
        print("corAL: ", corAlscore)

        #ndtscore:
        if classification == 0 or classification == 1:
            if ndt_score > -1.2:
                results.NDT.counts.tp += 1
                results.NDT.indices.tp.append(idx)
            else:
                results.NDT.counts.fn += 1
                results.NDT.indices.fn.append(idx)
            if fitness >  0.25:
                results.fitness.counts.tp += 1
                results.fitness.indices.tp.append(idx)
            else:
                results.fitness.counts.fn += 1
                results.fitness.indices.fn.append(idx)
            if normal_difference > 0.6:
                results.normal_difference.counts.tp += 1
                results.normal_difference.indices.tp.append(idx)
            else:
                results.normal_difference.counts.fn += 1
                results.normal_difference.indices.fn.append(idx)
            if corAlscore > -2.5:
                results.corAL.counts.tp += 1
                results.corAL.indices.tp.append(idx)
            else:
                results.corAL.counts.fn += 1
                results.corAL.indices.fn.append(idx)
        else:
            if ndt_score > 0:
                results.NDT.counts.fp += 1
                results.NDT.indices.fp.append(idx)
            else:
                results.NDT.counts.tn += 1
                results.NDT.indices.tn.append(idx)
            if fitness >  0.25:
                results.fitness.counts.fp += 1
                results.fitness.indices.fp.append(idx)
            else:
                results.fitness.counts.tn += 1
                results.fitness.indices.tn.append(idx)
            if normal_difference > 0.6:
                results.normal_difference.counts.fp += 1
                results.normal_difference.indices.fp.append(idx)
            else:
                results.normal_difference.counts.tn += 1
                results.normal_difference.indices.tn.append(idx)
            if corAlscore > -2.5:
                results.corAL.counts.fp += 1
                results.corAL.indices.fp.append(idx)
            else:
                results.corAL.counts.tn += 1
                results.corAL.indices.tn.append(idx)
    print(results)

