import math

import numpy as np
import open3d as o3d
from scipy.spatial.distance import cdist
from sklearn.linear_model import LinearRegression
from tqdm import tqdm


def bilateral_filter(point_cloud, sigma_spatial, sigma_range):
    filtered_cloud = np.zeros_like(point_cloud)

    # Compute spatial kernel weights
    spatial_distances = cdist(point_cloud[:, :3], point_cloud[:, :3])
    spatial_weights = np.exp(-spatial_distances / (2 * sigma_spatial**2))

    # Normalize spatial weights
    spatial_weights /= np.sum(spatial_weights, axis=1)[:, None]

    for i in range(point_cloud.shape[0]):
        # Compute range kernel weights
        range_distances = np.linalg.norm(point_cloud[i, 3:] - point_cloud[:, 3:], axis=1)
        range_weights = np.exp(-range_distances / (2 * sigma_range**2))

        # Normalize range weights
        max_range_weight = np.max(range_weights)
        range_weights /= np.sum(range_weights)

        # Compute combined weights
        weights = spatial_weights[i] * range_weights
        scaling_factor = np.sum(weights) / np.sum(range_weights)
        weights *= scaling_factor

        # Compute filtered point
        filtered_point = np.sum(weights[:, None] * point_cloud[:, :3], axis=0)

        filtered_cloud[i, :3] = filtered_point
        filtered_cloud[i, 3:] = point_cloud[i, 3:]  # Preserve additional attributes

    return filtered_cloud

def compute_regression_plane(points):
    # calculate plane in point-normal form from points
    # https://math.stackexchange.com/questions/99299/best-fitting-plane-given-a-set-of-points
    mean_points = np.mean(points, axis=0, keepdims=True)
    svd = np.linalg.svd(points - mean_points)
    normal = svd[2].T[:, -1]
    centroid = mean_points.squeeze()
    normal = normal / np.linalg.norm(normal)

    # calculate distances from centroid
    factor = np.abs(np.dot(points, normal) - np.dot(centroid, normal))
    rn = normal * max(factor)
    return normal, centroid, rn

def visualize_regression_plane(pcd, neighbors, normal_vector, mean):
    # Convert the grid points to an Open3D PointCloud object
    points = np.concatenate((neighbors, np.expand_dims(mean, axis=0), np.asarray(pcd.points)), axis=0)

    # init colors grid_plane = grey, neighbors = red, mean = green, pcd = pcd colors
    neighbors_color = np.tile([1, 0, 0], (len(neighbors), 1))
    mean_color = np.expand_dims([0, 1, 0], axis=0)
    pcd_color = np.tile([0.5, 0.5, 0.5], (len(pcd.points), 1))

    point_cloud = o3d.geometry.PointCloud()
    point_cloud.points = o3d.utility.Vector3dVector(points)
    point_cloud.colors = o3d.utility.Vector3dVector(np.concatenate((neighbors_color, mean_color, pcd_color), axis=0))

    # Create a line segment representing the normal vector
    line_start = mean
    line_end = mean + normal_vector
    line = o3d.geometry.LineSet()
    line.points = o3d.utility.Vector3dVector([line_start, line_end])
    line.lines = o3d.utility.Vector2iVector([[0, 1]])

    # Visualize the plane and points
    visualizer = o3d.visualization.Visualizer()
    visualizer.create_window()
    visualizer.add_geometry(point_cloud)
    visualizer.add_geometry(line)
    visualizer.run()
    visualizer.destroy_window()


def bilateral(tree, pcd, p, radius):
    neighbor_indices = tree.search_radius_vector_3d(p, radius)[1]
    neighbors = np.asarray(pcd.points)[neighbor_indices]
    normal_vector, mean, rn = compute_regression_plane(neighbors)
    sigma_spatial = radius / 3
    sigma_range = np.linalg.norm(rn) / 3
    if sigma_range == 0:
        sigma_range = sigma_spatial
    #visualize_regression_plane(pcd, neighbors, rn, mean)
    # draw regression plane
    d_d = np.linalg.norm(neighbors - p, axis=1)
    d_n = np.dot(neighbors - p, normal_vector)
    w = np.exp(-np.square(d_d) / (2 * sigma_spatial ** 2)) * np.exp(-np.square(d_n) / (2 * sigma_range ** 2))
    sum_w = np.sum(w)
    delta_p = np.dot(w, d_n)
    p = p + (delta_p / sum_w) * normal_vector
    return p

def make_pcd_noisy(pcd):
    # add gaussian noise to point cloud
    points = np.asarray(pcd.points)
    noise = np.random.normal(-0.05, 0.05, size=points.shape)
    points += noise
    pcd.points = o3d.utility.Vector3dVector(points)
    return pcd

def main():
    pcd = o3d.io.read_point_cloud("../temp/noisy.ply")
    pcd = pcd.voxel_down_sample(voxel_size=0.025)
    o3d.visualization.draw_geometries([pcd])

    points = np.asarray(pcd.points)
    # estimate avg nn distance:
    radius = 0.1
    for i in range (0,3):
        filtered_points = []
        pcd_tree = o3d.geometry.KDTreeFlann(pcd)
        for p_idx, p in tqdm(enumerate(points)):
            filtered_points.append(bilateral(pcd_tree, pcd, p, radius=radius))
        points = np.asarray(filtered_points)
        pcd.points = o3d.utility.Vector3dVector(points)
        pcd = pcd.voxel_down_sample(voxel_size=0.02)
        points = np.asarray(pcd.points)

    o3d.visualization.draw_geometries([pcd])
    """
    # load o3d pointcloud
   p = points[1500]
    # find neighbors of p using a knn search
    pcd_tree = o3d.geometry.KDTreeFlann(pcd)
    neighbors = pcd_tree.search_radius_vector_3d(pcd.points[1550], 0.1)
    print(neighbors)
    neighbors = np.asarray(pcd.points)[neighbors[1], :]

    # compute regression plane
    normal_vector, mean = compute_regression_plane(neighbors)
    # visualize normal vector

    visualize_regression_plane(pcd, neighbors, normal_vector, mean)
    # remove noise
    pcd = pcd.remove_statistical_outlier(nb_neighbors=20, std_ratio=2)[0]
    print(pcd)
    pcd = pcd.voxel_down_sample(voxel_size=0.025)
    print(pcd)
    color_intensities = np.expand_dims(np.asarray(pcd.colors)[:, 0] + np.asarray(pcd.colors)[:, 1] + np.asarray(pcd.colors)[:, 2]/ 3, axis=-1)
    print(color_intensities)
    filtered_cloud_pts = bilateral_filter(np.concatenate((np.asarray(pcd.points), color_intensities), axis=1), sigma_spatial=0.1, sigma_range=0.2)
    filtered_cloud = o3d.geometry.PointCloud()
    filtered_cloud.points = o3d.utility.Vector3dVector(filtered_cloud_pts[:, :3])
    #filtered_cloud.colors = o3d.utility.Vector3dVector(filtered_cloud_pts[:, 3:])
    #filtered_cloud = filtered_cloud.voxel_down_sample(voxel_size=0.025)
    print(filtered_cloud)
    # print min and max coordinate of pcd

    o3d.visualization.draw_geometries([filtered_cloud])
    """
if __name__ == "__main__":
    main()


