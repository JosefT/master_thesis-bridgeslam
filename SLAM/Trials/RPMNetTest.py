# 3 folders up
import copy
import os

import numpy as np
import open3d as o3d
import torch

from RPMNet_Code.src.arguments import rpmnet_eval_arguments
from RPMNet_Code.src.common.torch import CheckPointManager
from RPMNet_Code.src.models.rpmnet import RPMNet, get_model

if __name__ == '__main__':
    parser = rpmnet_eval_arguments()
    _args = parser.parse_args()
    model = get_model(_args)
    saver = CheckPointManager(os.path.join("RPMNet_Code/logs", 'ckpt', 'models'))
    saver.load("RPMNet_Code/model-best.pth", model)
    model.cuda()
    model.eval()
    print("Model loaded")
    print("Trying model ...")
    # load two pcds using open3d
    pcd0 = o3d.io.read_point_cloud("datasets/my_room/03/pcd/37.pcd")
    pcd1 = o3d.io.read_point_cloud("datasets/my_room/03/pcd/30.pcd")
    org_pcd_src = copy.deepcopy(pcd1)
    org_pcd_ref = copy.deepcopy(pcd0)
    pcd0 = pcd0.farthest_point_down_sample(512)
    pcd1 = pcd1.farthest_point_down_sample(512)
    pcd0.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.2, max_nn=64))
    pcd1.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.2, max_nn=64))

    o3d.visualization.draw_geometries([org_pcd_src, org_pcd_ref],)
    # points
    p0 = torch.from_numpy(np.asarray(pcd0.points)).cuda().float()
    p1 = torch.from_numpy(np.asarray(pcd1.points)).cuda().float()

    # compute fps


    # normals
    n0 = torch.from_numpy(np.asarray(pcd0.normals)).cuda().float()
    n1 = torch.from_numpy(np.asarray(pcd1.normals)).cuda().float()
    # concatenate points and normals
    x0 = torch.cat([p0, n0], dim=1).unsqueeze(0)
    x1 = torch.cat([p1, n1], dim=1).unsqueeze(0)
    print(x0.shape, x1.shape)
    data = dict(points_src=x1, points_ref=x0)
    for max_iter in range(3):
        pred_transform, endpoints = model(data, num_iter=max_iter + 1)

        transformation = pred_transform[max_iter].squeeze(0).detach().cpu().numpy()
        # append last row of homegenous transformation
        transformation = np.vstack([transformation, np.array([0, 0, 0, 1])])
        print(transformation)
        # transform pcd1
        pcd_src_cp = copy.deepcopy(org_pcd_src).transform(transformation)
        o3d.visualization.draw_geometries([pcd_src_cp, org_pcd_ref], )
