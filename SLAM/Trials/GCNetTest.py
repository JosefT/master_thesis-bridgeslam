# 3 folders up
import copy

import numpy as np

from GCNet.demo import NgeNet_pipeline
from GCNet.utils import voxel_ds, get_yellow, get_blue, vis_plys
from Serialization import load_fragments
from src.RigidRegistration import simple_point_to_plane_icp

if __name__ == '__main__':
    # load fragment collector
    fragment_file = "Outputs/bridge_02/fragment_collector.pkl"
    fragment_collector = load_fragments(fragment_file)

    # load two pcds using open3d
    source = fragment_collector[1].map
    target = fragment_collector[21].map
    source_pt = np.asarray(source.points)
    target_pt = np.asarray(target.points)
    #o3d.visualization.draw_geometries([source, target], )

    # loading model
    voxel_size = 0.04
    model = NgeNet_pipeline(
        ckpt_path="model_weights/default_GCNet/3dmatch.pth",
        voxel_size=voxel_size,
        vote_flag=True,
        cuda=True)
    print("Model loaded")
    print("Trying model ...")
    #o3d.io.write_point_cloud("Outputs/temp/source.ply", source)
    #o3d.io.write_point_cloud("Outputs/temp/target.ply", target)
    #T = model.pipeline(source, target, npts=5000)

    T = np.array([[0.339405745268,  0.780619800091,  0.524820446968, - 1.522936463356],
                    [-0.868090689182,   0.474808186293,  - 0.144829913974,   1.671807289124],
                    [  -0.362246155739, - 0.406435638666,   0.838801383972, 0.504443526268],
                    [ 0.00000000,  0.00000000,  0.00000000,  1.00000000]])
    T = np.linalg.inv(T)

    T = simple_point_to_plane_icp(source, target, voxel_size=0.2, initial_transformation=T).transformation
    T = simple_point_to_plane_icp(source, target, voxel_size=0.1, initial_transformation=T).transformation
    T = simple_point_to_plane_icp(source, target, voxel_size=0.05, initial_transformation=T).transformation
    print('Estimated transformation matrix: ', repr(T))

    # voxelization for fluent visualization
    source = voxel_ds(source, voxel_size)
    target = voxel_ds(target, voxel_size)
    estimate = copy.deepcopy(source).transform(T)
    source.paint_uniform_color(get_yellow())
    source.estimate_normals()
    target.paint_uniform_color(get_blue())
    target.estimate_normals()
    vis_plys([source, target], need_color=False)

    estimate.paint_uniform_color(get_yellow())
    estimate.estimate_normals()
    vis_plys([estimate, target], need_color=False)