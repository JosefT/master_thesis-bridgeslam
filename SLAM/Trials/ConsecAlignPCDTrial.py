import os

import numpy as np
import open3d as o3d
import yaml

from Serialization import load_fragments, load_array
from transformation_utils import poses_from_transformations


class Frame:
    def __init__(self, color_path, depth_path, pcd_path, active=True):
        self.color_path = color_path
        self.depth_path = depth_path
        self.pcd_path = pcd_path
        self.active = active

def main():
    path_dataset = "../../Datasets/SLAM/bridge/02/"
    with open("../Configuration_Files/bridge_02.yaml", 'r') as file:
        config = yaml.safe_load(file)
    output_path = "Outputs/" + config["Name"] + "/"
    fragment_file = output_path + "fragment_collector.pkl"
    active_frame_file = output_path + "active_frames.npy"

    n_frames = len(os.listdir(path_dataset + "rgb"))
    frames = []
    for i in range(0, n_frames):
        frame = Frame(path_dataset + "rgb/{}.png".format(i), path_dataset + "depth/{}.png".format(i),
                      path_dataset + "pcd/{}.pcd".format(i))
        frames.append(frame)

    fragment_collector = load_fragments(fragment_file)
    active_frames = load_array(active_frame_file)
    for idx, frame in enumerate(frames):
        frame.active = active_frames[idx]
    del active_frames

    consecutive_tans_file = output_path + "transformations.npy"
    transformations = load_array(consecutive_tans_file)

    pcds = []
    k = 0
    i = 0

    poses = poses_from_transformations(transformations)
    print(poses)
    for fragment in fragment_collector:
        for frame_idx in range(k, len(fragment.frames)+k):
            pcd = o3d.io.read_point_cloud(frames[frame_idx].pcd_path)
            pcds.append(pcd.transform(poses[i] @ np.linalg.inv(fragment.extrinsics[frame_idx-k])))
        print(len(pcds))
        o3d.visualization.draw_geometries(pcds)
        k += len(fragment.frames)


        i = i + 1
if __name__ == "__main__":
    main()
