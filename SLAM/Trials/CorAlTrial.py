import cProfile
import copy
import math

import numpy as np
import open3d as o3d
import cv2
import transforms3d

from RigidRegistration import simple_point_to_point_icp
from Serialization import load_fragments, load_array
from numba import jit
from VisualizationUtils import draw_registration_result
from src.metrics import NDT_score, get_normal_similarity
from transformation_utils import R_to_T
@jit(nopython=True)
def determinant_3x3(matrix):
    a, b, c = matrix[0]
    d, e, f = matrix[1]
    g, h, i = matrix[2]

    return a*(e*i - f*h) - b*(d*i - f*g) + c*(d*h - e*g)

@jit(nopython=True)
def ComputeInformation(points, covariances):
    num_points = len(points)
    if num_points == 0:
        return 0.
    const = 2 * math.pi * math.e
    entropy = np.empty(num_points)

    for i in range(num_points):
        covariance = covariances[i]
        determinant = determinant_3x3(covariance)
        if determinant == 0 or np.isnan(determinant):
            entropy[i] = 0.
        h_i = 0.5 * math.log(const*determinant+0.0000001)
        #if not np.isnan(h_i) and not np.isinf(h_i):
        entropy[i] = h_i
    total_entropy = np.sum(entropy)
    return total_entropy

if __name__ == "__main__":
    np.set_printoptions(precision=4, suppress=True, threshold=np.inf)
    fragment_file = "../Outputs/bridge_02/fragment_collector.pkl"
    fragment_collector = load_fragments(fragment_file)
    transformations = load_array("../Outputs/bridge_02/transformations.npy")

    profiler = cProfile.Profile()
    profiler.enable()
    scores = []
    print("Good: ")
    for i in range(1, len(fragment_collector[:10])):
        # R = transforms3d.euler.euler2mat(0, 45, 0)
        # T_R = R_to_T(R)
        source = copy.deepcopy(fragment_collector[i - 1].map)
        target = fragment_collector[i].map
        source = source.transform(transformations[i - 1])
        source.estimate_covariances()
        target.estimate_covariances()
        # calculate correspondences within aligned point clouds
        knn_search_param = o3d.geometry.KDTreeSearchParamKNN(1)
        kdtree = o3d.geometry.KDTreeFlann(target)
        corrs = []
        src_pts = np.asarray(source.points)
        for k in range(len(src_pts)):
            _, corr_idx, dist = kdtree.search_knn_vector_3d(src_pts[k], 1)
            if dist[0] < 0.06:
                corrs.append([k, corr_idx[0]])
        corrs_src = np.asarray(corrs)[:, 0]
        corrs_tgt = np.unique(np.asarray(corrs)[:, 1])

        src_corr_pcd = source.select_by_index(corrs_src)
        tgt_corr_pcd = target.select_by_index(corrs_tgt)
        H_src = ComputeInformation(np.asarray(src_corr_pcd.points), np.asarray(src_corr_pcd.covariances))
        H_tgt = ComputeInformation(np.asarray(tgt_corr_pcd.points), np.asarray(tgt_corr_pcd.covariances))
        H_sep = (H_src + H_tgt) / (len(src_corr_pcd.points) + len(src_corr_pcd.points))

        pcd_joint = source + target
        H_joint = ComputeInformation(np.asarray(pcd_joint.points), np.asarray(pcd_joint.covariances)) / len(pcd_joint.points)
        score = H_joint - H_sep
        p = 1 / (1 - math.exp(-score))
        print(p)
    print("Bad: ")
    for i in range(1, len(fragment_collector[:10])):
        # R = transforms3d.euler.euler2mat(0, 45, 0)
        # T_R = R_to_T(R)
        source = copy.deepcopy(fragment_collector[i - 1].map)
        target = fragment_collector[i].map
        
        source = source.transform(transformations[i - 1])
        source.estimate_covariances()
        target.estimate_covariances()
        # calculate correspondences within aligned point clouds
        knn_search_param = o3d.geometry.KDTreeSearchParamKNN(1)
        kdtree = o3d.geometry.KDTreeFlann(target)
        corrs = []
        src_pts = np.asarray(source.points)
        for k in range(len(src_pts)):
            _, corr_idx, dist = kdtree.search_knn_vector_3d(src_pts[k], 1)
            if dist[0] < 0.06:
                corrs.append([k, corr_idx[0]])
        corrs_src = np.asarray(corrs)[:, 0]
        corrs_tgt = np.unique(np.asarray(corrs)[:, 1])

        src_corr_pcd = source.select_by_index(corrs_src)
        tgt_corr_pcd = target.select_by_index(corrs_tgt)
        H_src = ComputeInformation(np.asarray(src_corr_pcd.points), np.asarray(src_corr_pcd.covariances))
        H_tgt = ComputeInformation(np.asarray(tgt_corr_pcd.points), np.asarray(tgt_corr_pcd.covariances))
        H_sep = (H_src + H_tgt) / (len(src_corr_pcd.points) + len(src_corr_pcd.points))

        pcd_joint = source + target # TODO False?
        H_joint = ComputeInformation(np.asarray(pcd_joint.points), np.asarray(pcd_joint.covariances)) / len(
            pcd_joint.points)
        z = 0 + 1 * H_joint - H_sep
        p = 1 / (1 + np.exp(-z))

        print(p)
        #profiler.disable()
        #profiler.print_stats(sort="cumulative")

