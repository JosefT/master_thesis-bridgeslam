import numpy as np
import open3d as o3d
from scipy.spatial.distance import cdist

def bilateral_filter(point_cloud, sigma_spatial, sigma_range):
    filtered_cloud = np.zeros_like(point_cloud)
    # Compute spatial kernel weights
    spatial_distances = cdist(point_cloud[:, :3], point_cloud[:, :3])
    spatial_weights = np.exp(-spatial_distances / (2 * sigma_spatial**2))
    # Normalize spatial weights
    spatial_weights /= np.sum(spatial_weights, axis=1)[:, None]

    for i in range(point_cloud.shape[0]):
        # Compute range kernel weights
        range_distances = np.linalg.norm(point_cloud[i, 3:] - point_cloud[:, 3:], axis=1)
        range_weights = np.exp(-range_distances / (2 * sigma_range ** 2))

        # Normalize range weights
        range_weights /= np.sum(range_weights)

        # Compute combined weights
        weights = spatial_weights[i] * range_weights

        # Compute filtered point
        filtered_point = np.sum(weights[:, None] * point_cloud[:, :3], axis=0)

        filtered_cloud[i, :3] = filtered_point
        filtered_cloud[i, 3:] = point_cloud[i, 3:]  # Preserve additional attributes

    return filtered_cloud

def main():
        # load o3d pointcloud
        pcd = o3d.io.read_point_cloud("../temp/noisy.ply")
        pcd = pcd.voxel_down_sample(voxel_size=0.03)
        for i in range(1):
            filtered_cloud_pts = bilateral_filter(np.concatenate((np.asarray(pcd.points), np.asarray(pcd.colors)), axis=1), sigma_spatial=0.1, sigma_range=10)
            filtered_cloud = o3d.geometry.PointCloud()
            filtered_cloud.points = o3d.utility.Vector3dVector(filtered_cloud_pts[:, :3])
            filtered_cloud.colors = pcd.colors
            pcd = filtered_cloud

        print(filtered_cloud)
        # print min and max coordinate of pcd

        o3d.visualization.draw_geometries([filtered_cloud])


if __name__ == "__main__":
    main()