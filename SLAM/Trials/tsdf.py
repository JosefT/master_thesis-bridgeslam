import time

import numpy as np
import open3d as o3d
import open3d.core as o3c
import cv2
from tqdm import tqdm

from src.DataLoadingMethods import load_pcds_generic


def main():
    path = "../datasets/my_room/04/"
    color_file_names = []
    depth_file_names = []

    device = o3d.core.Device("cuda:0")
    for i in range(32, 52):
        color_file_names.append(path + "rgb/{}.png".format(i))
        depth_file_names.append(path + "depth/{}.png".format(i))
    n_files = len(depth_file_names)
    extrinsics = [np.array([[1., 0., 0., 0.],
       [0., 1., 0., 0.],
       [0., 0., 1., 0.],
       [0., 0., 0., 1.]]), np.array([[ 1.    ,  0.0019, -0.0025,  0.003 ],
       [-0.0019,  0.9998, -0.0181, -0.0075],
       [ 0.0025,  0.0181,  0.9998, -0.0041],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 1.    ,  0.0062, -0.0013,  0.0023],
       [-0.0062,  0.9996, -0.0258, -0.0083],
       [ 0.0011,  0.0258,  0.9997, -0.0058],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 1.    ,  0.0018, -0.0014,  0.0016],
       [-0.0018,  0.9997, -0.025 , -0.006 ],
       [ 0.0013,  0.025 ,  0.9997, -0.0045],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 1.    , -0.0086,  0.0018,  0.0007],
       [ 0.0086,  0.9998, -0.0195, -0.0048],
       [-0.0016,  0.0195,  0.9998, -0.0039],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9998, -0.0139,  0.0118,  0.0054],
       [ 0.0142,  0.9996, -0.0258, -0.0137],
       [-0.0115,  0.0259,  0.9996, -0.0077],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9995, -0.0268,  0.0172,  0.0074],
       [ 0.0271,  0.9994, -0.0195, -0.001 ],
       [-0.0166,  0.0199,  0.9997, -0.0118],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9994, -0.0226,  0.0252,  0.0193],
       [ 0.0233,  0.9994, -0.027 , -0.0083],
       [-0.0245,  0.0276,  0.9993, -0.0184],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9995, -0.0203,  0.0248,  0.0241],
       [ 0.0211,  0.9992, -0.0346, -0.0151],
       [-0.0241,  0.0351,  0.9991, -0.0241],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9997, -0.0113,  0.0232,  0.0282],
       [ 0.0122,  0.9991, -0.0396, -0.0198],
       [-0.0227,  0.0399,  0.9989, -0.0229],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9998, -0.0074,  0.0163,  0.0239],
       [ 0.0081,  0.999 , -0.0431, -0.0165],
       [-0.0159,  0.0432,  0.9989, -0.0283],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9999, -0.0018,  0.0105,  0.0185],
       [ 0.0024,  0.9982, -0.0592, -0.0395],
       [-0.0103,  0.0593,  0.9982, -0.0323],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 1.    , -0.0038,  0.0077,  0.0188],
       [ 0.0042,  0.9984, -0.0572, -0.038 ],
       [-0.0075,  0.0572,  0.9983, -0.0416],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9999, -0.004 ,  0.0118,  0.025 ],
       [ 0.0045,  0.999 , -0.0438, -0.0176],
       [-0.0117,  0.0439,  0.999 , -0.0401],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9999, -0.0092,  0.0129,  0.0306],
       [ 0.0099,  0.9983, -0.0574, -0.035 ],
       [-0.0124,  0.0576,  0.9983, -0.0468],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9999, -0.0106,  0.0078,  0.0279],
       [ 0.011 ,  0.9987, -0.0491, -0.0146],
       [-0.0073,  0.0492,  0.9988, -0.0488],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9999, -0.0107,  0.0013,  0.0262],
       [ 0.0108,  0.9979, -0.0637, -0.0289],
       [-0.0006,  0.0637,  0.998 , -0.0535],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9999, -0.0134,  0.0021,  0.0305],
       [ 0.0136,  0.997 , -0.0761, -0.0398],
       [-0.0011,  0.0761,  0.9971, -0.054 ],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9999, -0.0148,  0.0056,  0.0363],
       [ 0.0151,  0.9973, -0.0716, -0.0266],
       [-0.0045,  0.0717,  0.9974, -0.056 ],
       [ 0.    ,  0.    ,  0.    ,  1.    ]]), np.array([[ 0.9998, -0.0175,  0.0092,  0.039 ],
       [ 0.0182,  0.9967, -0.0795, -0.034 ],
       [-0.0078,  0.0797,  0.9968, -0.0581],
       [ 0.    ,  0.    ,  0.    ,  1.    ]])]

    vbg = o3d.t.geometry.VoxelBlockGrid(
        attr_names=('tsdf', 'weight', 'color'),
        attr_dtypes=(o3c.float32, o3c.float32, o3c.float32),
        attr_channels=((1), (1), (3)),
        voxel_size=4.0 / 512,
        block_resolution=16,
        block_count=50000,
        device=device)

    start = time.time()
    depth_scale = 1000.0
    depth_max = 15.0
    intrinsic = o3d.camera.PinholeCameraIntrinsic(width=1280, height=720, fx=911.7221069335938, fy=911.2312622070312,
                                                  cx=651.0254516601562, cy=361.3611145019531).intrinsic_matrix
    print(intrinsic)
    intrinsic = o3c.Tensor(np.array(intrinsic))

    for i in tqdm(range(n_files)):
        depth = o3d.t.io.read_image(depth_file_names[i]).to(device)

        extrinsic = np.eye(4)

        frustum_block_coords = vbg.compute_unique_block_coordinates(
            depth, intrinsic, extrinsic, depth_scale,
            depth_max)

        color = o3d.t.io.read_image(color_file_names[i]).to(device)
        vbg.integrate(frustum_block_coords, depth, color, intrinsic,
                      intrinsic, extrinsic, depth_scale,
                      depth_max)
        dt = time.time() - start
    print('Finished integrating {} frames in {} seconds'.format(n_files, dt))

    return vbg
    #    color = o3d.t.io.read_image(color_file_names[i]).to(device)


if __name__ == "__main__":
    vbg = main()
    pcd = vbg.extract_point_cloud()
    pcd = pcd.to_legacy()
    pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.2, max_nn=60))
    o3d.visualization.draw_geometries([pcd])
    mesh = vbg.extract_triangle_mesh()
    example_pcd = o3d.io.read_point_cloud("../datasets/my_room/04/pcd/0.pcd")
    o3d.visualization.draw([mesh.to_legacy()])
