import cv2
import numpy as np

from src.VisualizationUtils import cv2_show
from src.DataLoadingMethods import load_rgbs_generic
from scipy.signal import convolve2d as conv2
import matplotlib.pyplot as plt
from skimage import color, data, restoration

def is_blurry(rgb):
    image = rgb
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    edge_img = cv2.Laplacian(gray, cv2.CV_32F)
    print(edge_img)
    cv2.imshow("variance", edge_img)
    variance = edge_img.var()
    cv2.waitKey(0)
    threshold = 140  # Adjust this threshold according to your needs
    print(variance)
    return variance < threshold

def deblur_image(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)  # Convert image to RGB color space

    # Perform deblurring using Richardson-Lucy algorithm
    psf = np.ones((5, 5)) / 25.0  # Define the point spread function
    deblurred = cv2.filter2D(image, -1, psf)

    return deblurred

def RGB2RGBA(rgb, fill_value=1):
    "Add alpha channl to RGB image"
    if rgb.shape[-1] >= 4:
        return rgb
    rgb2 = np.full(shape=(*rgb.shape[:-1], 4), fill_value=fill_value, dtype=rgb.dtype)
    rgb2[..., :-1] = rgb / 255.0
    return rgb2

def RGB_convolve(image, kernel):
    image_2 = np.copy(image)
    for dim in range(3):
        image_2[:, :, dim] = conv2(image[:, :, dim], kernel, 'same', 'symm')
    return image_2
if __name__ == '__main__':
    path = "../../Datasets/SLAM/bridge/01/"
    amount = 1700
    rgbs = load_rgbs_generic(path, amount)
    alpha = 0.5
    is_blurry(rgbs[1671])
    is_blurry(rgbs[1679])
    """
    for idx, rgb in enumerate(rgbs):
        if is_blurry(rgb):
            continue
            rgb = cv2.cvtColor(rgb, cv2.COLOR_BGR2RGB)
            rgb = RGB2RGBA(rgb.astype(np.float32))
            kernel_edge_detection = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
            kernel_sharpen = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])

            #rgb_edges = RGB_convolve(rgb, kernel_edge_detection)

            rgb_sharpened = RGB_convolve(rgb, kernel_sharpen)
            rgb_sharpened = np.clip(rgb_sharpened, 0, 1)

            plt.rcParams["figure.figsize"] = [16,9]
            fig, (axL, axR) = plt.subplots(ncols=2, tight_layout=True)
            fig.suptitle('Edge Detection and Sharpening')

            axL.imshow(rgb)
            axL.axis('off')
            axR.imshow(rgb_sharpened)
            axR.axis('off')
            plt.show()
    """