import glob
import cv2

from src.DataLoadingMethods import load_rgbs_generic
import numpy as np
def equalize_exposure(images):
    # Step 1: Compute the reference histogram from the first image
    equalized_imgs = []
    for img in images:
        img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
        img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])
        # convert the YUV image back to RGB format
        img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

        equalized_imgs.append(img_output)
    return equalized_imgs
    """
    eference_image = images[0]
    reference_hists = []
    for i in range(3):
        hist = cv2.calcHist([reference_image], [i], None, [256], [0, 256])
        cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX)
        reference_hists.append(hist)

    # Step 2: Apply histogram equalization to all images using the reference histograms
    equalized_images = []
    for image in images:
        equalized_image = np.zeros_like(image)
        for i in range(3):
            # Compute the histogram of the current channel
            hist = cv2.calcHist([image], [i], None, [256], [0, 256])
            cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX)

            # Compute the cumulative distribution function (CDF)
            cdf = hist.cumsum()
            cdf_normalized = cdf * 255 / cdf[-1]  # Normalize CDF to 0-255

            # Map the intensities of the channel to the normalized CDF values
            equalized_channel = np.interp(image[:, :, i].flatten(), np.arange(256), cdf_normalized.flatten()).reshape(image[:, :, i].shape)
            equalized_image[:, :, i] = equalized_channel.astype(np.uint8)

        equalized_images.append(equalized_image)
    return equalized_images
    """

def main2():
    rgbs = load_rgbs_generic("datasets/my_room/03/", 100)
    equalized_images = equalize_exposure(rgbs)
    for i in range(len(equalized_images)):
        numpy_horizontal_concat = np.concatenate((rgbs[i], equalized_images[i]), axis=1)
        cv2.imshow("RGB", numpy_horizontal_concat)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
def main():
    rgbs = load_rgbs_generic("datasets/my_room/03/", 200)
    gamma = 2.0
    for rgb in rgbs:
        normalized_image = rgb / 255.0
        # Apply gamma correction to each channel
        corrected_image = np.power(normalized_image, 1.0 / gamma)
        scaled_image = (corrected_image * 255).astype(np.uint8)


        # merge rgb and scaled_image
        numpy_horizontal_concat = np.concatenate((rgb, scaled_image), axis=1)

        cv2.imshow("RGB", numpy_horizontal_concat)
        cv2.waitKey(0)
        cv2.destroyAllWindows()





if __name__ == '__main__':
    main2()