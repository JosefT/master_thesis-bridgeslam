import random

import faiss
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import distance
from sklearn.metrics.pairwise import cosine_distances, cosine_similarity
from tqdm import tqdm

from src.DataLoadingMethods import load_rgbs_generic
import cv2

from src.General import measure_time
from src.LoopClosing import LoopDetector, load_dictionary_my_room, build_annoy_trees
from sklearn.feature_extraction.text import TfidfTransformer


def get_img_descriptor(rgb):
    sift = cv2.ORB_create()
    _, des = sift.detectAndCompute(rgb, None)
    # normalize
    des = np.asarray([desc / np.linalg.norm(desc) for desc in des])
    return des


@measure_time
def generate_VLAD(rgbs, num_words=512):
    descriptors = []
    print("Get image descriptors")
    for rgb in tqdm(rgbs):
        descriptors.append(get_img_descriptor(rgb))
    # flatten descriptors and compute mask
    desc_numbers = [len(desc) for desc in descriptors]
    # append a zero to the beginning of the list
    desc_numbers.insert(0, 0)
    desc_ptr = np.cumsum(desc_numbers, axis=0)

    print(f"Kmeans using the descriptors of {len(descriptors)} images for clustering.")
    descriptor__arr = np.concatenate([np.asarray(desc) for desc in descriptors], axis=0, dtype=np.float32)
    desc_dim = descriptor__arr.shape[1]




    kmeans = faiss.Kmeans(d=descriptor__arr.shape[-1], k=num_words, niter=20, verbose=True, gpu=False)
    kmeans.train(descriptor__arr)
    centroids = kmeans.centroids
    # generate bows from labels
    print("Generate bows")


    residuals = np.zeros((len(rgbs), num_words, desc_dim))
    for i in range(len(rgbs)):
        local_descriptors = descriptors[i]
        D, V = kmeans.index.search(local_descriptors, 1)
        V = V.squeeze(1)
        for desc_idx in range(len(local_descriptors)):
            vote = V[desc_idx]
            residuals[i][vote] += local_descriptors[desc_idx] - centroids[vote]

    return residuals


def similarity_search(img_vectors):
    pairwise_distances = cosine_similarity(img_vectors)
    # Compute pairs based on similarity and distance of the indices
    pairs = []
    num_images = len(pairwise_distances)
    for i in range(len(pairwise_distances)):
        for j in range(i + 1, len(pairwise_distances)):
            pairs.append((i, j, pairwise_distances[i][j]))
    return pairs

def null_safe_cosine_similarity(a, b):
    a_norm = np.linalg.norm(a, axis=1)
    b_norm = np.linalg.norm(b, axis=1)
    norms = np.multiply(a_norm, b_norm)

    nonzero_norms = norms != 0
    cosine_sim = np.zeros((a.shape[0]))
    cosine_sim[nonzero_norms] = np.einsum('ij,ij->i', a[nonzero_norms], b[nonzero_norms]) / norms[nonzero_norms]
    return cosine_sim

@measure_time
def similarity_search_vlad(img_vectors):
    num_vectors = len(img_vectors)
    pairwise_distances = np.zeros((num_vectors, num_vectors))
    vector_length = len(img_vectors[0])
    for i in range(num_vectors):
        a = img_vectors[i]
        for j in range(i + 1, num_vectors):
            b = img_vectors[j]
            pairwise_distances[i][j] = np.sum(null_safe_cosine_similarity(a, b)) / vector_length

    pairs = [(i, j, pairwise_distances[i][j]) for i in range(len(pairwise_distances)) for j in
             range(i + 1, len(pairwise_distances))]
    return pairs


def viz_bow(bow0, bow1):
    fig, axs = plt.subplots(2)
    fig.suptitle('Vertically stacked subplots')
    axs[0].plot(bow0)
    axs[1].plot(bow1)
    plt.show()


def filter_and_sort_pairs(pairs, num_images=500):
    #pairs.sort(key=lambda x: x[2] * 0.5 + 0.5 * (np.abs(x[0]-x[1]) / num_images), reverse=True)
    pairs.sort(key=lambda x: x[2], reverse=True)
    # filter all pairs where x[0] and x[1] are too close
    pairs = [pair for pair in pairs if np.abs(pair[0] - pair[1]) > 40]
    return pairs

def visualize_pairs(pairs, rgbs):
    # visualize pairs
    already_shown = set()
    for i, j, sim in pairs:
        # check whether pair was already shown in an interval of 10
        if any([abs(i - x) < 20 and abs(j - y) < 20 for x, y in already_shown]):
            continue
        already_shown.add((i, j))
        rgb0 = rgbs[i]
        rgb1 = rgbs[j]

        # concatenate rgbs to visualize them next to each other
        rgb = np.concatenate((rgb0, rgb1), axis=1)
        cv2.imshow(f"Frame {i} and {j} with similarity: {sim}", rgb)

        cv2.waitKey(0)
        cv2.destroyAllWindows()


def main():
    # load rgbs
    path = "datasets/my_room/04/"
    amount = 500
    rgbs = load_rgbs_generic(path, amount)

    bows = generate_VLAD(rgbs)

    print("Compute cosine similarities")
    pairs = similarity_search_vlad(bows)

    pairs = filter_and_sort_pairs(pairs, amount)

    visualize_pairs(pairs, rgbs)


if __name__ == '__main__':
    main()
