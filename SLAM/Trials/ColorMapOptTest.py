import copy
import os

import open3d as o3d

from Serialization import load_array


def main():
    pass


if __name__ == "__main__":
    debug_mode = False
    path = "datasets/my_room/04/"
    depth_image_path = []
    color_image_path = []
    rgbd_images = []
    pcd_file_names = []
    intrinsic = o3d.camera.PinholeCameraIntrinsic(width=1080, height=720, fx=911.7221069335938, fy=911.2312622070312, cx=651.0254516601562, cy=361.3611145019531)
    for i in range(0, 500):
        depth_image_path.append(path + "depth/{}.png".format(i))
        color_image_path.append(path + "rgb/{}.png".format(i))
    assert (len(depth_image_path) == len(color_image_path))
    for i in range(len(depth_image_path)):
        depth = o3d.io.read_image(os.path.join(depth_image_path[i]))
        color = o3d.io.read_image(os.path.join(color_image_path[i]))
        rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(
            color, depth, convert_rgb_to_intensity=False, depth_scale=1000.0)
        if debug_mode:
            pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
                rgbd_image,intrinsic)
            o3d.visualization.draw_geometries([pcd])
        rgbd_images.append(rgbd_image)
    rgbd_images = rgbd_images[::5]
    # load trajectory
    trajectory = load_array("Outputs/trajectory.npy")
    cam_params = []
    for pose in trajectory:
        cam_param = o3d.camera.PinholeCameraParameters()
        cam_param.extrinsic = pose
        cam_param.intrinsic = intrinsic
        cam_params.append(cam_param)
    o3d_trajectory = o3d.camera.PinholeCameraTrajectory()
    o3d_trajectory.parameters = cam_params[::5]
    mesh = o3d.io.read_triangle_mesh("Outputs/map_2023-06-26_20-44-35.ply")
    mesh_old = copy.deepcopy(mesh)

    # Before full optimization, let's just visualize texture map
    # with given geometry, RGBD images, and camera poses.
    option = o3d.pipelines.color_map.NonRigidOptimizerOption(maximum_iteration=30, maximum_allowable_depth=3)
    with o3d.utility.VerbosityContextManager(
            o3d.utility.VerbosityLevel.Debug) as cm:
        mesh, camera = o3d.pipelines.color_map.run_non_rigid_optimizer(mesh, rgbd_images, o3d_trajectory,
                                                       option)
        print(camera)
    print(mesh)
    o3d.visualization.draw_geometries([mesh])
