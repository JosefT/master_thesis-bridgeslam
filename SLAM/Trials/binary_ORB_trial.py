import copy
import os
import pickle
from concurrent.futures import ThreadPoolExecutor
from itertools import chain
import faiss
import cv2
import numpy as np
import open3d as o3d
import pytransform3d.visualizer as pv
import yaml
from cython import parallel
from pyclustering.cluster import cluster_visualizer
from pyclustering.cluster.kmedians import kmedians
from pyclustering.utils import type_metric, distance_metric
from pytransform3d.rotations import quaternion_from_matrix
from sklearn.cluster import AgglomerativeClustering
from tqdm import tqdm

from Serialization import load_array, load_fragments
from src.metrics import sliced_wasserstein_distance
from src.Loop_Closure_Pipeline import get_fragment_candidates, evaluate_bluriness, \
    get_candidates_using_Feature_BoW_approach, \
    reduce_candidates_to_one_per_fragment_pair, overlap_assuming_chamfer_distance
from Loop_Detector_BoW import LoopDetectorBoW
from src.transformation_utils import t_to_T, poses_from_transformations, transform_vector


def get_frames(fragment_collector):
    frames = []
    for fragment in fragment_collector:
        for frame in fragment.frames:
            frames.append(frame)
    return frames


def get_lcs_as_lines(lc_candidates, ts):
    line_indices = []
    colors = []
    def_color = [0., 0, 1.]
    for candidate in lc_candidates:
        start = candidate[0]
        end = candidate[1]
        line_indices.append([start, end])

        colors.append(def_color)

    lineSet = o3d.geometry.LineSet()
    lineSet.points = o3d.utility.Vector3dVector(ts)
    lineSet.lines = o3d.utility.Vector2iVector(line_indices)
    lineSet.colors = o3d.utility.Vector3dVector(colors)
    return lineSet


def get_ORB_Descriptors(frames):
    print("Extract Descriptors")
    orb = cv2.ORB_create()
    descriptors = []
    for frame in tqdm(frames):
        img = np.asarray(cv2.imread(frame.color_path))
        _, des = orb.detectAndCompute(img, None)
        descriptors.append(des)

    return descriptors

class TreeNode:
    def __init__(self, descriptor, depth, count=0):
        self.descriptor = descriptor
        self.children = []
        self.count = count
        self.depth = depth

    def add_child(self, child_node):
        self.children.append(child_node)

    def is_leaf(self):
        return False

    def traverse_tree(self, query):
        optimal_hamming_distance = 256
        optimal_child = None
        for child in self.children:
            hamming_distance = manhattanForBinary(child.descriptor, query)
            if hamming_distance < optimal_hamming_distance:
                optimal_hamming_distance = hamming_distance
                optimal_child = child
        return optimal_child.traverse_tree(query)

class TreeLeaf(TreeNode):
    def __init__(self, descriptor, depth, count=0, word_id=0):
        super().__init__(descriptor, depth, count)
        self.word_id = word_id
    def traverse_tree(self, query):
        return self.word_id

    def is_leaf(self):
        return True





def consecuitive_transformations_to_trajectory(transformations):
    ts = []
    for i in range(len(transformations) + 1):
        if i == 0:
            ts.append(np.eye(4))
        else:
            ts.append(transformations[i - 1] @ ts[i - 1])
    # for i in range(len(ts)):
    #    ts[i] = np.linalg.inv(ts[i])
    return ts


def get_loop_candidates_from_trajectory(fragment_collector, transformations):
    trajectory = poses_from_transformations(transformations)
    lc_candidates = []
    dist_val = 0.3
    points = []
    for f_idx_0 in range(0, len(fragment_collector)):
        center_0 = fragment_collector[f_idx_0].map.get_center()
        center_0_transf = (trajectory[f_idx_0] @ t_to_T(center_0))[:3, 3]

        points.append(center_0_transf)
        for f_idx_1 in range(f_idx_0 + 1, len(fragment_collector)):
            # calculate map _centers
            center_1 = fragment_collector[f_idx_1].map.get_center()

            center_1_transf = (trajectory[f_idx_1] @ t_to_T(center_1))[:3, 3]

            distance = np.linalg.norm(center_0_transf - center_1_transf)
            threshold = dist_val * 1.06 ** (f_idx_1 - f_idx_0)
            if distance < threshold:
                lc_candidates.append((f_idx_0, f_idx_1, distance, threshold))
    return lc_candidates, points


def fragment_idx_to_frame_idx(lc_candidates, fragment_collector):
    new_candidates = []
    for candidate in lc_candidates:
        median_frame_idx_0 = fragment_collector[candidate[0]].start_idx + (
                fragment_collector[candidate[0]].end_idx - fragment_collector[candidate[0]].start_idx) // 2
        median_frame_idx_1 = fragment_collector[candidate[1]].start_idx + (
                fragment_collector[candidate[1]].end_idx - fragment_collector[candidate[1]].start_idx) // 2
        new_candidates.append((median_frame_idx_0, median_frame_idx_1, *candidate[2:]))
    return new_candidates


def visualize_trajectory_and_LCs(trajectory, frame_lc_candidates=None, points=None, fragment_candidates=None, fragments=None, poses=None):
    fig = pv.figure()
    trajectory = [np.linalg.inv(pose) for pose in trajectory]
    rots = [pose[:3, :3] for pose in trajectory]
    rots_q = np.array([quaternion_from_matrix(rot) for rot in rots])

    ts = [pose[:3, 3] for pose in trajectory]
    trajectory_q = np.concatenate([ts, rots_q], axis=1)
    if frame_lc_candidates is not None:
        lc_lineSet = get_lcs_as_lines(frame_lc_candidates, ts)
        fig.add_geometry(lc_lineSet)
    fig.plot_trajectory(P=trajectory_q, n_frames=0, s=0.35, c=[1, 0, 0], )
    if points is not None:
        colors = np.empty((len(points), 3))
        for d in range(colors.shape[1]):
            colors[:, d] = np.linspace(0, 1, len(colors))
        fig.scatter(P=points, s=0.05, c=colors)
        if fragment_candidates is not None:
            lc_lineSet_frag = get_lcs_as_lines(fragment_candidates, points)
            fig.add_geometry(lc_lineSet_frag)
    if fragments is not None and poses is not None:
        for f_idx, fragment in enumerate(fragments):
            map = copy.deepcopy(fragment.map)
            map.transform(poses[f_idx])

            # remove all clusters except biggest one
            with o3d.utility.VerbosityContextManager(o3d.utility.VerbosityLevel.Debug) as cm:
                labels = np.array(
                    map.cluster_dbscan(eps=0.05, min_points=1, print_progress=True))
            counts = np.bincount(labels)
            biggest_cluster = np.argmax(np.bincount(labels))
            mask = np.zeros(len(labels), dtype=bool)
            mask[labels == biggest_cluster] = True
            map = map.select_by_index(np.where(mask)[0])
            fig.add_geometry(map)
    fig.show()


def calculate_acc_distances(trajectory):
    distances = np.zeros(len(trajectory))
    for i in range(1, len(trajectory)):
        distances[i] = distances[i - 1] + np.linalg.norm(trajectory[i][:3, 3] - trajectory[i - 1][:3, 3])
    return distances




def sub_sample_pcd(pcd, n_remaining_points):
    if len(pcd.points) < n_remaining_points:
        return pcd
    step = len(pcd.points) // n_remaining_points
    return pcd.uniform_down_sample(step)

def estimate_epsilon(fragments, poses):
    distances = []
    for i in range(len(fragments) - 1):
        map0 = fragments[i].map
        map1 = fragments[i + 1].map
        map0_down = sub_sample_pcd(map0, 1000)
        map1_down = sub_sample_pcd(map1, 1000)
        map0_down.transform(poses[i])
        map1_down.transform(poses[i + 1])

        #  # calculate chamfer distance
        distance = overlap_assuming_chamfer_distance(map0_down, map1_down)
        distances.append(distance)
    return np.mean(distances)

def reduce_lc_by_odometry_validity_with_sampling(lc_candidates, fragment_collector, poses, sigma=0.1):
    distances_trajectory = calculate_acc_distances(poses)

    t_errs = []
    epsilon = estimate_epsilon(fragment_collector, poses)
    print("Test Odometry Validity")
    for lc_candidate in tqdm(lc_candidates):
        map0 = fragment_collector[lc_candidate[0]].map
        map1 = fragment_collector[lc_candidate[1]].map
        map0_down = sub_sample_pcd(map0, 1000)
        map1_down = sub_sample_pcd(map1, 1000)
        map0_down.transform(poses[lc_candidate[0]])
        map1_down.transform(poses[lc_candidate[1]])

        #  # calculate chamfer distance
        distance = overlap_assuming_chamfer_distance(map0_down, map1_down)

        t_err = max(distance - epsilon, 0) / (distances_trajectory[lc_candidate[1]] - distances_trajectory[lc_candidate[0]])
        t_errs.append(t_err)
    d_odom = np.exp(-1 * np.array(t_errs) ** 2 / (2 * sigma ** 2))
    lc_fragment_candidates = [lc_candidates[i] for i in range(len(lc_candidates)) if d_odom[i] > 0.8]

    return lc_fragment_candidates


def get_binary_descriptors(descriptors):
    binary_descriptors = []
    for descriptor in descriptors:
        binary_array =  np.zeros(256, dtype=bool)
        for i in range(8):
            binary_array[i::8] = (descriptor & (1 << (7 - i))) > 0

        binary_descriptors.append(binary_array)
    print("Converted to binary descriptors")
    #convert to array
    binary_descriptors = np.array(binary_descriptors)
    return binary_descriptors

def l1norm(x,y):
    # check if x is 2d array
    if len(x.shape) == 2:
        distances = np.zeros(x.shape[0])
        for i in range(x.shape[0]):
            a = x[i] ^ y
            sum_result = np.sum(a)
            distances[i] = sum_result
        return distances
    #return np.sum(np.abs(x - y), axis=1)
    else:
        return np.sum(np.abs(x - y))

def manhattanForBinary(x,y):
    return np.sum(x ^ y)

def kmeans_plusplus(x, k=10):
    # get kmeans++ centers
    centers = []
    centers.append(x[np.random.randint(0, len(x))])
    distances = []
    for i in range(1, k):
        distances.append(l1norm(x, centers[-1]) ** 2)
        # calculate probabilities
        min_distances = np.min(distances, axis=0)
        probabilities = min_distances / np.sum(min_distances)

        new_center = x[np.random.choice(np.arange(len(x)), p=probabilities)]
        centers.append(new_center)
    return centers

def perform_kmedoids(x, k=10, n_samples=1000):
    if len(x) < n_samples:
        samples = x
    else:
        sample_indices = np.random.randint(0, len(x), n_samples)
        samples = x[sample_indices]
    initial_centers = kmeans_plusplus(samples, k=k)
    kmedians_instance = kmedians(samples, initial_centers, tolerance= 4.1)
    kmedians_instance.process()
    medians_floored = np.floor(kmedians_instance.get_medians()).astype(bool)
    return medians_floored

def get_assignments(x, centers):
    # use faiss for faster assignment
    index = faiss.IndexFlatL2(256)
    index.add(centers)
    distances, assignments = index.search(x, 1)
    assignments = assignments.reshape(-1)
    return assignments


def createTree(root, binary_descriptors, depth, k=10, n_samples=1000):

    print(depth)
    centers = perform_kmedoids(binary_descriptors, k=k, n_samples=n_samples)
    cluster_assignments = get_assignments(binary_descriptors, centers)
    unique, count = np.unique(cluster_assignments, return_counts=True)
    for idx, center in enumerate(centers):
        if count[idx] < 50 or depth > 2:
            root.add_child(TreeLeaf(center, depth =depth, count=count[idx], word_id=3))
        else:
            root.add_child(TreeNode(center, depth =depth, count=count[idx]))
    depth += 1
    with ThreadPoolExecutor(max_workers=8) as executor:
        for c_id, child in enumerate(root.children):
            if not child.is_leaf():
                executor.submit(createTree, child, binary_descriptors[cluster_assignments == c_id], depth, k=k, n_samples=n_samples)


def process_leaves(root, leaves):
    # add a word_id to each leaf and store leaf references in a list
    for child in root.children:
        if child.is_leaf():
            child.word_id = len(leaves)
            leaves.append(child)
        else:
            process_leaves(child, leaves)
def print_leaf_info(leaves):
    print(len(leaves))
    # print leaf metrics:
    mean_count = 0
    max_count = 0
    min_count = 10000
    for leaf in leaves:
        mean_count += leaf.count
        if leaf.count > max_count:
            max_count = leaf.count
        if leaf.count < min_count:
            min_count = leaf.count
    mean_count /= len(leaves)
    print("mean count: ", mean_count)
    print("max count: ", max_count)
    print("min count: ", min_count)


def construct_BoW(descriptor_lengths, binary_descriptors, root, n_words):
    start = 0
    bins = np.arange(n_words)
    histograms = []
    print("Constructing BoW")
    for i in tqdm(range(len(descriptor_lengths))):
        end = start + descriptor_lengths[i]
        descriptors = binary_descriptors[start:end]
        words = get_words(descriptors, root)
        histogram, _ = np.histogram(words, bins=bins)
        histograms.append(histogram)
        start = end
    return histograms
def get_words(descriptors, root):
    words = []
    for descriptor in descriptors:
        words.append(root.traverse_tree(descriptor))
    return words

def l1_score(x,y):
    sum0 = np.sum(x)
    sum1 = np.sum(y)
    norm_vec0 = x / sum0
    norm_vec1 = y / sum1
    return 1 - 0.5 * np.sum(np.abs(norm_vec0 - norm_vec1))


def get_TF_IDF(BoWs):
    print("Calculating TF-IDFs")
    df = np.count_nonzero(BoWs, axis=0)
    TF_IDFs = []
    idf = np.log(len(BoWs) / (df + 1))
    for BoW in tqdm(BoWs):
        # Calculate ltc tfidf

        tf = 1 + np.log(BoW+1)

        tfidf = tf * idf
        # apply cosine normalization
        tfidf /= np.sqrt(np.sum(tfidf ** 2))  # TODO try without
        TF_IDFs.append(tfidf)
    return TF_IDFs


def get_scores(TF_IDFs):
    print("calculate scores between all TF_IDFs")
    score_matrix = np.zeros((len(TF_IDFs), len(TF_IDFs)))
    for i in tqdm(range(len(TF_IDFs))):
        for j in range(i + 1, len(TF_IDFs)):
            score_matrix[i, j] = l1_score(TF_IDFs[i], TF_IDFs[j])
            score_matrix[j, i] = score_matrix[i, j]
    return score_matrix

def eta(scores, t, j):
    assert t > 0
    if scores[t, t-1] < 0.2:
        return 0
    return scores[t, j] / scores[t, t-1]



def get_island_scores(scores, fragment_collector, alpha=0.3):
    frame_boundaries = np.cumsum([len(fragment.frames) for fragment in fragment_collector])
    print(frame_boundaries)
    HScore_matrix = np.zeros((scores.shape[0], len(fragment_collector)))
    for i in tqdm(range(1, scores.shape[0])):
        Hscores = np.zeros(len(fragment_collector))
        fragment_idx_of_i = np.argmax(frame_boundaries > i)

        if fragment_idx_of_i > 2:
            fragment_ids = np.concatenate([np.arange(0, fragment_idx_of_i-1), np.arange(fragment_idx_of_i+2, len(fragment_collector))])
        else:
            fragment_ids = np.concatenate(
                [np.arange(fragment_idx_of_i + 2, len(fragment_collector))])


        for f_id in fragment_ids:
            for j in range(frame_boundaries[f_id-1], frame_boundaries[f_id]):
                score = eta(scores, i, j)
                if score > alpha:
                    Hscores[f_id] += score
        HScore_matrix[i] = Hscores / [len(fragment.frames) for fragment in fragment_collector]
    return HScore_matrix

def extract_island_LC_pairs(beta, fragment_collector, frame_boundaries, island_scores):
    visual_candidates = []
    counting_array = np.zeros(len(fragment_collector))
    cur_boundary = frame_boundaries[0]
    n_frames = frame_boundaries[0]
    cur_frag_idx = 0
    for idx, Hscore in enumerate(island_scores):

        best = np.argsort(Hscore)[::-1]
        for i in range(len(best)):
            if Hscore[best[i]] > beta:
                counting_array[best[i]] += 1
            else:
                break
        if idx >= cur_boundary:
            for frag_idx, count in enumerate(counting_array):
                if count > n_frames * 0.7:
                    visual_candidates.append((cur_frag_idx, frag_idx))
            counting_array = np.zeros(len(fragment_collector))
            cur_frag_idx += 1
            cur_boundary = frame_boundaries[cur_frag_idx]
            n_frames = frame_boundaries[cur_frag_idx] - frame_boundaries[cur_frag_idx - 1]
    return visual_candidates

def main(config):
    np.set_printoptions(precision=4, suppress=True, threshold=np.inf)
    bridge= "bridge_03"
    fragment_file = f"Outputs/{bridge}/fragment_collector.pkl"
    fragment_collector = load_fragments(fragment_file)
    trajectory_file = f"Outputs/{bridge}/trajectory.npy"

    poses = poses_from_transformations(load_array(f"Outputs/{bridge}/transformations.npy"))
    trajectory = load_array(trajectory_file)
    print(trajectory)
    #New A:
    frames = get_frames(fragment_collector)
    # evaluate bluriness and reduce


    # check if tree already exists
    if os.path.isfile(f"Outputs/{bridge}/root.pkl"):
        root = pickle.load(open(f"Outputs/{bridge}/root.pkl", "rb"))
    else:
        descriptors = get_ORB_Descriptors(frames=frames)
        descriptor_lengths = [len(x) for x in descriptors]
        flattened_descriptors = np.asarray(list(chain(*descriptors)))
        binary_descriptors = get_binary_descriptors(flattened_descriptors)
        root = TreeNode(-1, count=len(binary_descriptors), depth=0)
        print("Construct Vocab Tree")
        createTree(root, binary_descriptors, depth=1, k=10, n_samples=500)
        pickle.dump(root, open(f"Outputs/{bridge}/root.pkl", "wb"))
    leaves = []
    process_leaves(root, leaves)
    print_leaf_info(leaves)
    if os.path.isfile(f"Outputs/{bridge}/BoWs.pkl"):
        BoWs = pickle.load(open(f"Outputs/{bridge}/BoWs.pkl", "rb"))
    else:
        BoWs = construct_BoW(descriptor_lengths, binary_descriptors, root, len(leaves))
        pickle.dump(BoWs, open(f"Outputs/{bridge}/BoWs.pkl", "wb"))
    TF_IDFs = get_TF_IDF(BoWs)
    if os.path.isfile(f"Outputs/{bridge}/scores.pkl"):
        scores = pickle.load(open(f"Outputs/{bridge}/scores.pkl", "rb"))
    else:
        scores = get_scores(TF_IDFs)
        pickle.dump(scores, open(f"Outputs/{bridge}/scores.pkl", "wb"))
    alpha = 0.3
    beta = alpha + 0.3
    island_scores = get_island_scores(scores, fragment_collector, alpha=alpha)
    frame_boundaries = np.cumsum([len(fragment.frames) for fragment in fragment_collector])
    # get best 3 indices per island score

    lc_fragment_candidates = extract_island_LC_pairs(beta, fragment_collector, frame_boundaries, island_scores)

    # match groupings

    """
    # A:
    # Choose 10 candidates at random per fragment
    lc_mult_fragment_candidates = get_fragment_candidates(fragment_collector, 10)
    # evaluate bluriness and reduce
    lc_mult_fragment_candidates = evaluate_bluriness(lc_mult_fragment_candidates, 3)
    # Create BoW for all images
    lc_visual_candidates = get_candidates_using_Feature_BoW_approach(lc_mult_fragment_candidates)
    # filter and transform candidates to fragment-candidates
    lc_fragment_candidates = reduce_candidates_to_one_per_fragment_pair(lc_visual_candidates, 3)
    """
    # B:

    # calculate t_err
    sigma = 0.01
    lc_fragment_candidates_odom = reduce_lc_by_odometry_validity_with_sampling(lc_fragment_candidates, fragment_collector, poses, sigma)

    # visualize lc candidates
    lc_frame_candidates = fragment_idx_to_frame_idx(lc_fragment_candidates, fragment_collector)
    lc_frame_candidates_odom = fragment_idx_to_frame_idx(lc_fragment_candidates_odom, fragment_collector)
    points = []
    #print("Number of loop closure candidates: {}".format(len(lc_frame_candidates)))
    #for idx, fragment in enumerate(fragment_collector):
    #    center = fragment.map.get_center()
    #    center = transform_vector(center, poses[idx])
    #    points.append(center)

    visualize_trajectory_and_LCs(trajectory, lc_frame_candidates, points, lc_fragment_candidates, None, poses)
    visualize_trajectory_and_LCs(trajectory, lc_frame_candidates_odom, points, lc_fragment_candidates_odom, None, poses)





if __name__ == "__main__":
    with open("../Configuration_Files/bridge_01.yaml", 'r') as file:
        config = yaml.safe_load(file)
    main(config)
