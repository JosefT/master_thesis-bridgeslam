import numpy as np
import open3d as o3d
import cv2


def main():
    # load depth and color image
    depth = o3d.t.io.read_image("datasets/roof_structure/02/depth/100.png")
    # change datatype
    o3d.visualization.draw_geometries([depth.to_legacy()])
    print(depth)
    depth = depth.cuda()
    depth = depth.filter_bilateral(60, 250, 80)
    depth = depth.cpu()
    print(depth)
    o3d.visualization.draw_geometries([depth.to_legacy()])
    depth_filtered = depth.to_legacy()
    color = o3d.io.read_image("datasets/roof_structure/02/rgb/100.png")
    depth = o3d.io.read_image("datasets/roof_structure/02/depth/100.png")
    intrinsic = o3d.camera.PinholeCameraIntrinsic()
    intrinsic.set_intrinsics(1280, 720, 911.7221069335938, 911.2312622070312, 651.0254516601562, 361.3611145019531)
    rgbd_filtered = o3d.geometry.RGBDImage().create_from_color_and_depth(color, depth_filtered, 1000, 3)
    pcd_filtered = o3d.geometry.PointCloud().create_from_rgbd_image(rgbd_filtered, intrinsic, np.eye(4))
    pcd_filtered.paint_uniform_color([1, 0.706, 0])
    rgbd = o3d.geometry.RGBDImage().create_from_color_and_depth(color, depth, 1000, 3)
    pcd = o3d.geometry.PointCloud().create_from_rgbd_image(rgbd, intrinsic, np.eye(4))
    pcd.paint_uniform_color([0.5, 0.5, 0.5])
    o3d.visualization.draw_geometries([pcd_filtered, pcd])




if __name__ == "__main__":
    main()
