# ----------------------------------------------------------------------------
# -                        Open3D: www.open3d.org                            -
# ----------------------------------------------------------------------------
# Copyright (c) 2018-2023 www.open3d.org
# SPDX-License-Identifier: MIT
# ----------------------------------------------------------------------------
import random
import threading
import time

import numpy as np
from open3d import visualization as vis
from open3d.visualization import gui, rendering
import open3d as o3d
from open3d.visualization import O3DVisualizer

from SLAM5 import slam_main


class CustomVisualizer():
    ADD_GEOMETRY = 1

    def __init__(self, title="Open3D", width=1920, height=1080):
        self.window = gui.Application.instance.create_window(
            "Add Spheres Example", width, height)
        self.scene = gui.SceneWidget()
        self.scene.scene = rendering.Open3DScene(self.window.renderer)
        self.scene.scene.set_background((1.0, 1.0, 1.0, 1.0), None)
        self.scene.scene.scene.set_sun_light(
            [-1, -1, -1],  # direction
            [1, 1, 1],  # color
            100000)  # intensity
        self.scene.scene.scene.enable_sun_light(True)
        bbox = o3d.geometry.AxisAlignedBoundingBox([0, 0, 0],
                                                   [3,3, 3])
        intrinsic = o3d.camera.PinholeCameraIntrinsic(width=1920, height=1080, fx =911.7221069335938, fy = 911.2312622070312, cx = 651.0254516601562, cy = 361.3611145019531)
        self.scene.setup_camera(intrinsic, np.eye(4), bbox)
        self.scene.scene.show_settings = True
        self.window.add_child(self.scene)
        self._id = 0

    def add(self, g):
        mat = rendering.MaterialRecord()
        if isinstance(g, dict):
            self.scene.scene.add_geometry(g)
        else:
            self.scene.scene.add_geometry("Object " + str(self._id), g, mat)
        self._id += 1

    def draw(self, geometry=None,
             actions=None,
             lookat=None,
             eye=None,
             up=None,
             field_of_view=60.0,
             intrinsic_matrix=None,
             extrinsic_matrix=None,
             ibl=None,
             ibl_intensity=None,
             show_skybox=None,
             show_ui=None,
             raw_mode=False,
             point_size=None,
             line_width=None,
             animation_time_step=1.0,
             animation_duration=None,
             rpc_interface=False,
             on_init=None,
             on_animation_frame=None,
             on_animation_tick=None,
             non_blocking_and_return_uid=False):

        if actions is not None:
            for a in actions:
                self.scene.add_action(a[0], a[1])

        if point_size is not None:
            self.point_size = point_size

        if line_width is not None:
            self.line_width = line_width



        n = 1
        if isinstance(geometry, list):
            for g in geometry:
                self.add(g)
                n += 1
        elif geometry is not None:
            self.add(geometry)


        self.animation_time_step = animation_time_step
        if animation_duration is not None:
            self.animation_duration = animation_duration

        if show_ui is not None:
            self.show_settings = show_ui



        gui.Application.instance.add_window(self.window)
        #self.perform_thread(func)
        gui.Application.instance.run()



    def perform_thread(self, func):

        threading.Thread(target=func, args=[self]).start()
def func(vis):
        return
        gui.Application.instance.post_to_main_thread(vis.window, vis.add(o3d.geometry.TriangleMesh().create_box(
                                                                               width=2, height=2, depth=2) , 1))

def main():
    #vis = CustomVisualizer()

    pcd = o3d.io.read_point_cloud("datasets/my_room/04/pcd/0.pcd")
    #vis.draw([pcd])
    gui.Application.instance.initialize()
    vis = CustomVisualizer()
    vis.add(pcd)
    gui.Application.instance.run()


if __name__ == "__main__":
    main()