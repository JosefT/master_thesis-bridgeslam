import copy

import cv2
import numpy as np
import open3d as o3d
import pytransform3d.visualizer as pv
import yaml
from pytransform3d.rotations import quaternion_from_matrix
from tqdm import tqdm

from Loop_Detector_BoW import LoopDetectorBoW
from Serialization import load_array, load_fragments
from src.Loop_Closure_Pipeline import get_fragment_candidates, evaluate_bluriness, \
    get_candidates_using_Feature_BoW_approach, \
    reduce_candidates_to_one_per_fragment_pair
from src.transformation_utils import t_to_T, poses_from_transformations


def get_lcs_as_lines(lc_candidates, ts):
    line_indices = []
    max_score = max([candidate[2] for candidate in lc_candidates])
    min_score = min([candidate[2] for candidate in lc_candidates])
    divisor = max_score - min_score
    colors = []
    def_color = [0., 0, 1.]
    for candidate in lc_candidates:
        start = candidate[0]
        end = candidate[1]
        line_indices.append([start, end])
        score = (candidate[2] - min_score) / divisor
        score = (score + 0.4) / 1.4
        colors.append([x * score for x in def_color])

    lineSet = o3d.geometry.LineSet()
    lineSet.points = o3d.utility.Vector3dVector(ts)
    lineSet.lines = o3d.utility.Vector2iVector(line_indices)
    lineSet.colors = o3d.utility.Vector3dVector(colors)
    return lineSet


def get_loop_candidates_BoW(threshold, frames):
    loop_detector = LoopDetectorBoW()
    failed_registration_frames = []
    orb = cv2.ORB_create()
    for frame in frames:
        img = np.asarray(cv2.imread(frame.color_path))
        _, des = orb.detectAndCompute(img, None)
        loop_detector.add(des)
    pairs = loop_detector.pipeline(threshold=threshold)
    print("Number of pairs: {}".format(len(pairs)))
    return pairs


def consecuitive_transformations_to_trajectory(transformations):
    ts = []
    for i in range(len(transformations) + 1):
        if i == 0:
            ts.append(np.eye(4))
        else:
            ts.append(transformations[i - 1] @ ts[i - 1])
    # for i in range(len(ts)):
    #    ts[i] = np.linalg.inv(ts[i])
    return ts


def get_loop_candidates_from_trajectory(fragment_collector, transformations):
    trajectory = poses_from_transformations(transformations)
    lc_candidates = []
    dist_val = 0.3
    points = []
    for f_idx_0 in range(0, len(fragment_collector)):
        center_0 = fragment_collector[f_idx_0].map.get_center()
        center_0_transf = (trajectory[f_idx_0] @ t_to_T(center_0))[:3, 3]

        points.append(center_0_transf)
        for f_idx_1 in range(f_idx_0 + 1, len(fragment_collector)):
            # calculate map _centers
            center_1 = fragment_collector[f_idx_1].map.get_center()

            center_1_transf = (trajectory[f_idx_1] @ t_to_T(center_1))[:3, 3]

            distance = np.linalg.norm(center_0_transf - center_1_transf)
            threshold = dist_val * 1.06 ** (f_idx_1 - f_idx_0)
            if distance < threshold:
                lc_candidates.append((f_idx_0, f_idx_1, distance, threshold))
    return lc_candidates, points


def fragment_idx_to_frame_idx(lc_candidates, fragment_collector):
    new_candidates = []
    for candidate in lc_candidates:
        median_frame_idx_0 = fragment_collector[candidate[0]].start_idx + (
                fragment_collector[candidate[0]].end_idx - fragment_collector[candidate[0]].start_idx) // 2
        median_frame_idx_1 = fragment_collector[candidate[1]].start_idx + (
                fragment_collector[candidate[1]].end_idx - fragment_collector[candidate[1]].start_idx) // 2
        new_candidates.append((median_frame_idx_0, median_frame_idx_1, *candidate[2:]))
    return new_candidates


def visualize_trajectory_and_LCs(trajectory, lc_candidates, points=None, fragment_candidates=None, fragments=None,
                                 poses=None):
    fig = pv.figure()
    trajectory = [np.linalg.inv(pose) for pose in trajectory]
    rots = [pose[:3, :3] for pose in trajectory]
    const_rot = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])
    for rot in rots:
        rot = const_rot @ rot
    rots_q = np.array([quaternion_from_matrix(rot) for rot in rots])

    ts = [pose[:3, 3] for pose in trajectory]
    trajectory_q = np.concatenate([ts, rots_q], axis=1)
    lc_lineSet = get_lcs_as_lines(lc_candidates, ts)
    fig.add_geometry(lc_lineSet)
    fig.plot_trajectory(P=trajectory_q, n_frames=75, s=0.35, c=[1, 0, 0], )
    if points is not None:
        colors = np.empty((len(points), 3))
        for d in range(colors.shape[1]):
            colors[:, d] = np.linspace(0, 1, len(colors))
        fig.scatter(P=points, s=0.1, c=colors)
        if fragment_candidates is not None:
            lc_lineSet_frag = get_lcs_as_lines(fragment_candidates, points)
            fig.add_geometry(lc_lineSet_frag)
    if fragments is not None and poses is not None:
        for f_idx, fragment in enumerate(fragments):
            map = copy.deepcopy(fragment.map)
            map.transform(poses[f_idx])

            # remove all clusters except biggest one
            with o3d.utility.VerbosityContextManager(o3d.utility.VerbosityLevel.Debug) as cm:
                labels = np.array(
                    map.cluster_dbscan(eps=0.05, min_points=1, print_progress=True))
            counts = np.bincount(labels)
            biggest_cluster = np.argmax(np.bincount(labels))
            mask = np.zeros(len(labels), dtype=bool)
            mask[labels == biggest_cluster] = True
            map = map.select_by_index(np.where(mask)[0])
            fig.add_geometry(map)
    fig.show()


def calculate_acc_distances(trajectory):
    distances = np.zeros(len(trajectory))
    for i in range(1, len(trajectory)):
        distances[i] = distances[i - 1] + np.linalg.norm(trajectory[i][:3, 3] - trajectory[i - 1][:3, 3])
    return distances


def overlap_assuming_chamfer_distance(pcd0, pcd1, overlap_est=0.5):
    distances0 = np.asarray(pcd0.compute_point_cloud_distance(pcd1))
    distances0 = np.sort(distances0)
    distances0 = distances0[:int(len(distances0) * overlap_est)]
    distances1 = np.asarray(pcd1.compute_point_cloud_distance(pcd0))
    distances1 = np.sort(distances1)
    distances1 = distances1[:int(len(distances1) * overlap_est)]
    chamfer_dist = (np.mean(distances0) + np.mean(distances1)) / 2
    return chamfer_dist


def sub_sample_pcd(pcd, n_remaining_points):
    if len(pcd.points) < n_remaining_points:
        return pcd
    step = len(pcd.points) // n_remaining_points
    return pcd.uniform_down_sample(step)


def estimate_epsilon(fragments, poses):
    distances = []
    for i in range(len(fragments) - 1):
        map0 = fragments[i].map
        map1 = fragments[i + 1].map
        map0_down = sub_sample_pcd(map0, 1000)
        map1_down = sub_sample_pcd(map1, 1000)
        map0_down.transform(poses[i])
        map1_down.transform(poses[i + 1])

        #  # calculate chamfer distance
        distance = overlap_assuming_chamfer_distance(map0_down, map1_down)
        distances.append(distance)
    return np.mean(distances)


def reduce_lc_by_odometry_validity_with_sampling(lc_candidates, fragment_collector, poses, sigma=0.1):
    distances_trajectory = calculate_acc_distances(poses)

    t_errs = []
    epsilon = estimate_epsilon(fragment_collector, poses)
    print("Test Odometry Validity")
    for lc_candidate in tqdm(lc_candidates):
        map0 = fragment_collector[lc_candidate[0]].map
        map1 = fragment_collector[lc_candidate[1]].map
        map0_down = sub_sample_pcd(map0, 1000)
        map1_down = sub_sample_pcd(map1, 1000)
        map0_down.transform(poses[lc_candidate[0]])
        map1_down.transform(poses[lc_candidate[1]])

        #  # calculate chamfer distance
        distance = overlap_assuming_chamfer_distance(map0_down, map1_down)

        t_err = max(distance - epsilon, 0) / (
                    distances_trajectory[lc_candidate[1]] - distances_trajectory[lc_candidate[0]])
        t_errs.append(t_err)
    d_odom = np.exp(-1 * np.array(t_errs) ** 2 / (2 * sigma ** 2))
    lc_fragment_candidates = [lc_candidates[i] for i in range(len(lc_candidates)) if d_odom[i] > 0.8]

    return lc_fragment_candidates


def main(config):
    np.set_printoptions(precision=4, suppress=True)
    fragment_file = "../Outputs/bridge_03/fragment_collector.pkl"
    fragment_collector = load_fragments(fragment_file)
    trajectory_file = "../Outputs/bridge_03/trajectory.npy"
    poses = poses_from_transformations(load_array("../Outputs/bridge_03/transformations.npy"))
    trajectory = load_array(trajectory_file)

    # A:
    # Choose 10 candidates at random per fragment
    lc_mult_fragment_candidates = get_fragment_candidates(fragment_collector, 10)
    # evaluate bluriness and reduce
    lc_mult_fragment_candidates = evaluate_bluriness(lc_mult_fragment_candidates, 3)
    # Create BoW for all images
    lc_visual_candidates = get_candidates_using_Feature_BoW_approach(lc_mult_fragment_candidates)
    # filter and transform candidates to fragment-candidates
    lc_fragment_candidates = reduce_candidates_to_one_per_fragment_pair(lc_visual_candidates, 3)

    # B:
    # calculate t_err
    sigma = 0.005
    lc_fragment_candidates_odom = reduce_lc_by_odometry_validity_with_sampling(lc_fragment_candidates,
                                                                               fragment_collector, poses, sigma)

    # visualize lc candidates
    lc_frame_candidates = fragment_idx_to_frame_idx(lc_fragment_candidates, fragment_collector)
    lc_frame_candidates_odom = fragment_idx_to_frame_idx(lc_fragment_candidates_odom, fragment_collector)
    points = []
    # print("Number of loop closure candidates: {}".format(len(lc_frame_candidates)))
    # for idx, fragment in enumerate(fragment_collector):
    #    center = fragment.map.get_center()
    #    center = transform_vector(center, poses[idx])
    #    points.append(center)

    visualize_trajectory_and_LCs(trajectory, lc_frame_candidates, points, lc_fragment_candidates, None, poses)
    visualize_trajectory_and_LCs(trajectory, lc_frame_candidates_odom, points, lc_fragment_candidates_odom, None, poses)


if __name__ == "__main__":
    with open("bridge_01.yaml", 'r') as file:
        config = yaml.safe_load(file)
    main(config)
