import math
import pickle
from itertools import chain

import open3d as o3d
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.metrics.pairwise import cosine_similarity

from BinaryORB import *
from Entities.Loop_Detector_BoW import LoopDetectorBoW

from Entities.Tree import TreeNode
from RigidRegistration import estTransformationByModelAndICP
from src.metrics import NDT_score, get_normal_similarity


# ------------------ Feature BoW - Functions  ------------------
def get_fragment_candidates(fragment_collector, n_candidates_per_fragment):
    fragment_frames = []
    for fragment in fragment_collector:
        n_frames = fragment.num_frames
        step = n_frames // n_candidates_per_fragment
        if step == 0:
            step = 1
        frames = []
        for i in range(0, n_frames, step):
            frames.append(fragment.frames[i])
        fragment_frames.append(frames)
    return fragment_frames


def evaluate_bluriness(fragment_candidates, n_remain):
    reduced_fragment_candidates = []
    if len(fragment_candidates) > n_remain:
        for f_candidates in fragment_candidates:
            blurriness = np.zeros(len(f_candidates))
            for i, frame in enumerate(f_candidates):
                img_gray = cv2.imread(frame.color_path, cv2.COLOR_BGR2GRAY)
                variance = cv2.Laplacian(img_gray, cv2.CV_64F).var()
                blurriness[i] = variance
                # visualize
            sorted_indices = np.argsort(-1 * blurriness)
            reduced_fragment_candidates.append([f_candidates[i] for i in sorted_indices[:n_remain]])
        return reduced_fragment_candidates
    else:
        return fragment_candidates


def get_candidates_using_Feature_BoW_approach(fragment_candidates):
    loop_detector = LoopDetectorBoW()
    failed_registration_frames = []
    orb = cv2.ORB_create()
    n_per_fragment = len(fragment_candidates[0])
    frames = [item for row in fragment_candidates for item in row]
    for frame in frames:
        img = np.asarray(cv2.imread(frame.color_path))
        _, des = orb.detectAndCompute(img, None)
        # visualize keypoints
        loop_detector.add(des)
    # Pipeline
    bows = loop_detector.gen_bows()
    word_count_sum = bows.sum(axis=0)
    tfidfTransf = TfidfTransformer(use_idf=True, norm='l2', smooth_idf=True)
    img_tfidf_vectors = tfidfTransf.fit_transform(X=bows)
    pairs = []
    for i in range(img_tfidf_vectors.shape[0]):
        # only calculate pairs with j > i and one fragment distance to i
        start = i + n_per_fragment + (n_per_fragment - i % 3)
        for j in range(start, img_tfidf_vectors.shape[0]):
            cosine_sim = cosine_similarity(img_tfidf_vectors[i].reshape(1, -1), img_tfidf_vectors[j].reshape(1, -1))[0][
                0]
            scaled_similarity = 0.5 * (cosine_sim + 1)  # scale to [0, 1]
            pairs.append((i, j, scaled_similarity))

    pairs = loop_detector.sort_pairs(pairs)

    # cut off all pairs that have a similarity below the threshold
    threshold = 0.8
    for idx, pair in enumerate(pairs):
        if pair[2] < threshold:
            pairs = pairs[:idx]
            break
    return pairs


def reduce_candidates_to_one_per_fragment_pair(visual_candidates, n_candidates_per_fragment):
    fragment_candidates = [[visual_candidates[0][0] // 3, visual_candidates[0][1] // 3, visual_candidates[0][2]]]
    for candidate in visual_candidates[1:]:
        new_candidate = [candidate[0] // 3, candidate[1] // 3, candidate[2]]
        # check if candidate is already in list based on first two values
        if new_candidate[:2] in [cd[:2] for cd in fragment_candidates]:
            continue
        fragment_candidates.append(new_candidate)
    return fragment_candidates


# ------------------ Odometry - Functions  ------------------

def calculate_acc_distances(trajectory):
    distances = np.zeros(len(trajectory))
    for i in range(1, len(trajectory)):
        distances[i] = distances[i - 1] + np.linalg.norm(trajectory[i][:3, 3] - trajectory[i - 1][:3, 3])
    return distances


def get_visual_lc_candidates(fragment_collector, n_candidates_sample, n_candidates_reduce):
    fragment_candidates = get_fragment_candidates(fragment_collector, n_candidates_sample)
    reduced_fragment_candidates = evaluate_bluriness(fragment_candidates, n_candidates_reduce)
    visual_candidates = get_candidates_using_Feature_BoW_approach(reduced_fragment_candidates)
    lc_candidates = reduce_candidates_to_one_per_fragment_pair(visual_candidates, n_candidates_reduce)
    return lc_candidates


def extract_frames_from_fragment_collector(fragment_collector):
    frames = []
    for fragment in fragment_collector:
        for frame in fragment.frames:
            frames.append(frame)
    return frames


def visualize_LC_candidates(lc_fragment_candidates, fragment_collector):
    for candidate in lc_fragment_candidates:
        frame0 = fragment_collector[candidate[0]].frames[0]
        frame1 = fragment_collector[candidate[1]].frames[0]
        # load rgbs
        img0 = cv2.imread(frame0.color_path)
        img1 = cv2.imread(frame1.color_path)
        fig, ax = plt.subplots(1, 2, figsize=(10, 5))
        fig.suptitle(f'Matches:{candidate[2]}, avg_score:{candidate[3]}')
        ax[0].imshow(cv2.cvtColor(img0, cv2.COLOR_BGR2RGB))
        ax[0].set_title(f'Image {candidate[0]}')
        ax[0].axis('off')

        ax[1].imshow(cv2.cvtColor(img1, cv2.COLOR_BGR2RGB))
        ax[1].set_title(f'Image {candidate[1]}')
        ax[1].axis('off')
        plt.show()


def get_visual_lc_candidates_using_binary_BoWs(fragment_collector, alpha, beta, visualize_candidates=False):
    frames = extract_frames_from_fragment_collector(fragment_collector)
    frame_boundaries = np.cumsum([len(fragment.frames) for fragment in fragment_collector])

    descriptors = get_ORB_Descriptors(frames=frames)
    descriptor_lengths = [len(x) for x in descriptors]
    flattened_descriptors = np.asarray(list(chain(*descriptors)))
    root = TreeNode(-1, count=len(flattened_descriptors), depth=0)
    print("Construct Vocab Tree")
    createTree(root, flattened_descriptors, depth=1, k=10, n_samples=1000)
    leaves = []
    process_leaves(root, leaves)

    BoWs = construct_BoW(descriptor_lengths, flattened_descriptors, root, len(leaves))
    TF_IDFs = get_TF_IDF(BoWs)
    scores = get_scores(TF_IDFs)
    #pickle.dump(scores, open("scores.pkl", "wb"))

    #scores = pickle.load(open("scores.pkl", "rb"))
    island_scores = get_island_scores(scores, fragment_collector, alpha=alpha)

    lc_fragment_candidates = extract_island_LC_pairs(beta, fragment_collector, frame_boundaries, island_scores)
    if visualize_candidates:
        visualize_LC_candidates(lc_fragment_candidates, fragment_collector)
    return lc_fragment_candidates


def append_loop_closure_edge(src_idx, tgt_idx, transformation, information_matrix, pose_graph):
    pose_graph.edges.append(o3d.pipelines.registration.PoseGraphEdge(src_idx, tgt_idx, transformation,
                                                                     information_matrix, uncertain=True))
    return pose_graph


def sub_sample_pcd(pcd, n_remaining_points):
    if len(pcd.points) < n_remaining_points:
        return pcd
    step = len(pcd.points) // n_remaining_points
    return pcd.uniform_down_sample(step)


def estimate_epsilon(fragments, poses):
    distances = []
    for i in range(len(fragments) - 1):
        map0 = fragments[i].map
        map1 = fragments[i + 1].map
        map0_down = sub_sample_pcd(map0, 1000)
        map1_down = sub_sample_pcd(map1, 1000)
        map0_down.transform(poses[i])
        map1_down.transform(poses[i + 1])

        #  # calculate chamfer distance
        distance = overlap_assuming_chamfer_distance(map0_down, map1_down)
        distances.append(distance)
    return np.mean(distances)


def reduce_lc_by_odometry_validity(lc_candidates, fragment_collector, poses, epsilon=1, sigma=0.1):
    distances_trajectory = calculate_acc_distances(poses)
    t_errs = []
    epsilon = estimate_epsilon(fragment_collector, poses)
    for lc_candidate in lc_candidates:
        map0 = fragment_collector[lc_candidate[0]].map
        map1 = fragment_collector[lc_candidate[1]].map
        map0_down = sub_sample_pcd(map0, 1000)
        map1_down = sub_sample_pcd(map1, 1000)
        map0_down.transform(poses[lc_candidate[0]])
        map1_down.transform(poses[lc_candidate[1]])

        #  # calculate chamfer distance
        distance = overlap_assuming_chamfer_distance(map0_down, map1_down)

        t_err = max(distance - epsilon, 0) / (
                distances_trajectory[lc_candidate[1]] - distances_trajectory[lc_candidate[0]])
        t_errs.append(t_err)

    d_odom = 1 - np.exp(-1 * np.array(t_errs) ** 2 / (2 * sigma ** 2))

    lc_fragment_candidates = [lc_candidates[i] for i in range(len(lc_candidates)) if d_odom[i] < 0.5]
    return lc_fragment_candidates


def overlap_assuming_chamfer_distance(pcd0, pcd1, overlap_est=0.5):
    distances0 = np.asarray(pcd0.compute_point_cloud_distance(pcd1))
    distances0 = np.sort(distances0)
    distances0 = distances0[:int(len(distances0) * overlap_est)]
    distances1 = np.asarray(pcd1.compute_point_cloud_distance(pcd0))
    distances1 = np.sort(distances1)
    distances1 = distances1[:int(len(distances1) * overlap_est)]
    chamfer_dist = (np.mean(distances0) + np.mean(distances1)) / 2
    return chamfer_dist


def reduce_lc_by_odometry_validity_with_sampling(lc_candidates, fragment_collector, poses, epsilon=0.5, sigma=0.1):
    distances_trajectory = calculate_acc_distances(poses)

    t_errs = []
    print("Test Odometry Validity")
    for lc_candidate in lc_candidates:

        map0 = fragment_collector[lc_candidate[0]].map
        map1 = fragment_collector[lc_candidate[1]].map
        map0_down = map0
        map1_down = map1
        if len(map0.points) > 100:
            step = len(map0.points) // 100
            map0_down = map0.uniform_down_sample(step)
        if len(map1.points) > 100:
            step = len(map1.points) // 100
            map1_down = map1.uniform_down_sample(step)
        map0_down.transform(poses[lc_candidate[0]])
        map1_down.transform(poses[lc_candidate[1]])

        # calculate chamfer distance
        chamfer_dist = overlap_assuming_chamfer_distance(map0_down, map1_down, overlap_est=0.3)

        t_err = max(chamfer_dist - epsilon, 0) / (
                    distances_trajectory[lc_candidate[1]] - distances_trajectory[lc_candidate[0]])
        t_errs.append(t_err)
    d_odom = 1 - np.exp(-1 * np.array(t_errs) ** 2 / (2 * sigma ** 2))
    lc_fragment_candidates = [lc_candidates[i] for i in range(len(lc_candidates)) if d_odom[i] < 0.5]
    # for lc_fragment_candidate in lc_fragment_candidates:
    #     print(lc_fragment_candidate)
    #
    #     map0 = copy.deepcopy(fragment_collector[lc_fragment_candidate[0]].map)
    #     map1 = copy.deepcopy(fragment_collector[lc_fragment_candidate[1]].map)
    #     map0 = map0.transform(poses[lc_fragment_candidate[0]])
    #     map1 = map1.transform(poses[lc_fragment_candidate[1]])
    #     map0.paint_uniform_color([1, 0, 0])
    #     map1.paint_uniform_color([0, 1, 0])
    #     o3d.visualization.draw_geometries([map0, map1])

    return lc_fragment_candidates


# ------------------ Alignment  ------------------

def calculate_loop_closure_transformations(lc_candidates, fragment_collector, Model, max_corr_distance=0.1,
                                           model_voxel_size=0.05):
    for candidate in tqdm(lc_candidates):
        source = fragment_collector[candidate[0]].map
        target = fragment_collector[candidate[1]].map
        result, _ = estTransformationByModelAndICP(source, target, Model, max_corr_dist=max_corr_distance,
                                                   p2p=False, voxel_size=model_voxel_size)
        candidate.append(result)
    return lc_candidates


def all_pcd_metrics_okay(source, target, icp_result, voxel_size, ndt_box_size=0.5, min_pt_thres=0.25,
                         ndt_min_score=0.5):
    use_NDT_score = True
    use_fitness = True
    use_normals = True

    fitness = math.nan
    ndt_score = math.nan
    normal_similarity = math.nan

    result_okay = True
    metrics = dict()
    # --- NDT Score ---
    if use_NDT_score:
        ndt_score = NDT_score(source, target, icp_result, voxel_size, box_size=ndt_box_size, min_pt_thres=min_pt_thres,
                              visualize=False, visualize_threshold=ndt_min_score)
        result_okay = result_okay and ndt_score > ndt_min_score
        metrics["ndt_score"] = ndt_score

    # --- Fitness ---
    if use_fitness:
        fitness = icp_result.fitness
        result_okay = result_okay and fitness > 0.25
        metrics["fitness"] = fitness

    # --- Normals ---
    if use_normals:
        normal_similarity = get_normal_similarity(source, target, icp_result)
        result_okay = result_okay and normal_similarity > 0.6
        metrics["normal_difference"] = normal_similarity

    return result_okay, metrics


def filter_faulty_aligned_LCs(lc_candidates, fragment_collector, ndt_box_size=0.5, ndt_min_pt_thres=0.25,
                              ndt_voxel_size=0.05, ndt_min_score=0.5):
    filtered_candidates = []
    for candidate in lc_candidates:

        source = fragment_collector[candidate[0]].map
        target = fragment_collector[candidate[1]].map
        icp_result = candidate[-1]

        result_okay, metrics = all_pcd_metrics_okay(source, target, icp_result, ndt_voxel_size, ndt_box_size,
                                                    ndt_min_pt_thres, ndt_min_score)

        # --- Final Evaluation ---
        if result_okay:
            print(f"CL accepted between {candidate[0]} and {candidate[1]}: ", metrics["fitness"], metrics["ndt_score"],
                  metrics["normal_difference"])
            filtered_candidates.append(candidate)
        else:
            print(f"CL rejected between {candidate[0]} and {candidate[1]}: ", metrics["fitness"], metrics["ndt_score"],
                  metrics["normal_difference"])
        # cp_src = copy.deepcopy(source)
        # cp_src.paint_uniform_color([1, 0, 0])
        # cp_src.transform(candidate[3].transformation)
        # cp_tgt = copy.deepcopy(target)
        # cp_tgt.paint_uniform_color([0, 1, 0])
        # o3d.visualization.draw_geometries([cp_src, cp_tgt])
    return filtered_candidates


# ------------------ Graph Helper  ------------------

def get_sub_graphs(pose_graph):
    def get_neighbors(edges, node):
        neighbors = []
        for edge in edges:
            if edge.source_node_id == node:
                neighbors.append(edge.target_node_id)
            elif edge.target_node_id == node:
                neighbors.append(edge.source_node_id)
        return set(neighbors)

    def dfs(edges, node, visited, subgraph):
        visited.add(node)
        subgraph.append(node)
        for neighbor in get_neighbors(edges, node):
            if neighbor not in visited:
                dfs(edges, neighbor, visited, subgraph)

    connected_subgraphs = []
    visited = set()
    nodes = np.arange(len(pose_graph.nodes))
    edges = np.asarray(pose_graph.edges)
    for node in nodes:
        if node not in visited:
            subgraph = []
            dfs(edges, node, visited, subgraph)
            connected_subgraphs.append(subgraph)
    return connected_subgraphs


def extract_sub_graph(pose_graph, nodes_idx):
    edges = pose_graph.edges
    sub_edges = []
    for edge in edges:
        if edge.source_node_id in nodes_idx and edge.target_node_id in nodes_idx:
            edge = o3d.pipelines.registration.PoseGraphEdge(edge.source_node_id - nodes_idx[0],
                                                            edge.target_node_id - nodes_idx[0], edge.transformation,
                                                            edge.information, edge.uncertain)
            sub_edges.append(edge)
    sub_nodes = [pose_graph.nodes[i] for i in nodes_idx]
    sub_graph = o3d.pipelines.registration.PoseGraph()
    for node in sub_nodes:
        sub_graph.nodes.append(node)
    for edge in sub_edges:
        sub_graph.edges.append(edge)
    return sub_graph
