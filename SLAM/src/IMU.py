import numpy as np
from ahrs.filters import Mahony
from pyquaternion import Quaternion
from scipy.signal import butter, filtfilt


# SRC-Ref: Code borrowed from ahrs library (acc2q function)
def get_first_orientation(accel):
    a = np.array([accel[0], accel[1], accel[2]])
    q = np.array([1.0, 0.0, 0.0, 0.0])
    ex, ey, ez = 0.0, 0.0, 0.0
    if np.linalg.norm(a) > 0 and len(a) == 3:
        ax, ay, az = a
        # Normalize accelerometer measurements
        a_norm = np.linalg.norm(a)
        ax /= a_norm
        ay /= a_norm
        az /= a_norm
        # Euler Angles from Gravity vector
        ex = np.arctan2(ay, az)
        ey = np.arctan2(-ax, np.sqrt(ay ** 2 + az ** 2))
        ez = 0.0
        # Euler to Quaternion
        cx2 = np.cos(ex / 2.0)
        sx2 = np.sin(ex / 2.0)
        cy2 = np.cos(ey / 2.0)
        sy2 = np.sin(ey / 2.0)
        q = np.array([cx2 * cy2, sx2 * cy2, cx2 * sy2, -sx2 * sy2])
        q /= np.linalg.norm(q)
        q = Quaternion(q)
    return q


class RotationEstimator:
    def __init__(self):
        self.alpha = 0
        self.firstGyro = True
        self.firstMeasurement = True
        self.lastMeasurementTime = -1
        self.orientations_q = []
        self.accel_rotation_q = None
        self.mahony = None
        self.lastValidAccelVector = np.ones(3, float)
        self.linear_accelerations = None
        self.timestamps = None

    def get_rotation_from_prev_frame(self, frame_idx):
        if frame_idx == 0:
            return Quaternion(axis=[1, 0, 0], angle=0)
        else:
            return self.orientations_q[frame_idx] * self.orientations_q[frame_idx - 1].inverse

    def get_rotation_between_frames(self, frame_first, frame_second):
        return self.orientations_q[frame_second] * self.orientations_q[frame_first].inverse

    def register_new_IMU_Frame(self, gyro, accel, timestamp):

        if self.firstMeasurement:
            self.lastMeasurementTime = timestamp
            self.firstMeasurement = False
            self.mahony = Mahony(learning_rate=0.4, )
            self.orientations_q.append(get_first_orientation(accel))
            return
        else:
            dt_gyro = (timestamp - self.lastMeasurementTime) / 1000
            self.lastMeasurementTime = timestamp

            q = self.orientations_q[-1]
            self.mahony.Dt = dt_gyro

            # We need to check if the accel vector is valid. It is not if we are moving
            if abs(np.linalg.norm(accel) - 9.81) > 0.4:
                # We just continue using the last valid accel vector until we stop moving
                accel = self.lastValidAccelVector
            else:
                self.lastValidAccelVector = accel
            quad_np = self.mahony.updateIMU(gyr=gyro, acc=accel, q=np.array([q.w, q.x, q.y, q.z]))

            self.orientations_q.append(Quaternion(quad_np))

    def add_data(self, t, gyro, accel, base_cutoff_frequency=0.5):
        n_data = len(t)
        for i in range(n_data):
            self.register_new_IMU_Frame(gyro[i], accel[i], t[i])

        instantaneous_sampling_freq = self.calculate_instantaneous_sampling_freq(t)
        adjusted_cutoff_frequency = self.adjust_cutoff_frequency(base_cutoff_frequency, instantaneous_sampling_freq)
        self.linear_accelerations = self.high_pass_filter(accel, adjusted_cutoff_frequency, instantaneous_sampling_freq)
        self.timestamps = t

    def calculate_instantaneous_sampling_freq(self, timestamps):
        # Calculate time differences between consecutive timestamps
        time_diffs = np.diff(timestamps)

        # Calculate the instantaneous sampling frequency
        instantaneous_sampling_freq = 1.0 / np.mean(time_diffs)

        return instantaneous_sampling_freq

    def adjust_cutoff_frequency(self, cutoff_frequency, instantaneous_sampling_freq):
        # Adjust the cutoff frequency based on the instantaneous sampling frequency
        adjusted_cutoff_frequency = cutoff_frequency / instantaneous_sampling_freq

        return adjusted_cutoff_frequency

    def high_pass_filter(self, data, adjusted_cutoff_freq, instantaneous_sampling_freq):
        # Normalize the adjusted cutoff frequency
        nyquist_freq = 0.5 * instantaneous_sampling_freq
        normalized_cutoff_freq = adjusted_cutoff_freq / nyquist_freq

        # Design the filter
        b, a = butter(2, normalized_cutoff_freq, btype='high', analog=False, fs=10000)

        # Apply the filter to the data
        filtered_data = filtfilt(b, a, data, axis=0)

        return filtered_data
