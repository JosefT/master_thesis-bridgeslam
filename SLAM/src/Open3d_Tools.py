import numpy as np
import open3d as o3d


def addPointsToPointCloud(pcd, points):
    points_combined = np.vstack((pcd.points, points))
    pcd.points = o3d.utility.Vector3dVector(points_combined)
    return pcd


def points_to_pcd(points):
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)
    pcd.estimate_normals()
    return pcd


def segment_pcd_with_plane(pcd, dist_threshold):
    _, inliers = pcd.segment_plane(distance_threshold=dist_threshold, ransac_n=3, num_iterations=250)
    # inlier_cloud = pcd.select_by_index(inliers)
    # inlier_cloud.paint_uniform_color([1.0, 0, 0])
    ratio = len(inliers) / len(pcd.points)
    outlier_cloud = pcd.select_by_index(inliers, invert=True)

    return ratio, outlier_cloud


def convert_t_correspondences(correspondences):
    len_corr = len(correspondences)
    arr = np.expand_dims(np.arange(len_corr), 0)
    correspondences = correspondences.squeeze()
    correspondences = np.expand_dims(correspondences, 0)
    t_correspondences = np.vstack((arr, correspondences)).T
    return t_correspondences


def build_rgbd_from_image_paths(color_path, depth_path, depth_scale, depth_trunc=15):
    color = o3d.io.read_image(color_path)
    depth = o3d.io.read_image(depth_path)
    rgbd = o3d.geometry.RGBDImage().create_from_color_and_depth(color, depth, convert_rgb_to_intensity=False,
                                                                depth_scale=depth_scale, depth_trunc=depth_trunc)
    return rgbd


def delete_non_connected_components_from_mesh(mesh, min_component_size):
    triangle_clusters, cluster_n_triangles, cluster_area = (
        mesh.cluster_connected_triangles())
    triangle_clusters = np.asarray(triangle_clusters)
    cluster_n_triangles = np.asarray(cluster_n_triangles)
    triangles_to_remove = cluster_n_triangles[triangle_clusters] < min_component_size
    mesh.remove_triangles_by_mask(triangles_to_remove)
    return mesh
