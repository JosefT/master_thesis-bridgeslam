import torch

from GCNet.demo import NgeNet_pipeline

weight = "model_weights/color_adv/color_adv_GAT.pth"


class GCNModelAdapter:
    def __init__(self, use_cuda=True, voxel_size=0.025, vote_flag=True, weight_path=weight):
        self.model = NgeNet_pipeline(
            ckpt_path=weight_path,
            voxel_size=voxel_size,
            vote_flag=vote_flag,
            cuda=use_cuda)

    def fit(self, source, target, num_pts=5000):
        with torch.no_grad():
            return self.model.pipeline(source, target, npts=num_pts)
