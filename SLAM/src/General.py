import time

import numpy as np


def measure_time(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        duration = time.time() - start
        print("{} ran in {} sec.".format(func.__name__, "%.2f" % duration))
        return result

    return wrapper


def get_yellow():
    return np.array([1, 0.706, 0])


def get_blue():
    return np.array([0, 0.651, 0.929])


def quaternion_to_matrix(q):
    w, x, y, z = q
    return np.array([
        [1 - 2 * y ** 2 - 2 * z ** 2, 2 * x * y - 2 * w * z, 2 * x * z + 2 * w * y],
        [2 * x * y + 2 * w * z, 1 - 2 * x ** 2 - 2 * z ** 2, 2 * y * z - 2 * w * x],
        [2 * x * z - 2 * w * y, 2 * y * z + 2 * w * x, 1 - 2 * x ** 2 - 2 * y ** 2]
    ])


def matrix_to_quaternion(m):
    w = np.sqrt(1 + m[0, 0] + m[1, 1] + m[2, 2]) / 2
    x = (m[2, 1] - m[1, 2]) / (4 * w)
    y = (m[0, 2] - m[2, 0]) / (4 * w)
    z = (m[1, 0] - m[0, 1]) / (4 * w)
    return np.array([w, x, y, z])


def quaternion_from_rotation(rotation):
    theta = np.linalg.norm(rotation)
    if theta < 1e-6:
        return np.array([1, 0, 0, 0])
    else:
        axis = rotation / theta
        q = np.array(
            [np.cos(theta / 2), axis[0] * np.sin(theta / 2), axis[1] * np.sin(theta / 2), axis[2] * np.sin(theta / 2)])
        return q


def make_positive_semi_definite(matrix):
    eigenvalues, eigenvectors = np.linalg.eig(matrix)
    negative_indices = np.where(eigenvalues <= 0.0000001)[0]
    if len(negative_indices) > 0:
        eigenvalues[negative_indices] = 0.0000001
        matrix = eigenvectors.dot(np.diag(eigenvalues)).dot(np.linalg.inv(eigenvectors))
    return matrix
