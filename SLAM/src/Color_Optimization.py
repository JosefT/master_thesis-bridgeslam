import cv2
import numpy as np

from src.metrics import get_rotation_difference_as_degree


def get_blurriness(rgb):
    image = rgb
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    variance = cv2.Laplacian(gray, cv2.CV_64F).var()
    return variance


def get_rgbs_indices_for_color_optimization(frames, trajectory, rotation_threshold=10, translation_threshold=0.3,
                                            use_first_and_last_frame=True):
    # Split frames based on rotation and translation in trajectory

    best_frame_blurriness = -1
    best_frame_idx = -1

    cur_rot_init = trajectory[0][:3, :3]
    cur_trans_init = trajectory[0][:3, 3]
    best_frame_indices = []
    for frame_idx in range(len(frames)):
        rot_diff = get_rotation_difference_as_degree(cur_rot_init, trajectory[frame_idx][:3, :3])
        trans_diff = np.linalg.norm(cur_trans_init - trajectory[frame_idx][:3, 3])
        if rot_diff > rotation_threshold or trans_diff > translation_threshold:
            best_frame_indices.append(best_frame_idx)
            if len(frames) - 1 > frame_idx:
                cur_rot_init = trajectory[frame_idx + 1][:3, :3]
                cur_trans_init = trajectory[frame_idx + 1][:3, 3]
                best_frame_idx = -1
                best_frame_blurriness = - 1
        blurriness = get_blurriness(cv2.imread(frames[frame_idx].color_path))
        if frames[frame_idx].active and blurriness > best_frame_blurriness:
            best_frame_idx = frame_idx
            best_frame_blurriness = blurriness

    # Add first and last frame to avoid having missing data
    best_frame_indices = np.array(list(set(best_frame_indices)))
    best_frame_indices.sort()
    return best_frame_indices
