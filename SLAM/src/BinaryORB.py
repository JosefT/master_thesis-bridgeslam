from concurrent.futures import ThreadPoolExecutor

import cv2
import faiss
import numpy as np
from joblib import Parallel, delayed
from matplotlib import pyplot as plt
from pyclustering.cluster.kmedians import kmedians
from tqdm import tqdm

from General import measure_time
from Tree import TreeLeaf, TreeNode


@measure_time
def get_ORB_Descriptors(frames):
    def get_img_descriptors(path):
        descriptors_nonbinary = \
        cv2.ORB_create().detectAndCompute(np.asarray(cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2GRAY)), None)[1]
        return get_binary_descriptors(descriptors_nonbinary)

    print("Extract Descriptors")
    descriptors = Parallel(n_jobs=-1)(delayed(get_img_descriptors)(frame.color_path) for frame in frames)
    return descriptors


def get_binary_descriptors(descriptors):
    binary_descriptors = []
    for descriptor in descriptors:
        binary_array = np.zeros(256, dtype=bool)
        for i in range(8):
            binary_array[i::8] = (descriptor & (1 << (7 - i))) > 0

        binary_descriptors.append(binary_array)
    binary_descriptors = np.array(binary_descriptors)
    return binary_descriptors


def l1norm(x, y):
    # check if x is 2d array
    if len(x.shape) == 2:
        distances = np.zeros(x.shape[0])
        for i in range(x.shape[0]):
            a = x[i] ^ y
            sum_result = np.sum(a)
            distances[i] = sum_result
        return distances
    # return np.sum(np.abs(x - y), axis=1)
    else:
        return np.sum(np.abs(x - y))


def kmeans_plusplus(x, k=10):
    # get kmeans++ centers
    centers = []
    centers.append(x[np.random.randint(0, len(x))])
    distances = []
    for i in range(1, k):
        distances.append(l1norm(x, centers[-1]) ** 2)
        # calculate probabilities
        min_distances = np.min(distances, axis=0)
        probabilities = min_distances / np.sum(min_distances)

        new_center = x[np.random.choice(np.arange(len(x)), p=probabilities)]
        centers.append(new_center)
    return centers


def perform_kMedian(x, k=10, n_samples=1000):
    if len(x) < n_samples:
        samples = x
    else:
        sample_indices = np.random.randint(0, len(x), n_samples)
        samples = x[sample_indices]
    initial_centers = kmeans_plusplus(samples, k=k)
    kmedians_instance = kmedians(samples, initial_centers, tolerance=4.1)
    kmedians_instance.process()
    medians_floored = np.floor(kmedians_instance.get_medians()).astype(bool)
    return medians_floored


def get_assignments(x, centers):
    # use faiss for faster assignment
    index = faiss.IndexFlatL2(256)
    index.add(centers)
    distances, assignments = index.search(x, 1)
    assignments = assignments.reshape(-1)
    return assignments


def createTree(root, binary_descriptors, depth, k=10, n_samples=1000):
    centers = perform_kMedian(binary_descriptors, k=k, n_samples=n_samples)
    cluster_assignments = get_assignments(binary_descriptors, centers)
    unique, count = np.unique(cluster_assignments, return_counts=True)
    for idx, center in enumerate(centers):
        if count[idx] < 50 or depth > 2:
            root.add_child(TreeLeaf(center, depth=depth, count=count[idx], word_id=3))
        else:
            root.add_child(TreeNode(center, depth=depth, count=count[idx]))
    depth += 1
    with ThreadPoolExecutor(max_workers=8) as executor:
        for c_id, child in enumerate(root.children):
            if not child.is_leaf():
                executor.submit(createTree, child, binary_descriptors[cluster_assignments == c_id], depth, k=k,
                                n_samples=n_samples)


def process_leaves(root, leaves):
    # add a word_id to each leaf and store leaf references in a list
    for child in root.children:
        if child.is_leaf():
            child.word_id = len(leaves)
            leaves.append(child)
        else:
            process_leaves(child, leaves)


def print_leaf_info(leaves):
    print(len(leaves))
    # print leaf metrics:
    mean_count = 0
    max_count = 0
    min_count = 10000
    for leaf in leaves:
        mean_count += leaf.count
        if leaf.count > max_count:
            max_count = leaf.count
        if leaf.count < min_count:
            min_count = leaf.count
    mean_count /= len(leaves)
    print("mean count: ", mean_count)
    print("max count: ", max_count)
    print("min count: ", min_count)


@measure_time
def construct_BoW(descriptor_lengths, binary_descriptors, root, n_words):
    def get_BoW(limits):
        descriptors = binary_descriptors[limits[0]:limits[1]]
        histogram = np.histogram(get_words(descriptors, root), bins=bins)[0]
        return histogram

    print("Constructing BoW")
    bins = np.arange(n_words)
    histograms = []
    # get array of starts and ends:
    limits = []
    start = 0
    for i in range(len(descriptor_lengths)):
        end = start + descriptor_lengths[i]
        limits.append((start, end))
        start = end
    histograms = Parallel(n_jobs=-1)(delayed(get_BoW)(limits[i]) for i in range(len(limits)))
    return histograms


def get_words(descriptors, root):
    words = []
    for descriptor in descriptors:
        words.append(root.traverse_tree(descriptor))
    return words


def l1_score(x, y):
    sum0 = np.sum(x)
    sum1 = np.sum(y)
    norm_vec0 = x / sum0
    norm_vec1 = y / sum1
    return 1 - 0.5 * np.sum(np.abs(norm_vec0 - norm_vec1))


@measure_time
def get_TF_IDF(BoWs):
    print("Calculating TF-IDFs")
    df = np.count_nonzero(BoWs, axis=0)
    TF_IDFs = []
    idf = np.log(len(BoWs) / (df + 1))
    for BoW in tqdm(BoWs):
        # Calculate ltc tfidf
        tf = np.log(BoW + 1)

        tfidf = tf * idf
        # apply cosine normalization
        tfidf /= np.sqrt(np.sum(tfidf ** 2))  # TODO try without
        TF_IDFs.append(tfidf)
    return TF_IDFs


@measure_time
def get_scores(TF_IDFs):
    print("calculate scores between all TF_IDFs")
    score_matrix = np.zeros((len(TF_IDFs), len(TF_IDFs)))
    for i in tqdm(range(len(TF_IDFs))):
        for j in range(i + 1, len(TF_IDFs)):
            score_matrix[i, j] = l1_score(TF_IDFs[i], TF_IDFs[j])
            score_matrix[j, i] = score_matrix[i, j]
    return score_matrix


def eta(scores, t, j):
    if t == 0:
        return scores[t, j] / (scores[t, t + 1] + 0.00001)
    return scores[t, j] / (scores[t, t - 1] + 0.00001)


@measure_time
def get_island_scores(scores, fragment_collector, alpha=0.3):
    frame_boundaries = np.concatenate([[0], np.cumsum([len(fragment.frames) for fragment in fragment_collector])])
    print(frame_boundaries)
    HScore_matrix = np.zeros((scores.shape[0], len(fragment_collector)))
    for frame_idx in tqdm(range(0, scores.shape[0])):
        Hscores = np.zeros(len(fragment_collector))
        fragment_idx_of_frame = np.argmax(frame_boundaries > frame_idx) - 1
        if fragment_idx_of_frame > 1:
            fragment_ids = np.concatenate(
                [np.arange(0, fragment_idx_of_frame - 1),
                 np.arange(fragment_idx_of_frame + 2, len(fragment_collector))])
        else:
            fragment_ids = np.concatenate(
                [np.arange(fragment_idx_of_frame + 2, len(fragment_collector))])

        for f_id in fragment_ids:
            for j in range(frame_boundaries[f_id], frame_boundaries[f_id + 1]):
                score = eta(scores, frame_idx, j)
                if score > alpha:
                    Hscores[f_id] += score
        HScore_matrix[frame_idx] = Hscores / [len(fragment.frames) for fragment in fragment_collector]
    return HScore_matrix


@measure_time
def extract_island_LC_pairs(beta, fragment_collector, frame_boundaries, island_scores):
    visual_candidates = []
    counting_array = np.zeros(len(fragment_collector))  #
    cur_boundary = frame_boundaries[0]
    n_frames = frame_boundaries[0]
    cur_frag_idx = 0
    HscoreMean = np.zeros(len(fragment_collector))
    scorematrix = []
    for idx, Hscore in enumerate(island_scores):

        best = np.argsort(Hscore)[::-1]
        for i in range(len(best)):
            HscoreMean[best[i]] += Hscore[best[i]]
            if Hscore[best[i]] > beta:
                counting_array[best[i]] += 1
        if idx >= cur_boundary:
            HscoreMean /= n_frames
            scorematrix.append(HscoreMean)
            for frag_idx, count in enumerate(counting_array):
                if count > n_frames * 0.7:
                    # check if pair already exists
                    already_exists = False
                    for candidate in visual_candidates:
                        if candidate[0] == cur_frag_idx and candidate[1] == cur_frag_idx:
                            already_exists = True
                            break
                    if not already_exists:
                        visual_candidates.append([cur_frag_idx, frag_idx, count / n_frames, HscoreMean[frag_idx]])
            counting_array = np.zeros(len(fragment_collector))
            cur_frag_idx += 1
            cur_boundary = frame_boundaries[cur_frag_idx]
            n_frames = frame_boundaries[cur_frag_idx] - frame_boundaries[cur_frag_idx - 1]
            HscoreMean = np.zeros(len(fragment_collector))
    if False:  # plot score matrix for debugging
        fig, ax = plt.subplots(figsize=(20, 20))
        cax = ax.matshow(np.asarray(scorematrix), interpolation='nearest')
        ax.grid(True)
        plt.title('Score Matrix')
        fig.colorbar(cax, ticks=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, .75, .8, .85, .90, .95, 1])
        plt.show()
    return visual_candidates
