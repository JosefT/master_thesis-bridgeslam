import copy
import math
from itertools import chain

import numpy as np
import open3d as o3d
import torch
import transforms3d
from numba import jit
from scipy.stats import multivariate_normal

from src.General import make_positive_semi_definite
from src.Open3d_Tools import segment_pcd_with_plane
from src.VisualizationUtils import draw_bell_curves


def get_rotation_difference_as_degree(rotation1, rotation2):
    error_mat = np.dot(np.linalg.inv(rotation1), rotation2)
    _, angle = transforms3d.axangles.mat2axangle(error_mat)
    return abs(angle * (180 / np.pi))


def get_normal_similarity(pointcloud0, pointcloud1, icp_result):
    transformation = icp_result.transformation
    correspondences = np.array(icp_result.correspondence_set)
    pointcloud0 = copy.deepcopy(pointcloud0).transform(transformation)
    pointcloud1 = copy.deepcopy(pointcloud1)
    normals0 = np.asarray(pointcloud0.normals)[correspondences[:, 0]]
    normals1 = np.asarray(pointcloud1.normals)[correspondences[:, 1]]
    dot_product = [np.dot(normals0[i], normals1[i]) for i in range(0, len(normals1))]
    similarity = 1 - (np.arccos(dot_product) / math.pi)
    return np.mean(similarity)


def NDT_score(source, target, icp_result, voxel_size, box_size=0.5, min_pt_thres=0.25, remove_ground_floor=True,
              visualize=False, visualize_threshold=None):
    def use_box(n_pts_in_box, n_pts_total, n_boxes, threshold):
        avg_pts_per_box = n_pts_total / n_boxes
        return n_pts_in_box > (avg_pts_per_box * threshold)

    transformation = icp_result.transformation
    # NDT = Normal Distributions Transform
    source_cp = copy.deepcopy(source).transform(transformation)
    # remove statistical outliers from both
    # source_cp = source_cp.remove_statistical_outlier(30, 1.5)[0]
    target_cp = copy.deepcopy(target)  # .remove_statistical_outlier(30, 1.5)[0]
    # get axis aligned bounding box of target
    # check if majority of points are on a flat surface:
    if remove_ground_floor:
        ratio, outlier_cloud = segment_pcd_with_plane(target_cp, voxel_size * 2)
        if 0.6 < ratio < 0.9:
            target_cp = outlier_cloud
            _, source_cp = segment_pcd_with_plane(source_cp, voxel_size * 2)

    bbox = target_cp.get_axis_aligned_bounding_box()
    bb_min = np.asarray(bbox.min_bound)
    bb_max = np.asarray(bbox.max_bound)
    # create grid for bbox with voxel size
    grid = o3d.geometry.VoxelGrid().create_from_point_cloud_within_bounds(target_cp, box_size, bb_min, bb_max)
    # get min and max bound of each grid cell
    bboxes = []
    # Compue lineset for grid
    for cell in grid.get_voxels():
        # get coordinates of cell based on bounding box min and maxbound
        min_cell = cell.grid_index * box_size + bb_min
        max_cell = (cell.grid_index + 1) * box_size + bb_min
        bboxes.append(o3d.geometry.AxisAlignedBoundingBox(min_bound=min_cell, max_bound=max_cell))
        bboxes[-1].color = (1, 0, 0)
        # get points in cell

    gaussians = []
    src_scores = []
    # tgt_scores = []
    n_src_points_evaluted = 0
    n_points_tgt = len(target_cp.points)
    used_bboxes = []
    for bbox in bboxes:
        # crop both volumes with bbox
        target_points_in_bbox = np.asarray(target_cp.crop(bbox).points)
        source_points_in_bbox = np.asarray(source_cp.crop(bbox).points)
        n_tgt_points_in_bbox = len(target_points_in_bbox)
        if not use_box(n_tgt_points_in_bbox, n_points_tgt, len(bboxes), min_pt_thres) or len(
                source_points_in_bbox) < n_tgt_points_in_bbox * 0.25:
            continue
        used_bboxes.append(bbox)
        # visualize co
        mean = np.mean(target_points_in_bbox, axis=0)
        # shift points to mean
        mean_shifted_pts = target_points_in_bbox - mean
        cov_matrix = np.cov(mean_shifted_pts, rowvar=False)

        try:
            gaussian = multivariate_normal(mean=mean, cov=cov_matrix)
        except:
            cov_matrix = make_positive_semi_definite(cov_matrix)
            gaussian = multivariate_normal(mean=mean, cov=cov_matrix)
        gaussians.append(gaussian)
        # get scaling term
        # scaling = 1. / np.sqrt(((2 * np.pi)**3 * np.linalg.det(cov_matrix)))
        # print(scaling)
        # num_samples = 1000
        # random_points = np.random.uniform(low=bbox.min_bound,
        #                                  high=bbox.max_bound,
        #                                  size=(num_samples, 3))
        # probability = np.sum(gaussian.pdf(random_points))
        # xi_1 = probability / num_samples
        # inv_scaling = np.sqrt(((2 * np.pi) ** 3 * np.linalg.det(cov_matrix)))
        pdf_results = gaussian.pdf(source_points_in_bbox)
        pdf_results = [max(x, 0.0001) for x in pdf_results]
        scores_src = np.log(pdf_results)

        # scores_tgt = np.log(gaussian.logpdf(target_points_in_bbox) + 0.001)
        src_scores.append(scores_src)
        # tgt_scores.append(scores_tgt)
        n_src_points_evaluted += len(source_points_in_bbox)

    # evalute source_pcd with gaussians:

    # means_of_gaussians = np.asarray([gaussian.mean for gaussian in gaussians])
    evaluated_point_ratio = n_src_points_evaluted / len(source_cp.points)
    if evaluated_point_ratio < 0.1:
        print("Too few points of the source were evaluated. Ratio: ", evaluated_point_ratio)
        return -1
    else:
        scores_per_gaussian = [np.mean(row) for row in src_scores]
        scores_flattened = list(chain.from_iterable(src_scores))
        org_score = np.mean(scores_flattened)

        # tgt_scores_per_gaussian = [np.mean(row) for row in tgt_scores]
        # score_dif = np.divide(np.asarray(scores_per_gaussian), np.asarray(tgt_scores_per_gaussian))
        # rel_score = np.mean(score_dif)

        # Use ratio to decide whether too much points were excluded to give a good score. This is analogous to measuring the
        # overlap of the two pointclouds (or the remaining segments, if the plane was removed)

    # calculate scores
    # final_score = np.mean(scores) # The more negative the better

    if (visualize and visualize_threshold is None) or (visualize and org_score < visualize_threshold):
        draw_bell_curves(source_cp, target_cp, used_bboxes, gaussians, scores_per_gaussian, box_size, score=org_score)
    return org_score


def get_closest_gaussian_and_compute_score(point, mean_pts, gaussians):
    scores = []
    distances = np.linalg.norm(mean_pts - point, axis=1)
    closest_gaussian = gaussians[np.argmin(distances)]
    # print(closest_gaussian.pdf(point))
    score = - np.log(closest_gaussian.pdf(point) + 0.1)
    return score


# Copied from sw_approx.py in repository https://github.com/FlorentinCDX/Fast-Approx-SW.git
def sliced_wasserstein_distance(pcd0, pcd1) -> float:
    def m_2(X):
        return torch.mean(torch.pow(X, 2), dim=0)

    '''
    """
    Central Limite Theorem approximation of the Sliced Wasserstein distance

    .. math::
        \widehat{\mathbf{S W}}_{2}^{2}\left(\mu_{d}, \nu_{d}\right)=\mathbf{W}_{2}^{2}\left\{\mathrm{~N}\left(0, \mathrm{~m}_{2}\left(\bar{\mu}_{d}\right)\right), \mathrm{N}\left(0, \mathrm{~m}_{2}\left(\bar{\nu}_{d}\right)\right)\right\}+(1 / d)\left\|\mathbf{m}_{\mu_{d}}-\mathbf{m}_{\nu_{d}}\right\|^{2}


    Parameters
    ----------
    mu : ndarray, shape (n_samples_a, dim)
        samples in the source domain
    nu : ndarray, shape (n_samples_b, dim)
        samples in the target domain

    Returns
    -------
    cost: float
        Sliced Wasserstein Cost
    '''

    mu = torch.from_numpy(np.asarray(pcd0.points))
    nu = torch.from_numpy(np.asarray(pcd1.points))

    m_mu = torch.mean(mu, dim=0)
    m_nu = torch.mean(nu, dim=0)
    ### First lets compute d:=W2{N(0, m2(µd_bar)), N(0, m2(νd_bar))}
    # Centered version of mu and nu
    mu_bar = mu - m_mu
    nu_bar = nu - m_nu
    # Compute Wasserstein beetween two centered gaussians
    W = torch.pow(torch.sqrt(m_2(mu_bar)) - torch.sqrt(m_2(nu_bar)), 2)

    ## Compute the mean residuals
    d = mu.size(1)
    res = (1 / d) * torch.pow(m_mu - m_nu, 2)

    ## Approximation of the Sliced Wasserstein
    return torch.norm(W + res, p=2).item()


@jit(nopython=True)
def determinant_3x3(matrix):
    a, b, c = matrix[0]
    d, e, f = matrix[1]
    g, h, i = matrix[2]

    return a * (e * i - f * h) - b * (d * i - f * g) + c * (d * h - e * g)


@jit(nopython=True)
def ComputeInformation(points, covariances):
    num_points = len(points)
    if num_points == 0:
        return 0.
    const = 2 * math.pi * math.e
    entropy = np.empty(num_points)

    for i in range(num_points):
        covariance = covariances[i]
        determinant = determinant_3x3(covariance)
        if determinant == 0 or np.isnan(determinant):
            entropy[i] = 0.
        h_i = 0.5 * math.log(const * determinant)
        # if not np.isnan(h_i) and not np.isinf(h_i):
        entropy[i] = h_i
    total_entropy = np.sum(entropy)
    return total_entropy


def corAL(source, target, reg_result, beta0=0, beta1=1, beta2=-1):
    src_cpy = copy.deepcopy(source).transform(reg_result.transformation)
    tgt = copy.deepcopy(target)
    src_cpy.estimate_covariances()
    tgt.estimate_covariances()
    corrs = np.asarray(reg_result.correspondence_set)
    corrs_src = np.unique(np.asarray(corrs)[:, 0])
    corrs_tgt = np.unique(np.asarray(corrs)[:, 1])
    src_corr_pcd = src_cpy.select_by_index(corrs_src)
    tgt_corr_pcd = tgt.select_by_index(corrs_tgt)
    H_src = ComputeInformation(np.asarray(src_corr_pcd.points), np.asarray(src_corr_pcd.covariances))
    H_tgt = ComputeInformation(np.asarray(tgt_corr_pcd.points), np.asarray(tgt_corr_pcd.covariances))
    H_sep = (H_src + H_tgt) / (len(src_corr_pcd.points) + len(src_corr_pcd.points))

    pcd_joint = src_cpy + tgt
    H_joint = ComputeInformation(np.asarray(pcd_joint.points), np.asarray(pcd_joint.covariances)) / len(
        pcd_joint.points)

    z = beta0 + beta1 * H_joint + beta2 * H_sep
    p = 1 / (1 + np.exp(-z))

    return p
