import csv
import numpy as np
import yaml

from Frame import Frame


def get_frames(sequence_path, n_frames):
    frames = []
    for i in range(0, n_frames):
        frame = Frame(sequence_path + "rgb/{}.png".format(i), sequence_path + "depth/{}.png".format(i),
                      sequence_path + "pcd/{}.pcd".format(i))
        frames.append(frame)
    return frames


def read_sensor_data(filename):
    with open(filename, mode='r') as file:
        # Read CSV file using the csv module
        reader = csv.reader(file)
        next(reader)  # skip header row
        data = []
        for row in reader:
            # Parse each row and append to data list
            timestamp = float(row[0])
            ax, ay, az = float(row[1]), float(row[2]), float(row[3])
            gx, gy, gz = float(row[4]), float(row[5]), float(row[6])
            data.append([timestamp, ax, ay, az, gx, gy, gz])

    # Convert data list to numpy array
    data = np.array(data)

    # Extract gyro and accel data into separate arrays
    return data


def get_imu_data(path):
    data = read_sensor_data(path + "/imu_data.csv")
    return data


def get_config_file(path):
    with open(path, 'r') as file:
        config = yaml.safe_load(file)
    return config
