import open3d.core as o3c
import numpy as np
import open3d as o3d

def estTransformationByModelAndICP(src, tgt, model, max_corr_dist=0.03, p2p=True, k=0.1, voxel_size=0.01):
    # Estimate Normals
    if not src.has_normals():
        src.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=voxel_size*4, max_nn=30))
    if not tgt.has_normals():
        tgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=voxel_size*4, max_nn=30))
    # Estimate Transformation
    model_transformation = model.fit(src, tgt)

    # Choose ICP Method

    if p2p:
        icp_method = o3d.pipelines.registration.TransformationEstimationPointToPoint()
        icp_result = o3d.pipelines.registration.registration_icp(src, tgt, max_corr_dist, model_transformation, icp_method)
    else:
        icp_result = performRobustKernelICP(src, tgt, max_corr_dist, model_transformation, k)

    # Return intermediate and final result
    return icp_result, model_transformation

def performRobustKernelICP(src, tgt, max_corr_dist, trans_init, k=0.1):
    loss = o3d.pipelines.registration.TukeyLoss(k=k)
    icp_method = o3d.pipelines.registration.TransformationEstimationPointToPlane(loss)
    icp_result = o3d.pipelines.registration.registration_icp(src, tgt, max_corr_dist, trans_init, icp_method)
    return icp_result

def estTransformationByModelAndColorICP(source, target, model, voxel_size=0.01):
    T = model.fit(source, target)
    try:
        result = simple_color_icp(source, target, voxel_size=voxel_size, initial_transformation=T)
    except:
        result = simple_point_to_plane_icp(source, target, voxel_size=voxel_size, initial_transformation=T)
    return result

def simple_point_to_point_icp(src, tgt, voxel_size, initial_transformation = np.eye(4),
               icp_method=o3d.pipelines.registration.TransformationEstimationPointToPoint()):
    if not src.has_normals():
        src.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.4, max_nn=30))
    if not tgt.has_normals():
        tgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.4, max_nn=30))
    icp_result = o3d.pipelines.registration.registration_icp(src, tgt, voxel_size, initial_transformation, icp_method)
    return icp_result

def simple_point_to_plane_icp(src, tgt, voxel_size, initial_transformation = np.eye(4),
               icp_method=o3d.pipelines.registration.TransformationEstimationPointToPlane()):
    if not src.has_normals():
        src.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.4, max_nn=30))
    if not tgt.has_normals():
        tgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.4, max_nn=30))
    icp_result = o3d.pipelines.registration.registration_icp(src, tgt, voxel_size, initial_transformation, icp_method)
    return icp_result

def simple_color_icp(src, tgt, voxel_size, initial_transformation = np.eye(4)):
    icp_method = o3d.pipelines.registration.TransformationEstimationForColoredICP()
    return o3d.pipelines.registration.registration_colored_icp(src, tgt, voxel_size, initial_transformation, icp_method)

def simple_point_to_plane_icp_cuda(src, tgt, voxel_size, initial_transformation = np.eye(4)):
    src_t = o3d.t.geometry.PointCloud().from_legacy(src).cuda()
    tgt_t = o3d.t.geometry.PointCloud().from_legacy(tgt).cuda()
    # convert initial_transformation to tensor
    initial_transformation = o3c.Tensor(initial_transformation).cuda()
    p2p = o3d.t.pipelines.registration.TransformationEstimationPointToPlane()
    result = o3d.t.pipelines.registration.icp(src_t, tgt_t, voxel_size, initial_transformation, p2p)
    return result

def get_information_matrix_for_transformation(pcd0, pcd1, voxel_size, transformation):
    return o3d.pipelines.registration.get_information_matrix_from_point_clouds(pcd0, pcd1, voxel_size, transformation)

"""
def icp(src_frame: PointCloud, dst_frame: PointCloud, initial_transformation = np.eye(4),
        icp_method=o3d.pipelines.registration.TransformationEstimationPointToPlane()):
    icp_result = o3d.pipelines.registration.registration_icp(src_frame.pcd_down,
                                                             dst_frame.pcd_down,
                                                             dst_frame.pcd_down_voxel_size,
                                                             initial_transformation,
                                                             icp_method)
    return icp_result


def icp_color(src_frame: PointCloud, dst_frame: PointCloud, initial_transformation = np.eye(4)):
    icp_result = o3d.pipelines.registration.registration_colored_icp(src_frame.pcd_down, dst_frame.pcd_down,
                                                                    dst_frame.pcd_down_voxel_size, initial_transformation,
                                                                    o3d.pipelines.registration.TransformationEstimationForColoredICP())
    return icp_result


def icp_global(src_frame: PointCloud, dst_frame: PointCloud, init_transf=np.eye(4)):
    max_correspondence_distance_coarse = src_frame.pcd_down_voxel_size * 3
    max_correspondence_distance_fine = src_frame.pcd_down_voxel_size
    # Coarse ICP
    icp_coarse = o3d.pipelines.registration.registration_icp(
        src_frame.pcd_down, dst_frame.pcd_down, max_correspondence_distance_coarse, init_transf,
        o3d.pipelines.registration.TransformationEstimationPointToPlane())
    # Fine ICP
    icp_fine = o3d.pipelines.registration.registration_icp(
        src_frame.pcd_down, dst_frame.pcd_down, max_correspondence_distance_fine,
        icp_coarse.transformation,
        o3d.pipelines.registration.TransformationEstimationPointToPlane())

    return icp_fine


def rigid_registration(source:PointCloud, target:PointCloud, init_transf=np.eye(4), global_registration=False, colored_icp=False, point_to_point=False):

    if global_registration and colored_icp:
        icp_result_pre = icp_global(source, target, init_transf)
        icp_result = icp_color(source, target, icp_result_pre.transformation)
    elif global_registration:
        icp_result = icp_global(source, target, init_transf)
    elif colored_icp:
        icp_result = icp_color(source, target, init_transf)
    else:
        if point_to_point:
            method = o3d.pipelines.registration.TransformationEstimationPointToPoint()
        else:
            method = o3d.pipelines.registration.TransformationEstimationPointToPlane()
        icp_result = icp(source, target, init_transf, method)
    return icp_result


def rigid_registration_o3d(source:o3d.geometry.PointCloud, target:o3d.geometry.PointCloud, init_transf=np.eye(4), global_registration=False, colored_icp=False):
    source = PointCloud(source, pcd_down_voxel_size=0.05)
    target = PointCloud(target, pcd_down_voxel_size=0.05)

    return rigid_registration(source, target, init_transf, global_registration, colored_icp)


def rigid_registration_with_information_o3d(source:o3d.geometry.PointCloud, target:o3d.geometry.PointCloud, global_registration=False, init_transf=np.eye(4)):
    source = PointCloud(source, pcd_down_voxel_size=0.05)
    target = PointCloud(target, pcd_down_voxel_size=0.05)

    icp_result = rigid_registration(source, target, init_transf, global_registration)
    information = o3d.pipelines.registration.get_information_matrix_from_point_clouds(source.pcd_down, target.pcd_down,
                                                                                      target.pcd_down_voxel_size
                                                                                      , icp_result.transformation)
    return icp_result, information


def rigid_registration_with_information(source:PointCloud, target:PointCloud, global_registration=False, init_transf=np.eye(4)):
    icp_result = rigid_registration(source, target, global_registration, init_transf)
    information = o3d.pipelines.registration.get_information_matrix_from_point_clouds(source.pcd_down, target.pcd_down,
                                                                                      target.pcd_down_voxel_size
                                                                                      , icp_result.transformation)
    return icp_result, information


def ransac(src_frame: PointCloud, dst_frame: PointCloud):
    src = src_frame.pcd_down
    tgt = dst_frame.pcd_down

    # Compute FPFH features

    radius_feature = src_frame.pcd_down_voxel_size * 5
    source_fpfh = o3d.pipelines.registration.compute_fpfh_feature(src, o3d.geometry.KDTreeSearchParamHybrid(
        radius=radius_feature, max_nn=50))
    target_fpfh = o3d.pipelines.registration.compute_fpfh_feature(tgt, o3d.geometry.KDTreeSearchParamHybrid(
        radius=radius_feature, max_nn=50))

    distance_threshold = src_frame.pcd_down_voxel_size * 1.5
    result = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(
        src, tgt, source_fpfh, target_fpfh, True,
        distance_threshold,
        o3d.pipelines.registration.TransformationEstimationPointToPoint(False),
        3, [
            o3d.pipelines.registration.CorrespondenceCheckerBasedOnEdgeLength(
                0.9),
            o3d.pipelines.registration.CorrespondenceCheckerBasedOnDistance(
                distance_threshold)
        ], o3d.pipelines.registration.RANSACConvergenceCriteria(100000, 0.999))
    return result


def ransac_and_icp(src_frame: PointCloud, dst_frame: PointCloud):
    ransac_result = ransac(src_frame, dst_frame)
    method = o3d.pipelines.registration.TransformationEstimationPointToPlane()
    icp_result = icp(src_frame, dst_frame, ransac_result.transformation, method)
    return icp_result


def ransac_and_icp_o3d(source: o3d.geometry.PointCloud, target: o3d.geometry.PointCloud):
    source = PointCloud(source, pcd_down_voxel_size=0.05)
    target = PointCloud(target, pcd_down_voxel_size=0.05)

    ransac_result = ransac(source, target)
    method = o3d.pipelines.registration.TransformationEstimationPointToPlane()
    icp_result = icp(source, target, ransac_result.transformation, method)
    return icp_result


def establish_correspondances_through_fine_registration(source: PointCloud, target: PointCloud, transf_init):
    icp_method = o3d.pipelines.registration.TransformationEstimationPointToPoint()
    # small amount of iterations for fast alignment. Maybe less are also sufficient.
    criteria = o3d.pipelines.registration.ICPConvergenceCriteria(max_iteration=1)
    result_with_correspondances = o3d.pipelines.registration.registration_icp(source.pcd, target.pcd, 0.015,
                                                                              transf_init, icp_method,criteria)
    return result_with_correspondances





def calculate_transformation_using_features(pcd0, pcd1, max_correspondence_distance, keypoints0, keypoints1,
                                            descriptors0, descriptors1):
    # compute matches between source and target map
    matches = match_sift_descriptors_flann(descriptors0, descriptors1)
    if len(matches) < 10:
        print(f"Not enough matches found")
        return None
    src_kpts_filtered, tgt_kpts_filtered = filter_by_matches(matches, keypoints0, keypoints1)


    transformation = estimate_transformation_from_correspondances(src_kpts_filtered, tgt_kpts_filtered)
    if not pcd0.has_normals():
        pcd0.estimate_normals()
    if not pcd1.has_normals():
        pcd1.estimate_normals()
    icp_method = o3d.pipelines.registration.TransformationEstimationPointToPlane()
    result = o3d.pipelines.registration.registration_icp(pcd0, pcd1, max_correspondence_distance, transformation,
                                                         icp_method)
    return result


def estimate_transformation_from_correspondances(source_kpts, target_kpts):
    def kabsch_umeyama(A, B):
        assert A.shape == B.shape
        n, m = A.shape
        EA = np.mean(A, axis=0)
        EB = np.mean(B, axis=0)
        VarA = np.mean(np.linalg.norm(A - EA, axis=1) ** 2)
        H = ((A - EA).T @ (B - EB)) / n
        U, D, VT = np.linalg.svd(H)
        d = np.sign(np.linalg.det(U) * np.linalg.det(VT))
        S = np.diag([1] * (m - 1) + [d])
        R = U @ S @ VT
        c = VarA / np.trace(np.diag(D) @ S)
        t = EA - c * R @ EB
        return R, c, t

    R, c, t = kabsch_umeyama(target_kpts, source_kpts)
    transformation = np.identity(4)
    transformation[:3, :3] = c * R
    transformation[:3, 3] = t
    return transformation 
"""