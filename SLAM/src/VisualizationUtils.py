import copy

import cv2
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
import pytransform3d.visualizer as pv
from pytransform3d.rotations import quaternion_from_matrix

from src.Open3d_Tools import points_to_pcd


def draw_registration_result(source, target, transformation=np.eye(4), uniform_color=True, correspondences=None,
                             rotate=True, draw=True, window_name=""):
    source_cp = copy.deepcopy(source)
    target_cp = copy.deepcopy(target)
    if uniform_color:
        source_cp.paint_uniform_color([1, 0.706, 0])
        target_cp.paint_uniform_color([0, 0.651, 0.929])
    source_cp = source_cp.transform(transformation)
    if rotate:
        deg_180_rot = np.eye(4)
        # deg_180_rot[0, 0] = -1
        deg_180_rot[1, 1] = -1
        deg_180_rot[2, 2] = -1
        source_cp = source_cp.transform(deg_180_rot)
        target_cp = target_cp.transform(deg_180_rot)

    if correspondences is not None:
        line_set = o3d.geometry.LineSet().create_from_point_cloud_correspondences(source_cp, target_cp, correspondences)
        if draw:
            o3d.visualization.draw_geometries([source_cp, target_cp, line_set], window_name=window_name)
        return [source_cp, target_cp, line_set]
    else:
        if draw:
            o3d.visualization.draw_geometries([source_cp, target_cp], window_name=window_name)
        return [source_cp, target_cp]


def display_inlier_outlier(cloud, ind):
    inlier_cloud = cloud.select_by_index(ind)
    outlier_cloud = cloud.select_by_index(ind, invert=True)

    print("Showing outliers (red) and inliers (gray): ")
    outlier_cloud.paint_uniform_color([1, 0, 0])
    inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8])
    o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])


def draw_registration_result_with_keypoints(source, target, source_kp, target_kp, transformation=np.eye(4)):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)

    target_temp += points_to_pcd(target_kp).paint_uniform_color([1, 0, 0])  # colored red
    source_temp += points_to_pcd(source_kp).paint_uniform_color([0, 1, 0])  # colored green

    # Apply transformation
    source_temp.transform(transformation)

    o3d.visualization.draw_geometries([source_temp, target_temp])


def draw_3d_points_open3d(points, transformation_matrices=None):
    pcds = []
    if len(np.shape(points[0])) == 1:
        wrapper_pcd = o3d.geometry.PointCloud()
        wrapper_pcd.points = o3d.utility.Vector3dVector(points)
        if transformation_matrices is not None:
            wrapper_pcd.transform(transformation_matrices[0])
        pcds.append(wrapper_pcd)
    elif len(np.shape(points[0])) == 2:
        pcds = []
        colors = np.random.rand(len(points), 3)
        for idx, pointset in enumerate(points):
            wrapper_pcd = o3d.geometry.PointCloud()
            wrapper_pcd.points = o3d.utility.Vector3dVector(pointset)
            wrapper_pcd.paint_uniform_color(colors[idx])
            if transformation_matrices is not None:
                wrapper_pcd.transform(transformation_matrices[idx])
            pcds.append(wrapper_pcd)
    else:
        print(np.shape(points))
        raise ValueError("Wrong shape for Points")
    o3d.visualization.draw_geometries(pcds)


def drawValuesAs3DPlot(coordinates):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    cmap = plt.cm.get_cmap('viridis')

    sc = ax.scatter(coordinates[0], coordinates[1], coordinates[2],
                    c=np.linspace(0, 1, len(coordinates[0])), cmap=cmap)
    cbar = plt.colorbar(sc)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    cbar.ax.set_ylabel('Time')

    plt.show()


def create_color_scale(score):
    cmap = plt.get_cmap('RdYlGn')  # Select the colormap (green to red)
    norm = mcolors.Normalize(vmin=0, vmax=1)  # Normalize the score between 0 and 1

    # Obtain the RGBA color corresponding to the normalized score
    color = cmap(norm(score))

    return color


def visualize_rotation(quaternion_rotations, fps=10):
    vertices = np.array([[-1, -0.25, -0.25],
                         [1, -0.25, -0.25],
                         [1, 0.25, -0.25],
                         [-1, 0.25, -0.25],
                         [-1, -0.25, 0.25],
                         [1, -0.25, 0.25],
                         [1, 0.25, 0.25],
                         [-1, 0.25, 0.25],

                         [-0.25, -0.25, 0.25],
                         [0.25, -0.25, 0.25],
                         [0.25, -0.25, 0.375],
                         [-0.25, -0.25, 0.375],
                         [-0.25, 0.25, 0.25],
                         [0.25, 0.25, 0.25],
                         [0.25, 0.25, 0.375],
                         [-0.25, 0.25, 0.375]])
    edges = [(0, 1), (1, 2), (2, 3), (3, 0),
             (4, 5), (5, 6), (6, 7), (7, 4),
             (0, 4), (1, 5), (2, 6), (3, 7),
             (8, 9), (9, 10), (10, 11), (11, 8),
             (12, 13), (13, 14), (14, 15), (15, 12),
             (8, 12), (9, 13), (10, 14), (11, 15)]

    # Create a mesh with the rotation
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for idx, rotation_q in enumerate(quaternion_rotations):
        vertices_rotated = []
        for vertex in vertices:
            vertices_rotated.append(rotation_q.rotate(vertex))
        vertices_rotated = np.array(vertices_rotated)
        ax.clear()
        for edge in edges:
            ax.plot(vertices_rotated[edge, 0], vertices_rotated[edge, 1], vertices_rotated[edge, 2], 'b')
        print(idx)
        # update the rotation matrix
        # set the plot limits and labels
        ax.set_xlim(-1, 1)
        ax.set_ylim(-1, 1)
        ax.set_zlim(-1, 1)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        # ax.view_init(elev=135, azim=90)
        # show the plot
        plt.pause(1. / fps)


def get_edges_as_lines(edges, poses):
    line_indices = []
    for i in range(len(edges)):
        start = edges[i].source_node_id
        end = edges[i].target_node_id
        line_indices.append([start, end])
    line_colors = [[0, 0, 1] for i in range(len(line_indices))]
    lineSet = o3d.geometry.LineSet()
    lineSet.points = o3d.utility.Vector3dVector(poses)
    lineSet.lines = o3d.utility.Vector2iVector(line_indices)
    lineSet.colors = o3d.utility.Vector3dVector(line_colors)
    return lineSet


def visualize_posegraph(graph):
    # Nodes:
    nodes = np.array(graph.nodes)
    # Edges:
    edges = np.array(graph.edges)
    fig = pv.figure()
    colors = np.empty((len(nodes), 3))
    poses = [node.pose[:3, 3] for node in nodes]
    for d in range(colors.shape[1]):
        colors[:, d] = np.linspace(0, 1, len(colors))
    fig.scatter(P=poses, s=0.1, c=colors)

    lc_lineSet_frag = get_edges_as_lines(edges, poses)
    fig.add_geometry(lc_lineSet_frag)
    fig.show()


"""
def visualize(input):
    # TODO: Optimize for automatic enpacking of arrays / lists

    if isinstance(input, o3d.geometry.PointCloud):
        o3d.visualization.draw_geometries([input])
    elif isinstance(input, (list, np.ndarray)) and all(isinstance(item, MapFragment) for item in input):
        for fragment in input:
            o3d.visualization.draw_geometries([fragment.map.pcd])
    elif isinstance(input, o3d.pipelines.registration.PoseGraph):
        visualize_posegraph(input)
"""


def print_statistics_of_array(arr):
    print("Mean: ", np.average(arr))
    # print standard deviation
    print("Std: ", np.std(arr))
    # print minimum distance
    print("Min: ", np.min(arr))
    # print maximum distance
    print("Max: ", np.max(arr))
    # print median distance
    print("Median: ", np.median(arr))
    # print 25% quantile distance
    print("25%: ", np.quantile(arr))
    # print 75% quantile distance
    print("75%: ", np.quantile(arr))
    # print 10% quantile distance
    print("10%: ", np.quantile(arr))
    # print 90% quantile distance
    print("90%: ", np.quantile(arr))


def visualize_rgb_pairs(pairs, color_file_names):
    # visualize pairs
    already_shown = set()
    for i, j, sim in pairs:
        # check whether pair was already shown in an interval of 10
        if any([abs(i - x) < 20 and abs(j - y) < 20 for x, y in already_shown]):
            continue
        already_shown.add((i, j))
        rgb0 = cv2.imread(color_file_names[i])
        rgb1 = cv2.imread(color_file_names[j])

        # concatenate rgbs to visualize them next to each other
        rgb = np.concatenate((rgb0, rgb1), axis=1)
        cv2_show(f"Frame {i} and {j} with similarity: {sim}", rgb)


def cv2_show(img, title="Image"):
    cv2.imshow(title, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def get_ellipsoid_from_trivariate_normal_dist(mu, cov, radius, color=(0, 0, 1), sig=3):
    ellipsoid = o3d.geometry.TriangleMesh().create_sphere(radius, 20)

    eigenvalues, eigenvectors = np.linalg.eig(cov)
    S_prime = np.diag(np.sqrt(eigenvalues) * sig)  # sig =  sigma level
    U = eigenvectors.dot(S_prime)
    U_homog = np.eye(4)
    U_homog[:3, :3] = U
    ellipsoid.transform(U_homog)
    # translate to bbox center
    ellipsoid.translate(mu)
    ellipsoid.paint_uniform_color(color)

    ellipsoid.compute_vertex_normals()
    return ellipsoid


def draw_bell_curves(src, tgt, bboxes, gaussians, scores, box_size, score=-1):
    src.paint_uniform_color([0.8, 0.8, 0.8])
    tgt.paint_uniform_color([0.5, 0.5, 0.5])

    # visualize gaussians as ellipsoids
    bell_curves = []
    for gaussian, sc in zip(gaussians, scores):
        # create ellipsoids
        random_samples = gaussian.rvs(100)
        # sample_score = np.mean(np.log(gaussian.pdf(random_samples) + 0.001))
        sc = np.clip(sc, -0.5, 0.5) + 0.5
        color = create_color_scale(sc)
        ellipsoid = get_ellipsoid_from_trivariate_normal_dist(gaussian.mean, gaussian.cov, radius=box_size,
                                                              color=color[:3], sig=2)
        bell_curves.append(ellipsoid)

    # reset_normals
    src.normals = o3d.utility.Vector3dVector(np.zeros((1, 3)))
    tgt.normals = o3d.utility.Vector3dVector(np.zeros((1, 3)))
    o3d.visualization.draw([src, tgt, *bboxes, *bell_curves], )


def visualize_lc_candidate_imgs(idx0, idx1, frames):
    source_rgb = cv2.imread(frames[idx0].color_path)
    target_rgb = cv2.imread(frames[idx1].color_path)
    # draw loop closure
    # append imgs
    fig, ax = plt.subplots(1, 2, figsize=(10, 5))
    ax[0].imshow(cv2.cvtColor(source_rgb, cv2.COLOR_BGR2RGB))
    ax[0].set_title(f'Image {idx0}')
    ax[0].axis('off')

    ax[1].imshow(cv2.cvtColor(target_rgb, cv2.COLOR_BGR2RGB))
    ax[1].set_title(f'Image {idx1}')
    ax[1].axis('off')
    plt.show()


def visualize_trajectory_and_LCs(trajectory, lc_candidates, points=None, fragment_candidates=None):
    def get_lcs_as_lines(lc_candidates, ts):
        line_indices = []

        colors = []
        color = [0., 0, 1.]
        for candidate in lc_candidates:
            start = candidate[0]
            end = candidate[1]
            line_indices.append([start, end])
            colors.append(color)

        lineSet = o3d.geometry.LineSet()
        lineSet.points = o3d.utility.Vector3dVector(ts)
        lineSet.lines = o3d.utility.Vector2iVector(line_indices)
        lineSet.colors = o3d.utility.Vector3dVector(colors)
        return lineSet

    fig = pv.figure()
    trajectory = [np.linalg.inv(pose) for pose in trajectory]
    rots = [pose[:3, :3] for pose in trajectory]
    rots_q = np.array([quaternion_from_matrix(rot) for rot in rots])
    ts = [pose[:3, 3] for pose in trajectory]
    trajectory_q = np.concatenate([ts, rots_q], axis=1)
    if len(lc_candidates) > 0:
        lc_lineSet = get_lcs_as_lines(lc_candidates, ts)
        fig.add_geometry(lc_lineSet)
    fig.plot_trajectory(P=trajectory_q, n_frames=100, s=0.1, c=[1, 0, 0])
    if points is not None:
        colors = np.empty((len(points), 3))
        for d in range(colors.shape[1]):
            colors[:, d] = np.linspace(0, 1, len(colors))
        fig.scatter(P=points, s=0.1, c=colors)
        if fragment_candidates is not None:
            lc_lineSet_frag = get_lcs_as_lines(fragment_candidates, points)
            fig.add_geometry(lc_lineSet_frag)
    fig.show()


def draw_func(group, name, geometries):
    if group is None:
        group = ""
    if name is None:
        name = ""
    if not isinstance(geometries, list):
        geometries = [geometries]
    o3d.visualization.draw_geometries(geometries, window_name=group + " " + name)
    return
