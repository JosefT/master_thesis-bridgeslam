import numpy as np


# Convention for all functions: t = translation vector, R = rotation matrix, T = transformation matrix, q = quaternion

def rotate_in_center(pcd, current_translation, rotation_matrix):
    pcd.rotate(rotation_matrix, center=current_translation)
    return pcd


def rotate_vector(v, q):
    q_conj = np.array([q[0], -q[1], -q[2], -q[3]])
    v_quat = np.concatenate(([0], v))
    v_rot_quat = q * v_quat * q_conj
    v_rot = v_rot_quat[1:]
    return v_rot


def R_to_T(rotation):
    matrix = np.eye(4)
    matrix[:3, :3] = rotation
    return matrix  #


def updateRotationOfT(T, R_update):
    # decompose
    t = T[:3, 3]
    R = T[:3, :3]
    # update rotation:
    R = R_update @ R
    # compose
    T = R_and_t_to_T(R, t)
    return T


def t_to_T(translation):
    matrix = np.eye(4)
    matrix[:3, 3] = translation
    return matrix


def R_and_t_to_T(rotation, translation):
    matrix = np.eye(4)
    matrix[:3, :3] = rotation
    matrix[:3, 3] = translation
    return matrix


def calc_t_between_two_Ts(T1, T0):
    # function calculates translation vector between two transformation matrices
    # T1 and T2 are 4x4 transformation matrices
    # returns 3x1 translation vector
    return T1[:3, 3] - T0[:3, 3]


def add_t_to_T(T, t):
    # function adds translation vector t to transformation matrix T
    # t is 3x1 translation vector
    # T is 4x4 transformation matrix
    # returns 4x4 transformation matrix
    T[:3, 3] += t
    return T


def get_R_from_T(T):
    # function extracts rotation matrix from transformation matrix
    # T is 4x4 transformation matrix
    # returns 3x3 rotation matrix
    return T[:3, :3]


def vector3d_to_homogeneous(v):
    # function converts 3d vector to homogeneous coordinates
    # v is 3d vector
    # returns 4d vector
    return np.concatenate((v, [1]))


def poses_from_transformations(transformations):
    absolute_pose = np.eye(4)
    poses = [absolute_pose]
    for transformation in transformations:
        if transformation is None:
            poses.append(np.linalg.inv(absolute_pose))  # remain at the same pose
            continue
        absolute_pose = transformation @ absolute_pose
        poses.append(np.linalg.inv(absolute_pose))
    return poses


def transform_vector(vector, transfromation):
    # add homogeneous coordinate
    vector = vector3d_to_homogeneous(vector)
    # transform vector
    vector = transfromation @ vector
    return vector[:3]
