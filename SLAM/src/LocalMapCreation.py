import numpy as np
from src.metrics import get_rotation_difference_as_degree


def needNewKeyframe(last_key_frame, candidate_frame, fps, feat_identifier, feature_matches):
    def sufficient_frames_passed(key_frame, frame, fps):
        return abs(frame.id - key_frame.id) >= fps

    def sufficient_features_in_frame(frame, minimum):
        return len(frame.keypoints[feat_identifier]) >= minimum

    def features_distinct_enough_from_keyframe(frame, min_distinct_rate):
        print("Distinctive Rate :", len(feature_matches) / len(frame.keypoints[feat_identifier]))
        return len(feature_matches) / len(frame.keypoints[feat_identifier]) < (1 - min_distinct_rate)

    return (sufficient_frames_passed(last_key_frame, candidate_frame, fps) and sufficient_features_in_frame(
        candidate_frame, 50) and features_distinct_enough_from_keyframe(candidate_frame, 0.9))


def needNewKeyframeV2(key_frame_transformation, cur_transformation, max_rotation, max_translation,
                      odometry_tracking_lost=False):
    # TODO: Fast changes in rotation or translation can cause the tracking to be lost. This should be handled.
    def rotation_limit_exceeded(key_frame_transformation, cur_transformation, max_rotation_diff):
        angle = get_rotation_difference_as_degree(key_frame_transformation[:3, :3], cur_transformation[:3, :3])
        return angle > max_rotation_diff

    def translation_limit_exceeded(key_frame_transformation, cur_transformation, max_translation_diff):
        translation_error = np.linalg.norm(cur_transformation[0:3, 3] - key_frame_transformation[0:3, 3])
        return translation_error > max_translation_diff

    rot_exceed = rotation_limit_exceeded(key_frame_transformation, cur_transformation, max_rotation)
    translation_exceed = translation_limit_exceeded(key_frame_transformation, cur_transformation, max_translation)
    if rot_exceed:
        return True, "Rotation exceeded"
    if translation_exceed:
        return True, "Translation exceeded"
    if odometry_tracking_lost:
        return True, "Odometry tracking lost"
    return False, None
