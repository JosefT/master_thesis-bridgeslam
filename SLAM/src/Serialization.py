import dill

from Entities.MapFragment import MapFragment


def save_fragments(fragment_collector, file_name):
    # serealize fragments
    serialized_fragments = []
    for fragment in fragment_collector:
        serialized_fragments.append(fragment.serialize())
    with open(file_name, 'wb') as f:
        dill.dump(serialized_fragments, f)
    del serialized_fragments


def load_fragments(file_name):
    with open(file_name, 'rb') as f:
        serialized_fragments = dill.load(f)
        fragment_collector = []
        for data in serialized_fragments:
            intrinsic = data["intrinsic"]
            start_idx = data["start_idx"]
            map_voxel_size = data["map_voxel_size"]
            depth_scale = data["depth_scale"]
            max_depth = data["max_depth"]
            sdf_trunc = data["sdf_trunc"]
            fragment_collector.append(
                MapFragment(intrinsic, start_idx, map_voxel_size, depth_scale, max_depth, sdf_trunc).deserialize(data))
        return fragment_collector


def save_array(array, file_name):
    with open(file_name, 'wb') as f:
        dill.dump(array, f)


def load_array(file_name):
    with open(file_name, 'rb') as f:
        return dill.load(f)
