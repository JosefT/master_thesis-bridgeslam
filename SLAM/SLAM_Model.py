import copy

import easydict
import numpy as np
import open3d as o3d
from tqdm import tqdm

from Entities.MapFragment import MapFragment
from General import measure_time
from Trials.binary_ORB_trial import visualize_trajectory_and_LCs
from src.Color_Optimization import get_rgbs_indices_for_color_optimization
from src.LocalMapCreation import needNewKeyframeV2
from src.Loop_Closure_Pipeline import reduce_lc_by_odometry_validity, \
    append_loop_closure_edge, get_sub_graphs, calculate_loop_closure_transformations, filter_faulty_aligned_LCs, \
    all_pcd_metrics_okay
from src.Open3d_Tools import build_rgbd_from_image_paths
from src.RigidRegistration import estTransformationByModelAndICP, get_information_matrix_for_transformation
from src.VisualizationUtils import draw_registration_result, draw_func
from src.metrics import get_rotation_difference_as_degree
from src.transformation_utils import calc_t_between_two_Ts, add_t_to_T, poses_from_transformations, \
    updateRotationOfT


def construct_local_pose_graph(odom_Ts, gicp_Ts, pcds):
    pose_graph = o3d.pipelines.registration.PoseGraph()

    # node_poses = poses_from_transformations(odom_Ts)
    gicp_Ts.insert(0, np.eye(4))
    node_poses = [np.linalg.inv(t) for t in gicp_Ts]
    print(node_poses)
    pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(node_poses[0]))
    assert len(node_poses) == len(odom_Ts) + 1
    node_id = 0
    for i in range(len(odom_Ts)):
        # Add node for first frame and connect it to the last frame of the previous fragment by an edge
        pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(node_poses[i + 1]))
        # From source to target - Note that for the information, the order of maps is reversed
        # if transformations[i] is None:
        #    node_id += 1
        #    continue
        # draw_registration_result(map_fragments[i + 1].map, map_fragments[i].map, transformations[i],)
        # pose_graph.edges.append(o3d.pipelines.registration.PoseGraphEdge(node_id, node_id + 1,
        #                                                                 odom_Ts[i], np.linalg.inv(odom_information),
        #                                                                 uncertain=False))
        gicp_information = get_information_matrix_for_transformation(pcds[0], pcds[i + 1], 0.04,
                                                                     np.linalg.inv(gicp_Ts[i]))
        pose_graph.edges.append(o3d.pipelines.registration.PoseGraphEdge(0, node_id + 1,
                                                                         gicp_Ts[i], np.linalg.inv(gicp_information),
                                                                         uncertain=False))

        node_id += 1
    return pose_graph


class SLAM:
    def __init__(self, config, model, draw_errors=False, visualize_loop_closures=False):
        self.config = config
        self.draw_errors = draw_errors
        self.visualize_loop_closures = visualize_loop_closures
        self.Model = model
        self.rotation_estimator = None
        # Camera Settings
        self.intrinsic = o3d.camera.PinholeCameraIntrinsic(width=config['Camera']['width'],
                                                           height=config['Camera']['height'],
                                                           fx=config['Camera']['fx'], fy=config['Camera']['fy'],
                                                           cx=config['Camera']['cx'], cy=config['Camera']['cy'])
        self.depth_scale = config['Camera']['depth_scale']
        self.max_depth = config['Camera']['max_depth']

        # Mapping Settings
        self.align_voxel_size = config['Mapping']['align_vx_size']
        self.map_voxel_size = config['Mapping']['map_vx_size']
        self.use_imu_support = config['Mapping']['use_imu_support']
        self.final_voxel_size = config['Mapping']['final_voxel_size']
        self.max_corr_distance = config['Mapping']['max_corr_distance']
        self.model_voxel_size = config['Mapping']['model_voxel_size']
        self.sdf_trunc = config['Mapping']['sdf_trunc']
        self.use_clustering = config['Mapping']['cluster']
        self.cluster_threshold = config['Mapping']['cluster_threshold']
        self.use_odometry_estimation = config['Mapping']['use_odom_estimation']

        # Loop Closure Settings
        self.lc_epsilon = config['Loop_Closure']['odom_epsilon']
        self.lc_sigma = config['Loop_Closure']['odom_sigma']
        self.edge_prune_threshold = config['Loop_Closure']['edge_prune_threshold']
        self.preference_loop_closure = config['Loop_Closure']['preference_loop_closure']

        # Color Optimization Settings
        self.color_opt_img_choice_rot_threshold = config['Color_Optimization']['img_choice_rot_threshold']
        self.color_opt_img_choice_trans_threshold = config['Color_Optimization']['img_choice_trans_threshold']
        self.color_opt_iterations = config['Color_Optimization']['color_opt_iterations']

        # SLAM Settings
        self.max_rot = config['SLAM_Settings']['max_rotation_difference']
        self.max_translation = config['SLAM_Settings']['max_translation_difference']
        self.min_frames_per_map = config['SLAM_Settings']['min_frames_per_map']
        self.loop_closure_distance = config['SLAM_Settings']['loop_closure_distance']
        self.ignore_errors = config['SLAM_Settings']['ignore_errors']

        # Error Settings
        self.max_rot_frame = config['Error_Prevention']['max_rot_frame']

        # NDT Score
        self.ndt_box_size = config['NDT_score']['box_size']
        self.ndt_min_pt_thres = config['NDT_score']['ndt_min_pt_thres']
        self.min_score = config['NDT_score']['min_score']

    # ============================================== FRAGMENT CREATION =================================================

    @measure_time
    def create_local_maps(self, frames, visualize_local_maps_during_registration=False, visualize_icp_alignment=False,
                          optimize_maps=False):

        extrinsic = np.eye(4)
        fragment = MapFragment(self.intrinsic, start_idx=0, depth_scale=self.depth_scale,
                               max_depth=self.max_depth,
                               sdf_trunc=self.sdf_trunc, map_voxel_size=self.map_voxel_size, keyframe=frames[0],
                               init_transformation=np.eye(4))
        fragment_collector = []
        loss = o3d.pipelines.registration.TukeyLoss(k=0.1)
        p2l = o3d.pipelines.registration.TransformationEstimationPointToPlane(loss)
        odometry_lost = False

        tgt_rgb = o3d.io.read_image(fragment.frames[0].color_path)
        tgt_depth = o3d.io.read_image(fragment.frames[0].depth_path)
        target_rgbd_image = o3d.geometry.RGBDImage().create_from_color_and_depth(tgt_rgb,
                                                                                 tgt_depth,
                                                                                 depth_scale=self.depth_scale,
                                                                                 depth_trunc=self.max_depth,
                                                                                 convert_rgb_to_intensity=False)
        for m in tqdm(range(1, len(frames))):
            cur_frame = frames[m]

            need_new_keyframe, reason = needNewKeyframeV2(fragment.extrinsics[0], extrinsic, self.max_rot,
                                                          self.max_translation, odometry_lost)
            odometry_lost = False
            if need_new_keyframe:
                if fragment.num_frames >= self.min_frames_per_map:
                    fragment.update_map(self.model_voxel_size, filter_sky_points=False, optimize=optimize_maps)
                    if len(fragment.map.points) > 1000:
                        fragment_collector.append(fragment)
                    else:
                        print("Fragment to small, discarding")
                    if visualize_local_maps_during_registration:
                        o3d.visualization.draw_geometries([fragment.map])
                else:
                    for frame in fragment.frames:
                        frame.active = False
                extrinsic = np.eye(4)
                fragment = MapFragment(self.intrinsic, start_idx=m, max_depth=self.max_depth,
                                       depth_scale=self.depth_scale, map_voxel_size=self.map_voxel_size,
                                       sdf_trunc=self.sdf_trunc, keyframe=cur_frame, init_transformation=extrinsic)
                tgt_rgb = o3d.io.read_image(fragment.frames[0].color_path)
                tgt_depth = o3d.io.read_image(fragment.frames[0].depth_path)
                target_rgbd_image = o3d.geometry.RGBDImage().create_from_color_and_depth(tgt_rgb,
                                                                                         tgt_depth,
                                                                                         depth_scale=self.depth_scale,
                                                                                         depth_trunc=self.max_depth,
                                                                                         convert_rgb_to_intensity=False)
                print(f"Recently introduced keyframe at Image: {m}, {reason}")
            else:
                # ------------- Estimate Transformation --------------
                # Fix rotation based on IMU and previous frame
                if self.use_imu_support:
                    transformation_est = self.get_imu_estimation_for_next_transformation(extrinsic, fragment, m)
                else:
                    transformation_est = np.linalg.inv(extrinsic)

                # ------------- Registration -------------------------
                if self.use_odometry_estimation:
                    # if len(pcd.points) > 4 and len(template.points) > 4:
                    src_rgb = o3d.io.read_image(cur_frame.color_path)

                    src_depth = o3d.io.read_image(cur_frame.depth_path)

                    source_rgbd_image = o3d.geometry.RGBDImage(). \
                        create_from_color_and_depth(src_rgb, src_depth,
                                                    depth_scale=self.depth_scale,
                                                    depth_trunc=self.max_depth,
                                                    convert_rgb_to_intensity=False)

                    option = o3d.pipelines.odometry.OdometryOption(depth_max=6)
                    result = o3d.pipelines.odometry.compute_rgbd_odometry(source_rgbd_image, target_rgbd_image,
                                                                          self.intrinsic,
                                                                          transformation_est,
                                                                          o3d.pipelines.odometry.RGBDOdometryJacobianFromHybridTerm(),
                                                                          option)
                    print(result[0])
                    corr = o3d.pipelines.odometry.compute_correspondence(self.intrinsic.intrinsic_matrix, result[1],
                                                                         o3d.io.read_image(cur_frame.depth_path),
                                                                         o3d.io.read_image(
                                                                             fragment.frames[0].depth_path), option)
                    icp_result = dict(
                        transformation=result[1],
                        correspondences=corr,
                        fitness=0.9,
                    )
                    icp_result = easydict.EasyDict(icp_result)
                    transformation = result[1]
                else:
                    pcd = o3d.io.read_point_cloud(cur_frame.pcd_path)
                    pcd = pcd.voxel_down_sample(self.align_voxel_size)
                    # cut of points that are too far away in z direction
                    pcd = pcd.crop(
                        o3d.geometry.AxisAlignedBoundingBox([-np.inf, -np.inf, -np.inf],
                                                            [np.inf, np.inf, self.max_depth]))

                    template = fragment.tsdf_volume.extract_point_cloud()
                    template = template.voxel_down_sample(self.align_voxel_size)
                    icp_result = o3d.pipelines.registration.registration_icp(pcd, template, self.align_voxel_size * 3,
                                                                             transformation_est,
                                                                             p2l)
                    transformation = icp_result.transformation

                # ------------- Check Result -------------------------
                result_okay = self.does_alignment_not_deviate_too_much(icp_result, 0.3, extrinsic, self.max_rot_frame)
                if not result_okay and not self.ignore_errors:
                    print("Odometry lost due to bad ICP result. Maybe the cameera moved to fast")
                    odometry_lost = True
                    cur_frame.active = False
                    continue  # Skip this frame
                elif not result_okay:
                    print("Odometry lost due to bad ICP result. However, this is ignored due to the ignore_errors flag")

                # ------------- Visualization ------------------------

                if visualize_icp_alignment or (self.draw_errors and not result_okay and not self.ignore_errors):
                    pcd = o3d.io.read_point_cloud(cur_frame.pcd_path)
                    template = fragment.tsdf_volume.extract_point_cloud().voxel_down_sample(self.align_voxel_size)
                    geom = draw_registration_result(pcd, template, icp_result.transformation, uniform_color=True,
                                                    correspondences=icp_result.correspondence_set, draw=False)
                    draw_func("Map Creation", f"Frame {m}", geom)

                # ------------- Update Fragment ----------------------
                # Extrinsic need to be the inverse, because icp was done backwards and we want to have the
                # forward transformation
                extrinsic = np.linalg.inv(transformation)
                fragment.add_and_integrate_frame(frames[m], extrinsic, m)
                # ------------- End of Loop ---------------------------
        else:  # Yaaaay, I finally used a for-else loop :D
            # Register last fragment
            fragment.update_map(self.model_voxel_size, filter_sky_points=False, optimize=optimize_maps)
            fragment_collector.append(fragment)
            if visualize_icp_alignment:
                o3d.visualization.draw_geometries([fragment.map])
        print("Number of fragments: ", len(fragment_collector))
        return fragment_collector

    def get_imu_estimation_for_next_transformation(self, extrinsic, fragment, m):  # TODO test if correct
        delta_m_rotation_imu = self.rotation_estimator.get_rotation_from_prev_frame(m).rotation_matrix
        # transformation_est = R_to_T(delta_m_rotation_imu) @ np.linalg.inv(extrinsic)

        transformation_est = updateRotationOfT(np.linalg.inv(extrinsic), delta_m_rotation_imu)

        # Fix translation based on IMU and previous frame
        if len(fragment.extrinsics) > 1:
            dt_prev = (self.rotation_estimator.timestamps[m - 1] - self.rotation_estimator.timestamps[
                m - 2]) / 1000
            if dt_prev == 0.0:
                velocity = np.zeros(3)
            else:
                delta_t_prev = calc_t_between_two_Ts(np.linalg.inv(fragment.extrinsics[-1]),
                                                     np.linalg.inv(fragment.extrinsics[-2]))
                velocity = delta_t_prev / dt_prev
        else:
            velocity = np.zeros(3)
        dt = (self.rotation_estimator.timestamps[m] - self.rotation_estimator.timestamps[m - 1]) / 1000
        # IMU
        accel_R = np.linalg.inv(transformation_est[:3, :3])
        accel_world = accel_R @ self.rotation_estimator.linear_accelerations[m]
        velocity_accel_update = 0.5 * accel_world * dt
        # Add velocity to translation
        delta_t = (velocity + velocity_accel_update) * dt
        transformation_est = add_t_to_T(transformation_est, delta_t)
        return transformation_est

    def does_alignment_not_deviate_too_much(self, icp_result, min_fitness, last_extrinsic, max_rotation_diff):
        is_score_okay = icp_result.fitness > min_fitness
        rot_angle = get_rotation_difference_as_degree(icp_result.transformation[:3, :3],
                                                      np.linalg.inv(last_extrinsic[:3, :3]))
        is_rot_okay = rot_angle < max_rotation_diff
        return is_score_okay and is_rot_okay

    # ==================================================================================================================

    # ========================================== CONSECUTIVE FRAGMENT ALIGNMENT ========================================

    def estimate_consecutive_transformations_between_local_maps(self, fragment_collector, use_model=False,
                                                                try_model_if_icp_failed=True):
        maps = [f.map for f in fragment_collector]
        forward_transformations_collector = []
        for i in tqdm(range(1, len(maps))):
            last_src_transf = fragment_collector[i - 1].extrinsics[-1]
            if use_model:
                result, T = estTransformationByModelAndICP(maps[i - 1], maps[i], self.Model,
                                                           max_corr_dist=self.model_voxel_size * 3,
                                                           p2p=False, voxel_size=self.model_voxel_size)
            else:
                # result = performRobustKernelICP(maps[i - 1], maps[i], self.max_corr_distance, last_src_transf)
                result = o3d.pipelines.registration.registration_generalized_icp(maps[i - 1], maps[i],
                                                                                 self.max_corr_distance,
                                                                                 last_src_transf)
            T_okay, metrics = all_pcd_metrics_okay(maps[i - 1], maps[i], result, self.map_voxel_size, self.ndt_box_size,
                                                   self.ndt_min_pt_thres,
                                                   self.min_score)
            if self.draw_errors and not T_okay:
                draw_registration_result(maps[i - 1], maps[i], last_src_transf, window_name="Previous Transformation")
                draw_registration_result(maps[i - 1], maps[i], result.transformation, window_name="ICP Transformation")

            if not T_okay and try_model_if_icp_failed:
                result2, _ = estTransformationByModelAndICP(maps[i - 1], maps[i], self.Model,
                                                            max_corr_dist=self.model_voxel_size * 3,
                                                            p2p=False, voxel_size=self.model_voxel_size)
                T2_okay, metrics = all_pcd_metrics_okay(maps[i - 1], maps[i], result2, self.map_voxel_size,
                                                        self.ndt_box_size,
                                                        self.ndt_min_pt_thres, self.min_score)
                if self.draw_errors:
                    draw_registration_result(maps[i - 1], maps[i], result2.transformation,
                                             window_name="Model Transformation")
                if result2.fitness > result.fitness and T2_okay:
                    forward_transformations_collector.append(result2.transformation)
                    continue
                else:  # FALLBACK
                    forward_transformations_collector.append(last_src_transf)
            else:
                forward_transformations_collector.append(result.transformation)
        return forward_transformations_collector

    # ==================================================================================================================

    # ===================================================== LOOP CLOSURE ===============================================

    def build_consecutive_pose_graph(self, transformations, map_fragments):
        pose_graph = o3d.pipelines.registration.PoseGraph()
        node_poses = poses_from_transformations(transformations)
        pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(node_poses[0]))
        assert len(node_poses) == len(transformations) + 1
        node_id = 0
        for i in range(len(transformations)):
            # Add node for first frame and connect it to the last frame of the previous fragment by an edge
            pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(node_poses[i + 1]))
            # From source to target - Note that for the information, the order of maps is reversed
            # if transformations[i] is None:
            #    node_id += 1
            #    continue
            information = get_information_matrix_for_transformation(map_fragments[i].map, map_fragments[i + 1].map,
                                                                    self.max_corr_distance, transformations[i])

            # draw_registration_result(map_fragments[i + 1].map, map_fragments[i].map, transformations[i],)
            pose_graph.edges.append(o3d.pipelines.registration.PoseGraphEdge(node_id, node_id + 1,
                                                                             transformations[i], information,
                                                                             uncertain=False))
            node_id += 1
        return pose_graph

    def add_lc_edges_to_pose_graph(self, lc_candidates, fragment_collector, pose_graph, cur_idx=0):
        for candidate in lc_candidates:
            source = fragment_collector[candidate[0]].map
            target = fragment_collector[candidate[1]].map
            information = get_information_matrix_for_transformation(source, target, self.max_corr_distance,
                                                                    candidate[-1].transformation)
            pose_graph = append_loop_closure_edge(candidate[0] - cur_idx, candidate[1] - cur_idx,
                                                  candidate[-1].transformation,
                                                  information, pose_graph)
        return pose_graph

    def loop_closure_pipeline(self, lc_candidates, fragment_collector, pose_graph, visualize_loop_closures):

        print(f"Found LC Candidates:{len(lc_candidates)}")
        print(lc_candidates)

        for i in range(3):
            poses = [node.pose for node in pose_graph.nodes]

            lc_candidates_verified = reduce_lc_by_odometry_validity(lc_candidates, fragment_collector, poses,
                                                                    self.lc_epsilon,
                                                                    self.lc_sigma)
            if self.visualize_loop_closures:
                poses = [node.pose for node in pose_graph.nodes]
                trajectory = self.extract_trajectory(fragment_collector, poses)
                points = [pose[:3, 3] for pose in poses]
                visualize_trajectory_and_LCs(trajectory, None, points, lc_candidates, None, poses)
                visualize_trajectory_and_LCs(trajectory, None, points, lc_candidates_verified, None,
                                             poses)
            self.lc_sigma *= 0.25
            print(f"Verified LC Candidates:{len(lc_candidates_verified)}")
            # try to align them
            lc_candidates_with_transformations = calculate_loop_closure_transformations(lc_candidates_verified,
                                                                                        fragment_collector,
                                                                                        self.Model,
                                                                                        self.max_corr_distance,
                                                                                        self.model_voxel_size)
            # filter wrong aligned loop closures
            lc_candidates_with_transformations = filter_faulty_aligned_LCs(lc_candidates_with_transformations,
                                                                           fragment_collector, self.ndt_box_size,
                                                                           self.ndt_min_pt_thres, self.map_voxel_size,
                                                                           self.min_score)

            if visualize_loop_closures:
                for candidate in lc_candidates_with_transformations:
                    source = fragment_collector[candidate[0]].map
                    target = fragment_collector[candidate[1]].map
                    draw_registration_result(source, target, candidate[-1].transformation,
                                             window_name=f"LC {candidate[0]}-{candidate[1]}")

            # construct and optimize pose graph
            pose_graph = self.add_lc_edges_to_pose_graph(lc_candidates_with_transformations, fragment_collector,
                                                         pose_graph)

            # OPTIMIZATION
            self.optimize(pose_graph, self.preference_loop_closure)

        subgraphs = get_sub_graphs(pose_graph)
        if len(subgraphs) > 1:
            raise AssertionError("Pose graph is not connected")
        poses = [node.pose for node in pose_graph.nodes]
        print("Loop Closure - Optimization done.")
        return poses

    def optimize(self, pose_graph, preference_loop_closure):
        option = o3d.pipelines.registration.GlobalOptimizationOption(max_correspondence_distance=self.max_corr_distance,
                                                                     edge_prune_threshold=self.edge_prune_threshold,
                                                                     reference_node=0,
                                                                     preference_loop_closure=preference_loop_closure + 1.5)
        if len(pose_graph.nodes) > 1:
            with o3d.utility.VerbosityContextManager(o3d.utility.VerbosityLevel.Debug) as cm:
                o3d.pipelines.registration.global_optimization(pose_graph,
                                                               o3d.pipelines.registration.GlobalOptimizationLevenbergMarquardt(),
                                                               o3d.pipelines.registration.GlobalOptimizationConvergenceCriteria(),
                                                               option)

    # ==================================================================================================================

    # ========================================= ALIGNED FRAGMENT VISUALIZATION =========================================

    def visualize_aligned_fragments(self, fragment_collector, transformations, uniform_color=False):
        final_map = o3d.geometry.PointCloud()
        colors = []
        if uniform_color:
            color_values = np.linspace(0.25, 0.9, int(len(fragment_collector))) ** 2
            for value in color_values:
                colors.append([value, value, value])
        for i in range(len(fragment_collector)):
            map = copy.deepcopy(fragment_collector[i].map)
            transformation = transformations[i]
            map = map.transform(transformation)
            if uniform_color:
                map.paint_uniform_color(colors[i])
            final_map += map
        # final_map = final_map.remove_statistical_outlier(nb_neighbors=30, std_ratio=2.0)[0]
        final_map = final_map.voxel_down_sample(voxel_size=self.align_voxel_size)
        draw_func(None, "Aligned Maps", final_map)

    # ==================================================================================================================

    # ============================================= FINAL TSDF CREATION ================================================
    def RecreateTSDFFromTransformations(self, fragment_collector, trajectory):
        frame = fragment_collector[0].frames[0]
        final_fragment = MapFragment(self.intrinsic, start_idx=0,
                                     map_voxel_size=self.final_voxel_size,
                                     depth_scale=self.depth_scale, max_depth=self.max_depth, sdf_trunc=self.sdf_trunc,
                                     keyframe=frame, init_transformation=np.eye(4))
        cur_idx = 1
        print("Integrating final map...")

        for i in tqdm(range(len(fragment_collector))):
            for rgbd_idx in range(fragment_collector[i].num_frames):
                if i == 0 and rgbd_idx == 0:
                    continue
                # Exactly opposite transformations needed than expected
                final_fragment.add_and_integrate_frame(fragment_collector[i].frames[rgbd_idx],
                                                       trajectory[cur_idx - 1], cur_idx)

                cur_idx += 1
        # Update final map (this extracts the point cloud from tsdf volume)
        print("Created Final TSDF Volume.")
        mesh = final_fragment.tsdf_volume.extract_triangle_mesh()
        # show backside of triangles too
        mesh.compute_vertex_normals()

        # Filter only biggest cluster from point cloud
        if self.use_clustering:
            with o3d.utility.VerbosityContextManager(o3d.utility.VerbosityLevel.Debug) as cm:
                triangle_clusters, cluster_n_triangles, cluster_area = (mesh.cluster_connected_triangles())
            triangle_clusters = np.asarray(triangle_clusters)
            cluster_n_triangles = np.asarray(cluster_n_triangles)
            triangles_to_remove = cluster_n_triangles[
                                      triangle_clusters] < self.cluster_threshold  # Needs to be set based on voxel size
            mesh.remove_triangles_by_mask(triangles_to_remove)

        final_fragment.update_map(self.final_voxel_size, filter_sky_points=False, optimize=False)
        final_map_pcd = final_fragment.map
        return final_map_pcd, mesh

    # ======================================================================================================================

    # ============================================= COLOR OPTIMIZATION =====================================================

    def optimize_color(self, fragment_collector, trajectory, mesh):
        print("Optimizing color...")
        cam_params = []
        # filter frames that are not active
        frames = []
        for fragment in fragment_collector:
            frames = frames + fragment.frames
        for pose in trajectory:
            cam_param = o3d.camera.PinholeCameraParameters()
            cam_param.extrinsic = pose
            cam_param.intrinsic = self.intrinsic
            cam_params.append(cam_param)
        o3d_trajectory = o3d.camera.PinholeCameraTrajectory()

        f_indices_to_use = get_rgbs_indices_for_color_optimization(frames, trajectory,
                                                                   self.color_opt_img_choice_rot_threshold,
                                                                   self.color_opt_img_choice_trans_threshold)
        o3d_trajectory.parameters = [cam_params[i] for i in f_indices_to_use]
        frames_thinned_out = [frames[i] for i in f_indices_to_use]
        print("Picked ", len(frames_thinned_out), " frames for color optimization.")

        # get rgbd images:
        rgbd_images = []
        for frame in frames_thinned_out:
            rgbd_images.append(
                build_rgbd_from_image_paths(frame.color_path, frame.depth_path, self.depth_scale, self.max_depth))
        # Start optimzation
        option = o3d.pipelines.color_map.NonRigidOptimizerOption(maximum_iteration=self.color_opt_iterations,
                                                                 maximum_allowable_depth=self.max_depth,
                                                                 depth_threshold_for_discontinuity_check=2)
        with o3d.utility.VerbosityContextManager(
                o3d.utility.VerbosityLevel.Debug) as cm:
            mesh, camera = o3d.pipelines.color_map.run_non_rigid_optimizer(mesh, rgbd_images, o3d_trajectory,
                                                                           option)
        return mesh

    # ======================================================================================================================

    # ======================================================== MISC ====================================================

    def build_consecuitive_poses(self, transformations):
        absolute_pose = np.eye(4)
        poses = [absolute_pose]
        for transformation in transformations:
            if transformation is None:
                poses.append(np.linalg.inv(absolute_pose))  # remain at the same pose
                continue
            absolute_pose = transformation @ absolute_pose
            poses.append(np.linalg.inv(absolute_pose))
        return poses

    def extract_trajectory(self, fragment_collector, optimized_transformations):
        trajectory = []
        for i in range(len(fragment_collector)):
            local_map_transformation = np.linalg.inv(optimized_transformations[i])
            for rgbd_idx in range(fragment_collector[i].num_frames):
                if i == 0 and rgbd_idx == 0:
                    trajectory.append(np.eye(4))
                    continue
                transformation_f2f = fragment_collector[i].extrinsics[rgbd_idx] @ local_map_transformation
                trajectory.append(transformation_f2f)
        return trajectory

# ======================================================================================================================
