import copy

import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.linalg import svd
from scipy.spatial.transform import Rotation as R
from scipy.spatial import distance

from Entities.FeatureMatchManager import FeatureMatchManager
from Entities.RGBDFrame import RGBDFrame
from annoy import AnnoyIndex

from Entities.Wrapper.MapFragmentManager import MapFragmentManager
from Entities.Wrapper.RGBDFrameSequence import RGBDFrameSequence
from VisualizationUtils import draw_registration_result
from src.FeatureMatching import matchSIFTFeaturesFLANN, remove_strange_matches, filter_by_matches, \
    match_sift_descriptors_flann
from src.General import measure_time
from src.Open3d_Tools import points_to_pcd
from src.RigidRegistration import rigid_registration, ransac_and_icp, estimate_transformation_from_correspondances, \
    calculate_transformation_using_features
import open3d as o3d


@measure_time
def load_dictionary():
    raw_dict = np.load("Vocabulary/freiburg/dictionary.npy")
    return raw_dict


@measure_time
def load_dictionary_my_room(room="01"):
    raw_dict = np.load("Vocabulary/my_room/" + room + "/dictionary.npy")

    build_annoy_trees(raw_dict, 512)  # TODO put this into bow generation file
    annoy_forest = AnnoyIndex(128, 'angular')
    annoy_forest.load('temp/annoyTrees.ann')
    return raw_dict, annoy_forest


@measure_time
def build_annoy_trees(raw_dict, amount_of_trees=512):
    f = np.shape(raw_dict)[-1]
    t = AnnoyIndex(f, 'angular')
    for i in range(len(raw_dict)):
        t.add_item(i, raw_dict[i])
    t.build(amount_of_trees, n_jobs=7)
    return t


def find_closest_word_for_descriptor(desc, index_tree):
    return index_tree.get_nns_by_vector


def find_closest_words_for_descriptor(desc, index_tree, number_of_words):
    return index_tree.get_nns_by_vector(desc, number_of_words, search_k=-1, include_distances=False)


def construct_bag_of_words_representation_for_frame_voting(frame: RGBDFrame, desc_identifier, index_tree):
    if frame.descriptors[desc_identifier] is None:
        # create evenly distributed histogram
        return np.ones(512) / 512
    descs = frame.descriptors[desc_identifier]
    histogram = np.zeros(512)
    # Normalize descriptors
    for desc in descs:
        closest_words = find_closest_words_for_descriptor(desc, index_tree, 10)
        for i in range(1, 11):
            histogram[closest_words[i - 1]] += 1. / i
    # Normalize histogram
    histogram = histogram / np.sum(histogram)
    return histogram


def construct_bag_of_words_representation_for_frame_fast(frame: RGBDFrame, desc_identifier, index_tree):
    if frame.descriptors[desc_identifier] is None:
        raise ValueError("No descriptors found for identifier: " + desc_identifier)
    descs = frame.descriptors[desc_identifier]
    histogram = np.zeros(512)
    # Normalize descriptors
    for desc in descs:
        histogram[find_closest_word_for_descriptor(desc, index_tree)] += 1
    # Normalize histogram
    histogram = histogram / np.sum(histogram)
    return histogram


class LoopDetector:
    def __init__(self, dictionary, index_tree, match_manager: FeatureMatchManager, fitness_threshold=0.50):
        self.dictionary = dictionary
        self.index_tree = index_tree
        self.histograms = []
        self.fitness_threshold = fitness_threshold
        self.match_manager = match_manager

    def add_frame(self, frame: RGBDFrame, desc_identifier):
        self.histograms.append(
            construct_bag_of_words_representation_for_frame_voting(frame, desc_identifier, self.index_tree))

    @measure_time
    def add_all_active_frames(self, frame_seq: RGBDFrameSequence, desc_identifier):
        for frame in frame_seq.RGBDFrames:
            if frame.active:
                self.add_frame(frame, desc_identifier)

    def __generate_loop_closures(self, frame_sequence: RGBDFrameSequence):
        loop_closures = []
        active_frame_ids = []
        for frame in frame_sequence.RGBDFrames:
            if frame.active:
                active_frame_ids.append(frame.id)

        for i in range(len(self.histograms)):
            for j in range(i + 1, len(self.histograms)):
                if distance.cosine(self.histograms[i], self.histograms[j]) < 0.15:
                    loop_closures.append((active_frame_ids[i], active_frame_ids[j]))
        return loop_closures

    def __get_loop_closures_grouped_by_local_map_membership(self, frame_sequence: RGBDFrameSequence,
                                                            fragment_manager: MapFragmentManager):
        loop_closures = self.__generate_loop_closures(frame_sequence)

        loop_closures_grouped_by_fragments = []
        while len(loop_closures) > 0:

            pivot_closure = loop_closures.pop(0)
            source_fragment_idx_pivot = fragment_manager.map_fragments.index(
                fragment_manager.get_fragment_by_frame_id(pivot_closure[0]))
            target_fragment_idx_pivot = fragment_manager.map_fragments.index(
                fragment_manager.get_fragment_by_frame_id(pivot_closure[1]))
            if source_fragment_idx_pivot == target_fragment_idx_pivot or target_fragment_idx_pivot - source_fragment_idx_pivot == 1:
                continue
            loop_closures_grouped_by_fragments.append([pivot_closure])
            i = 0
            while i < len(loop_closures):
                source_fragment_idx = fragment_manager.map_fragments.index(
                    fragment_manager.get_fragment_by_frame_id(loop_closures[i][0]))
                target_fragment_idx = fragment_manager.map_fragments.index(
                    fragment_manager.get_fragment_by_frame_id(loop_closures[i][1]))
                if source_fragment_idx == source_fragment_idx_pivot and target_fragment_idx == target_fragment_idx_pivot:
                    loop_closures_grouped_by_fragments[-1].append(loop_closures[i])
                    loop_closures.pop(i)
                else:
                    i += 1
        return loop_closures_grouped_by_fragments

    def __get_loop_closures_grouped_by_vicinity(self, frame_sequence: RGBDFrameSequence):
        loop_closures = self.__generate_loop_closures(frame_sequence)
        # Group loop closures based by their vicinity into lists of tuples. Each tuple represents a loop closure.
        # The first element of the tuple is the index of the first frame of the loop closure. The second element is the
        # index of the second frame of the loop closure. Loop closures are close to each other, if less than 10 frames
        # have passed since the last loop closure.
        loop_closures_grouped_by_vicinity = []
        while len(loop_closures) > 0:
            pivot_closure = loop_closures.pop(0)
            loop_closures_grouped_by_vicinity.append([pivot_closure])
            i = 0
            while i < len(loop_closures):
                if abs(loop_closures[i][0] - pivot_closure[0]) < 10 and abs(
                        loop_closures[i][1] - pivot_closure[1]) < 10:
                    loop_closures_grouped_by_vicinity[-1].append(loop_closures[i])
                    loop_closures.pop(i)
                else:
                    i += 1
        return loop_closures_grouped_by_vicinity

    def __isolate_best_loop_closure_candidates(self, frame_sequence: RGBDFrameSequence, raw_loop_closures):
        print(raw_loop_closures)
        filtered_loop_closures = []
        filtered_transformations = []
        for loop_set in raw_loop_closures:
            # Calculate registration fitness for each potential loop closure
            fitness_values = []
            transformations = []
            for loop_closure in loop_set:
                result = self.loop_closure_alignment(frame_sequence.RGBDFrames[loop_closure[0]],
                                                     frame_sequence.RGBDFrames[loop_closure[1]])

                fitness_values.append(result.fitness)
                transformations.append(result.transformation)
            # Choose the best loop closure
            if np.max(fitness_values) < self.fitness_threshold:
                continue

            best_loop_closure_idx = np.argmax(fitness_values)
            filtered_loop_closures.append(loop_set[best_loop_closure_idx])
            filtered_transformations.append(transformations[best_loop_closure_idx])

        return filtered_loop_closures, filtered_transformations

    @measure_time
    def calculate_loop_closures_with_transformations(self, frame_sequence: RGBDFrameSequence,
                                                     fragment_manager: MapFragmentManager):
        raw_loop_closures = self.__get_loop_closures_grouped_by_local_map_membership(frame_sequence, fragment_manager)
        loop_closures, transformations = self.__isolate_best_loop_closure_candidates(frame_sequence, raw_loop_closures)
        return loop_closures, transformations

    @measure_time
    def calculate_loop_closures_with_transformations_directly(self, frame_sequence: RGBDFrameSequence,
                                                              fragment_manager: MapFragmentManager):
        raw_frame_loop_closures = self.__get_loop_closures_grouped_by_local_map_membership(frame_sequence,
                                                                                           fragment_manager)
        map_loop_closures = []
        transformations = []
        for loop_set in raw_frame_loop_closures:
            source_map = fragment_manager.get_fragment_by_frame_id(loop_set[0][0])
            target_map = fragment_manager.get_fragment_by_frame_id(loop_set[0][1])
            source_fragment_idx = fragment_manager.map_fragments.index(source_map)
            target_fragment_idx = fragment_manager.map_fragments.index(target_map)

            result = calculate_transformation_using_features(source_map.map.pcd, target_map.map.pcd, 0.01,
                                                             source_map.features3d[0], target_map.features3d[0],
                                                             source_map.features3d[1], target_map.features3d[1])
            if result is not None:
                transformations.append(result)
                map_loop_closures.append((source_fragment_idx, target_fragment_idx))
        return map_loop_closures, transformations

    @measure_time
    def calculate_loop_closures_with_transformations_directly_model(self, frame_sequence: RGBDFrameSequence,
                                                              fragment_manager: MapFragmentManager, model):
        raw_frame_loop_closures = self.__get_loop_closures_grouped_by_local_map_membership(frame_sequence,
                                                                                           fragment_manager)
        map_loop_closures = []
        transformations = []
        for loop_set in raw_frame_loop_closures:
            src_fragment = fragment_manager.get_fragment_by_frame_id(loop_set[0][0])
            tgt_fragment = fragment_manager.get_fragment_by_frame_id(loop_set[0][1])
            source_fragment_idx = fragment_manager.map_fragments.index(src_fragment)
            target_fragment_idx = fragment_manager.map_fragments.index(tgt_fragment)

            T = model.fit(src_fragment.local_map_dense(), tgt_fragment.local_map_dense())
            result = rigid_registration(src_fragment.map, tgt_fragment.map, T, point_to_point=True)

            if result is not None:
                transformations.append(result)
                map_loop_closures.append((source_fragment_idx, target_fragment_idx))
        return map_loop_closures, transformations

    def __show_histograms(self):
        for histogram1 in self.histograms:
            for histogram2 in self.histograms:
                fig, axs = plt.subplots(2)
                fig.suptitle('Vertically stacked subplots')
                axs[0].plot(histogram1)
                axs[1].plot(histogram2)
                plt.show()
        # Plot first histogram using matplotlib

    def loop_closure_alignment(self, source_frame: RGBDFrame, target_frame: RGBDFrame):
        # Match keypoints and descriptors
        # Perform feature matching with the RANSAC approach

        matches = matchSIFTFeaturesFLANN(source_frame, target_frame, "SIFT_filtered_3d")
        source_kpts = source_frame.keypoints["SIFT_filtered_3d"]
        target_kpts = target_frame.keypoints["SIFT_filtered_3d"]

        # isolate points based on matches
        source_kpts, target_kpts = filter_by_matches(source_kpts, target_kpts, matches)
        if len(source_kpts) < 3 or len(target_kpts) < 3:
            return rigid_registration(source_frame, target_frame)

        transformation = estimate_transformation_from_correspondances(source_kpts, target_kpts)

        icp_method = o3d.pipelines.registration.TransformationEstimationPointToPlane()
        result = o3d.pipelines.registration.registration_icp(source_frame.pcd,
                                                             target_frame.pcd, source_frame.pcd_voxel_size * 1.5,
                                                             transformation, icp_method)
        return result
