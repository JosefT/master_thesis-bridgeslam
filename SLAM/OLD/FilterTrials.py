import open3d as o3d
import numpy as np
from scipy.signal import medfilt

from VisualizationUtils import display_inlier_outlier


def create_organized_pointcloud(pointcloud):
    # Set voxel size
    voxel_size = 0.01

    # Voxelization
    voxel_grid = o3d.geometry.VoxelGrid.create_from_point_cloud(pointcloud, voxel_size)
    # Convert voxel grid to numpy array
    voxel_array = np.asarray(voxel_grid.get_voxels())

    # Create organized point cloud
    organized_pc = o3d.geometry.PointCloud()
    organized_pc.points = o3d.utility.Vector3dVector([pt.grid_index for pt in voxel_array])
    organized_pc.colors = o3d.utility.Vector3dVector([pt.color for pt in voxel_array])

    return organized_pc


def points_to_array(points, colors, width, height):
    array_2d = np.zeros((width, height))
    color_2d = np.zeros((width, height, 3))
    depths = [[[] for j in range(height)] for i in range(width)]
    point_color = [[[] for j in range(height)] for i in range(width)]
    for point, color in zip(points, colors):
        depths[int(point[0])][int(point[1])].append(point[2])
        point_color[int(point[0])][int(point[1])].append(color)
    for y in range(height):
        for x in range(width):
            if len(depths[x][y]) == 0:
                continue
            # output depth will be the median of the surrounding points
            median_depth = np.median(depths[x][y])
            mean_color = np.mean(point_color[x][y], axis= 0)
            array_2d[x, y] = median_depth
            color_2d[x, y] = mean_color
    return array_2d, color_2d



def median_filter(pcd):
    window_size = 3
    points = np.asarray(pcd.points)
    colors = np.asarray(pcd.colors)
    width = int(pcd.get_max_bound()[0] - pcd.get_min_bound()[0]) +1
    height = int(pcd.get_max_bound()[1] - pcd.get_min_bound()[1]) +1
    array_2d, color_2d = points_to_array(points, colors, width, height)
    new_array = np.zeros((width, height))
    for y in range(height):
        for x in range(width):
            if array_2d[x, y] == 0:
                continue
            vals = []
            for y_dev in range(-window_size // 2, window_size // 2 +1):
                for x_dev in range(-window_size // 2, window_size // 2 + 1):
                    if x + x_dev >= 0 and x + x_dev < width and y + y_dev >= 0 and y + y_dev < height:
                        if array_2d[x + x_dev, y + y_dev] != 0:
                            vals.append(array_2d[x + x_dev, y + y_dev])
            if len(vals) == 0:
                continue
            # output depth will be the median of the surrounding points
            middle_value = np.median(vals)
            new_array[x, y] = middle_value
    new_points = []
    new_colors = []
    for y in range(height):
        for x in range(width):
            if new_array[x, y] != 0:
                new_points.append([x, y, array_2d[x, y]])
                new_colors.append(color_2d[x, y])
    new_pcd = o3d.geometry.PointCloud()
    new_pcd.points = o3d.utility.Vector3dVector(new_points)
    new_pcd.colors = o3d.utility.Vector3dVector(new_colors)
    return new_pcd



def normal_based_filter(pointcloud, pcd_tree):
    points = np.asarray(pointcloud.points)
    normals = np.asarray(pointcloud.normals)
    colors = np.asarray(pointcloud.colors)
    data = list(zip(points, normals, colors))
    # Isolate points p0, ..., pm that lie on the normal vector of a point pi
    active_points = set([i for i in range(len(data))])
    new_points = []
    new_colors = []
    while len(active_points) > 0:
        print(len(active_points))
        cur_pt, cur_normal, cur_color = data[active_points.pop()]
        # Get points, that are close to the current point
        vicinity_pts = [cur_pt]
        vicinity_colors = [cur_color]

        nearby_point_indices = set(pcd_tree.search_radius_vector_3d(cur_pt, 0.15)[1])
        ex_nearby_pt_indices = nearby_point_indices.intersection(active_points)
        processed_indices = set()
        for i in ex_nearby_pt_indices:
            if np.linalg.norm(np.cross(cur_pt - np.array(data[i][0]), cur_normal)) < 0.009:
                vicinity_pts.append(data[i][0])
                vicinity_colors.append(data[i][2])
                processed_indices.add(i)
        active_points -= processed_indices

        new_points.append(np.mean(vicinity_pts, axis=0))
        new_colors.append(np.mean(vicinity_colors, axis=0))
    new_pcd = o3d.geometry.PointCloud()
    new_pcd.points = o3d.utility.Vector3dVector(new_points)
    new_pcd.colors = o3d.utility.Vector3dVector(new_colors)
    new_pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
    return new_pcd





if __name__ == '__main__':
    # generate a random point cloud
    pcd = o3d.io.read_point_cloud("../test_fragment/pcd/map.pcd")
    #pcd = pcd.voxel_down_sample(voxel_size=0.01)
    # create voxel grid
    grid = o3d.geometry.VoxelGrid.create_from_point_cloud(pcd, voxel_size=0.005)
    # display voxel grid
    o3d.visualization.draw_geometries([grid])
    pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=1, max_nn=30))
    #o3d.visualization.draw_geometries([pcd])
    octree = o3d.geometry.Octree(max_depth=8)
    pcd_tree = o3d.geometry.KDTreeFlann(pcd)
    new_pcd = normal_based_filter(pcd, pcd_tree)
    #display_inlier_outlier(pcd, new_pcd[1])
    o3d.visualization.draw_geometries([new_pcd])
    pcd_cleaned0, _ = new_pcd.remove_statistical_outlier(nb_neighbors=20, std_ratio=3.0)
    pcd_cleaned1, _ = new_pcd.remove_statistical_outlier(nb_neighbors=20, std_ratio=2.5)
    pcd_cleaned2, _ = new_pcd.remove_statistical_outlier(nb_neighbors=20, std_ratio=2.0)
    pcd_cleaned3, _ = new_pcd.remove_statistical_outlier(nb_neighbors=20, std_ratio=1.5)
    pcd_cleaned4, _ = new_pcd.remove_statistical_outlier(nb_neighbors=20, std_ratio=1)
    o3d.visualization.draw_geometries([pcd_cleaned0])
    o3d.visualization.draw_geometries([pcd_cleaned1])
    o3d.visualization.draw_geometries([pcd_cleaned2])
    o3d.visualization.draw_geometries([pcd_cleaned3])
    o3d.visualization.draw_geometries([pcd_cleaned4])
    print(pcd)
    print(new_pcd)
    print(pcd_cleaned0)
    print(pcd_cleaned1)
    print(pcd_cleaned2)
    print(pcd_cleaned3)
    print(pcd_cleaned4)

    #o3d.visualization.draw_geometries([new_pcd])

    #org_pcd = create_organized_pointcloud(pcd)
    #org_pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=10, max_nn=300))
    # visualize the organized point cloud
    #o3d.visualization.draw_geometries([org_pcd])
    #filtered_pcd = median_filter(org_pcd)

    #mesh_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(size=50)
    #o3d.visualization.draw_geometries([filtered_pcd, mesh_frame])

