import copy

import numpy as np
import open3d as o3d

from Entities.PointCloud import PointCloud
from VisualizationUtils import draw_registration_result







def merge_pointcloud_points_and_colors(source: PointCloud, target: PointCloud, registration_result):

    source_points, source_colors = np.asarray(copy.deepcopy(source.pcd.points)), np.asarray(copy.deepcopy(source.pcd.colors))
    target_points, target_colors = np.asarray(copy.deepcopy(target.pcd.points)), np.asarray(copy.deepcopy(target.pcd.colors))
    source_transformed = copy.deepcopy(source.pcd).transform(registration_result.transformation)
    source_points_transformed, source_colors_transformed = np.asarray(source_transformed.points), np.asarray(source_transformed.colors)
    target_transformed = copy.deepcopy(target.pcd).transform(np.linalg.inv(registration_result.transformation))
    target_points_transformed, target_colors_transformed = np.asarray(target_transformed.points), np.asarray(target_transformed.colors)

    corr_set = np.asarray(registration_result.correspondence_set)
    for j in range(len(corr_set)):
        src_idx = corr_set[j][0]
        tgt_idx = corr_set[j][1]



        source_points[src_idx] = np.divide(np.add(source_points[src_idx], target_points_transformed[tgt_idx]), 2)
        source_colors[src_idx] = np.divide(np.add(source_colors[src_idx], target_colors_transformed[tgt_idx]), 2)
        target_points[tgt_idx] = np.divide(np.add(target_points[tgt_idx], source_points_transformed[src_idx]), 2)
        target_colors[tgt_idx] = np.divide(np.add(target_colors[tgt_idx], source_colors_transformed[src_idx]), 2)

    source.pcd.points = o3d.utility.Vector3dVector(source_points)
    source.pcd.colors = o3d.utility.Vector3dVector(source_colors)
    target.pcd.points = o3d.utility.Vector3dVector(target_points)
    target.pcd.colors = o3d.utility.Vector3dVector(target_colors)

    source.make_chorerent()
    target.make_chorerent()



def average_pointcloud_colors(source: PointCloud, target: PointCloud, registration_result, radius):
    source_points, source_colors = np.asarray(copy.deepcopy(source.pcd.points)), np.asarray(
        copy.deepcopy(source.pcd.colors))
    target_transformed = copy.deepcopy(target.pcd).transform(np.linalg.inv(registration_result.transformation))
    target_points_transformed, target_colors_transformed = np.asarray(target_transformed.points), np.asarray(
        target_transformed.colors)

    # Combine points and colors
    points = np.concatenate((source_points, target_points_transformed), axis=0)
    colors = np.concatenate((source_colors, target_colors_transformed), axis=0)
    pcd_combined = o3d.geometry.PointCloud()
    pcd_combined.points = o3d.utility.Vector3dVector(points)
    pcd_combined.colors = o3d.utility.Vector3dVector(colors)


    # Take median of colors for points within the radius
    kdtree = o3d.geometry.KDTreeFlann(pcd_combined)
    new_colors = np.zeros((len(colors), 3))
    for i in range(len(points)):
        [k, idx, _] = kdtree.search_radius_vector_3d(pcd_combined.points[i], radius)
        colors_squared = colors[idx] ** 2
        new_colors[i] = np.sqrt(np.mean(colors_squared, axis=0))
    source.pcd.colors = o3d.utility.Vector3dVector(new_colors[:len(source_points)])
    target.pcd.colors = o3d.utility.Vector3dVector(new_colors[len(source_points):])