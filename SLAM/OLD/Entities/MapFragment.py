import copy
import open3d as o3d
import numpy as np

from Entities import RGBDFrame


class MapFragment:
    def __init__(self, rgbd_frame: RGBDFrame):
        self.frames = [rgbd_frame]
        self.transformations = [np.eye(4)]
        self.map = rgbd_frame.get_PointCloud_from_RGBD()
        self.pt_occurences = list()
        self.number_of_frames = 1
        self.features3d = list()
        for i in range(len(self.map.pcd.points)):
            self.pt_occurences.append(1)

    def get_all_rgbs(self):
        return [frame.rgb for frame in self.frames]

    def get_all_depths(self):
        return [frame.depth for frame in self.frames]

    def get_trajectory(self):
        return self.transformations

    def local_map_dense(self):
        return self.map.pcd

    def local_map_sparse(self):
        return self.map.pcd_down

    def add_frame(self, frame: RGBDFrame, transformation):
        self.frames.append(frame)
        self.transformations.append(transformation)
        self.number_of_frames = len(self.frames)

    def head(self):
        return self.frames[0]

    # Equivalent to head()
    def get_keyframe(self):
        return self.frames[0]

    def tail(self):
        return self.frames[-1]

    def update_map(self, points, colors):
        self.map.pcd.points = o3d.utility.Vector3dVector(points)
        self.map.pcd.colors = o3d.utility.Vector3dVector(colors)
        # downsample
        self.map.pcd = self.map.pcd.voxel_down_sample(self.map.pcd_voxel_size)
        self.map.pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.08, max_nn=30))
        self.update_sparse_map()

    def update_sparse_map(self):
        self.map.pcd_down = self.map.pcd.voxel_down_sample(self.map.pcd_down_voxel_size)
        self.map.pcd_down.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.2, max_nn=30))


    def __add_pcd_with_transformation_to_map(self, pointCloud):
        pcd = copy.deepcopy(pointCloud)
        #self.rel_transformations.append(np.linalg.inv(self.transformations[-2]) @ self.transformations[-1])
        pcd.transform(self.transformations[-1])
        self.map.add(pcd)

    def get_as_o3d_point_cloud(self):
        return self.map.pcd

    def get_frame_ids(self):
        return [frame.id for frame in self.frames]

    def add_3d_features(self, keypoints, descriptors):
        self.features3d[0] = np.concatenate((self.features3d[0], keypoints), axis=0)
        self.features3d[1] = np.concatenate((self.features3d[1], descriptors), axis=0)
