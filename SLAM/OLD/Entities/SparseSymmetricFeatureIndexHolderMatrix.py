from collections import defaultdict


class SparseSymmetricFeatureIndexHolderMatrix:
    def __init__(self):
        self.data = defaultdict(lambda: defaultdict(lambda: ([], [])))

    def __getitem__(self, index):
        i, j = index
        if i > j:
            i, j = j, i
            return self.data[i][j][1], self.data[i][j][0]
        return self.data[i][j]
       # return self.data[i][j] if i == index[0] else (self.data[i][j][1], self.data[i][j][0])

    def __setitem__(self, index, value):
        i, j = index
        if i > j:
            i, j = j, i
            self.data[i][j] = (value[1][:], value[0][:])
        else:
            self.data[i][j] = (value[0][:], value[1][:])
