import open3d as o3d


class PointCloud:
    def __init__(self, pcd, pcd_voxel_size=0.01, pcd_down=None, pcd_down_voxel_size=-1):
        self.pcd = pcd
        self.pcd_voxel_size = pcd_voxel_size
        if pcd_down is None and pcd_down_voxel_size > 0:
            self.pcd_down = pcd.voxel_down_sample(pcd_down_voxel_size)
            self.pcd_down.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
        else:
            self.pcd_down = pcd_down
        self.pcd_down_voxel_size = pcd_down_voxel_size

    def add(self, other):
        self.pcd = self.pcd + other.pcd
        self.pcd_down = self.pcd_down + other.pcd_down
        # Downsample pcd_down again
        self.pcd_down = self.pcd_down.voxel_down_sample(self.pcd_down_voxel_size)

    def transform(self, transformation):
        self.pcd.transform(transformation)
        self.pcd_down.transform(transformation)

    def make_chorerent(self):
        # Recompute normals
        self.pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
        # Recompute downsampled pointcloud
        self.pcd_down = self.pcd.voxel_down_sample(voxel_size=self.pcd_down_voxel_size)
