import copy
from Entities.PointCloud import PointCloud
from Entities.PointcloudWithFeatures import PointCloudWithFeatures


class RGBDFrame(PointCloudWithFeatures):
    def __init__(self, frame_id, rgb, depth, pcd=None, pcd_voxel_size=0.01, pcd_down=None,  pcd_down_voxel_size=-1):
        super().__init__(pcd, pcd_voxel_size, pcd_down, pcd_down_voxel_size, None)
        # Convert read in opencv image to open3D image format

        self.id = frame_id
        self.rgb = rgb  # cv2 Image
        self.depth = depth  # o3d.geometry.Image(depth)
        self.active = True  # Not active if frame was rejected during map generation or because of missing features

    def get_PointCloud_from_RGBD(self):
        return PointCloud(copy.deepcopy(self.pcd), copy.deepcopy(self.pcd_voxel_size), copy.deepcopy(self.pcd_down),
                          copy.deepcopy(self.pcd_down_voxel_size))

    def get_sift_3d_features(self):
        return [self.keypoints["SIFT_filtered_3d"], self.descriptors["SIFT_filtered_3d"]]
