from Entities.SparseSymmetricFeatureIndexHolderMatrix import SparseSymmetricFeatureIndexHolderMatrix
from Entities.Wrapper.RGBDFrameSequence import RGBDFrameSequence
from src.FeatureMatching import chooseMatchingMethod


# Sparse Matrix that uses CSR-Format
class FeatureMatchManager:
    def __init__(self, RGBD_Image_Sequence:RGBDFrameSequence, feature2DMethod:str):
        self.match_matrix = SparseSymmetricFeatureIndexHolderMatrix()  # Holds all matched features as indices
        self.identifier = feature2DMethod
        self.RGBD_Image_Sequence = RGBD_Image_Sequence # Only use as a pointer to the real sequence

        # Get Matching Method for calculating the matches
        self.matching_method = chooseMatchingMethod(feature2DMethod)

    def get_matched_feature_indices(self, idx1, idx2):
        result = self.match_matrix[idx1, idx2]
        if len(result[0]) == 0:
            raise ValueError ("Features not initialized for the given frame combination")
        return result

    def get_matched_keypoints(self, frame1, frame2):
        first_mask, second_mask = self.get_matched_feature_indices(frame1.id, frame2.id)
        return frame1.keypoints[self.identifier][first_mask], frame2.keypoints[self.identifier][second_mask]

    def get_matched_descriptors(self, frame1, frame2):
        first_mask, second_mask = self.get_matched_feature_indices(frame1.id, frame2.id)
        return frame1.descriptors[self.identifier][first_mask], frame2.descriptors[self.identifier][second_mask]

    def set_matches(self, matches, query_frame, train_frame):
        first_indices = [m.queryIdx for m in matches]
        second_indices = [m.trainIdx for m in matches]
        if len(first_indices) != len(second_indices):
            raise ValueError("Match number not equal. Something went wrong.")
        self.match_matrix[query_frame.id, train_frame.id] = (first_indices, second_indices)

    def get_matches_if_available(self, query_frame, train_frame):
        saved_matches = self.match_matrix[query_frame.id, train_frame.id]
        if len(saved_matches[0]) == 0:
            return None
        return saved_matches

    def get_matches(self, query_frame, train_frame):
        saved_matches = self.match_matrix[query_frame.id, train_frame.id]
        if len(saved_matches[0]) == 0:
            matches = self.matching_method(query_frame, train_frame, self.identifier)
            self.set_matches(matches, query_frame, train_frame)
            return matches
        return saved_matches

