from Entities.FeatureMatchManager import FeatureMatchManager
from Entities.RGBDFrame import RGBDFrame


class LandmarkManager:

    def __init__(self):
        self.counter = 100_000  # Assume that frames never reach this high number
        #self.landmarks = dict() # contains key (unique_lm_id) and value (list of tuples with a frame_id as well as a feature id that references this landmark)
        self.frame_landmark_holder = dict()
        self.landmark_counter = dict()

    def get_landmark_id(self, frame_id, feature_id):
        if self.frame_landmark_holder.get(frame_id) is not None:
            pot_lm_id = self.frame_landmark_holder.get(frame_id).get(feature_id)
            if pot_lm_id is not None:
                return pot_lm_id
        return -1


    def get_already_registered_landmark_ids_for_matched_feature_ids(self, frame_id, match_ids):
        found_lm_ids = list()
        for match_id in match_ids:
            found_lm_ids.append(self.get_landmark_id(frame_id, match_id))
        return found_lm_ids


    def register_landmarks(self, frame0:RGBDFrame, frame1:RGBDFrame, match_manager: FeatureMatchManager):
        match_indices0, match_indices1 = match_manager.get_matched_feature_indices(frame0.id, frame1.id)
        # Get already present landmark ids (perhaps registered from other matching process or previous matching)
        frame0_landmark_ids = self.get_already_registered_landmark_ids_for_matched_feature_ids(frame0.id, match_indices0)
        frame1_landmark_ids = self.get_already_registered_landmark_ids_for_matched_feature_ids(frame1.id, match_indices1)

        """
        3 Cases:
         Case 1: Landmark already exists for frame 1 -> register landmark in frame 2 with same id
         Case 2: Landmark already exists in frame 2 -> register landmark in frame 1 with same id
         Case 3: Landmark does not yet exist -> create new Landmark and register it for both
        """
        for idx in range(len(match_indices0)):
            if frame0_landmark_ids[idx] != -1:
                if frame1_landmark_ids[idx] != -1:
                    raise NotImplementedError()
                    # TODO The same landmarks could for example be detected in frame 1 and 2 as well as frame 4 and 5.
                    #  Connecting frame 3 through matches would generate a problem. Only implement this if it ever becomes a problem
                else:
                    self._copy_landmark_to_frame(frame0_landmark_ids[idx], frame1.id, match_indices1[idx])

            elif frame1_landmark_ids[idx] != -1:

                self._copy_landmark_to_frame(frame1_landmark_ids[idx], frame0.id, match_indices0[idx])

            else:
                # Match with other existing landmarks
                # TODO

                # Register count for landmark appearances (twice, since the "same" feature appeared two times)
                self.register_landmark_appearance_first_time(frame0, frame1, match_indices0[idx], match_indices1[idx])

    def _copy_landmark_to_frame(self, lm_id, frame_id, feature_idx):
        if (self.frame_landmark_holder.get(frame_id)) is None:
            self.frame_landmark_holder.__setitem__(frame_id, dict())
        # Remove end
        self.frame_landmark_holder.get(frame_id).__setitem__(feature_idx, lm_id)
        self.landmark_counter[lm_id] += 1



    def register_landmark_appearance_first_time(self, frame0, frame1, feature_idx0, feature_idx1):
        lm_id = self.counter
        if(self.frame_landmark_holder.get(frame0.id)) is None:
            self.frame_landmark_holder.__setitem__(frame0.id, dict())
        if (self.frame_landmark_holder.get(frame1.id)) is None:
            self.frame_landmark_holder.__setitem__(frame1.id, dict())

        self.frame_landmark_holder.get(frame0.id).__setitem__(feature_idx0, lm_id)
        self.frame_landmark_holder.get(frame1.id).__setitem__(feature_idx1, lm_id)
        self.landmark_counter[lm_id] = 2
        self.counter += 1

    def filter_landmarks_by_appearance_count(self, min_count):
        return [key for key, value in self.landmark_counter.items() if value >= min_count]

    def printStatistics(self):
        print(f"Number of Landmarks total: {self.landmark_counter.__sizeof__()}.")
        print(f"Number of Landmarks with at least 3 appearances: {sum(1 for value in self.landmark_counter.values() if value > 2)}.")
        print(f"Number of Landmarks with at least 5 appearances: {sum(1 for value in self.landmark_counter.values() if value > 4)}.")
        print(f"Number of Landmarks with at least 10 appearances: {sum(1 for value in self.landmark_counter.values() if value > 9)}.")
        print(f"Number of Landmarks with at least 25 appearances: {sum(1 for value in self.landmark_counter.values() if value > 24)}.")
        print(f"Number of Landmarks with at least 100 appearances: {sum(1 for value in self.landmark_counter.values() if value > 99)}.")
