from Entities.MapFragment import MapFragment


class MapFragmentManager:

    def __init__(self):
        self.map_fragments = []
        self.frameFragments = dict()

    def add_map_fragment(self, map_fragment: MapFragment):
        self.map_fragments.append(map_fragment)

    def get_fragment_by_frame_id(self, frame_id):
        return self.frameFragments[frame_id]

    def tail(self):
        return self.map_fragments[-1]

    def post_process_all_fragments(self):
        for map_fragment in self.map_fragments:
            for frame in map_fragment.frames:
                self.frameFragments[frame.id] = map_fragment
