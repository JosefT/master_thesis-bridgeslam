import numpy as np

from Entities.RGBDFrame import RGBDFrame


class RGBDFrameSequence:
    def __init__(self, rgbs, depths, pcds):
        self.RGBDFrames = [RGBDFrame(idx, rgb, depth, pcd) for idx, (rgb, depth, pcd) in enumerate(zip(rgbs, depths, pcds))]

    def get_all_rgbs(self):
        return [frame.rgb for frame in self.RGBDFrames]

    def get_all_depths(self):
        return [frame.depth for frame in self.RGBDFrames]

    def get_all_pcds(self):
        return [frame.pcd for frame in self.RGBDFrames]

    def get_all_pcds_down(self):
        return [frame.pcd_down for frame in self.RGBDFrames]

    def get_all_features(self, identifier):
        return [frame.keypoints[identifier] for frame in self.RGBDFrames]

    def get_all_descriptors(self, identifier):
        return [frame.descriptors[identifier] for frame in self.RGBDFrames]


    def get_frame(self, index):
        return self.RGBDFrames[index]

    def remove_frame(self, index):
        self.RGBDFrames.pop(index)

    def create_down_sampled_pointclouds(self, voxel_size):
        for frame in self.RGBDFrames:
            frame.pcd_down = frame.pcd.voxel_down_sample(voxel_size)
            frame.pcd_down_voxel_size = voxel_size

    def delete_all_rgbs(self):
        for frame in self.RGBDFrames:
            del frame.rgb