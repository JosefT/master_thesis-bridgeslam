from Entities.PointCloud import PointCloud
import open3d as o3d


class PointCloudWithFeatures(PointCloud):
    def __init__(self, pcd, pcd_voxel_size=0.01, pcd_down=None, pcd_down_voxel_size=-1, fpfh_down=None):
        super().__init__(pcd, pcd_voxel_size, pcd_down, pcd_down_voxel_size)
        self.keypoints = {}
        if fpfh_down is not None:
            self.descriptors = {"FPFH": fpfh_down}
        else:
            self.descriptors = {}
        self.keypoints = {}

    def add(self, other):
        super().add(other)
        if self.descriptors.keys().__contains__("FPFH"):
            pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(self.pcd_down,
                                                                       o3d.geometry.KDTreeSearchParamHybrid(
                                                                           radius=self.pcd_down_voxel_size * 5,
                                                                           max_nn=100))
            # Add features and descriptors to RGBD_images
            self.descriptors["FPFH"] = pcd_fpfh
