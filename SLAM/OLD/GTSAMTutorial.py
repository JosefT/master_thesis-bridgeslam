import math

import gtsam
import gtsam as gtsam
import numpy as np
from matplotlib import pyplot as plt
import gtsam.utils.plot as gtsam_plot

if __name__ == '__main__':
    # Create a nonlinear graph
    graph = gtsam.NonlinearFactorGraph()
    prior_noise = gtsam.noiseModel.Diagonal.Sigmas(gtsam.Point3(0.3, 0.3, 0))
    odometry_noise = gtsam.noiseModel.Diagonal.Sigmas(gtsam.Point3(0.2, 0.2, 0))

    #Add initial pose
    first_pose = gtsam.Pose2(np.zeros(3))
    graph.add(gtsam.PriorFactorPose2(1, first_pose, prior_noise))

    #Add odometry measurements
    odometry1 = gtsam.Pose2(2,0,0)
    graph.add(gtsam.BetweenFactorPose2(1,2,gtsam.Pose2(2,0,0), odometry_noise))
    graph.add(gtsam.BetweenFactorPose2(2, 3, gtsam.Pose2(2,0, math.pi/2), odometry_noise))
    graph.add(gtsam.BetweenFactorPose2(3, 4, gtsam.Pose2(2, 0, math.pi/2), odometry_noise))
    graph.add(gtsam.BetweenFactorPose2(4, 5, gtsam.Pose2(2, 0, math.pi/2), odometry_noise))

    graph.add(gtsam.BetweenFactorPose2(5, 2, gtsam.Pose2(2, 0, math.pi/2), odometry_noise))


    parameters = gtsam.GaussNewtonParams()

    initial_estimate = gtsam.Values()
    initial_estimate.insert(1, gtsam.Pose2(-0.5, 0.0, 0.2))
    initial_estimate.insert(2, gtsam.Pose2(2.3, 0.1, -0.2))
    initial_estimate.insert(3, gtsam.Pose2(4.1, 0.1, 1.58))
    initial_estimate.insert(4, gtsam.Pose2(4, 2, -3.15))
    initial_estimate.insert(5, gtsam.Pose2(2, 2, -1.53))

    # Stop iterating once the change in error between steps is less than this value
    parameters.setRelativeErrorTol(1e-5)
    # Do not perform more than N iteration steps
    parameters.setMaxIterations(100)
    # Create the optimizer ...
    optimizer = gtsam.GaussNewtonOptimizer(graph, initial_estimate, parameters)
    # ... and optimize
    result = optimizer.optimize()


    # Visualization
    marginals = gtsam.Marginals(graph, result)
    for i in range(1, 6):
        gtsam_plot.plot_pose2(0, result.atPose2(i), 0.5,
                              marginals.marginalCovariance(i))
        print(f"Result for position {i}: {[round(val,2) for val in [result.atPose2(i).x() ,result.atPose2(i).y(), result.atPose2(i).theta()]]}")
    plt.axis('equal')
    plt.show()
