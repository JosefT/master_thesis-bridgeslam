import cv2 as cv2
import numpy as np

from Entities.RGBDFrame import RGBDFrame
from src.DataLoadingMethods import extractDepthMapCorrespondences
from src.General import measure_time
from Entities.Wrapper.RGBDFrameSequence import RGBDFrameSequence
import open3d as o3d

@measure_time
def extractFeatures(RGBD_images: RGBDFrameSequence, identifier):
    if identifier == "SIFT":
        sift = cv2.SIFT_create()
        detector = sift.detect
        computer = sift.compute
    elif identifier == "ORB":
        orb = cv2.ORB_create()
        detector = orb.detect
        computer = orb.compute
    elif identifier == "BRISK":
        brisk = cv2.BRISK_create()
        detector = brisk.detect
        computer = brisk.compute
    elif identifier == "SURF":
        surf = cv2.SURF_create(400)
        detector = surf.detect
        computer = surf.compute
    else:
        raise ValueError("Defined Type not implemented")

    # Extract features for every RGB Image. The Image is converted to grayscale for it. Afterwards, the features are
    # stored within the respective RGBDFrame
    for idx in range(len(RGBD_images.RGBDFrames)):
        gray = cv2.cvtColor(RGBD_images.get_frame(idx).rgb, cv2.COLOR_BGR2GRAY)
        kp = detector(gray, None)
        _, des = computer(gray, kp)
        RGBD_images.get_frame(idx).keypoints[identifier] = kp
        RGBD_images.get_frame(idx).descriptors[identifier] = des
        # Visualize extracted features
        """
        output_image = cv2.drawKeypoints(gray, kp, 0, (255, 0, 0),
                                         flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        cv2.imshow("Features",output_image)
        cv2.waitKey(0)
        """
        

def filter_bad_keypoints(rgb_K, depth_K, rgb_des):
    mask = np.ones(len(depth_K), dtype=int)
    for kp_idx, pts in enumerate(depth_K):
        if np.array_equal(pts, [0., 0., 0.]):
            mask[kp_idx] = 0
    return np.array(rgb_K)[mask == 1], np.array(depth_K)[mask == 1], np.array(rgb_des)[mask == 1]


"""
def helper3dFeaturesFrom2D(RGBD_images: RGBDFrameSequence, identifier2D, identifier2D_filtered, identifier3D_filtered):
    for idx, all_features_from_frame in enumerate(RGBD_images.get_all_features(identifier2D)):
        #Load 2D Keypoints as points (x,y)
        kp_2D = [keyPoint.pt for keyPoint in all_features_from_frame]
        # Retrieve 3D keypoints from Depth Map
        kp_3D = extractDepthMapCorrespondences(kp_2D, RGBD_images.get_frame(idx).depth)
        # Filter keypoints with invalid depth value
        filtered_kp_2D, filtered_kp_3D, filtered_rgb_des = filter_bad_keypoints(kp_2D, kp_3D, RGBD_images.get_frame(idx).descriptors[identifier2D])
        # Add keypoints and (old) descriptor to RGBD_images
        RGBD_images.get_frame(idx).keypoints[identifier2D_filtered] = filtered_kp_2D
        RGBD_images.get_frame(idx).keypoints[identifier3D_filtered] = filtered_kp_3D
        RGBD_images.get_frame(idx).descriptors[identifier2D_filtered] = filtered_rgb_des
        RGBD_images.get_frame(idx).descriptors[identifier3D_filtered] = filtered_rgb_des
"""

def helper3dFeaturesFrom2D(frame : RGBDFrame, identifier2D, identifier2D_filtered, identifier3D_filtered):
    kp_2D = [keyPoint.pt for keyPoint in frame.keypoints[identifier2D]]
    des = frame.descriptors[identifier2D]

    #Load 2D Keypoints as points (x,y)
    # Retrieve 3D keypoints from Depth Map
    kp_3D = extractDepthMapCorrespondences(kp_2D, np.asarray(frame.depth))
    # Filter keypoints with invalid depth value
    filtered_kp_2D, filtered_kp_3D, filtered_rgb_des = filter_bad_keypoints(kp_2D, kp_3D, des)
    # Add keypoints and (old) descriptor to RGBD_images
    frame.keypoints[identifier2D_filtered] = filtered_kp_2D
    frame.keypoints[identifier3D_filtered] = filtered_kp_3D
    frame.descriptors[identifier2D_filtered] = filtered_rgb_des
    frame.descriptors[identifier3D_filtered] = filtered_rgb_des


def get_3d_features_from_2D_features(frame : RGBDFrame, keypoint_2d, descriptor_2d):
    # Retrieve 3D keypoints from Depth Map
    kp_3D = extractDepthMapCorrespondences(keypoint_2d, np.asarray(frame.depth))
    # Filter keypoints with invalid depth value
    filtered_kp_2D, filtered_kp_3D, filtered_rgb_des = filter_bad_keypoints(keypoint_2d, kp_3D, descriptor_2d)
    # Add keypoints and (old) descriptor to RGBD_images
    return filtered_kp_3D, filtered_rgb_des

@measure_time
def extract_3d_features(RGBD_images: RGBDFrameSequence, identifier3D, use_downsampled=True):
    def get_pcd_down(rgbd_frame):
        return rgbd_frame.pcd_down

    def get_pcd(rgbd_frame):
        return rgbd_frame.pcd
    # Check depending on use_downsampled if the normal or the down-sampled pointcloud has already normals. If they do not
    # have normals, an exception is raised.
    if use_downsampled:
        if not RGBD_images.get_frame(0).pcd_down.has_normals():
            raise ValueError("Pointcloud does not have normals. Please compute normals first.")
        radius = 5 * RGBD_images.get_frame(0).pcd_voxel_size
        acquire_pcd = get_pcd_down
    else:
        if not RGBD_images.get_frame(0).pcd.has_normals():
            raise ValueError("Pointcloud does not have normals. Please compute normals first.")
        radius = 5 * RGBD_images.get_frame(0).pcd_voxel_size
        acquire_pcd = get_pcd
    # Currently we only implement FPFH feautres. Hence, they are computed directly:

    for idx, frame in enumerate(RGBD_images.RGBDFrames):
        frame.pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=radius, max_nn=100))
        pcd = acquire_pcd(frame)
        pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(pcd, o3d.geometry.KDTreeSearchParamHybrid(radius=radius, max_nn=100))
        # Add features and descriptors to RGBD_images
        RGBD_images.get_frame(idx).descriptors[identifier3D] = pcd_fpfh


