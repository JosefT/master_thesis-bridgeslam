import glob

import numpy as np
import open3d as o3d
from PointCloudUtils import *


def pairwise_registration(source, target):
    icp_coarse = o3d.pipelines.registration.registration_icp(
        source, target, max_correspondence_distance_coarse, np.identity(4),
        o3d.pipelines.registration.TransformationEstimationPointToPlane())

    icp_fine = o3d.pipelines.registration.registration_icp(
        source, target, max_correspondence_distance_fine,
        icp_coarse.transformation,
        o3d.pipelines.registration.TransformationEstimationPointToPlane())
    transformation_icp = icp_fine.transformation
    information_icp = o3d.pipelines.registration.get_information_matrix_from_point_clouds(
        source, target, max_correspondence_distance_fine,
        icp_fine.transformation)
    return transformation_icp, information_icp


def full_registration(pcds, max_correspondence_distance_coarse,
                      max_correspondence_distance_fine):
    pose_graph = o3d.pipelines.registration.PoseGraph()
    odometry = np.identity(4)
    pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(odometry))
    n_pcds = len(pcds)
    for source_id in tqdm(range(n_pcds)):
        for target_id in tqdm(range(source_id + 1, n_pcds)):
            transformation_icp, information_icp = pairwise_registration(pcds[source_id], pcds[target_id])
            if target_id == source_id + 1:  # next to each other case
                odometry = np.dot(transformation_icp, odometry)
                pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(np.linalg.inv(odometry)))
                pose_graph.edges.append( o3d.pipelines.registration.PoseGraphEdge(source_id,target_id,transformation_icp, information_icp,uncertain=False))
            else:  # loop closure case
                pose_graph.edges.append(o3d.pipelines.registration.PoseGraphEdge(source_id,target_id,transformation_icp,information_icp, uncertain=True))

    return pose_graph




if __name__ == '__main__':
    print("Multiway Registration begin...")

    print("Set parameters")
    voxel_size = 0.05
    max_correspondence_distance_coarse = voxel_size * 15
    max_correspondence_distance_fine = voxel_size * 1.5
    amount = 693
    point_clouds = loadPointCloudsFromFreiburgDataset(amount)
    for i in range(len(point_clouds)):
        point_clouds[i] = point_cloud_preprocess(point_clouds[i], voxel_size)
    print("Apply registration")
    pose_graph = full_registration(point_clouds, max_correspondence_distance_coarse, max_correspondence_distance_fine)
    print("Optimizing PoseGraph ...")
    option = o3d.pipelines.registration.GlobalOptimizationOption(
        max_correspondence_distance=max_correspondence_distance_fine,
        edge_prune_threshold=0.25,
        reference_node=0)


    with o3d.utility.VerbosityContextManager(
            o3d.utility.VerbosityLevel.Debug) as cm:
        o3d.pipelines.registration.global_optimization(
            pose_graph,
            o3d.pipelines.registration.GlobalOptimizationLevenbergMarquardt(),
            o3d.pipelines.registration.GlobalOptimizationConvergenceCriteria(),
            option)

    print("Transform points and display")
    for point_id in range(len(point_clouds)):
        point_clouds[point_id].transform(pose_graph.nodes[point_id].pose)
    o3d.visualization.draw_geometries(point_clouds)