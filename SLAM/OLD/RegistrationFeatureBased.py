import copy

import cv2
import numpy as np
import open3d as o3d
from scipy.optimize import least_squares
from Entities.FeatureMatchManager import FeatureMatchManager

from Entities.RGBDFrame import RGBDFrame
from src.General import measure_time
from src.Open3d_Tools import addPointsToPointCloud


def extractFeaturePoints(matches, frame_1: RGBDFrame, frame_2: RGBDFrame, identifier):
    query_indices = [match.queryIdx for match in matches]
    train_indices = [match.trainIdx for match in matches]
    keypoints1 = frame_1.keypoints[identifier][train_indices]
    keypoints2 = frame_2.keypoints[identifier][query_indices]
    return keypoints1, keypoints2

def estimate_error(x, src_pts, dst_pts):
    # Define the transformation matrix as a 4x4 homogeneous transformation matrix
    M = np.eye(4)
    M[:3, :3] = np.reshape(x[:9], (3, 3))
    M[:3, 3] = x[9:]
    # Apply the transformation to the source points
    dst_pts_est = np.dot(M, np.vstack((src_pts.T, np.ones((1, src_pts.shape[0])))))
    dst_pts_est = dst_pts_est[:3, :].T
    # Compute the difference between the estimated and actual destination points
    error = (dst_pts_est - dst_pts).ravel()
    return error


@measure_time
def levenberg_marquardt(feature_match_manager: FeatureMatchManager, src_frame: RGBDFrame, dst_frame: RGBDFrame, identifier, initialGuess):
    src_keypoints, dst_keypoints = feature_match_manager.get_matched_keypoints(src_frame,dst_frame, identifier)
    # Initial guess for the transformation parameters
    x0 = initialGuess[:3, :4].flatten()
    # Call the least_squares function to minimize the function
    res = least_squares(estimate_error, x0, args=(src_keypoints, dst_keypoints), method='lm')
    M_est = np.eye(4)
    M_est[:3, :3] = np.reshape(res.x[:9], (3, 3))
    M_est[:3, 3] = res.x[9:]
    return M_est, res.cost

def keyPointsToFeatures(frame_1: RGBDFrame, keypoints):
    feat = o3d.pipelines.registration.Feature()
    feat.data = keypoints
    return feat


