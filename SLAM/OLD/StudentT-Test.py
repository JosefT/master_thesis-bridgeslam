import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal, multivariate_t


class IncrementalBivariateGaussian:
    def __init__(self, mu=None, cov=None):
        if mu is None:
            self.mu = np.zeros(2)
        else:
            self.mu = mu
        if cov is None:
            self.cov = np.eye(2)
        else:
            self.cov = cov
        self.n = 0

    def update(self, x):
        self.n += 1
        delta = x - self.mu
        self.mu += delta / self.n
        new_delta = x - self.mu
        self.cov += np.outer(new_delta, delta)

    def pdf(self, x, y):
        rv = multivariate_normal(self.mu, self.cov)
        return rv.pdf(np.array([x, y]))

class IncrementalBivariateStudentT:
    def __init__(self, df, mu=None, cov=None, n=0):
        self.df = df
        if mu is None:
            self.mu = np.zeros(2)
        else:
            self.mu = mu
        if cov is None:
            self.cov = np.eye(2)
        else:
            self.cov = cov
        self.n = n

    def update(self, x):
        self.n += 1
        delta = x - self.mu
        self.mu += delta / self.n
        new_delta = x - self.mu
        alpha = (self.df + self.n - 2) / (self.df + self.n * np.sum(new_delta ** 2) / (self.df + self.n - 2))
        self.cov = alpha * self.cov + (1 - alpha) * np.outer(delta, delta) / (self.df + self.n - 2)

    def pdf(self, x, y):
        return multivariate_t.pdf(np.array([x, y]), self.mu, self.cov, self.df)

def plot_incremental_bivariate_gaussian(ibg):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x, y = np.meshgrid(np.linspace(-10, 10, 100), np.linspace(-10, 10, 100))
    z = np.zeros_like(x)
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            z[i, j] = ibg.pdf(x[i, j], y[i, j])

    ax.plot_surface(x, y, z, cmap='coolwarm')
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    ax.set_title("Incremental Bivariate Gaussian")

    plt.show()



if __name__ == '__main__':
    ibst = IncrementalBivariateStudentT(df=5)

    # Add some observations
    x = np.array([1., 2.])
    y = np.array([-1., 3.])
    z = np.array([2., 4.])
    ibst.update(x)
    ibst.update(y)
    ibst.update(z)

    # Check that the mean and covariance are being updated correctly
    expected_mean = np.array([0.66666667, 3])
    expected_cov = np.array([[1.4286, 0.2857], [0.2857, 1.2857]])
    print(ibst.mu)
    print(ibst.cov)
    assert np.allclose(ibst.mu, expected_mean)
    assert np.allclose(ibst.cov, expected_cov)

    # Add some more observations
    w = np.array([0., 0.])
    ibst.update(w)

    # Check that the mean and covariance are being updated correctly
    expected_mean = np.array([0.5, 2])
    expected_cov = np.array([[1.6667, 0.1667], [0.1667, 1.3333]])
    assert np.allclose(ibst.mu, expected_mean)
    assert np.allclose(ibst.cov, expected_cov)