import numpy as np
import open3d as o3d
import cv2
from matplotlib import pyplot as plt


def get_edges():

    depth_map = cv2.imread('datasets/bridge/02/depth/0.png')
    # Apply Gaussian smoothing
    smoothed = cv2.GaussianBlur(depth_map, (5, 5), 0)

    # Perform Canny edge detection
    edges = cv2.Canny(depth_map, 20, 60)  # Adjust the threshold values as needed

    # Display the original depth map and edges
    #cv2.imshow('Depth Map', depth_map)
    #cv2.imshow('Smoothed', smoothed)
    #cv2.imshow('Edges', edges)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    return edges
def guided_filter(input_image, guidance_image, radius, epsilon):
    # Convert the input and guidance images to float32
    input_float = input_image.astype(np.float32) / 255.0
    guidance_float = guidance_image.astype(np.float32) / 255.0

    # Compute the mean of the guidance image
    guidance_mean = cv2.boxFilter(guidance_float, -1, (radius, radius))

    # Compute the mean of the input image
    input_mean = cv2.boxFilter(input_float, -1, (radius, radius))

    # Compute the correlation of input and guidance images
    guidance_input_mean = cv2.boxFilter(guidance_float * input_float, -1, (radius, radius))

    # Compute the variance of the guidance image
    guidance_variance = cv2.boxFilter(guidance_float * guidance_float, -1, (radius, radius)) - (guidance_mean * guidance_mean)

    # Compute the covariance of input and guidance images
    guidance_input_covariance = guidance_input_mean - (guidance_mean * input_mean)

    # Compute the slope of the filter
    a = guidance_input_covariance / (guidance_variance + epsilon)

    # Compute the offset of the filter
    b = input_mean - (a * guidance_mean)

    # Compute the mean of the slope and offset
    a_mean = cv2.boxFilter(a, -1, (radius, radius))
    b_mean = cv2.boxFilter(b, -1, (radius, radius))

    # Compute the filtered image
    filtered_image = (a_mean * guidance_float) + b_mean

    return (filtered_image * 255.0).clip(0, 255).astype(np.uint8)

def filter_bilateral(image):
    # Filtering window radius (r)
    r = 8

    # Apply bilateral filter
    filtered = cv2.bilateralFilter(image, r, 75, 75)

    return filtered
if __name__ == "__main__":
    # Load the original depth map and edges
    depth_map = cv2.imread("datasets/bridge/02/depth/0.png", cv2.IMREAD_GRAYSCALE)

    edges = get_edges()

    # Set the radius and epsilon parameters for the guided filter
    radius = 20
    epsilon = 0.1
    # Apply the guided filter to enhance the edges in the depth map
    filtered_depth_map = filter_bilateral(depth_map)

    # Display the original depth map, edges, and filtered depth map
    cv2.imshow('Depth Map', depth_map)
    cv2.imshow('Edges', edges)
    cv2.imshow('Filtered Depth Map', filtered_depth_map)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    color = o3d.io.read_image("datasets/bridge/02/rgb/0.png")
    rgbd = o3d.geometry.RGBDImage.create_from_color_and_depth(color, depth_map, depth_trunc=100.0, convert_rgb_to_intensity=False)
    pcd = o3d.geometry.PointCloud.create_from_rgbd_image(rgbd, o3d.camera.PinholeCameraIntrinsic(o3d.camera.PinholeCameraIntrinsicParameters.PrimeSenseDefault))


