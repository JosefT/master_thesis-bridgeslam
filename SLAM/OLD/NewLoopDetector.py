from logging import info, critical

import faiss
import numpy as np


class LoopDetectorVLAD:
    def __init__(self, RGBDFrameSequence, feature_identifier, num_words=512):
        self.rgbs = RGBDFrameSequence.get_all_rgbs()
        # Test Descriptor dimensions:

        descriptors_temp = []
        for i in range(len(self.rgbs)):
            descriptors_temp = RGBDFrameSequence.get_all_descriptors(feature_identifier)

        # flatten descriptors and compute mask
        desc_numbers = [len(desc)  for desc in descriptors_temp]
        # append a zero to the beginning of the list
        desc_numbers.insert(0, 0)
        self.rgb_desc_ptr = np.cumsum(desc_numbers, axis=0)

        self.descriptors = np.concatenate([np.asarray(desc) for desc in descriptors_temp], axis=0, dtype=np.float32)

        print(self.descriptors.shape)
        self.desc_dim = self.descriptors.shape[1]
        self.num_words = num_words
        super().__init__()

    def desc_for_rgb(self, rgb_idx):
        return self.descriptors[self.rgb_desc_ptr[rgb_idx]:self.rgb_desc_ptr[rgb_idx + 1]]

    def gen_vlad_represenation(self):

        kmeans = faiss.Kmeans(d=self.descriptors.shape[-1], k=self.num_words, niter=20, verbose=True, gpu=False)
        kmeans.train(self.descriptors)
        centroids = kmeans.centroids

        residuals = np.zeros((len(self.rgbs), self.num_words, self.desc_dim))
        for i in range(len(self.rgbs)):
            local_descriptors = self.desc_for_rgb(i)
            D, V = kmeans.index.search(local_descriptors, 1)
            V = V.squeeze(1)
            for desc_idx in range(len(local_descriptors)):
                vote = V[desc_idx]
                residuals[i][vote] += local_descriptors[desc_idx] - centroids[vote]
        return residuals


    def similarity_search_vlad(self, img_vectors):
        num_vectors = len(img_vectors)
        pairwise_distances = np.zeros((num_vectors, num_vectors))
        vector_length = len(img_vectors[0])
        for i in range(num_vectors):
            a = img_vectors[i]
            for j in range(i + 1, num_vectors):
                b = img_vectors[j]
                pairwise_distances[i][j] = np.sum(self.null_safe_cosine_similarity(a, b)) / vector_length

        pairs = [(i, j, pairwise_distances[i][j]) for i in range(len(pairwise_distances)) for j in
                 range(i + 1, len(pairwise_distances))]
        return pairs


    def null_safe_cosine_similarity(self, a, b):
        a_norm = np.linalg.norm(a, axis=1)
        b_norm = np.linalg.norm(b, axis=1)
        norms = np.multiply(a_norm, b_norm)

        nonzero_norms = norms != 0
        cosine_sim = np.zeros((a.shape[0]))
        cosine_sim[nonzero_norms] = np.einsum('ij,ij->i', a[nonzero_norms], b[nonzero_norms]) / norms[nonzero_norms]
        return cosine_sim

    def filter_by_importance(self, pairs):
        # pairs.sort(key=lambda x: x[2] * 0.5 + 0.5 * (np.abs(x[0]-x[1]) / num_images), reverse=True)
        pairs.sort(key=lambda x: x[2], reverse=True)
        # filter all pairs where x[0] and x[1] are too close
        pairs = [pair for pair in pairs if np.abs(pair[0] - pair[1]) > 40]

    def sort_pairs(self, pairs):
        pairs.sort(key=lambda x: x[2], reverse=True)
        return pairs

    def pipeline(self, threshold=0.3, pair_filter=None):
        info("Generating VLAD representation for each Image")
        vlads = self.gen_vlad_represenation()
        info("Compute cosine similarities")
        pairs = self.similarity_search_vlad(vlads)

        pairs = self.sort_pairs(pairs)
        # cut off all pairs that have a similarity below the threshold
        for i in range(len(pairs)):
            if pairs[i][2] < threshold:
                pairs = pairs[:i]
                break
        info(f"Pairs after thresholding: {len(pairs)}")
        if pair_filter is not None:
            pairs = pair_filter(pairs)
        return pairs


def filter_by_frame_distance(pairs):
    new_pairs = []
    for pair in pairs:
        if np.abs(pair[0] - pair[1]) >= 20:
            new_pairs.append(pair)
    return new_pairs

"""
def filter_by_bins(pairs, bins):
    for pair in pairs:
        for i in range(len(bins)):
            start = bins[i]
            end = bins[i + 1]
            if start <= pair[0] < end and :
"""