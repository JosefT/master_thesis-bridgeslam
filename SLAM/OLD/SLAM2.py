import copy

import cv2
import numpy as np
import open3d as o3d
import gtsam
from matplotlib import pyplot as plt

from Entities.FeatureMatchManager import FeatureMatchManager
from VisualizationUtils import draw_registration_result
from src.Bundle_Adjustment import bundle_adjustment_sparsity
from src.DataLoadingMethods import dataAcquisition
from src.FeatureExtraction import extractFeatures, helper3dFeaturesFrom2D
from src.FeatureMatching import chooseMatchingMethod
from scipy.spatial import distance

from src.LoopClosing import *
from src.RigidRegistration import rigid_registration_with_information


def get_camera_intrinsics_as_3x3_matrix():
    fx = 517.3  # focal length x
    fy = 516.5  # focal length y
    cx = 318.6  # optical center x
    cy = 255.3  # optical center y
    return np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])


if __name__ == '__main__':
    np.set_printoptions(precision=5, suppress=True)
    # Load camera intrinsics
    #o3d_intrinsics = o3d.camera.PinholeCameraIntrinsic(640, 480, 517.3, 516.5, 318.6, 255.3)

    # Define initial pose and covariance
    pose = gtsam.Pose3(gtsam.Rot3(np.eye(3)), np.zeros(3))

    # Acquisition
    RGBD_Image_Sequence = dataAcquisition(600)
    RGBD_Image_Sequence.create_down_sampled_pointclouds(0.05)

    # Load dictionary and set up loop detector
    dictionary = load_dictionary()
    build_annoy_trees(dictionary)  # TODO put this into bow generation file
    annoy_forest = AnnoyIndex(128, 'angular')
    annoy_forest.load('temp/annoyTrees.ann')
    loop_detector = LoopDetector(dictionary, annoy_forest)


    featureMethod = "SIFT"
    extractFeatures(RGBD_Image_Sequence, featureMethod)
    # Add descriptors to loop detector
    loop_detector.add_all_frames(RGBD_Image_Sequence, "SIFT")


    ## Consecutive Feature Matching
    matchingMethod = chooseMatchingMethod(featureMethod)
    match_manager = FeatureMatchManager(RGBD_Image_Sequence, "SIFT")

    # Initialize different types of noise
    noise_of_transformation_estimation = gtsam.noiseModel.Diagonal.Sigmas(sigmas=np.array([0.1, 0.1, 0.1, 0.05, 0.05, 0.05]), smart=True)

    # Initialize factorgraph, initial position estimates
    pose_graph = o3d.pipelines.registration.PoseGraph()
    odometry = np.identity(4)
    pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(odometry))

    # Initialize final Map
    point_cloud_map = o3d.geometry.PointCloud()

    # Initialize collectors for pose and transformations
    pose_collector = [] # As homogenous 4x4 transformations from origin
    transformation_collector = []
    information_collector = []

    # Set initial positions
    pose_collector.append(np.eye(4))
    transformation_collector.append(np.eye(4))
    pose_estimation = pose_collector[0]



    first_frame_idx = 0
    second_frame_idx = 1
    while second_frame_idx < len(RGBD_Image_Sequence.RGBDFrames):
        last_frame = RGBD_Image_Sequence.RGBDFrames[first_frame_idx]
        cur_frame = RGBD_Image_Sequence.RGBDFrames[second_frame_idx]

        # Transformation estimation # rough
        T, info = rigid_registration_with_information(last_frame, cur_frame, global_registration=False)



        transformation_collector.append(T)
        information_collector.append(info)
        pose_estimation = np.matmul(T, pose_estimation)
        pose_collector.append(pose_estimation)

        first_frame_idx += 1
        second_frame_idx += 1

    # Add nodes and edges to the graph
    for transf_idx in range(1, len(transformation_collector)):
        relative_pose = gtsam.Pose3(transformation_collector[transf_idx])
        #graph.add(gtsam.BetweenFactorPose3(transf_idx - 1, transf_idx, relative_pose, noise_of_transformation_estimation))
        odometry = np.dot(transformation_collector[transf_idx], odometry)
        pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(np.linalg.inv(odometry)))
        pose_graph.edges.append(o3d.pipelines.registration.PoseGraphEdge(transf_idx - 1, transf_idx,
                                                                         transformation_collector[transf_idx],
                                                                         information_collector[transf_idx-1],
                                                                         uncertain=False))

    # Generate loop closure edges
    loop_closures, closure_transformations, closure_informations = loop_detector.calculate_loop_closures_with_transformations(RGBD_Image_Sequence)
    print(f"Found loop closures: {loop_closures}")
    for l_idx, lc in enumerate(loop_closures):
        pose_graph.edges.append(
            o3d.pipelines.registration.PoseGraphEdge(lc[0],
                                                     lc[1],
                                                     closure_transformations[l_idx],
                                                     closure_informations[l_idx],
                                                     uncertain=True))
        #relative_pose = gtsam.Pose3(closure_transformations[l_idx])
        #draw_registration_result(RGBD_Image_Sequence.RGBDFrames[lc[0]].pcd, RGBD_Image_Sequence.RGBDFrames[lc[1]].pcd, closure_transformations[l_idx])
        #graph.add(gtsam.BetweenFactorPose3(lc[0], lc[1], relative_pose, noise_of_transformation_estimation))


    # Perform graph optimization
    option = o3d.pipelines.registration.GlobalOptimizationOption(
        max_correspondence_distance=0.05,
        edge_prune_threshold=0.25,
        reference_node=0)
    with o3d.utility.VerbosityContextManager(
            o3d.utility.VerbosityLevel.Debug) as cm:
        o3d.pipelines.registration.global_optimization(
            pose_graph,
            o3d.pipelines.registration.GlobalOptimizationLevenbergMarquardt(),
            o3d.pipelines.registration.GlobalOptimizationConvergenceCriteria(),
            option)

    #for i in range(resultPoses.size()):
    #    plot.plot_pose3(1, resultPoses.atPose3(i))
    #plt.axis('equal')
    # plt.show()

    cur_transformation = np.eye(4)
    for idx, frame in enumerate(RGBD_Image_Sequence.RGBDFrames):
        cur_transformation = pose_graph.nodes[idx].pose
        point_cloud_map += copy.deepcopy(frame.pcd).transform(cur_transformation)
    point_cloud_map = point_cloud_map.voxel_down_sample(0.01)
    print(point_cloud_map)
    o3d.visualization.draw_geometries([o3d.geometry.VoxelGrid.create_from_point_cloud(point_cloud_map, 0.01)])

    """
    for idx, frame in enumerate(RGBD_Image_Sequence.RGBDFrames):
        cur_transformation = np.linalg.inv(resultPoses.atPose3(idx).matrix())
        point_cloud_map += frame.pcd.transform(cur_transformation)
    print(point_cloud_map)
    point_cloud_map = point_cloud_map.voxel_down_sample(0.01)
    print(point_cloud_map)
    o3d.visualization.draw_geometries([point_cloud_map])
    """