import argparse
import os
import sys
import numpy as np
import torch
import torch.utils.data
import torchvision

# visualize the point cloud
import open3d as o3d
#o3d.visualization.webrtc_server.enable_webrtc()
import sys

from src.DataLoadingMethods import loadPointCloudsFromFreiburgDataset

sys.path.append('/home/josef/other_projects/PointNetLK_Revisited')
import PointNetLK_Revisited.data_utils as data_utils
import PointNetLK_Revisited.trainer as trainer



if __name__ == '__main__':
    args = argparse.Namespace()
    # dimension for the PointNet embedding
    args.dim_k = 1024

    # device: cuda/cpu
    # args.device = 'cuda:0'
    args.device = 'cuda:0'

    # maximum iterations for the LK
    args.max_iter = 50

    # embedding function: pointnet
    args.embedding = 'pointnet'

    # output log file name
    args.outfile = 'toyexample_2021_04_17'

    # specify data type: real
    args.data_type = 'real'

    # specify visualize result or not
    args.vis = True
    # load data
    pcds = loadPointCloudsFromFreiburgDataset(3)
    p0 = np.asarray(pcds[0].points)[np.newaxis,...]
    p1 = np.asarray(pcds[2].points)[np.newaxis,...]

    # randomly set the twist parameters for the ground truth pose
    x = np.array([[0.57, -0.29, 0.73, -0.37, 0.48, -0.54]])

    # set voxelization parameters
    voxel_ratio = 0.01
    voxel = 2
    max_voxel_points = 100
    num_voxels = 8

    # construct the testing dataset
    testset = data_utils.ToyExampleData(p0, p1, voxel_ratio, voxel, max_voxel_points, num_voxels, x, args.vis)
    # create model
    dptnetlk = trainer.TrainerAnalyticalPointNetLK(args)
    model = dptnetlk.create_model()

    # specify device
    if not torch.cuda.is_available():
        args.device = 'cpu'
        print("CUDA is not available, using CPU instead.")
    args.device = torch.device(args.device)
    model.to(args.device)

    # load pre-trained model
    model.load_state_dict(torch.load('PointNetLK_Revisited/logs/model_trained_on_ModelNet40_model_best.pth', map_location='cpu'))

    # testloader
    testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False, num_workers=1, drop_last=False)

    # begin testing
    dptnetlk.test_one_epoch(model, testloader, args.device, 'test', args.data_type, args.vis, toyexample=False)