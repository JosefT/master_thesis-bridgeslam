import numpy as np
import open3d as o3d
import cv2
import open3d.core as o3c
from tqdm import tqdm

from Entities.Wrapper import MapFragmentManager


class TSDFProcessor:
    def __init__(self):
        super().__init__()

    def integrate_all(self, fragment_manager: MapFragmentManager, intrinsic):
        print("Integrating all fragments...")
        intrinsic = o3c.Tensor(np.array(intrinsic))
        for map_fragment in tqdm(fragment_manager.map_fragments):
            map_fragment.map.pcd = self.integrate(map_fragment, intrinsic)
            map_fragment.map.pcd.voxel_down_sample(voxel_size=0.025)
            map_fragment.map.pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
            map_fragment.update_sparse_map()
        print("Done integrating all fragments.")
    def integrate(self, map_fragment, intrinsic):
        rgbs = map_fragment.get_all_rgbs()
        depths = map_fragment.get_all_depths()
        trajectory = map_fragment.get_trajectory()
        return self.integrate_rgbd(rgbs, depths, trajectory, intrinsic)

    def integrate_rgbd(self, rgbs, depths, extrinsics, intrinsic):
        device = o3d.core.Device("cuda:0")
        vbg = o3d.t.geometry.VoxelBlockGrid(
            attr_names=('tsdf', 'weight', 'color'),
            attr_dtypes=(o3c.float32, o3c.float32, o3c.float32),
            attr_channels=((1), (1), (3)),
            voxel_size=4.0 / 512,
            block_resolution=16,
            block_count=50000,
            device=device)
        depth_scale = 1000.0
        depth_max = 4.0
        for rgb, depth, extrinsic in zip(rgbs, depths, extrinsics):
            extrinsic = o3c.Tensor(np.array(extrinsic))
            depth = o3d.t.geometry.Image().from_legacy(depth).to(device)
            color = o3d.t.geometry.Image(np.asarray(rgb)).to(device)
            frustum_block_coords = vbg.compute_unique_block_coordinates(
                depth, intrinsic, extrinsic, depth_scale,
                depth_max)
            vbg.integrate(frustum_block_coords, depth, color, intrinsic,
                          intrinsic, extrinsic, depth_scale,
                          depth_max)
        pcd = vbg.extract_point_cloud().to_legacy()
        pcd = pcd.remove_statistical_outlier(nb_neighbors=30, std_ratio=2.0)[0]
        return pcd
