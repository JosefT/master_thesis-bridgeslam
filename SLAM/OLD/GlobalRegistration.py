import copy

import numpy as np
import open3d as o3d

from PointCloudUtils import loadPointCloudsFromFreiburgDataset
from VisualizationUtils import draw_registration_result


def execute_fast_global_registration(source_down, target_down, source_fpfh,
                                     target_fpfh, voxel_size):
    distance_threshold = voxel_size * 0.5
    print(":: Apply fast global registration with distance threshold %.3f" \
            % distance_threshold)
    result = o3d.pipelines.registration.registration_fgr_based_on_feature_matching(
        source_down, target_down, source_fpfh, target_fpfh,
        o3d.pipelines.registration.FastGlobalRegistrationOption(
            maximum_correspondence_distance=distance_threshold))
    return result

if __name__ == '__main__':
    print("Global Registration begin...")

    print("Set parameters")
    voxel_size = 0.05
    max_correspondence_distance_coarse = voxel_size * 15
    max_correspondence_distance_fine = voxel_size * 1.5

    amount = 2
    point_clouds = loadPointCloudsFromFreiburgDataset(amount, voxel_size)

    radius_feature = voxel_size * 5
    pcd_fpfhs = []
    for cloud in point_clouds:
        pcd_fpfhs.append(o3d.pipelines.registration.compute_fpfh_feature(cloud,
                                                                         o3d.geometry.KDTreeSearchParamHybrid(
                                                                             radius=radius_feature, max_nn=100)))

    reg_res = execute_fast_global_registration(point_clouds[0], point_clouds[1], pcd_fpfhs[0], pcd_fpfhs[1], voxel_size)
    draw_registration_result(point_clouds[0], point_clouds[1], reg_res.transformation)


    print("Apply registration")
