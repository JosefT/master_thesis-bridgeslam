
import numpy as np
import open3d as o3d
import gtsam
from Entities.FeatureMatchManager import FeatureMatchManager
from Entities.LandmarkManager import LandmarkManager
from src.DataLoadingMethods import dataAcquisition
from src.FeatureExtraction import extractFeatures, helper3dFeaturesFrom2D
from src.FeatureMatching import chooseMatchingMethod

from src.RegistrationFeatureBased import icp


def get_camera_intrinsics_as_3x3_matrix():
    fx = 517.3  # focal length x
    fy = 516.5  # focal length y
    cx = 318.6  # optical center x
    cy = 255.3  # optical center y
    return np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])


if __name__ == '__main__':
    np.set_printoptions(precision=5, suppress=True)
    # Load camera intrinsics
    K = get_camera_intrinsics_as_3x3_matrix()
    # fx, fy, cx, cy = K[0][0], K[1][1], K[0][2], K[1][2]

    # Define initial pose and covariance
    pose = gtsam.Pose3(gtsam.Rot3(np.eye(3)), np.zeros(3))
    covariance = np.eye(6) * 1e-6

    # Acquisition
    RGBD_Image_Sequence = dataAcquisition(50)
    RGBD_Image_Sequence.create_down_sampled_pointclouds(0.05)

    featureMethod = "BRISK"
    extractFeatures(RGBD_Image_Sequence, featureMethod)
    helper3dFeaturesFrom2D(RGBD_Image_Sequence, featureMethod, "2D_filtered", "3D_filtered")

    # Consecutive Feature Matching
    matchingMethod = chooseMatchingMethod(featureMethod)
    match_manager = FeatureMatchManager()

    # Initialize different types of noise
    initial_position_noise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.1] * 6))
    noise_of_transformation_estimation = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.2] * 6))
    bearing_noise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.1] * 3))

    # Initialize factorgraph, initial position estimates and LandmarkManagement
    graph = gtsam.NonlinearFactorGraph()
    init_estimate = gtsam.Values()
    landmark_manager = LandmarkManager()

    # Initialize final Map
    point_cloud_map = o3d.geometry.PointCloud()

    # Initialize collectors for pose and transformations
    pose_collector = [] # As homogenous 4x4 transformations from origin
    transformation_collector = []

    #Set initial positions
    pose_collector.append(np.eye(4))
    transformation_collector.append(np.eye(4))
    pose_estimation = pose_collector[0]

    stride = 1
    while stride < 4:
        first_frame_idx = 0
        second_frame_idx = stride
        while second_frame_idx < len(RGBD_Image_Sequence.RGBDFrames):
            last_frame = RGBD_Image_Sequence.RGBDFrames[first_frame_idx]
            cur_frame = RGBD_Image_Sequence.RGBDFrames[second_frame_idx]

            matches = matchingMethod(last_frame, cur_frame, "3D_filtered")
            match_manager.set_matches(matches, last_frame, cur_frame)
            #dst_pts, src_pts = match_manager.get_matched_keypoints(last_frame, cur_frame, "3D_filtered")

            #Register matched points as Landmarks
            landmark_manager.register_landmarks(last_frame, cur_frame, match_manager)

            #T, cost = levenberg_marquardt(match_manager, cur_frame, last_frame, "3D_filtered", np.eye(4))
            T = icp(last_frame, cur_frame)
            """
            if cost > 1 and stride == 1 and second_frame_idx < len(RGBD_Image_Sequence.RGBDFrames) -1 :
                print(f"Error between Frame {first_frame_idx} and {second_frame_idx} is too high ({cost}).")
                print(f"Try to manually match frame {first_frame_idx} with {second_frame_idx + 1}")
                # prev_good_frame = RGBD_Image_Sequence.get_frame(frame_idx - 2)
                alt_frame = RGBD_Image_Sequence.get_frame(second_frame_idx + 1)
                matches = matchingMethod(last_frame, alt_frame, "3D_filtered")
                match_manager.set_matches(matches, last_frame, alt_frame)
                dst_pts, src_pts = match_manager.get_matched_keypoints(last_frame, alt_frame, "3D_filtered")

                # Register matched points as Landmarks
                landmark_manager.register_landmarks(last_frame, alt_frame, match_manager)

                T, cost1 = levenberg_marquardt(match_manager, alt_frame, last_frame, "3D_filtered",
                                               np.eye(4))
                print(f"New matches: {len(matches)}")
                print(f"New cost: {cost1}")
                if cost1 < 1:
                    RGBD_Image_Sequence.remove_frame(second_frame_idx)
                    cur_frame = alt_frame
                else:
                    print("Cost is still to high")
                    #raise NotImplementedError()
            """

            if stride == 1:
                transformation_collector.append(T)
                pose_estimation = np.matmul(T, pose_estimation)
                pose_collector.append(pose_estimation)


            # draw_registration_result_with_keypoints(cur_frame.pcd, last_frame.pcd, src_pts, dst_pts, T)
            first_frame_idx += stride
            second_frame_idx += stride
        stride += 4

        # Add nodes and edges to the graph
        for transf_idx in range(1, len(transformation_collector)):
            relative_pose = gtsam.Pose3(transformation_collector[transf_idx])
            graph.add(gtsam.BetweenFactorPose3(transf_idx - 1, transf_idx, relative_pose, noise_of_transformation_estimation))
        # Generate an initial estimate from consecutive pose estimations
        for pose_idx, pose in enumerate(pose_collector):
            pose_gtsam = gtsam.Pose3(pose)
            init_estimate.insert(pose_idx, pose_gtsam)  # +1 because of initial pose estimate

        # Add Landmarks
        #Get interesting landmarks
        landmark_ids_sorted_by_counts =  dict(sorted(landmark_manager.landmark_counter.items(), key=lambda x:x[1], reverse=True))

        number_of_Landmarks = round(len(landmark_ids_sorted_by_counts.keys()) / 10)
        relevant_landmark_ids = list(landmark_ids_sorted_by_counts.keys())[:number_of_Landmarks]


        for frame in RGBD_Image_Sequence.RGBDFrames:
            feature_landmark_idx_dict = landmark_manager.frame_landmark_holder.get(frame.id)
            for feature_idx, lm_id in feature_landmark_idx_dict.items():
                if relevant_landmark_ids.__contains__(lm_id):
                    feature_point = frame.keypoints["3D_filtered"][feature_idx]
                    frame_estimated_relative_pose = gtsam.Pose3(transformation_collector[frame.id])
                    landmark_range = gtsam.Pose3.range(frame_estimated_relative_pose, feature_point)
                    bearing = gtsam.Unit3(feature_point)

                    # Add Landmark to the graph
                    graph.add(gtsam.BearingRangeFactorPose3(frame.id, lm_id, bearing, landmark_range, bearing_noise))

                    # Add Landmark absolute estimate
                    if not init_estimate.exists(lm_id):
                        f_keypoint = np.append(feature_point, 1)
                        absolute_estimate = pose_collector[frame.id] @ f_keypoint
                        init_estimate.insert(lm_id, gtsam.Pose3(absolute_estimate))

    # Perform graph optimization
    landmark_manager.printStatistics()
    print(graph.error(init_estimate))
    params = gtsam.LevenbergMarquardtParams()
    # params.setVerbosityLM("trylambda")
    params.setlambdaInitial(1e-5)
    params.setlambdaLowerBound(1e-6)
    params.setlambdaUpperBound(1e6)
    params.setMaxIterations(1000)
    optimizer = gtsam.LevenbergMarquardtOptimizer(graph, init_estimate, params)
    optimizer_result = optimizer.optimize()
    print(graph.error(optimizer_result))
    resultPoses = gtsam.utilities.allPose3s(optimizer_result)
    # marginals = gtsam.Marginals(graph, resultPoses)

    #for i in range(resultPoses.size()):
    #    plot.plot_pose3(1, resultPoses.atPose3(i))
    #plt.axis('equal')
    # plt.show()

    cur_transformation = np.eye(4)
    for idx, frame in enumerate(RGBD_Image_Sequence.RGBDFrames):
        cur_transformation = np.linalg.inv(resultPoses.atPose3(idx).matrix())
        #frame.pcd.paint_uniform_color([0, 0, 0.25 + 0.75 * idx / len(RGBD_Image_Sequence.RGBDFrames)])
        point_cloud_map += frame.pcd.transform(cur_transformation)
        #point_cloud_map.colors += frame.pcd.colors
    print(point_cloud_map)
    point_cloud_map = point_cloud_map.voxel_down_sample(0.01)
    print(point_cloud_map)
    o3d.visualization.draw_geometries([point_cloud_map])
