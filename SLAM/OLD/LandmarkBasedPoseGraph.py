"""
GTSAM Copyright 2010-2018, Georgia Tech Research Corporation,
Atlanta, Georgia 30332-0415
All Rights Reserved
Authors: Frank Dellaert, et al. (see THANKS for the full author list)

See LICENSE for the license information

Simple robot motion example, with prior and two odometry measurements
Author: Frank Dellaert
"""
# pylint: disable=invalid-name, E1101

from __future__ import print_function

import gtsam
import gtsam.utils.plot as gtsam_plot
import matplotlib.pyplot as plt
import numpy as np

# Create noise models
ODOMETRY_NOISE = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.2, 0.2, 0.1]))
PRIOR_NOISE = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.1]))


def main():
    # Define the robot's initial pose
    R = np.array([[0.98, -0.17, 0], [0.17,0.98, 0], [0, 0, 1]])

    # convert the rotation matrix to a gtsam Rot3 object
    rot = gtsam.Rot3(R)

    initial_pose = gtsam.Pose3(gtsam.Rot3.Rodrigues(np.array([0, 0, 0])),
                                gtsam.Point3(np.array([0, 0, 0])))

    # Define the robot's motion between poses
    motion_model = gtsam.Pose3(rot,
                               gtsam.Point3(np.array([1, 0, 0])))

    # Define the graph
    graph = gtsam.NonlinearFactorGraph()

    # Add the initial pose as a prior factor
    prior_noise_model = gtsam.noiseModel.Isotropic.Sigma(6, 0.1)
    graph.add(gtsam.PriorFactorPose3(0, initial_pose, prior_noise_model))



    # Add the motion model as a factor between poses
    motion_noise_model = gtsam.noiseModel.Isotropic.Sigma(6, 0.1)
    for i in range(1, 10):
        graph.add(gtsam.BetweenFactorPose3(i-1, i, motion_model, motion_noise_model))

    # Initialize the values to optimize
    initial_estimates = gtsam.Values()
    initial_estimates.insert(0, initial_pose)

    pose_estimate = initial_pose
    for i in range(1, 10):
        # Compute the pose estimate for pose i
        pose_estimate = pose_estimate.compose(motion_model)

        # Add the pose estimate to the initial estimates object
        initial_estimates.insert(i, pose_estimate)

    # Optimize the graph
    optimizer = gtsam.LevenbergMarquardtOptimizer(graph, initial_estimates)
    optimized_estimates = optimizer.optimize()

    marginals = gtsam.Marginals(graph, optimized_estimates)
    # Print the optimized poses
    for i in range(10):
        print(f"Pose {i}: {optimized_estimates.atPose3(i)}")

    for i in range(1, 10):
        gtsam_plot.plot_pose3(0, optimized_estimates.atPose3(i), 0.5,
                              marginals.marginalCovariance(i))
    plt.axis('equal')
    plt.show()


if __name__ == "__main__":
    main()