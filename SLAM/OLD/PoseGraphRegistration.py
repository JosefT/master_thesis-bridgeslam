import numpy as np
from tqdm import tqdm

from PointCloudUtils import loadPointCloudsFromFreiburgDataset
import open3d as o3d


def performRegistration(source, target):
    icp_coarse = o3d.pipelines.registration.registration_icp(
        source, target, max_correspondence_distance_coarse, np.identity(4),
        o3d.pipelines.registration.TransformationEstimationPointToPlane())

    icp_fine = o3d.pipelines.registration.registration_icp(
        source, target, max_correspondence_distance_fine,
        icp_coarse.transformation,
        o3d.pipelines.registration.TransformationEstimationPointToPlane())
    transformation_icp = icp_fine.transformation
    information_icp = o3d.pipelines.registration.get_information_matrix_from_point_clouds(
        source, target, max_correspondence_distance_fine,
        icp_fine.transformation)
    return transformation_icp, information_icp

def full_registration(pcds, max_correspondence_distance_coarse,
                      max_correspondence_distance_fine):
    pose_graph = o3d.pipelines.registration.PoseGraph()
    odometry = np.identity(4)
    pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(odometry))
    n_pcds = len(pcds)
    for source_id in tqdm(range(n_pcds)):
        for target_id in range(source_id + 1, n_pcds):
            transformation_icp, information_icp = performRegistration(pcds[source_id], pcds[target_id])
            if target_id == source_id + 1:  # next to each other case
                odometry = np.dot(transformation_icp, odometry)
                pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(np.linalg.inv(odometry)))
                pose_graph.edges.append( o3d.pipelines.registration.PoseGraphEdge(source_id,target_id,transformation_icp, information_icp,uncertain=False))
            else:  # loop closure case
                pose_graph.edges.append(o3d.pipelines.registration.PoseGraphEdge(source_id,target_id,transformation_icp,information_icp, uncertain=True))

    return pose_graph

def constructFragmentPoseGraph(pointcloudset):
    pose_graph = o3d.pipelines.registration.PoseGraph()

    # Add the first point cloud as the first node in the pose graph
    pose_graph.nodes.append(o3d.pipelines.registration.PoseGraphNode(np.identity(4)))

    # Register the point clouds and add edges to the pose graph
    for j in range(len(pointcloudset) - 1):
        trans, info = performRegistration(pointcloudset[j], pointcloudset[j + 1])
        node = o3d.pipelines.registration.PoseGraphNode(trans)
        edge = o3d.pipelines.registration.PoseGraphEdge(j, j + 1, trans, info, uncertain=False)
        pose_graph.nodes.append(node)
        pose_graph.edges.append(edge)
    return pose_graph


def align_pcd_segment_frames_consecutive(pcds):
    for k in range(0, len(pcds)-1):
        trans, _ = performRegistration(pcds[k+1], pcds[k])
        pcds[k+1].transform(trans)
    return pcds
if __name__ == '__main__':
    print("Multiway Registration begin...")

    print("Set parameters")
    voxel_size = 0.01
    max_correspondence_distance_coarse = voxel_size * 15
    max_correspondence_distance_fine = voxel_size * 1.5
    fragment_size = 10
    amount = 100
    pcd_list = loadPointCloudsFromFreiburgDataset(amount, voxel_size)

    fragments_combined = []
    for i in range(0, amount, fragment_size):
        print("Construction of fragment {}".format(int(i/fragment_size)))
        pcd_segment = pcd_list[i:i+fragment_size]
        pcd_segment = align_pcd_segment_frames_consecutive(pcd_segment)
        fragment_pcds = pcd_segment[0]
        for j in range(1, fragment_size):
            fragment_pcds += pcd_segment[j]
        fragments_combined.append(fragment_pcds)

    pose_graph = full_registration(fragments_combined, max_correspondence_distance_coarse, max_correspondence_distance_fine)
    print("Optimizing PoseGraph ...")
    option = o3d.pipelines.registration.GlobalOptimizationOption(
        max_correspondence_distance=max_correspondence_distance_fine,
        edge_prune_threshold=0.25,
        reference_node=0)


    with o3d.utility.VerbosityContextManager(
            o3d.utility.VerbosityLevel.Debug) as cm:
        o3d.pipelines.registration.global_optimization(
            pose_graph,
            o3d.pipelines.registration.GlobalOptimizationLevenbergMarquardt(),
            o3d.pipelines.registration.GlobalOptimizationConvergenceCriteria(),
            option)

    print("Transform points and display")
    for point_id in range(len(fragments_combined)):
        fragments_combined[point_id].transform(pose_graph.nodes[point_id].pose)
    o3d.visualization.draw_geometries(fragments_combined)