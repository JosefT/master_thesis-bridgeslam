import time
from itertools import count

import pyrealsense2 as rs
import numpy as np
import matplotlib.pyplot as plt
from pyquaternion import Quaternion
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation

from src.General import quaternion_to_matrix, quaternion_from_rotation, rotate_vector
from VisualizationUtils import drawValuesAs3DPlot


def rotation_quaternion(roll, pitch, yaw):
    q_roll = Quaternion(axis=[1, 0, 0], radians=roll)
    q_pitch = Quaternion(axis=[0, 1, 0], radians=pitch)
    q_yaw = Quaternion(axis=[0, 0, 1], radians=yaw)
    q = q_yaw * q_pitch * q_roll
    return q


def rotate_vector_by_quaternion(vector, quaternion):
    rotated_vector = quaternion.rotate(vector)
    return rotated_vector


def process_gyro(gyro, old_rotation_q, el_time):
    theta_rotation = [radian * el_time for radian in gyro]
    R_q = rotation_quaternion(theta_rotation[0], theta_rotation[1], theta_rotation[2])
    new_R_q = old_rotation_q * R_q
    return new_R_q


def process_accel(acceleration, rot_matrix, elapsed_time, prev_position):
    gravity_global = np.array([0, 9.81, 0])  # gravity in device coordinate system
    gravity_device = np.dot(rot_matrix, gravity_global)
    acceleration += np.dot(rot_matrix, gravity_device) # subtract gravity
    return acceleration




if __name__ == '__main__':
    pipeline = rs.pipeline()

    config = rs.config()
    config.enable_stream(rs.stream.accel, rs.format.motion_xyz32f, 200)  # Eigentlich rs.stream.gyro ?
    config.enable_stream(rs.stream.gyro, rs.format.motion_xyz32f, 200)

    # Start streaming
    pipeline.start(config)

    # initialize variables
    position = np.array([0, 0, 0])  # starting position
    velocity = np.array([0, 0, 0]) # current_velocity
    q_orientation = Quaternion(axis=[0, 0, 1], angle=0)

    position_history = [[0, 0, 0]]
    velocity_history = [[0, 0, 0]]
    gravity_global = np.array([0, 9.81, 0])  # gravity in world coordinate system

    start_time = time.time()
    iteration = 0
    while True:
        frame = pipeline.wait_for_frames()
        g = frame[1].as_motion_frame().get_motion_data()
        gyro_data = [g.x, g.y, g.z]
        a = frame[0].as_motion_frame().get_motion_data()
        accel_data = [a.x, a.y, a.z]
        if(iteration == 0):
            gravity_global = [-1*acc for acc in accel_data]
            print("Start")
        #print(accel_data)
        #time measurement
        dt = time.time() - start_time
        start_time = time.time()

        q_orientation = process_gyro(gyro_data, q_orientation, dt)
        #position = process_accel(accel_data, q_orientation, dt, position)

        #R_inv = q_orientation.inverse.rotation_matrix
        #T = np.concatenate((R_inv, -R_inv.dot(position.reshape((3, 1)))), axis=1)
        #T = np.concatenate((T, np.array([[0, 0, 0, 1]])), axis=0)
        #gravity_global_homogenous = np.concatenate((gravity_global, [1]))
        #gravity_device = T.dot(gravity_global_homogenous)
        #gravity_device_norm = gravity_device[:3]/np.linalg.norm(gravity_global_homogenous)
        #print(accel_data + gravity_device[:3])


        de_g_forced_accel_data = q_orientation.rotation_matrix.dot(accel_data) + gravity_global
        #de_g_forced_accel_data = q_orientation.rotation_matrix.dot(de_g_forced_accel_data)

        velocity_history.append(velocity_history[-1:][0] + de_g_forced_accel_data * dt)
        position_history.append(position_history[-1:][0] + velocity_history[-1:][0] * dt)
        iteration += 1
        #print(de_g_forced_accel_data)
        step = 2_000
        if iteration % step == 0:
            # Define a 3D vector to rotate
            for i in range(0, step, 100):
                print(f"Velocity {iteration - step + i}: {np.average(velocity_history[iteration - step + i:iteration - step +100 + i], axis=0)}")
            print("\n")
            for i in range(0, step, 100):
                print(f"Position {iteration - step + i}: {position_history[iteration - step + i]}")
            print("\n")

            drawValuesAs3DPlot(np.array(position_history).T.tolist())
            if False:
                v = np.array([0, 0, 1])  # Example vector
                # Rotate the vector
                v_rotated = rotate_vector_by_quaternion(v, q_orientation)
                # Plot the original and rotated vectors
                ax = plt.figure().add_subplot(projection='3d')
                ax.quiver(0, 0, 0, 0, 0, 1, color='b')
                ax.quiver(0, 0, 0, v_rotated[0], v_rotated[1], v_rotated[2], color='r')
                ax.quiver(0, 0, 0, 0, 1, 0, color='g')
                ax.quiver(0, 0, 0, 1, 0, 0, color='y')
                ax.quiver(0, 0, 0, gravity_device_norm[0], gravity_device_norm[1], gravity_device_norm[2], color='black')
                ax.set_xlim([-1, 1])
                ax.set_ylim([-1, 1])
                ax.set_zlim([-1, 1])
                ax.set_xlabel('X')
                ax.set_ylabel('Y')
                ax.set_zlabel('Z')
                ax.view_init(elev=-50, azim=-88, roll=0)
                plt.show()