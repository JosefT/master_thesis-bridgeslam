import copy

import numpy as np

from Entities.FeatureMatchManager import FeatureMatchManager
from Entities.RGBDFrame import RGBDFrame
from src.Open3d_Tools import addPointsToPointCloud, points_to_pcd
from src.RegistrationFeatureBased import levenberg_marquardt, ransac
from src.DataLoadingMethods import dataAcquisition
import cv2
import open3d as o3d

from scipy.optimize import least_squares

from src.FeatureExtraction import  extractFeatures, helper3dFeaturesFrom2D
from src.FeatureMatching import matchSIFTFeaturesFLANN, chooseMatchingMethod
from VisualizationUtils import draw_registration_result




def getIntrinsicCameraMatrix():
    fx = 517.3  # focal length x
    fy = 516.5  # focal length y
    cx = 318.6  # optical center x
    cy = 255.3  # optical center y
    return np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])




if __name__ == '__main__':
    #Acquisition
    RGBD_Images = dataAcquisition(2)
    RGBD_Images.create_down_sampled_pointclouds(0.05)

    #Feature Extraction
    featureMethod = "BRISK"
    extractFeatures(RGBD_Images, featureMethod)
    helper3dFeaturesFrom2D(RGBD_Images, featureMethod, "2D_filtered", "3D_filtered")

    # Feature Matching
    matches = None
    matchingMethod = chooseMatchingMethod(featureMethod)
    feature_match_manager = FeatureMatchManager()
    for idx in range(1, len(RGBD_Images.RGBDFrames)):
        matches = matchingMethod(RGBD_Images.get_frame(idx-1), RGBD_Images.get_frame(idx), "3D_filtered")
        feature_match_manager.set_matches(matches, RGBD_Images.get_frame(idx-1), RGBD_Images.get_frame(idx))
    print("Original loss: {}".format(np.average(RGBD_Images.get_frame(0).pcd.compute_point_cloud_distance(RGBD_Images.get_frame(1).pcd))))

    # Transformation Estimation
    current_transformation, _ = levenberg_marquardt(feature_match_manager, RGBD_Images.get_frame(1), RGBD_Images.get_frame(0),"3D_filtered", np.eye(4))
    lev_marq_transformed = copy.deepcopy(RGBD_Images.get_frame(1).pcd).transform(current_transformation)
    print("Lev_Marquardt: {}".format(np.average(lev_marq_transformed.compute_point_cloud_distance(RGBD_Images.get_frame(0).pcd))))
    print(current_transformation)
    draw_registration_result(RGBD_Images.get_frame(1).pcd, RGBD_Images.get_frame(0).pcd, current_transformation)

    """
    current_transformation = ransac(feature_match_manager, RGBD_Images.get_frame(1), RGBD_Images.get_frame(0), "3D_filtered")
    ransac_transformed_pcd = copy.deepcopy(RGBD_Images.get_frame(1).pcd).transform(current_transformation)
    print("Ransac : {}".format(np.average(ransac_transformed_pcd.compute_point_cloud_distance(RGBD_Images.get_frame(0).pcd))))
    print(current_transformation)
    draw_registration_result(RGBD_Images.get_frame(1).pcd, RGBD_Images.get_frame(0).pcd, current_transformation)
    """
    icp_method = o3d.pipelines.registration.TransformationEstimationPointToPoint()
    icp_result = o3d.pipelines.registration.registration_icp(RGBD_Images.get_frame(1).pcd_down,
                                                          RGBD_Images.get_frame(0).pcd_down,
                                                          RGBD_Images.get_frame(0).pcd_down_voxel_size,
                                                          np.eye(4),
                                                          icp_method)
    icp_transformed_pcd = copy.deepcopy(RGBD_Images.get_frame(1).pcd).transform(icp_result.transformation)
    print("ICP : {}".format(
        np.average(icp_transformed_pcd.compute_point_cloud_distance(RGBD_Images.get_frame(0).pcd))))
    print(icp_result.transformation)
    draw_registration_result(RGBD_Images.get_frame(1).pcd, RGBD_Images.get_frame(0).pcd, icp_result.transformation)
    kp1, kp2 = feature_match_manager.get_matched_keypoints(RGBD_Images.get_frame(0), RGBD_Images.get_frame(1),
                                                           "3D_filtered")

    camera_matrix = getIntrinsicCameraMatrix()
    


    """
    #Compute FPFH features
    used_voxel_size = 0.01
    radius_feature = used_voxel_size * 5
    pcds_fpfh = []
    for pcd in [triple[2] for triple in rgb_depth_pcd_triple]:
        pcds_fpfh.append(o3d.pipelines.registration.compute_fpfh_feature(pcd,o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100)))
    fpfh_ransac_transformation = execute_global_registration(rgb_depth_pcd_triple[0][2], rgb_depth_pcd_triple[1][2], pcds_fpfh[0], pcds_fpfh[1], used_voxel_size).transformation
    fpfh_ransac_transformed_test_pcd = copy.deepcopy(rgb_depth_pcd_triple[0][2]).transform(fpfh_ransac_transformation)
    print("FPFH & Ransac: {}".format(np.average(fpfh_ransac_transformed_test_pcd.compute_point_cloud_distance(rgb_depth_pcd_triple[1][2]))))
    fpfh_ransac_icp_transf = o3d.pipelines.registration.registration_icp(rgb_depth_pcd_triple[0][2], rgb_depth_pcd_triple[1][2], used_voxel_size*0.4, fpfh_ransac_transformation,
        o3d.pipelines.registration.TransformationEstimationPointToPlane()).transformation
    fpfh_ransac_ICP_transformed_test_pcd = copy.deepcopy(rgb_depth_pcd_triple[0][2]).transform(fpfh_ransac_icp_transf)
    print("FPFH & Ransac & ICP: {}".format(np.average(fpfh_ransac_ICP_transformed_test_pcd.compute_point_cloud_distance(rgb_depth_pcd_triple[1][2]))))
    draw_registration_result(rgb_depth_pcd_triple[0][2], rgb_depth_pcd_triple[1][2], fpfh_ransac_icp_transf)
    """
    """
    for match_idx, matches in enumerate(all_matches):
        query_indices = [match.queryIdx for match in matches]
        train_indices = [match.trainIdx for match in matches]
        src_pts = filtered_depth_features[match_idx][query_indices]
        dst_pts = filtered_depth_features[match_idx+1][train_indices]
        
        
        reg_p2p = o3d.pipelines.registration.registration_icp(
            rgb_depth_pcd_triple[0][2], rgb_depth_pcd_triple[1][2], 0.001, np.eye(4),
            o3d.pipelines.registration.TransformationEstimationPointToPoint())
        print(reg_p2p)
        print(reg_p2p.transformation)
        draw_registration_result( rgb_depth_pcd_triple[0][2],  rgb_depth_pcd_triple[1][2], reg_p2p.transformation)
    """

    """transformation = []
    for rgb_K, depth_K in zip(filtered_rgb_features, filtered_depth_features):
        camera_matrix = getIntrinsicCameraMatrix()
        dist_coeffs = np.zeros((4, 1), dtype=np.float32)

        # Solve PnP problem
        _, rvecs, tvecs, inliers = cv2.solvePnPRansac(np.asarray(depth_K), np.asarray(rgb_K), camera_matrix, dist_coeffs)
        # Print results
        #print("Rotation vector:\n", rvec)
        #print("Translation vector:\n", tvec)
        R, _ = cv2.Rodrigues(rvecs)
        #print("Rotation matrix:\n", R)

        T = np.squeeze(tvecs)
        transformation = [[R[0][0], R[0][1],R[0][2],T[0]],[R[1][0], R[1][1],R[1][2],T[1]],[R[2][0], R[2][1],R[2][2],T[2]],[0,0,0,1]]
        break
    """

    """
    reg_p2p = o3d.pipelines.registration.registration_icp(
        rgb_depth_pcd_triple[0][2], rgb_depth_pcd_triple[1][2], 0.05, np.eye(4),
        o3d.pipelines.registration.TransformationEstimationPointToPlane())
    rgb_depth_pcd_triple[0][2].transform(reg_p2p.transformation)
    print("ICP: {}".format(np.average(rgb_depth_pcd_triple[0][2].compute_point_cloud_distance(rgb_depth_pcd_triple[1][2]))))
    o3d.visualization.draw_geometries([rgb_depth_pcd_triple[0][2], rgb_depth_pcd_triple[1][2]])
        #print(estimate_error(T.flatten, src_pts, dest))
    """
    """
    T1 = lastTransformation
    T2 = currentTransformation
    T_relative = np.linalg.inv(T1) @ T2
    print(T_relative)
    o3d.visualization.draw_geometries([rgb_depth_pcd_triple[0][2], rgb_depth_pcd_triple[1][2], pcd])
    # Apply the transformation to pcd1 to align it with pcd2
    pcd1 = rgb_depth_pcd_triple[0][2].transform(T_relative)

    # Visualize the aligned point clouds
    print(lastTransformation)
    pcd.paint_uniform_color((1,0,0))
    o3d.visualization.draw_geometries([pcd1, rgb_depth_pcd_triple[1][2], pcd])
    """

