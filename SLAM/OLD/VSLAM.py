import cv2
import numpy as np
from torch.linalg import svd
import open3d as o3d
from matplotlib import pyplot as plt


def drawlines(img1,img2,lines,pts1,pts2):
    ''' img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines '''
    r,c = img1.shape
    img1 = cv2.cvtColor(img1,cv2.COLOR_GRAY2BGR)
    img2 = cv2.cvtColor(img2,cv2.COLOR_GRAY2BGR)
    for r,pt1,pt2 in zip(lines,pts1,pts2):
        color = tuple(np.random.randint(0,255,3).tolist())
        x0,y0 = map(int, [0, -r[2]/r[1] ])
        x1,y1 = map(int, [c, -(r[2]+r[0]*c)/r[1] ])
        img1 = cv2.line(img1, (x0,y0), (x1,y1), color,1)
        img1 = cv2.circle(img1,tuple(pt1),5,color,-1)
        img2 = cv2.circle(img2,tuple(pt2),5,color,-1)
    return img1,img2

if __name__ == '__main__':
    # Load rgb images
    print("Load RGBs form my_room_mono Dataset")
    color_imgs = []
    for idx in range(5):
        if(idx < 10):
            color_imgs.append(cv2.imread("datasets/my_room_mono/frames_vid_2/MOV20230403_121619_00{}.jpg".format(idx)))
        elif(idx < 100):
            color_imgs.append(cv2.imread("datasets/my_room_mono/frames_vid_2/MOV20230403_121619_0{}.jpg".format(idx)))
        else:
            color_imgs.append(cv2.imread("datasets/my_room_mono/frames_vid_2/MOV20230403_121619_{}.jpg".format(idx)))
    #Show first image using open3d




    #INITIALIZATION
    # Detect
    sift = cv2.SIFT_create()
    detector = sift.detect
    computer = sift.compute

    # Detect and extract
    ft_keypoints = []
    ft_descriptors = []
    gray_imgs = []
    for idx in range(len(color_imgs)):
        gray_imgs.append(cv2.cvtColor(color_imgs[idx], cv2.COLOR_BGR2GRAY))
        ft_keypoints.append(detector(gray_imgs[-1], None))
        _, des = computer(gray_imgs[-1], ft_keypoints[-1])
        ft_descriptors.append(des)

    # Match
    bf = cv2.BFMatcher()
    all_matches = []
    for idx in range(len(color_imgs) - 1):
        index_params = dict(algorithm=1, trees=5)
        search_params = dict(checks=50)
        flann = cv2.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(ft_descriptors[idx], ft_descriptors[idx + 1], k=2)
        pts1 = []
        pts2 = []
        # ratio test as per Lowe's paper
        for i, (m, n) in enumerate(matches):
            if m.distance < 0.8 * n.distance:
                pts2.append(ft_keypoints[idx + 1][m.trainIdx].pt)
                pts1.append(ft_keypoints[idx][m.queryIdx].pt)
        all_matches.append((pts1, pts2))

    for m_idx, matches in enumerate(all_matches):
        pts1 = np.int32(matches[0])
        pts2 = np.int32(matches[1])
        F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_LMEDS)
        # We select only inlier points
        pts1 = pts1[mask.ravel() == 1]
        pts2 = pts2[mask.ravel() == 1]

        # Find epilines corresponding to points in right image (second image) and
        # drawing its lines on left image
        lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
        lines1 = lines1.reshape(-1, 3)
        img5, img6 = drawlines(gray_imgs[m_idx], gray_imgs[m_idx+1], lines1, pts1, pts2)
        # Find epilines corresponding to points in left image (first image) and
        # drawing its lines on right image
        lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
        lines2 = lines2.reshape(-1, 3)
        img3, img4 = drawlines(gray_imgs[m_idx+1], gray_imgs[m_idx], lines2, pts2, pts1)
        plt.subplot(121), plt.imshow(img5)
        plt.subplot(122), plt.imshow(img3)
        plt.show()