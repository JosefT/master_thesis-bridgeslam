import open3d.cpu.pybind.camera
import open3d.cpu.pybind.io

from PointCloudUtils import loadPointCloudsFromFreiburgDataset

if __name__ == '__main__':
    print("Outlier Removal Start")
    print("Set parameters")
    voxel_size = 0.05
    max_correspondence_distance_coarse = voxel_size * 15
    max_correspondence_distance_fine = voxel_size * 1.5
    intrinsic_camera = open3d.cpu.pybind.camera.PinholeCameraIntrinsic(open3d.cpu.pybind.camera.PinholeCameraIntrinsicParameters.PrimeSenseDefault)
    print(intrinsic_camera.intrinsic_matrix)
    x = open3d.cpu.pybind.camera.PinholeCameraIntrinsic(640,480,525,525,320,240)
    print(x)
    print(x.intrinsic_matrix)
    open3d.io.write_pinhole_camera_intrinsic("test.json", x)
    y = open3d.io.read_pinhole_camera_intrinsic("test.json")
    print(y)
    amount = 30
    point_clouds = loadPointCloudsFromFreiburgDataset(amount)
    tracjectory = open3d.cpu.pybind.io.read_pinhole_camera_trajectory()
