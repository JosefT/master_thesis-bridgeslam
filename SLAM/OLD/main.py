# This is a sample Python script.

# Press Umschalt+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import tqdm as tqdm

from src.DataLoadingMethods import *
from VisualizationUtils import draw_registration_result


def featureDetection(pcds, method):
    # Compute ISS Keypoints on Armadillo
    print("Estimating Keypoints:\n")
    keypoint_set = []
    for pcd in tqdm(pcds):
        keypoints = method(pcd)
        keypoint_set.append(keypoints)
        keypoint_set[len(keypoint_set) - 1].paint_uniform_color([1.0, 0.0, 0.0])  # TODO Remove
    return keypoint_set


def featureDescriptionForPointsWithinPointcloud(o3dpoint, o3dpcd):
    pcd = pcl.PointCloud.PointXYZ()
    pcd.from_array(np.asarray(o3dpcd.points))
    query_point = np.asarray(o3dpcd)

    # search_indices = search_indices.append(len(pcd.points)-1)
    # Define the search radius for neighbors
    radius = 0.2

    fpfh = pcl.keypoints.FPFHEstimation.PointXYZ_Normal_FPFHSignature33()
    # Define the FPFH radius
    fpfh_radius = radius * 5

    # Compute the point normals
    ne = pcl.keypoints.NormalEstimationOMP.PointXYZ_Normal()
    ne.setInputCloud(pcd)
    tree = pcl.search.KdTree.PointXYZ()
    ne.setSearchMethod(tree)
    cloud_normals = pcl.PointCloud.Normal()
    ne.setRadiusSearch(radius)
    ne.compute(cloud_normals)

    # Define the query point

    # Find the k nearest neighbors of the query point
    search_indices, sqr_distances = tree.nearest_k_search_for_point_in_radius(query_point, radius)

    # Extract the FPFH features for the query point
    fpfh = pcl.keypoints.FPFHEstimation.PointXYZ_Normal_FPFHSignature33()
    fpfh.setInputCloud(pcd, search_indices)
    fpfh.setInputNormals(cloud_normals, search_indices)
    fpfh.setRadiusSearch(fpfh_radius)
    fpfh.compute()

    # Print the FPFH descriptor for the query point
    fpfh_descriptor = fpfh.points[0].histogram
    print(fpfh_descriptor)

    # Create a search tree to find the nearest neighbors


if __name__ == '__main__':
    pcds = multiple_point_cloud_preprocess(loadPointCloudsFromFreiburgDataset(2), 0.05)

    # Keypoint detection
    featureDetector = o3d.geometry.keypoint.compute_iss_keypoints
    bunchOfKeypoints = featureDetection(pcds, featureDetector)

    # Keypoint description
    points = np.asarray(bunchOfKeypoints.__getitem__(0).points)

    features = []
    for ida, key_pcd in enumerate(bunchOfKeypoints):
        keypoint_fpfhs = []
        indices = []
        pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(pcds[ida], o3d.geometry.KDTreeSearchParamHybrid(radius=0.25, max_nn=100))
        for idy, keypoint in enumerate(np.asarray(key_pcd.points)):
            for idx, point in enumerate(np.asarray(pcds[ida].points)):
                if np.array_equal(point, keypoint):
                    indices.append(idx)
                    break
        if len(indices) != len(np.asarray(key_pcd.points)):
            raise ValueError("Wrong number of indices")
        for ido in indices:
            keypoint_fpfhs.append(pcd_fpfh.data.T[ido])
        feature = o3d.pipelines.registration.Feature()
        feature.data = np.asarray(keypoint_fpfhs).T
        features.append(feature)

    print(len(features))
    print(len(bunchOfKeypoints))
    reg_res = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(bunchOfKeypoints[0], bunchOfKeypoints[1], features[0], features[1], True, max_correspondence_distance=0.01)
    draw_registration_result(pcds[0], pcds[1],
                             reg_res.transformation)