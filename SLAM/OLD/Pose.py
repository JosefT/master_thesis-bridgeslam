import cmath
import math
from threading import Lock

import numpy as np
import pyrealsense2 as rs
import vector3d.vector as vc

class Pose:
    alpha = 0.98
    last_ts_gyro = 0
    gyro_lock = Lock()

    first_accel_received = False

    theta = None



    def process_gyro_frame(self, frame):
        ts = frame.get_timestamp()

        if not self.first_accel_received:
            self.last_ts_gyro = ts
            return

        gyro_data = frame.as_motion_frame().get_motion_data()

        gyro_angle = vc.Vector()
        gyro_angle.x = gyro_data.x #Pitch
        gyro_angle.y = gyro_data.y #Yaw
        gyro_angle.z = gyro_data.z #Roll

        dt_gyro = (ts - self.last_ts_gyro) / 1000.0
        self.last_ts_gyro = ts
        gyro_angle = gyro_angle * dt_gyro

        self.gyro_lock.acquire()
        self.theta.x = self.theta.x - gyro_angle.z
        self.theta.y = self.theta.y - gyro_angle.y
        self.theta.z = self.theta.z + gyro_angle.x
        self.gyro_lock.release()

    def process_acceleration_frame(self, frame):
        accel_data = frame.as_motion_frame().get_motion_data()

        accel_angle = rs.vector()
        accel_angle.z = math.atan2(accel_data.y, accel_data.z)
        accel_angle.x = math.atan2(accel_data.x, math.sqrt(accel_data.y * accel_data.y + accel_data.z * accel_data.z))

        self.gyro_lock.acquire()
        if not self.first_accel_received:
            self.first_accel_received = True
            self.theta = accel_angle
            self.theta.y = cmath.pi  # TODO correct ?
        else:
            self.theta.x = self.theta.x * self.alpha + accel_angle.x * (1 - self.alpha)
            self.theta.z = self.theta.z * self.alpha + accel_angle.z * (1 - self.alpha)
        self.gyro_lock.release()
