import cv2
import numpy as np

from Entities.RGBDFrame import RGBDFrame
from src.General import measure_time


def chooseMatchingMethod(identifier):
    if identifier == "SIFT":
        return matchSIFTFeaturesFLANN
    elif identifier == "BRISK":
        return matchBriskFeaturesFLANN
    elif identifier == "ORB":
        return matchORBFeaturesFLANN
    else:
        raise ValueError("No Matching Procedure implemented for this feature type yet.")

@measure_time
def matchConsecutively(RGBD_Image_Sequence, matchingMethod):
    matches = []
    for idx in range(1, len(RGBD_Image_Sequence.RGBDFrames)):
        matches.append(matchingMethod(RGBD_Image_Sequence.get_frame(idx - 1), RGBD_Image_Sequence.get_frame(idx), "3D_filtered"))
    return matches


def remove_strange_matches(matches):
    # Strange matches are matches for with multiple query indices point to one train index or vice versa.
    matches_one_side_unique = {}
    for i in range(len(matches)):
        m = matches[i]
        matches_one_side_unique[m.trainIdx] = m.queryIdx

    matches_unique = {}
    for trainIdx, queryIdx in matches_one_side_unique.items():
        matches_unique[queryIdx] = trainIdx

    cleaned = []
    for queryIdx, trainIdx in matches_unique.items():
        m = cv2.DMatch()
        m.trainIdx = trainIdx
        m.queryIdx = queryIdx
        cleaned.append(m)

    return cleaned


def filter_by_matches(matches, arr0, arr1):
    arr0 = arr0[[m.queryIdx for m in matches]]
    arr1 = arr1[[m.trainIdx for m in matches]]
    return arr0, arr1

def match_sift_descriptors_flann(des1, des2):
    flann_index_kdtree = 1
    index_params = dict(algorithm=flann_index_kdtree, trees=5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(queryDescriptors=des1, trainDescriptors=des2, k=2)
    good = []
    for m, n in matches:
        if m.distance < n.distance * 0.5:
            good.append(m)
    return remove_strange_matches(good)


def matchSIFTFeaturesFLANN(frame_1: RGBDFrame, frame_2: RGBDFrame, identifier):
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    des1 = frame_1.descriptors[identifier]
    des2 = frame_2.descriptors[identifier]

    matches = flann.knnMatch(queryDescriptors=des1, trainDescriptors=des2, k=2)

    good = []
    for m, n in matches:
        if m.distance < n.distance * 0.65:
            good.append(m)
    cleaned = remove_strange_matches(good)
    return cleaned

def matchBriskFeaturesFLANN(frame_1: RGBDFrame, frame_2: RGBDFrame, identifier):
    des1 = frame_1.descriptors[identifier]
    des2 = frame_2.descriptors[identifier]

    FLANN_INDEX_LSH = 6
    flann_params = dict(algorithm=FLANN_INDEX_LSH,
                        table_number=6,  # 12
                        key_size=12,  # 20
                        multi_probe_level=1)  # 2
    matcher = cv2.FlannBasedMatcher(flann_params, {})
    raw_matches = matcher.knnMatch(des1, trainDescriptors=des2, k=2)

    mkp1, mkp2, good = [], [], []
    for m in raw_matches:
        if len(m) == 2 and m[0].distance < m[1].distance * 0.6:
            m = m[0]
            good.append(m)
    cleaned = remove_strange_matches(good)
    return cleaned

def matchORBFeaturesFLANN(frame_1: RGBDFrame, frame_2: RGBDFrame, identifier):
    des1 = frame_1.descriptors[identifier]
    des2 = frame_2.descriptors[identifier]
    FLANN_INDEX_LSH = 6
    flann_params = dict(algorithm=FLANN_INDEX_LSH,
                        table_number=6,  # 12
                        key_size=12,  # 20
                        multi_probe_level=1)  # 2
    matcher = cv2.FlannBasedMatcher(flann_params, {})
    raw_matches = matcher.knnMatch(des1, des2, k=2)

    mkp1, mkp2, good = [], [], []
    for m in raw_matches:
        if len(m) == 2 and m[0].distance < m[1].distance * 0.6:
            m = m[0]
            good.append(m)
    cleaned = remove_strange_matches(good)
    return cleaned