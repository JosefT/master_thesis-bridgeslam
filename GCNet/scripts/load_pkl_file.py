import pickle

import numpy as np
import open3d as o3d
import torch

if __name__ == "__main__":
    link = "../data/ThreeDMatch_Color/test_info.pkl"
    data = pickle.load(open(link, "rb"))
    root = "../../Datasets/3DMatch_Color/"

    indices = np.arange(0, len(data["src"]))
    # random permute
    #np.random.shuffle(indices)

    for idx in indices:


        transformation = np.eye(4)
        transformation[:3, :3] = np.asarray(data["rot"][idx]).reshape(3, 3)
        transformation[:3, 3] = np.asarray(data["trans"][idx]).reshape(3)

        src_path = root+data["src"][idx]
        print(data["src"])
        tgt_path  = root+data["tgt"][idx]
        if src_path.endswith(".ply"):
            pcd_src = o3d.io.read_point_cloud(src_path)
            pcd_tgt = o3d.io.read_point_cloud(tgt_path)
            pcd_src.transform(transformation)
            #pcd_src.paint_uniform_color([1, 0.706, 0])
            #pcd_tgt.paint_uniform_color([0, 0.651, 0.929])
        else:
            pcd_src = o3d.geometry.PointCloud()
            pcd_tgt = o3d.geometry.PointCloud()
            pcd_src.points = o3d.utility.Vector3dVector(torch.load(root+data["src"][idx]))
            pcd_src.transform(transformation)
            pcd_tgt.points = o3d.utility.Vector3dVector(torch.load(root+data["tgt"][idx]))
            pcd_src.paint_uniform_color([1, 0.706, 0])
            pcd_tgt.paint_uniform_color([0, 0.651, 0.929])
        o3d.visualization.draw_geometries([pcd_src, pcd_tgt])
