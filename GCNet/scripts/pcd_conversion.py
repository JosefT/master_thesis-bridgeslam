import glob
import os
import re
import shutil

import numpy as np
import open3d as o3d
import torch

from utils.helper import sorted_alphanum, list_folders


def convert_too_big_files(root, output_root, max_points):

    # get all subfolders
    scenes = list_folders(root, sort=False)
    for scene in scenes:
        sequences = list_folders(os.path.join(root, scene), sort=False)
        for sequence in sequences:
            # get all ply files
            ply_files = glob.glob(os.path.join(root, scene, sequence, "*.ply"))
            ply_files = sorted_alphanum(ply_files)
            npy_files = glob.glob(os.path.join(root, scene, sequence, "*.npy"))
            for ply in ply_files:
                file_name = ply.split("/")[-1].split(".")[0]
                pcd = o3d.io.read_point_cloud(ply)
                n_points_before = len(pcd.points)
                if n_points_before > max_points:
                    idx = np.random.permutation(n_points_before)[:max_points]
                    pcd = pcd.select_by_index(idx)
                print(file_name, "before: ", n_points_before, "after: ", len(pcd.points))
                # mkdir if not exist
                if not os.path.exists(os.path.join(output_root, scene, sequence)):
                    os.makedirs(os.path.join(output_root, scene, sequence))
                o3d.io.write_point_cloud(os.path.join(output_root, scene, sequence, ply.split("/")[-1].split(".")[0]+".ply"), pcd)
            # copy npy files to output folder
            for npy in npy_files:
                file_name = npy.split("/")[-1]
                shutil.copyfile(npy, os.path.join(output_root, scene, sequence, file_name))


def display_new_files(output_root):
    scenes = list_folders(output_root, sort=False)
    for scene in scenes:
        sequences = list_folders(os.path.join(output_root, scene), sort=False)
        for sequence in sequences:
            pth_files = glob.glob(os.path.join(output_root, scene, sequence, "*.ply"))
            npy_files = glob.glob(os.path.join(output_root, scene, sequence, "*.npy"))
            pth_files = sorted_alphanum(pth_files)
            npy_files = sorted_alphanum(npy_files)
            map = o3d.geometry.PointCloud()
            for pth, npy in zip(pth_files, npy_files):
                pcd = o3d.io.read_point_cloud(pth)
                T = np.load(npy)
                map += pcd.transform(T)
            o3d.visualization.draw_geometries([map], window_name=f"{scene}_{sequence}")

if __name__ == "__main__":
    max_points = 30_000
    root = "/home/josef/Masterarbeit/Masterarbeit/Datasets/3DMatch_Color/train"
    output_root = "/home/josef/Masterarbeit/Masterarbeit/Datasets/3DMatch_Color/train_ply"
    #convert_too_big_files(root, output_root, max_points)
    #output_root = "/media/josef/01D976D7071B5BE0/indoor/train/7-scenes-fire"
    display_new_files(output_root)
