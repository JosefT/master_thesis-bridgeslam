import glob
import json
import os
import pickle

import numpy as np
import open3d as o3d
from tqdm import tqdm

from scripts.cal_overlap import load_info
from scripts.pcd_conversion import list_folders, sorted_alphanum
from utils.helper import get_sequences, get_scenes


def create_info_txt_files(train_input_pth, scenes_train, output_pth):
    print("Creating info.txt files...")
    for scene in tqdm(scenes_train):
        sequences = get_sequences(train_input_pth, scene)
        for sequence in sequences:
            path = os.path.join(train_input_pth, scene, sequence)
            # load all files ending with .npy
            npy_files = glob.glob(os.path.join(train_input_pth, scene, sequence, "*.npy"))
            npy_files = sorted_alphanum(npy_files)
            for npy in npy_files:
                file_name = npy.split("/")[-1].split(".")[0]
                transf = np.load(npy)
                matrix_str = "\n".join("\t".join(f"{x:.8e}" for x in row) for row in transf)
                output_content = f"{scene}\t{sequence}\n{matrix_str}"
                output_file_path = os.path.join(output_pth, scene, sequence, file_name+".info.txt")
                if not os.path.exists(os.path.join(output_pth, scene, sequence)):
                    os.makedirs(os.path.join(output_pth, scene, sequence))
                with open(output_file_path, "w") as file:
                    file.write(output_content)

def copy_ply_files(input_pth, scenes, output_pth):
    counter = 0
    for scene in tqdm(scenes):
        sequences = get_sequences(input_pth, scene)
        for sequence in sequences:
            # load all files ending with .npy
            ply_files = glob.glob(os.path.join(input_pth, scene, sequence, "*.ply"))
            ply_files = sorted_alphanum(ply_files)
            for ply in ply_files:
                counter += 1
                file_name = ply.split("/")[-1]
                output_file_path = os.path.join(output_pth, scene, sequence, file_name)
                if not os.path.exists(os.path.join(output_pth, scene, sequence)):
                    os.makedirs(os.path.join(output_pth, scene, sequence))
                # copy file
                os.system(f"cp {ply} {output_file_path}")
    print(f"Copied {counter} files.")

def create_txt_file_of_folder_names(path, data_path):
    folders = list_folders(path, sort=False)
    if not os.path.exists(data_path):
        os.makedirs(data_path)
    with open(os.path.join(data_path,"train_3dmatch.txt"), "w") as file:
        file.write("\n".join(folders))
    print(folders)

def create_gt_overlap_files(path, json_file_name):
    # read in json file
    with open(os.path.join(path, json_file_name)) as file:
        overlap_info = json.load(file)
    for scene in overlap_info.keys():
        for sequence in overlap_info[scene].keys():
            overlap_infos = overlap_info[scene][sequence]
            for i, info in enumerate(overlap_infos):
                values = info.split(" ")
                overlap_infos[i] = [int(values[0]), int(values[1]), float(values[2])]
            max_frame = max([info[1] for info in overlap_infos])
            overlap_matrix = np.zeros((max_frame+1, max_frame+1))
            for info in overlap_infos:
                overlap_matrix[int(info[0]), int(info[1])] = info[2]

            # create output
            output = ""
            for i in range(overlap_matrix.shape[0]):
                for j in range(i+1, overlap_matrix.shape[1]):
                   output += f"{i},{j},{overlap_matrix[i,j]}\n"
            print(scene)
            print(output)

def get_T_from_info(root, scene, sequence, idx):
    path = os.path.join(root, scene, sequence, "cloud_bin_" + str(idx) + ".info.txt")
    T = load_info(path)
    return T

def create_pkl_file(input_path, prefix, output_path, output_name, scene_info_file):
    dict_file =output_name
    root = os.path.join(input_path, prefix)

    # load train_3dmatch.txt from output_path and put it into a list
    with open(os.path.join(output_path, scene_info_file)) as file:
        scenes_to_be_included = file.read().split("\n")
    info_dict = {}
    info_dict["src"] = list()
    info_dict["tgt"] = list()
    info_dict["rot"] = list()
    info_dict["trans"] = list()
    info_dict["overlap"] = list()
    with open(os.path.join(root, "overlap.json")) as file:
        overlap_info = json.load(file)
    for scene in tqdm(overlap_info.keys()):
        if scene not in scenes_to_be_included:
            continue
        for sequence in overlap_info[scene].keys():
            overlap_pairs = overlap_info[scene][sequence]
            for i, info in enumerate(overlap_pairs):
                values = info.split(" ")
                overlap_pairs[i] = [int(values[0]), int(values[1]), float(values[2])]
            for pair in overlap_pairs:
                src_idx = pair[0]
                tgt_idx = pair[1]
                overlap = pair[2]
                src_trans = get_T_from_info(root, scene, sequence, src_idx)
                tgt_trans = get_T_from_info(root, scene, sequence, tgt_idx)
                T = np.linalg.inv(tgt_trans) @ src_trans
                R = T[:3, :3]
                t = T[:3, 3]
                info_dict["src"].append(os.path.join(prefix, scene, sequence, "cloud_bin_" + str(src_idx) + ".ply"))
                info_dict["tgt"].append(os.path.join(prefix, scene, sequence, "cloud_bin_" + str(tgt_idx) + ".ply"))
                info_dict["rot"].append(R)
                info_dict["trans"].append(t)
                info_dict["overlap"].append(overlap)
    print(info_dict["src"])
    with open(os.path.join(output_path, dict_file), "wb") as file:
        pickle.dump(info_dict, file)


if __name__ == "__main__":
    root = "../../Datasets/3DMatch_Color"
    train_input_pth = root+"/train"
    test_input_pth = root+"/test"
    train_output_pth = root+"/train_converted"
    test_output_pth = root+"/test_converted"
    artifacts_path = root+"/data"

    #create_txt_file_of_folder_names(train_input_pth, atrifacts_path)

    scenes_test = get_scenes(test_input_pth)
    #create_info_txt_files(train_input_pth, scenes_train, train_output_pth)
    #copy_ply_files(train_input_pth, scenes_train, train_output_pth)

    create_pkl_file(root, "test", artifacts_path, "3dmatch.pkl", None)
    #create_gt_overlap_files(train_input_pth, "train_overlap.json")