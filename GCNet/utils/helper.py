import os
import re
from pathlib import Path

import numpy as np
import open3d as o3d
from rich.console import Console
from rich.table import Table


def list_folders(folder_path, name_filter=None, sort=True):
    folders = list()
    for subfolder in Path(folder_path).iterdir():
        if subfolder.is_dir() and not subfolder.name.startswith('.'):
            folder_name = subfolder.name
            if name_filter is not None:
                if name_filter in folder_name:
                    folders.append(folder_name)
            else:
                folders.append(folder_name)
    if sort:
        return sorted_alphanum(folders)
    else:
        return folders


def get_scenes(input_pth):
    # get scenes in train, val, test
    scenes = list_folders(input_pth, sort=False)
    return scenes


def get_sequences(root, scene):
    sequences = list_folders(os.path.join(root, scene), sort=False)
    sequences = sorted_alphanum(sequences)
    return sequences


def sorted_alphanum(file_list_ordered):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key) if len(c) > 0]
    return sorted(file_list_ordered, key=alphanum_key)


def print_table(scenes, scene_recall, rmse, error_r, error_t, overlaps):
    # filter empty scenes
    good_lines = list()
    for i in range(len(scenes)):
        if not (len(rmse[i]) == 0 or len(error_r[i]) == 0 or len(error_t[i]) == 0):
            good_lines.append(i)
    scenes = [scenes[i] for i in good_lines]
    scene_recall = [scene_recall[i] for i in good_lines]
    rmse = [rmse[i] for i in good_lines]
    error_r = [error_r[i] for i in good_lines]
    error_t = [error_t[i] for i in good_lines]
    overlaps = [overlaps[i] for i in good_lines]


    error_r_mean = np.array([np.mean(item) for item in error_r])
    error_r_std = np.array([np.std(item) for item in error_r])
    error_t_mean = np.array([np.mean(item) for item in error_t])
    error_t_std = np.array([np.std(item) for item in error_t])
    rmse_mean = np.array([np.mean(item) for item in rmse])
    rmse_std = np.array([np.std(item) for item in rmse])
    overlap_mean = np.array([np.mean(item) for item in overlaps])

    n_samples = np.array([len(item) for item in rmse])
    console = Console()
    table = Table(show_header=True, header_style="bold")

    columns = ["scene", "recall", "corr. rmse", "r mean", "t mean", "overlap","n_samples"]
    for col in columns:
        table.add_column(col)

    for i in range(len(scenes)):
        row = [f'{scene_recall[i]:.3f}', f'{rmse_mean[i]:.3f} +- {rmse_std[i]:.3f}',
               f'{error_r_mean[i]:.3f} +- {error_r_std[i]:.3f}',
               f'{error_t_mean[i]:.3f} +- {error_t_std[i]:.3f}',  f'{overlap_mean[i]:.3f}', f'{n_samples[i]}']
        table.add_row(str(scenes[i]), *row)

    scene_recall_mean = np.mean(scene_recall)
    rmse_mean_mean = np.mean(rmse_mean)
    re_mean_mean = np.mean(error_r_mean)
    te_mean_mean = np.mean(error_t_mean)
    overlap_mean_mean = np.mean(overlap_mean)

    table.add_row('scene avg', *[f'{scene_recall_mean:.3f}', f'{rmse_mean_mean:.3f}',
                           f'{re_mean_mean:.3f}', f'{te_mean_mean:.3f}',f'{overlap_mean_mean:.3f}', "-"])

    console.print(table)
