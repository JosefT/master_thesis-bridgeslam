import numpy as np
import open3d as o3d
import torch

from eval_3dmatch_color import print_table


def main():
    scenes = ["1", "2", "3"]
    recall = np.asarray([5, 4,3])
    rs = [[0.1, 0.4, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9]]
    ts = [[0.1, 0.2, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9]]


    print_table(scenes, recall, rs, ts)
if __name__ == "__main__":
    main()
