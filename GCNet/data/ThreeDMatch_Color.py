import os
import pickle
import open3d as o3d
import numpy as np
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset

CUR = os.path.dirname(os.path.abspath(__file__))


class ThreeDMatch_Color(Dataset):
    # noinspection PyPackageRequirements
    def __init__(self, root, split, aug, overlap_radius, noise_scale=0.005):
        super().__init__()
        self.root = root
        self.split = split
        self.aug = aug
        self.noise_scale = noise_scale
        self.overlap_radius = overlap_radius
        self.max_points = 30000

        pkl_path = os.path.join(CUR, 'ThreeDMatch_Color', f'{split}_info.pkl')
        if not os.path.exists(pkl_path):
            pkl_path = os.path.join(CUR, 'ThreeDMatch_Color', f'{split}.pkl')
        with open(pkl_path, 'rb') as f:
            self.infos = pickle.load(f)

        # Uncomment to shrink down the dataset for debugging
        #new_infos = {}
        #for inf_key in self.infos.keys():
        #    new_infos[inf_key] = self.infos[inf_key][::30]
        #self.infos = new_infos


    def __len__(self):
        return len(self.infos['rot'])

    def __getitem__(self, item):
        src_path, tgt_path = self.infos['src'][item], self.infos['tgt'][item]  # str, str
        rot, trans = self.infos['rot'][item], self.infos['trans'][item]  # (3, 3), (3, 1)
        # overlap = self.infos['overlap'][item] # float

        src_pcd = o3d.io.read_point_cloud(os.path.join(self.root, src_path))
        tgt_pcd = o3d.io.read_point_cloud(os.path.join(self.root, tgt_path))
        # for gpu memory
        if len(tgt_pcd.points) > self.max_points:
            idx = np.random.permutation(len(src_pcd.points))[:self.max_points]
            src_pcd = src_pcd.select_by_index(idx)
        if len(tgt_pcd.points) > self.max_points:
            idx = np.random.permutation(len(tgt_pcd.points))[:self.max_points]
            tgt_pcd = tgt_pcd.select_by_index(idx)

        if self.aug:
            euler_ab = np.random.rand(3) * 2 * np.pi
            rot_ab = Rotation.from_euler('zyx', euler_ab).as_matrix()
            if np.random.rand() > 0.5:
                src_pcd = src_pcd.rotate(rot_ab, center=(0, 0, 0))
                rot = rot @ rot_ab.T
            else:
                tgt_pcd = tgt_pcd.rotate(rot_ab, center=(0, 0, 0))
                rot = rot_ab @ rot
                trans = rot_ab @ trans
        src_points = np.asarray(src_pcd.points)
        tgt_points = np.asarray(tgt_pcd.points)
        if self.aug:
            src_points += (np.random.rand(src_points.shape[0], 3) - 0.5) * self.noise_scale
            tgt_points += (np.random.rand(tgt_points.shape[0], 3) - 0.5) * self.noise_scale
        T = np.eye(4).astype(np.float32)
        T[:3, :3] = rot
        T[:3, 3] = trans

        src_normals = np.asarray(src_pcd.normals)
        tgt_normals = np.asarray(tgt_pcd.normals)
        src_colors = np.asarray(src_pcd.colors)
        tgt_colors = np.asarray(tgt_pcd.colors)

        # coors = get_correspondences(src_pcd, tgt_pcd, T, self.overlap_radius)
        reg_result = o3d.pipelines.registration.evaluate_registration(src_pcd, tgt_pcd, self.overlap_radius, T)
        coors = np.asarray(reg_result.correspondence_set, dtype=np.int64)
        del reg_result

        src_feats = np.ones_like(src_points[:, :1], dtype=np.float32)
        tgt_feats = np.ones_like(tgt_points[:, :1], dtype=np.float32)

        pair = dict(
            src_points=src_points,
            tgt_points=tgt_points,
            src_feats=src_feats,
            tgt_feats=tgt_feats,
            src_normals=src_normals,
            tgt_normals=tgt_normals,
            src_colors=src_colors,
            tgt_colors=tgt_colors,
            transf=T,
            coors=coors,
            src_points_raw=src_points,
            tgt_points_raw=tgt_points)
        return pair
