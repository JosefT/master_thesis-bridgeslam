import copy

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch_geometric as tg
from torch.nn import ReLU
from torch_geometric.data import Batch
from torch_geometric.nn import GCNConv, global_mean_pool, GATConv
import torch_cluster as tc

from utils import square_dists, gather_points, sample_and_group, angle


def normalize_colors(colors):
    norms = torch.norm(colors, dim=1)
    norms[norms == 0] = 1e-6
    normalized_colors = torch.div(colors, norms.view(-1, 1))
    return normalized_colors


def get_graph_features(feats, coords, k=10):
    '''

    :param feats: (B, N, C)
    :param coords: (B, N, 3)
    :param k: float
    :return: (B, N, k, 2C)
    '''

    sq_dists = square_dists(coords, coords)
    n = coords.size(1)
    inds = torch.topk(sq_dists, min(n, k+1), dim=-1, largest=False, sorted=True)[1]
    inds = inds[:, :, 1:] # (B, N, k)

    neigh_feats = gather_points(feats, inds) # (B, N, k, c)
    feats = torch.unsqueeze(feats, 2).repeat(1, 1, min(n-1, k), 1) # (B, N, k, c)
    return torch.cat([feats, neigh_feats - feats], dim=-1)


class LocalFeatureFused(nn.Module):
    def __init__(self, in_dim, out_dims):
        super(LocalFeatureFused, self).__init__()
        self.blocks = nn.Sequential()
        for i, out_dim in enumerate(out_dims):
            self.blocks.add_module(f'conv2d_{i}',
                                   nn.Conv2d(in_dim, out_dim, 1, bias=False))
            self.blocks.add_module(f'in_{i}',
                                   nn.InstanceNorm2d(out_dims))
            self.blocks.add_module(f'relu_{i}', nn.ReLU())
            in_dim = out_dim

    def forward(self, x):
        '''
        :param x: (B, C1, K, M)
        :return: (B, C2, M)
        '''
        x = self.blocks(x)
        x = torch.max(x, dim=2)[0]
        return x


class PPF(nn.Module):
    def __init__(self, feats_dim, k, radius, use_colors=False):
        super().__init__()
        self.k = k
        self.radius = radius
        if use_colors:
            self.local_feature_fused = LocalFeatureFused(in_dim=13,
                                                         out_dims=feats_dim)
        else:
            self.local_feature_fused = LocalFeatureFused(in_dim=10,
                                                         out_dims=feats_dim)


    def forward(self, coords, feats, colors=None):
        '''

        :param coors: (B, 3, N)
        :param feats: (B, 3, N)
        :param k: int
        :return: (B, C, N)
        '''

        feats = feats.permute(0, 2, 1).contiguous()  # (B, N, 3)
        coords = coords.permute(0, 2, 1).contiguous()  # (B, N, 3)
        if colors is not None:
            colors = colors.permute(0, 2, 1).contiguous()  # (B, N, 3)
        new_xyz, new_points, grouped_inds, grouped_xyz = \
            sample_and_group(xyz=coords,
                             points=feats,
                             M=-1,
                             radius=self.radius,
                             K=self.k)

        #show_pts_with_neighbors(new_xyz, grouped_inds)
        nr_d = angle(feats[:, :, None, :], grouped_xyz)
        ni_d = angle(new_points[..., 3:], grouped_xyz)
        nr_ni = angle(feats[:, :, None, :], new_points[..., 3:])
        d_norm = torch.norm(grouped_xyz, dim=-1)
        ppf_feat = torch.stack([nr_d, ni_d, nr_ni, d_norm], dim=-1) # (B, N, K, 4)
        new_points = torch.cat([new_points[..., :3], ppf_feat], dim=-1)

        coords = torch.unsqueeze(coords, dim=2).repeat(1, 1, min(self.k, new_points.size(2)), 1)
        if colors is not None:
            colors = torch.unsqueeze(colors, dim=2).repeat(1, 1, min(self.k, new_points.size(2)), 1).cuda()
            new_points = torch.cat([coords, new_points, colors], dim=-1)
        else:
            new_points = torch.cat([coords, new_points], dim=-1)
        feature_local = new_points.permute(0, 3, 2, 1).contiguous() # (B, C1 + 3, K, M)
        feature_local = self.local_feature_fused(feature_local)
        return feature_local


class GCN(nn.Module):
    def __init__(self, feats_dim, k):
        super().__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(feats_dim * 2, feats_dim, 1, bias=False),
            nn.InstanceNorm2d(feats_dim),
            nn.LeakyReLU(0.2)
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(feats_dim * 2, feats_dim * 2, 1, bias=False),
            nn.InstanceNorm2d(feats_dim * 2),
            nn.LeakyReLU(0.2)
        )
        self.conv3 = nn.Sequential(
            nn.Conv1d(feats_dim * 4, feats_dim, 1, bias=False),
            nn.InstanceNorm1d(feats_dim),
            nn.LeakyReLU(0.2)
        )
        self.k = k


    def forward(self, coords, feats):
        '''

        :param coors: (B, 3, N)
        :param feats: (B, C, N)
        :param k: int
        :return: (B, C, N)
        '''
        feats1 = get_graph_features(feats=feats.permute(0, 2, 1).contiguous(),
                                    coords=coords.permute(0, 2, 1).contiguous(),
                                    k=self.k)
        feats1 = self.conv1(feats1.permute(0, 3, 1, 2).contiguous())
        feats1 = torch.max(feats1, dim=-1)[0]

        feats2 = get_graph_features(feats=feats1.permute(0, 2, 1).contiguous(),
                                    coords=coords.permute(0, 2, 1).contiguous(),
                                    k=self.k)
        feats2 = self.conv2(feats2.permute(0, 3, 1, 2).contiguous())
        feats2 = torch.max(feats2, dim=-1)[0]

        feats3 = torch.cat([feats, feats1, feats2], dim=1)
        feats3 = self.conv3(feats3)

        return feats3


class GGE(nn.Module):
    def __init__(self, feats_dim, gcn_k, ppf_k, radius, bottleneck, use_colors):
        super().__init__()
        self.gcn = GCN(feats_dim, gcn_k)
        if bottleneck:
            self.ppf = PPF([feats_dim // 2, feats_dim, feats_dim // 2], ppf_k, radius, use_colors=use_colors)
            self.fused = nn.Sequential(
                nn.Conv1d(feats_dim + feats_dim // 2, feats_dim + feats_dim // 2, 1),
                nn.InstanceNorm1d(feats_dim + feats_dim // 2),
                nn.LeakyReLU(0.2),
                nn.Conv1d(feats_dim + feats_dim // 2, feats_dim, 1),
                nn.InstanceNorm1d(feats_dim),
                nn.LeakyReLU(0.2)
                )
        else:
            self.ppf = PPF([feats_dim, feats_dim*2, feats_dim], ppf_k, radius, use_colors=use_colors)
            self.fused = nn.Sequential(
                nn.Conv1d(feats_dim * 2, feats_dim * 2, 1),
                nn.InstanceNorm1d(feats_dim * 2),
                nn.LeakyReLU(0.2),
                nn.Conv1d(feats_dim * 2, feats_dim, 1),
                nn.InstanceNorm1d(feats_dim),
                nn.LeakyReLU(0.2)
                )


    def forward(self, coords, feats, normals, colors=None):
        if colors is not None:
            colors = self.average_colors(colors[:, :, :3])
            colors = colors.unsqueeze(0).permute(0, 2, 1).contiguous()
            feats_ppf = self.ppf(coords, normals, colors)
        else:
            feats_ppf = self.ppf(coords, normals)
        feats_gcn = self.gcn(coords, feats)
        feats_fused = self.fused(torch.cat([feats_ppf, feats_gcn], dim=1))
        return feats_fused

    def average_colors(self, colors):
        mean_colors = torch.mean(colors, dim=1)
        mean_normed_colors = normalize_colors(mean_colors)
        return mean_normed_colors

class GGE_Color_Adv_OLD(nn.Module): # TODO
    def __init__(self, feats_dim, gcn_k, ppf_k, radius, bottleneck):
        super().__init__()
        self.ppf = PPF([feats_dim, feats_dim*2, feats_dim], ppf_k, radius, use_colors=False)
        self.gcn = GCN(feats_dim, gcn_k)

        color_out_dim = 256
        #self.conv1 = GCNConv(3, 16)
        #self.conv2 = GCNConv(16, 16)
        #self.conv3 = GCNConv(16, 16)
        #self.MLP = nn.Sequential(
        #    nn.Linear(16, 64),
        #    nn.ReLU(),
        #    nn.Linear(64, color_out_dim)
        #)
        self.conv1 = GATConv(3, 16, 2)
        self.conv2 = GATConv(32, 16, 2)
        self.conv3 = GATConv(32, 16, 2)
        self.MLP = nn.Sequential(
            nn.Linear(32, 64),
            nn.ReLU(),
            nn.Linear(64, color_out_dim)
        )


        self.fused = nn.Sequential(
            nn.Conv1d(feats_dim * 2+color_out_dim, feats_dim * 2+color_out_dim, 1),
            nn.InstanceNorm1d(feats_dim * 2+color_out_dim),
            nn.LeakyReLU(0.2),
            nn.Conv1d(feats_dim * 2+color_out_dim, feats_dim, 1),
            nn.InstanceNorm1d(feats_dim),
            nn.LeakyReLU(0.2)
        )


    def forward(self, coords, feats, normals, colors):
        # construct KNN graphs for each spot around a chosen point
        data_list = []
        for idx, spot in enumerate(colors):
            pts = spot[:, 3:]
            mask = pts.any(axis=1)
            pts = pts[mask]
            edges = tc.knn_graph(pts, k=5)

            edge_index = edges
            x = spot[mask][:, :3]
            data = tg.data.Data(x=x, edge_index=edge_index)
            data_list.append(data)

        batch = Batch.from_data_list(data_list).to('cuda')
        x = self.conv1(batch.x, batch.edge_index)
        x = F.relu(x)
        x = self.conv2(x, batch.edge_index)
        x = F.relu(x)
        x = self.conv3(x, batch.edge_index)
        graph_vectors = global_mean_pool(x, batch.batch)

        feats_color = self.MLP(graph_vectors)

        # get geometric features
        feats_ppf = self.ppf(coords, normals)
        # get semantic features
        feats_gcn = self.gcn(coords, feats)
        feats_color = feats_color.T.unsqueeze_(0)
        feats_fused = self.fused(torch.cat([feats_color, feats_ppf, feats_gcn], dim=1))

        return feats_fused

class GGE_Color_Adv(nn.Module):
    def __init__(self, feats_dim, gcn_k, ppf_k, radius, config):
        super().__init__()
        self.ppf = PPF([feats_dim, feats_dim*2, feats_dim], ppf_k, radius, use_colors=False)
        self.gcn = GCN(feats_dim, gcn_k)

        color_out_dim = 256
        # set args
        if "graph_layer_type" in config:
            self.graph_layer_type = config.graph_layer_type
            self.n_graph_layers = config.n_graph_layers
            self.num_gat_heads = config.num_gat_heads
            self.graph_hidden_units = config.graph_hidden_units
            self.n_mlp_layers = config.n_mlp_layers
            self.mlp_activation = config.mlp_activation
            self.mlp_hidden_units = config.mlp_hidden_units
            self.mlp_dropout = config.mlp_dropout
        else:
            self.graph_layer_type = "GATConv"
            self.n_graph_layers = 3
            self.num_gat_heads = 2
            self.graph_hidden_units = 16
            self.n_mlp_layers = 2
            self.mlp_activation = "relu"
            self.mlp_hidden_units = 64
            self.mlp_dropout = 0.


        self.graph_layers = nn.ModuleList() # TODO add dropout
        for i in range(self.n_graph_layers):
            if  self.graph_layer_type == "GATConv":
                if i == 0:
                    self.graph_layers.append(GATConv(3, self.graph_hidden_units, self.num_gat_heads))
                else:
                    self.graph_layers.append(GATConv(self.graph_hidden_units * self.num_gat_heads, self.graph_hidden_units, self.num_gat_heads))

            elif self.graph_layer_type == "GCNConv":
                if i == 0:
                    self.graph_layers.append(GCNConv(3, self.graph_hidden_units))
                else:
                    self.graph_layers.append(GCNConv(self.graph_hidden_units, self.graph_hidden_units))
            else:
                raise NotImplementedError


        self.mlp_activation_func = None
        if self.mlp_activation == 'relu':
            self.mlp_activation_func = nn.ReLU()
        elif self.mlp_activation == 'leaky_relu':
            self.mlp_activation_func = nn.LeakyReLU(0.2)
        elif self.mlp_activation == 'elu':
            self.mlp_activation_func = nn.ELU()
        else:
            raise NotImplementedError

        self.MLP = nn.ModuleList()
        for i in range(self.n_mlp_layers):
            self.MLP.append(nn.Dropout(self.mlp_dropout))
            if i == 0:
                if self.graph_layer_type == "GATConv":
                    self.MLP.append(nn.Linear(self.graph_hidden_units * self.num_gat_heads, self.mlp_hidden_units)) # first layer if GAT
                else:
                    self.MLP.append(nn.Linear(self.graph_hidden_units, self.mlp_hidden_units)) # first layer if GCN
                self.MLP.append(self.mlp_activation_func)
            else:
                if not i == self.n_mlp_layers - 1:
                    self.MLP.append(nn.Linear(self.mlp_hidden_units, self.mlp_hidden_units)) # hidden layer
                else:
                    self.MLP.append(nn.Linear(self.mlp_hidden_units, color_out_dim)) # final layer
                if not i == self.n_mlp_layers - 1:
                    self.MLP.append(self.mlp_activation_func)

        self.fused = nn.Sequential(
            nn.Conv1d(feats_dim * 2+color_out_dim, feats_dim * 2+color_out_dim, 1),
            nn.InstanceNorm1d(feats_dim * 2+color_out_dim),
            nn.LeakyReLU(0.2),
            nn.Conv1d(feats_dim * 2+color_out_dim, feats_dim, 1),
            nn.InstanceNorm1d(feats_dim),
            nn.LeakyReLU(0.2)
        )

    def forward(self, coords, feats, normals, colors):
        # construct KNN graphs for each spot around a chosen point
        data_list = []
        for idx, spot in enumerate(colors):
            pts = spot[:, 3:]
            mask = pts.any(axis=1)
            pts = pts[mask]
            edges = tc.knn_graph(pts, k=5)

            edge_index = edges
            x = spot[mask][:, :3]
            data = tg.data.Data(x=x, edge_index=edge_index)
            data_list.append(data)

        batch = Batch.from_data_list(data_list).to('cuda')
        x = batch.x
        n_graph_layers = len(self.graph_layers)
        for layer_idx in range(n_graph_layers):
            layer = self.graph_layers[layer_idx]
            x = layer(x, batch.edge_index)
            if not layer_idx == n_graph_layers - 1:
                x = F.relu(x)
        graph_vectors = global_mean_pool(x, batch.batch)

        for layer in self.MLP:
            graph_vectors = layer(graph_vectors)
        feats_color = graph_vectors

        # get geometric features
        feats_ppf = self.ppf(coords, normals)
        # get semantic features
        feats_gcn = self.gcn(coords, feats)
        feats_color = feats_color.T.unsqueeze_(0)
        feats_fused = self.fused(torch.cat([feats_color, feats_ppf, feats_gcn], dim=1))

        return feats_fused

def multi_head_attention(query, key, value):
    '''

    :param query: (B, dim, nhead, N)
    :param key: (B, dim, nhead, M)
    :param value: (B, dim, nhead, M)
    :return: (B, dim, nhead, N)
    '''
    dim = query.size(1)
    scores = torch.einsum('bdhn, bdhm->bhnm', query, key) / dim**0.5
    attention = torch.nn.functional.softmax(scores, dim=-1)
    feats = torch.einsum('bhnm, bdhm->bdhn', attention, value)
    return feats


class Cross_Attention(nn.Module):
    def __init__(self, feat_dims, nhead):
        super().__init__()
        assert feat_dims % nhead == 0
        self.feats_dim = feat_dims
        self.nhead = nhead
        # self.q_conv = nn.Conv1d(feat_dims, feat_dims, 1, bias=True)
        # self.k_conv = nn.Conv1d(feat_dims, feat_dims, 1, bias=True)
        # self.v_conv = nn.Conv1d(feat_dims, feat_dims, 1, bias=True)
        self.conv = nn.Conv1d(feat_dims, feat_dims, 1)
        self.q_conv, self.k_conv, self.v_conv = [copy.deepcopy(self.conv) for _ in range(3)] # a good way than better ?
        self.mlp = nn.Sequential(
            nn.Conv1d(feat_dims * 2, feat_dims * 2, 1),
            nn.InstanceNorm1d(feat_dims * 2),
            nn.ReLU(inplace=True),
            nn.Conv1d(feat_dims * 2, feat_dims, 1),
        )


    def forward(self, feats1, feats2):
        '''

        :param feats1: (B, C, N)
        :param feats2: (B, C, M)
        :return: (B, C, N)
        '''
        b = feats1.size(0)
        dims = self.feats_dim // self.nhead
        query = self.q_conv(feats1).reshape(b, dims, self.nhead, -1)
        key = self.k_conv(feats2).reshape(b, dims, self.nhead, -1)
        value = self.v_conv(feats2).reshape(b, dims, self.nhead, -1)
        feats = multi_head_attention(query, key, value)
        feats = feats.reshape(b, self.feats_dim, -1)
        feats = self.conv(feats)
        cross_feats = self.mlp(torch.cat([feats1, feats], dim=1))
        return cross_feats


class InformationInteractive(nn.Module):
    def __init__(self, layer_names, feat_dims, gcn_k, ppf_k, radius, bottleneck, nhead, config=None):
        super().__init__()
        self.layer_names = layer_names
        self.blocks = nn.ModuleList()
        for layer_name in layer_names:
            if layer_name == 'gcn':
                self.blocks.append(GCN(feat_dims, gcn_k))
            elif layer_name == 'gge':
                self.blocks.append(GGE(feat_dims, gcn_k, ppf_k, radius, bottleneck, use_colors=False))
            elif layer_name == 'gge_color_simple':
                self.blocks.append(GGE(feat_dims, gcn_k, ppf_k, radius, bottleneck, use_colors=True))
            elif layer_name == 'gge_color_adv':
                self.blocks.append(GGE_Color_Adv(feat_dims, gcn_k, ppf_k, radius, config=config))
            elif layer_name == 'gge_color_adv_old':
                self.blocks.append(GGE_Color_Adv_OLD(feat_dims, gcn_k, ppf_k, radius, None))
            elif layer_name == 'cross_attn':
                self.blocks.append(Cross_Attention(feat_dims, nhead))
            else:
                raise NotImplementedError

    def forward(self, coords1, feats1, coords2, feats2, normals1, normals2, color_feats1=None, color_feats2=None):
        '''

        :param coords1: (B, 3, N)
        :param feats1: (B, C, N)
        :param coords2: (B, 3, M)
        :param feats2: (B, C, M)
        :return: feats1=(B, C, N), feats2=(B, C, M)

        Args:
            color1_org: colors of the original (non downsampled) point cloud: (B, 3, N*)
            colors2_org: colors of the original (non downsampled) point cloud: (B, 3, M*)
            coords1_org: coords of the original (non downsampled) point cloud: (B, 3, N*)
            coords2_org: coords of the original (non downsampled) point cloud: (B, 3, M*)
        '''
        for layer_name, block in zip(self.layer_names, self.blocks):
            if layer_name == 'gcn':
                feats1 = block(coords1, feats1)
                feats2 = block(coords2, feats2)
            elif layer_name == 'gge':
                feats1 = block(coords1, feats1, normals1)
                feats2 = block(coords2, feats2, normals2)
            elif layer_name == 'gge_color_simple' or layer_name == 'gge_color_adv' or layer_name == 'gge_color_adv_old':
                feats1 = block(coords1, feats1, normals1, color_feats1)
                feats2 = block(coords2, feats2, normals2, color_feats2)
            elif layer_name == 'cross_attn':
                feats1 = feats1 + block(feats1, feats2)
                feats2 = feats2 + block(feats2, feats1)
            else:
                raise NotImplementedError
        return feats1, feats2
