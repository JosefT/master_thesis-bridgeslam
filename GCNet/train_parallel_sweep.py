import shutil
import sys
import pprint
import numpy as np
import open3d as o3d
import torch
from easydict import EasyDict as edict
import torch.multiprocessing as mp
from torch.utils.data.distributed import DistributedSampler
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.distributed import init_process_group, destroy_process_group, gather
from torch.utils.tensorboard import SummaryWriter
import os

from tqdm import tqdm

from data import get_dataloader, get_dataset
from helper import flatten_list
from losses import Loss
from models import NgeNet, architectures
from train import save_summary
from utils import decode_config, setup_seed
import wandb


def ddp_setup(rank, world_size):
    os.environ["MASTER_ADDR"] = "localhost"
    os.environ["MASTER_PORT"] = "12355"
    init_process_group(backend="nccl", rank=rank, world_size=world_size)
    torch.cuda.set_device(rank)


def get_both_dataloaders(config):
    train_dataset, val_dataset = get_dataset(config.dataset, config)
    train_dataloader, neighborhood_limits = get_dataloader(config=config,
                                                           dataset=train_dataset,
                                                           batch_size=config.batch_size,
                                                           num_workers=config.num_workers,
                                                           shuffle=True,
                                                           neighborhood_limits=None,
                                                           sampler=DistributedSampler(train_dataset))
    val_dataloader, _ = get_dataloader(config=config,
                                       dataset=val_dataset,
                                       batch_size=config.batch_size,
                                       num_workers=config.num_workers,
                                       shuffle=False,
                                       neighborhood_limits=neighborhood_limits,
                                       sampler=DistributedSampler(val_dataset))
    return train_dataloader, val_dataloader


def get_optimizer(config, model):
    if config.optimizer == 'SGD':
        optimizer = torch.optim.SGD(
            model.parameters(),
            lr=config.lr,
            momentum=config.momentum,
            weight_decay=config.weight_decay,
        )
    elif config.optimizer == 'ADAM':
        optimizer = torch.optim.Adam(
            model.parameters(),
            lr=config.lr,
            betas=(0.9, 0.999),
            weight_decay=config.weight_decay,
        )
    return optimizer


def make_log(param, loss_dict):
    log = {}
    for k, v in loss_dict.items():
        log[f'{param}/{k}'] = v
    return log


class Trainer:
    def __init__(self, model, train_data, val_data, optimizer, config, gpu_id):
        self.gpu_id = gpu_id
        self.train_data = train_data
        self.val_data = val_data
        self.optimizer = optimizer
        self.save_every = 1
        self.model = DDP(model, device_ids=[gpu_id])
        self.config = config
        self.fp16_scaler = torch.cuda.amp.GradScaler(enabled=True)
        self.scheduler = self.get_scheduler()
        self.model_loss = Loss(self.config)

    def get_scheduler(self):
        if self.config.scheduler == 'ExpLR':
            scheduler = torch.optim.lr_scheduler.ExponentialLR(self.optimizer, gamma=self.config.scheduler_gamma)
        elif self.config.scheduler == 'CosA':
            scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(self.optimizer,
                                                                             T_0=self.config.T_0,
                                                                             T_mult=self.config.T_mult,
                                                                             eta_min=self.config.eta_min,
                                                                             last_epoch=-1)
        else:
            raise ValueError('Unknown scheduler')
        return scheduler

    def run_forward_pass_on_model(self, inputs, w_saliency):
        for k, v in inputs.items():
            if isinstance(v, list):
                for i in range(len(v)):
                    inputs[k][i] = inputs[k][i].cuda()
            else:
                inputs[k] = inputs[k].cuda()
        self.optimizer.zero_grad()
        with torch.cuda.amp.autocast():
            batched_feats, batched_feats_m, batched_feats_l = self.model(inputs)
            del inputs['points'], inputs['pools'], inputs['neighbors'], inputs['colors'], inputs['normals'], inputs[
                'feats'], inputs['upsamples']
            torch.cuda.empty_cache()
            stack_lengths = inputs['stacked_lengths']
            feats_src, feats_tgt = batched_feats[:stack_lengths[0][0]], batched_feats[stack_lengths[0][0]:]
            feats_src_m, feats_tgt_m = batched_feats_m[:stack_lengths[0][0]], batched_feats_m[stack_lengths[0][0]:]
            feats_src_l, feats_tgt_l = batched_feats_l[:stack_lengths[0][0]], batched_feats_l[stack_lengths[0][0]:]
            coors = inputs['coors'][0]  # list, [coors1, coors2, ..], preparation for batchsize > 1
            transf = inputs['transf'][0]  # (1, 4, 4), preparation for batchsize > 1
            points_raw = inputs['batched_points_raw']
            coords_src = points_raw[:stack_lengths[0][0]]
            coords_tgt = points_raw[stack_lengths[0][0]:]

            loss_dict = self.model_loss(coords_src=coords_src,
                                        coords_tgt=coords_tgt,
                                        feats_src=feats_src,
                                        feats_tgt=feats_tgt,
                                        feats_src_m=feats_src_m,
                                        feats_tgt_m=feats_tgt_m,
                                        feats_src_l=feats_src_l,
                                        feats_tgt_l=feats_tgt_l,
                                        coors=coors,
                                        transf=transf,
                                        w_saliency=w_saliency)
        return loss_dict

    def epoch_train(self, epoch, run, w_saliency):
        self.model.train()
        train_step = 0
        last_frequency_modulo = 0
        self.train_data.sampler.set_epoch(epoch)
        for inputs in tqdm(self.train_data, disable=self.gpu_id != 0):
            loss_dict = self.run_forward_pass_on_model(inputs, w_saliency)
            torch.cuda.empty_cache()
            loss = loss_dict['total_loss']
            self.fp16_scaler.scale(loss).backward()
            self.fp16_scaler.step(self.optimizer)
            self.fp16_scaler.update()

            global_step = (epoch * len(self.train_data) + train_step) * self.config.world_size + 1 + self.gpu_id
            run.log({"epoch": epoch, "learning_rate": self.optimizer.param_groups[0]['lr']}, step=global_step)
            train_log = make_log("train", loss_dict)
            run.log(train_log, step=global_step) # TODO Check if this is correct

            frequency_modulo = global_step % self.config.log_freq
            if frequency_modulo <= last_frequency_modulo:
                save_summary(self.writer, loss_dict, global_step, 'train',
                             lr=self.optimizer.param_groups[0]['lr'])
            last_frequency_modulo = frequency_modulo
            train_step += 1  # Use world_size as train step, since we use DistributedSampler
            if train_step > 100:
                break
        self.scheduler.step()

    def epoch_eval(self, epoch, run, w_saliency):
        total_circle_loss, total_recall, total_loss, total_recall_sum = [], [], [], []
        self.model.eval()
        val_step = 0
        with torch.no_grad():
            for inputs in tqdm(self.val_data, disable=self.gpu_id != 0):
                loss_dict = self.run_forward_pass_on_model(inputs, w_saliency)

                # Compute loss values
                loss = loss_dict['circle_loss'] + loss_dict['circle_loss_m'] + loss_dict['circle_loss_l']
                total_loss.append(loss.detach().cpu().numpy())
                circle_loss = loss_dict['circle_loss']
                total_circle_loss.append(circle_loss.detach().cpu().numpy())
                recall = loss_dict['recall']
                total_recall.append(recall.detach().cpu().numpy())
                recall_sum = loss_dict['recall'] + loss_dict['recall_m'] + loss_dict['recall_l']
                total_recall_sum.append(recall_sum.detach().cpu().numpy())

                global_step = (epoch * len(self.val_data) + val_step) * self.config.world_size + 1 + self.gpu_id

                if global_step % self.config.log_freq == 0:
                    save_summary(self.writer, loss_dict, global_step, 'val')
                val_step += 1
                torch.cuda.empty_cache()
        return total_circle_loss, total_recall, total_loss, total_recall_sum

    def start_train_loop(self, run):
        # Paths:
        name = "default"
        if hasattr(run, name):
            name = run.name
        saved_path = self.config.exp_dir + name
        saved_ckpt_path = os.path.join(saved_path, 'checkpoints')
        saved_logs_path = os.path.join(saved_path, 'summary')
        os.makedirs(saved_path, exist_ok=True)
        os.makedirs(saved_ckpt_path, exist_ok=True)
        os.makedirs(saved_logs_path, exist_ok=True)
        self.writer = SummaryWriter(saved_logs_path)
        if self.gpu_id == 0:
            shutil.copyfile(sys.argv[1], os.path.join(saved_path, f'{self.config.dataset}.yaml'))
            if run is not None:
                run.watch(self.model)

        best_recall, best_recall_sum, best_circle_loss, best_loss = 0, 0, 1e8, 1e8
        w_saliency = self.config.w_saliency_loss
        w_saliency_update = False
        for epoch in range(self.config.max_epoch):
            if self.gpu_id == 0:
                print('=' * 20, epoch, '=' * 20)

            self.epoch_train(epoch, run, w_saliency)

            total_circle_loss, total_recall, total_loss, _ = self.epoch_eval(epoch, run, w_saliency)
            circle_losses = [None for _ in range(self.config.world_size)]
            recalls = [None for _ in range(self.config.world_size)]
            losses = [None for _ in range(self.config.world_size)]
            torch.distributed.all_gather_object(circle_losses, total_circle_loss)
            torch.distributed.all_gather_object(recalls, total_recall)
            torch.distributed.all_gather_object(losses, total_loss)
            if self.gpu_id == 0 and run is not None:
                outputs = [flatten_list(circle_losses), flatten_list(recalls), flatten_list(losses)]
                # compute mean values of outputs
                # combine column values
                mean_circle_loss = np.mean(outputs[0])
                mean_recall = np.mean(outputs[1])
                mean_loss = np.mean(outputs[2])
                wandb_loss_dict = {'circle_loss': mean_circle_loss,
                                   'recall':mean_recall,
                                   'loss': mean_loss
                                   }
                val_log = make_log("val", wandb_loss_dict)
                wandb.log(val_log)
                if mean_circle_loss < best_circle_loss:
                    best_circle_loss = np.mean(total_circle_loss)
                    torch.save(self.model.module.state_dict(), os.path.join(saved_ckpt_path, 'best_loss.pth'))
                if mean_recall > best_recall:
                    best_recall = np.mean(total_recall)
                    torch.save(self.model.module.state_dict(), os.path.join(saved_ckpt_path, 'best_recall.pth'))
                if not w_saliency_update and np.mean(total_recall) > 0.3:
                    w_saliency_update = True
                    w_saliency = 1



def main(rank, run, config):
    ddp_setup(rank, config.world_size)
    train_data, val_data = get_both_dataloaders(config)
    model = NgeNet(config)
    if config.init_param_path is not None:
        model.load_state_dict(torch.load(config.init_param_path))
    model.cuda()
    optimizer = get_optimizer(config, model)
    trainer = Trainer(model, train_data, val_data, optimizer, config, rank)
    trainer.start_train_loop(run)
    destroy_process_group()

def main_wrapper():
    run = wandb.init(mode="disabled")
    config = decode_config(run.config["config_file"])
    config = {**config, **run.config}
    config = edict(config)

    if len(sys.argv) > 2:
        config.init_param_path = sys.argv[2]
        print("Start training from provided pretrained model")
    else:
        config.init_param_path = None
        print("Start training from scratch")
    config.architecture = architectures[config.dataset]
    config.world_size = torch.cuda.device_count()
    print('GPUs to be utilized: ', config.world_size)
    print('CPUs to be utilized: ', config.num_workers)

    if run == "RunDisabled":
       run = None
    mp.spawn(main, args=(run, config), nprocs=config.world_size)

if __name__ == "__main__":
    setup_seed(1234)


    sweep_config = dict()
    sweep_config['method'] = 'random'
    sweep_config['metric'] = {'name': 'val/recall', 'goal': 'maximize'}
    sweep_parameters_dict = {
        'graph_layer_type': {'values': ['GATConv', 'GCNConv']},
        'n_graph_layers': {'values': [2, 3, 4]},
        'num_gat_heads': {
            'distribution': 'int_uniform',
            'min': 1,
            'max': 4,
        },
        'color_level': {'values': [0,1,2]},
        'graph_hidden_units': {'values': [8, 16, 32, 64]},
        'augment_noise': {
            'values': [0.05, 0.075, 0.1, 0.3, 0.6],
        },
        'n_mlp_layers': {'values': [2, 3, 4]},
        'mlp_activation': {'values': ['relu', 'leaky_relu', 'elu']},
        'mlp_hidden_units': {'values': [8, 16, 32, 64, 128]},
        'mlp_dropout': {
            'values': [0.0, 0.1, 0.3, 0.5],
        },
    }

    # add all parameters to sweep
    sweep_parameters_dict.update({'config_file': {'values': [sys.argv[1]]}})
    sweep_config['parameters'] = sweep_parameters_dict

    sweep_id = wandb.sweep(sweep_config, project="3DMatch_Color_Sweep_2")

    wandb.agent(sweep_id, function=main_wrapper, count=100, project="3DMatch_Color_Sweep_2")

