import argparse
import copy
import glob
import pickle

import numpy as np
import os
import torch
import open3d as o3d
import transforms3d as transforms3d
from easydict import EasyDict as edict
from tqdm import tqdm

from data import get_dataloader, Custom_Dataset, ThreeDMatch_Color
from eval_bridge_dataset import calOverlapFromCoors
from models import architectures, NgeNet, vote
from utils import decode_config, npy2pcd, pcd2npy, execute_global_registration, \
    npy2feat, vis_plys, setup_seed, fmat, to_tensor, get_blue, \
    get_yellow

from utils.helper import get_sequences, print_table

CUR = os.path.dirname(os.path.abspath(__file__))


def angular_diff(rotation1, rotation2):
    error_mat = np.dot(np.linalg.inv(rotation1), rotation2)
    _, angle = transforms3d.axangles.mat2axangle(error_mat, unit_thresh=1e-3)
    return abs(angle * (180 / np.pi))


def get_scene_split(scenes, dataset):
    buckets = {scene: 0 for scene in scenes}
    for link in dataset.infos['src']:
        for scene in scenes:
            if scene in link:
                buckets[scene] += 1

    splits = []
    splits.append([0, buckets[scenes[0]]])
    for scene in scenes[1:]:
        split_start = splits[-1][1]
        splits.append([split_start, buckets[scene] + split_start])
    return splits


def main(args):
    setup_seed(22)
    config = decode_config(os.path.join(CUR, 'configs', 'threedmatch_color.yaml'))
    #config = decode_config(os.path.join(CUR, 'configs', 'threedmatch_color_with_sweep_config.yaml'))

    config = edict(config)
    config.architecture = architectures[config.dataset]
    config.num_workers = 1
    test_dataset = ThreeDMatch_Color(root=args.data_root,
                                     split="test",
                                     aug=False,
                                     overlap_radius=config.overlap_radius)

    test_dataloader, neighborhood_limits = get_dataloader(config=config,
                                                          dataset=test_dataset,
                                                          batch_size=config.batch_size,
                                                          num_workers=config.num_workers,
                                                          shuffle=False,
                                                          neighborhood_limits=None)
    scenes = np.array(['sun3d-hotel_umd-maryland_hotel1',
                       'sun3d-hotel_uc-scan3',
                       'sun3d-home_md-home_md_scan9_2012_sep_30',
                       'sun3d-mit_76_studyroom-76-1studyroom2',
                       '7-scenes-redkitchen',
                       'sun3d-home_at-home_at_scan1_2013_jan_1',
                       'sun3d-hotel_umd-maryland_hotel3',
                       'sun3d-mit_lab_hj-lab_hj_tea_nov_2_2012_scan1_erika'])
    splits = get_scene_split(scenes, test_dataset)
    model = NgeNet(config)
    use_cuda = not args.no_cuda
    if use_cuda:
        model = model.cuda()
        model.load_state_dict(torch.load(args.checkpoint))
    else:
        model.load_state_dict(
            torch.load(args.checkpoint, map_location=torch.device('cpu')))
    model.eval()
    fmr_threshold = 0.05
    rmse_threshold = 0.2
    inlier_ratios, mutual_inlier_ratios = [], []
    mutual_feature_match_recalls, feature_match_recalls = [], []
    Ts = []

    dist_thresh_maps = {
        '5000': config.first_subsampling_dl,
        '2500': config.first_subsampling_dl * 1.5,
        '1000': config.first_subsampling_dl * 1.5,
        '500': config.first_subsampling_dl * 1.5,
        '250': config.first_subsampling_dl * 2,
    }

    errors_r, errors_t, reg_success, rmses, overlaps = [], [], [], [], []
    with torch.no_grad():
        for pair_ind, inputs in enumerate(tqdm(test_dataloader)):
            if use_cuda:
                for k, v in inputs.items():
                    if isinstance(v, list):
                        for i in range(len(v)):
                            inputs[k][i] = inputs[k][i].cuda()
                    else:
                        inputs[k] = inputs[k].cuda()

            batched_feats_h, batched_feats_m, batched_feats_l = model(inputs)
            stack_points = inputs['points']
            stack_lengths = inputs['stacked_lengths']
            coords_src = stack_points[0][:stack_lengths[0][0]]
            coords_tgt = stack_points[0][stack_lengths[0][0]:]
            feats_src_h = batched_feats_h[:stack_lengths[0][0]]
            feats_tgt_h = batched_feats_h[stack_lengths[0][0]:]
            feats_src_m = batched_feats_m[:stack_lengths[0][0]]
            feats_tgt_m = batched_feats_m[stack_lengths[0][0]:]
            feats_src_l = batched_feats_l[:stack_lengths[0][0]]
            feats_tgt_l = batched_feats_l[stack_lengths[0][0]:]

            coors = inputs['coors'][0]  # list, [coors1, coors2, ..], preparation for batchsize > 1
            transf = inputs['transf'][0]  # (1, 4, 4), preparation for batchsize > 1

            coors = coors.detach().cpu().numpy()
            gt_T = transf.detach().cpu().numpy()

            source_npy = coords_src.detach().cpu().numpy()
            target_npy = coords_tgt.detach().cpu().numpy()

            source_npy_raw = copy.deepcopy(source_npy)
            target_npy_raw = copy.deepcopy(target_npy)
            source_feats_h = feats_src_h[:, :-2].detach().cpu().numpy()
            target_feats_h = feats_tgt_h[:, :-2].detach().cpu().numpy()
            source_feats_m = feats_src_m.detach().cpu().numpy()
            target_feats_m = feats_tgt_m.detach().cpu().numpy()
            source_feats_l = feats_src_l.detach().cpu().numpy()
            target_feats_l = feats_tgt_l.detach().cpu().numpy()

            source_overlap_scores = feats_src_h[:, -2].detach().cpu().numpy()
            target_overlap_scores = feats_tgt_h[:, -2].detach().cpu().numpy()
            source_saliency_scores = feats_src_h[:, -1].detach().cpu().numpy()
            target_saliency_scores = feats_tgt_h[:, -1].detach().cpu().numpy()

            source_scores = source_overlap_scores * source_saliency_scores
            target_scores = target_overlap_scores * target_saliency_scores

            npoints = args.npts
            if source_npy.shape[0] > npoints:
                p = source_scores / np.sum(source_scores)
                idx = np.random.choice(len(source_npy), size=npoints, replace=False, p=p)
                source_npy = source_npy[idx]
                source_feats_h = source_feats_h[idx]
                source_feats_m = source_feats_m[idx]
                source_feats_l = source_feats_l[idx]

            if target_npy.shape[0] > npoints:
                p = target_scores / np.sum(target_scores)
                idx = np.random.choice(len(target_npy), size=npoints, replace=False, p=p)
                target_npy = target_npy[idx]
                target_feats_h = target_feats_h[idx]
                target_feats_m = target_feats_m[idx]
                target_feats_l = target_feats_l[idx]

            after_vote = vote(source_npy=source_npy,
                              target_npy=target_npy,
                              source_feats=[source_feats_h, source_feats_m, source_feats_l],
                              target_feats=[target_feats_h, target_feats_m, target_feats_l],
                              voxel_size=config.first_subsampling_dl,
                              use_cuda=use_cuda)
            source_npy, target_npy, source_feats_npy, target_feats_npy = after_vote

            M = torch.cdist(to_tensor(source_feats_npy, use_cuda), to_tensor(target_feats_npy, use_cuda))

            source, target = npy2pcd(source_npy), npy2pcd(target_npy)

            source_feats, target_feats = npy2feat(source_feats_npy), npy2feat(target_feats_npy)
            pred_T, estimate = execute_global_registration(source=source,
                                                           target=target,
                                                           source_feats=source_feats,
                                                           target_feats=target_feats,
                                                           voxel_size=dist_thresh_maps[str(args.npts)])

            Ts.append(pred_T)

            if args.vis:
                source_ply = npy2pcd(source_npy_raw)
                source_ply.paint_uniform_color(get_yellow())
                estimate_ply = copy.deepcopy(source_ply).transform(pred_T)
                target_ply = npy2pcd(target_npy_raw)
                target_ply.paint_uniform_color(get_blue())
                estimate_ply.estimate_normals(o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
                target_ply.estimate_normals(o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
                vis_plys([target_ply, estimate_ply], need_color=False)
            # calculate angular difference
            errors_r.append(angular_diff(pred_T[:3, :3], gt_T[:3, :3]))
            errors_t.append(np.linalg.norm(pred_T[:3, 3] - gt_T[:3, 3]))
            coor_dists = [np.linalg.norm(pred_T[:3, :3] @ source_npy_raw[i] + pred_T[:3, 3] - target_npy_raw[j]) for
                          i, j in coors]
            rmse = np.sqrt(np.mean(np.square(coor_dists)))
            rmses.append(rmse)
            if rmse < 0.2:
                reg_success.append(1)
            else:
                reg_success.append(0)

            gt_overlap = calOverlapFromCoors(coors, stack_lengths[0][0].item(), stack_lengths[0][1].item())
            overlaps.append(gt_overlap)

    Ts = np.array(Ts)
    scene_recall = []
    error_r_unsqeezed = []
    error_t_unsqeezed = []
    rmse_unsqeezed = []
    overlaps_unsqeezed = []
    for split in splits:
        scene_recall.append(np.mean(reg_success[split[0]:split[1]]))
        error_r_unsqeezed.append(errors_r[split[0]:split[1]])
        error_t_unsqeezed.append(errors_t[split[0]:split[1]])
        rmse_unsqeezed.append(rmses[split[0]:split[1]])
        overlaps_unsqeezed.append(np.array(overlaps[split[0]:split[1]]))

    print_table(scenes, scene_recall, rmse_unsqeezed, error_r_unsqeezed, error_t_unsqeezed, overlaps_unsqeezed)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Configuration Parameters')
    parser.add_argument('--benchmark', default='3DMatch', help='3DMatch or 3DLoMatch')
    parser.add_argument('--data_root', required=True, help='data root')
    parser.add_argument('--checkpoint', required=True, help='checkpoint path')
    parser.add_argument('--saved_path', default='work_dirs', help='saved path')
    parser.add_argument('--npts', type=int, default=5000,
                        help='the number of sampled points for registration')
    parser.add_argument('--vis', action='store_true',
                        help='whether to visualize the point clouds')
    parser.add_argument('--no_cuda', action='store_true',
                        help='whether to use cuda')
    args = parser.parse_args()
    main(args)
