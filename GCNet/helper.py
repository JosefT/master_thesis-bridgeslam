import numpy as np
import open3d as o3d

from utils import get_yellow


def show_pts_with_neighbors(points, stack_neighbors):
    for pts, neighbors in zip(points, stack_neighbors):
        pcd = o3d.geometry.PointCloud()
        coords_np = pts.cpu().numpy()
        pcd.points = o3d.utility.Vector3dVector(coords_np)
        pcd.paint_uniform_color(get_yellow())

        first_neighbors = neighbors[0].cpu().numpy()
        # color neighbor pts with red
        filtered_nbrs = np.asarray(first_neighbors[first_neighbors != len(pts)])
        color = np.asarray([1, 0, 0])
        for idx in filtered_nbrs:
            pcd.colors[idx] = color
        o3d.visualization.draw_geometries([pcd])


def flatten_list(_2d_list):
    flat_list = []
    # Iterate through the outer list
    for element in _2d_list:
        if type(element) is list:
            # If the element is of type list, iterate through the sublist
            for item in element:
                flat_list.append(item)
        else:
            flat_list.append(element)
    return flat_list
